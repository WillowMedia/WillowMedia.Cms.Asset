

## Adding additional javascript files / modules

Using external scripts attempts to avoid to use additional frameworks. Therefor for some
included scripts, some extra work is required. Here is an example for dayjs.

Dayjs is added by npm:

```
npm install dayjs --save
```

After this its being added to the `package.json` and `package-lock.json`. On `npm update`, 
the lock file might be updated with a newer version. Dayjs includes plugins, but we don't
want to add all the plugins to the final scripts, only what we require.

To bundle the required plugins, we use the `bundleconfig.json`, which is 'executed' on a 
`dotnet build`. In the WillowMedia.Cms.Asset.csproj, the `BuildBundlerMinifier` is included.
On bundling, the plugins are merged with the dayjs script, and an additional `Static/dayjs.js`.

```
{
    "outputFileName": "wwwroot/Asset/script/dayjs.min.js",
    "inputFiles": [
      "node_modules/dayjs/dayjs.min.js",
      "node_modules/dayjs/plugin/isoWeek.js",
      "node_modules/dayjs/plugin/isoWeeksInYear.js",
      "Static/dayjs.js"
    ],
    "minify": { "enabled": false },
    "sourceMap": false
}
```

This last file is added as last to the output file, and makes sure that the plugins are 
loaded in the browser:

```
dayjs.extend(dayjs_plugin_isoWeek);
dayjs.extend(dayjs_plugin_isoWeeksInYear);
```

Additional featured might be added here.

Before running `update-asset` for a solution, it might be required to run a build from the
IDE to make sure all latest files are added.