/// <reference types="../../../node_modules/dayjs/index" />
/// <reference types="../../../node_modules/dayjs/plugin/utc" />
/// <reference types="../../../node_modules/dayjs/plugin/isoWeek" />
/// <reference types="../../../node_modules/dayjs/plugin/isoWeeksInYear" />
/// <reference types="../../../node_modules/dayjs/plugin/customParseFormat" />
/// <reference types="../../../node_modules/dayjs/plugin/advancedFormat" />
/// <reference types="../../../node_modules/dayjs/plugin/weekOfYear" />
/// <reference types="../../../node_modules/dayjs/plugin/duration" />
/// <reference types="../../../node_modules/dayjs/plugin/relativeTime" />

interface Window {
    storageHandler: IStorageHandler,
    sweetAlert: Function,
    app: ICmsApp,
    Promise: Promise<any>
    DocumentTouch: any;

    addEvent(type: string, fn: Function): void;

    isDateInputSupported: boolean;
    isTouchDevice: boolean;
}

/// Requests

interface ICmsFetchRequest {
    self: any; // = null;
    state: CmsFetchState; // = null

    retry: number; // = null;
    options: CmsFetchOptions ; //= null;
    response: CmsVerboseResponse; // = null;
    handled?: boolean; // = false;

    onStart?: Function; // = null;
    onDone?: Function; // = null;

    // abort?: Function = null;
    _controller: any; // = null;
    _signal: any; // = null;
}

interface ICmsVerboseResponse {

    Data: any ;
    Succes?: boolean;
    Error: string;
    Redirect: string;
    ExpireHtml: string;
    sre: any;
    RestoreUrl: string;
}
/// Requests

interface Promise<T> {
    // @ts-ignore
    spread(fn: Function): Promise<T>;
}

interface Object {
    mapKeys(resolveKey: any): Object;
}

interface IStorageHandler {
    available(): boolean;

    handleBroadcastEvent(): boolean;

    handleStorageEvent(e: any);

    setItem(key: string, value: any);

    getItem(key: string): any;

    register(key: string, callback: Function);

    clearRegister();
}

interface IParsedUrl {
    protocol: string,
    host: string,
    hostname: string,
    port: string,
    pathname: string,
    search: string,
    searchObject: Object,
    hash: string,

    addQuery(key: string, value: string),

    toUrl(): string,

    toRelativeUrl(): string
}

interface ICmsToast {
    text: string;
    duration: number;
    id: string,
    buttons: []
}

interface ICmsToaster {
    current: Array<HTMLElement>,
    queue: Array<ICmsToast>,

    show(toast: string);

    show(toast: ICmsToast);

    updateView(): void;
}

interface ICmsLoadOnDemand {
    allowLoad: boolean;

    onLoad(callback?: Function): void;

    onQueued: Function,

    queue: Array<Function>;

    readCookie(name: string): string,

    validate(): void
}

interface ICmsLocation {
    //_location: CmsLocation;
    getLocation(): Object;

    requestLocation(callback: Function);

    responseError(error: Object, callback: Function);

    responseLocation(position: Object, callback: Function);
}

interface ICmsColor {
    colourIsLight(r, g, b): boolean;

    contrastColorLight(color): boolean;

    stringToHslColor(str, s, l): string
}

interface ICmsSettings {
    requestTimeout?: number;
}

interface ICmsApp {
    isIE: boolean;
    lang: string;
    toaster: ICmsToaster;
    color: ICmsColor;
    settings: ICmsSettings;
    sw: boolean;

    onReady();
    uuidv4(): string;

    browser: {
        supportsColorpicker(): boolean;
        svg(): boolean;
    },
    loadOnDemand: ICmsLoadOnDemand;

    requires(validate: boolean, src: string, callback: Function): void;

    loadScript(src: string, callback: Function): void;
    loadAsset(element: HTMLElement, callback: Function) :void;

    parseURL(url: string): IParsedUrl;

    parseJwt(token: string): any;

    resolveValue(obj: any): any;

    trigger($elements, eventType: string): void;

    confirmNavigate(onTrue?: Function): boolean;

    // @deprecated
    IEUploadFix(url: string) /* deprecated */

    toast(toast: string): void;
    toast(toast: ICmsToast): void;

    messages: ICmsMessages;
    locationHash: string; /* use getter and setter */
    debug: boolean;
    cookieAvailableCheck: boolean;
    // unloading: boolean;
    dateFormat: string;
    datetimeFormat: string;
    timeFormat: string;
    scriptUID: string;

    storage: IStorageHandler;
    json: {
        toJSON(any): string,
        parse(string): any,
    };
    extend: {
        onAjaxBefore: Function,
        onAjaxDone: Function,
        onAjaxFail: Function,
        form: {
            onFormLoaded: Function,
            onBeforeAjax: Function
        }
    };

    findObject(myArray: Array<any>, Function: boolean): any;

    onAlert();

    alertBox(...args: any[]);

    alert(title: any, msg: any, icon: any);

    readCookie(name: string): string;

    geo: {
        getLocation(): Object,
        requestLocation(callback: Function),
        responseError(error: Object, callback: Function),
        responseLocation(position: Object, callback: Function)
    };

    scroll: {
        allow(bValue: Boolean),
        preventScroll($element: JQuery): void,
    };

    uniqueId(prefix?: string): string;

    currencyFormat: {
        d: string,
        s: string,
        n: number,
        c: string,
        g: string,
        gs: number
    };

    numberFormat: {
        format(number: number): string,
        decimal(number: number): string,
        float(number: number): string
    };

    basePath: string;

    resolveUrl(relPath: string): string;

    cookie(key: string): string;

    convert: {
        to(valuetype: string, value: any): any,
        toBoolean(value: any): boolean,
        toTime(val: any): string,
        timespanToTime(jsonTime: string): string,
        toDate(val: any, format?: string): Date,
        toNumber(val: any): number,
        numberStringToNumber(val: any): number,
        round(val: number, d?: number): number,
    };

    fixDates(obj: Object): Object,

    fixDate(val: object): Date,

    fixResponse(obj: Object): Object,

    hasProperty(propertyStr: string, data: Object): any,

    log(obj: any),

    formatting: {
        maskformat(value: any, pattern: string): string,
        secondsToMinutes(fVal: number): number,
        percentage(fVal: number): string,
        decimal(fVal: number, d?: number): string,
        number(fVal: number): string,
        currency(fVal: number): string,
        datetime(val: any): string,
        date(val: any): string,
        time(val: any): string,
        dateFormat(dateVal: any, format: string): string,
        isDate(obj: any): boolean,
        format(value: any, valuetype: string): string,
        dateFormatToParser(format: string): string,
        dateFormatToDateTimePickerFormat(format: string): string,
        padding(value: number, padding: number): string
    },

    showGlobalError(obj: any, options?: Object): boolean

    focus: ICmsFocusTracker,

    date(s: string, format: string): Date,

    extendWithProperties(obj1: Object, obj2: Object): Object,

    fetch(options?: CmsFetchOptions): Promise<any>,
    ajax(options?: CmsAjaxOptions): Promise<any>,
    // createAjaxRequest(request: CmsAjaxRequest): Promise<any>,
    expireRestore(response: CmsVerboseResponse, request: CmsFetchRequest): Promise<any>,

    validators: {
        pc6: RegExp,
        iso8601: RegExp
        timespan: RegExp
        time: RegExp
        dateSerialized: RegExp
        dateISO: RegExp,
        reMsAjax: RegExp,
        email: RegExp
    },

    downloadInlineOrAttached(url: string, filename?: string, mimetype?: string, inline?: boolean),

    download(url: string),

    downloadFrame(url: string),

    templateEngine(html: string, options: Object),
}

// IE compatibility
// ReadyState and onreadystatechange are used for backward 
// declare global {
interface HTMLElement {
    readyState: string;
    onreadystatechange: Function;

    hasClass(className: string);
}

interface Document {
    createEventObject(): Object;

// IE
    onfocusin();

    onfocusout();
}

interface ArrayConstructor {
    asArray(someArray: any): Array<any>;
}

interface Array<T> {
    groupBy(getProperty: any): Object;

    joinStringReadable(list: Array<any>): string;
}

interface String {
    slim(): string;

    format(...args: any[]): string;

    isNumber(this: string, ...args: any[]): boolean;

    ConvertToDate(this: string): Date;

    camelCase(this: string): string;

    endsWith(search: string, this_len?: number): boolean;

    replaceAll(this: string, search: string, replace: string): string;

    htmlEscape(this: string): string;

    trim(this: string): string;

    hashCode(this: string): number;

    parseNumber(this: string): number;

    hasIndexerDescription(this: string): RegExpExecArray;
}

interface StringConstructor {
    join(seperator: string, values: Array<any>): string;

    joinReadable(list: Array<any>) : string;

    encaps(value, left, right): string;

    findFirst(haystack: string, needle: string): string;

    escape(value: string): string;

    leftPad(value: number, pad: number, c: string): string;
}

interface Number {
    currencyFormat(this: number, decPlaces: number, thouSeparator: string, decSeparator: string): string;

    pad(this: number): string;

    countDecimals(this: number): number;
}

interface DateConstructor {
    today(): Date;

    parseFunctions: { count: number };
    formatFunctions: { count: number };
    parseRegexes: any;

    createNewFormat(format: string): any;

    getFormatCode(a): any;

    parseDate(a, c): Date;

    createParser(format): any;

    formatCodeToRegex(b, a): any;

    dayNames: Array<string>;
    daysInMonth: Array<number>;
    monthNames: Array<string>;
    y2kYear: number;
    monthNumbers: Object;
    patterns: {
        ISO8601LongPattern: string,
        ISO8601ShortPattern: string,
        ShortDatePattern: string,
        LongDatePattern: string,
        FullDateTimePattern: string,
        MonthDayPattern: string,
        ShortTimePattern: string,
        LongTimePattern: string,
        SortableDateTimePattern: string,
        UniversalSortableDateTimePattern: string,
        YearMonthPattern: string
    };
}

interface Date {
    isToday(this: Date): boolean;

    addDays(days: Number): Date;

    dateOnly(this: Date): Date;

    isValid(this: Date): boolean;

    dateStamp(this: Date): number;

    getWeekWithYear(this: Date): number;

    toJSON(this: Date): string;

    dateFormat(b: string): any;

    getTimezone(): any;

    getGMTOffset(): any;

    getFirstDayOfMonth(): any;

    getLastDayOfMonth(): any;

    getDaysInMonth(): any;

    getSuffix(): any;

    getDayOfYear(): number;

    getWeekOfYear(): string;

    isLeapYear(): boolean;
}

interface JQuery {
    requestToken(optionalToken?: string): string;

    requestTokenUpdate(requestToken: string);

    refreshTokenUpdate(refreshToken: string);

    hasElement($element?: JQuery): boolean;

    isDisabled(): boolean;

    // Check if list of elements contains an element
    contains($element: JQuery): boolean;

    hasEvent(eventName: string): boolean;

    sortChildren(sortingFunction: Function): JQuery;

    hasHandler(handlerName: string): boolean;

    triggerHandlerRecursive(handler: string): JQuery;

    datetimepicker(options);

    debugEvent(event);
}

interface ICmsMessages {
    required: string;
    UnknownError: string;
    email: string;
    DecimalBeforeNegative: string;
    DecimalDecimalPlacement: string;
    DecimalGrouping: string;
    DecimalUnexpected: string;
    regexMatch: string;
    postcodeNl: string;
    phone: string;
    integer: string;
    Decimal: string;
    time: string;
    date: string;
    color: string;
    RequestCancelled: string;
}

interface ICmsFocusTracker {
    onBlur();

    onFocus();
}

interface JQueryStatic {
    // example: formHandler(element: JQuery, options: any): void;
    overlayHandler: {
        defaultOptions: CmsOverlayOptions;
        createIframeMessageHandler(): any;
    }
}

interface JQuery {
    overlayDetails(options: object): JQuery;

    iframeOverlay(options?: CmsIframeOverlayOptions): JQuery;

    overlay(options?: CmsOverlayOptions): CmsOverlay;

    inOverlay(): CmsOverlay;
}

interface Array<T> {
    asOptions(options: {
        value: string,
        text: string,
        disabled: any,
        hide: any
    }): JQuery;
}

interface String {
    safeUrl(): string;
    removeDiacritics(): string;
}

interface IFormHandler {
    serialize(): object;

    isChanged(): boolean;

    fill(data: Object);

    options: {
        formData: object,
        isNew: boolean,
        nestedDataBinding: string
    };
}

interface JQueryStatic {
    cloneObject(obj: any): any;

    formHandler(element: JQuery, options: any): void;

    formHandlerDefaultOptions: {
        dataProperty: string,
        elementSerializableClass: string,
        debug: boolean,
        onBeforeSend(element: JQuery, dataObj: any),
        onCheckChanged(): boolean,
        exclude()                       // function() filter to exclude elements on serialize
    };
    datetimepicker;
    validator;

    debugEvent(event: Event);

    convertTimespanToTime(jsonTime: string): string;

    createFormWithUid(options): JQuery;

    _createFormWithUidDefaults;
    base64;

    dynamicForm(obj): any;

    cmsValidators: {
        hasValueError($element: JQuery, value: any, valuetype: string): string;
        date(strVal: string, format: string): boolean;
        time(strVal, fmt): boolean;
        datetime(strVal, fmt): boolean;
        postcodeNL(value): boolean;
        phone(value): boolean;
        integer(value): boolean;
        decimal(value?: any, onInvalid?: Function): boolean;
    };

    calformatToRegex(format: string): RegExp;

    loadAsset(src: string, callback?: Function, isScript?: boolean, isModule?: boolean);
    resolveAssetFormPromise(form: any): Promise<any>;

    roleAuthorization(): object;
}

interface JQuery {
    dataSource(value?: any): any; // get or set the datasource on an element
    setDataSource(value: any): JQuery;

    setFormData(any: object): void;

    formData(): object;

    hasChanges(): boolean;

    weekControl(options?: object): JQuery;

    checkbox(): JQuery;

    // formHandler(): IFormHandler;
    formHandler(options?: any): IFormHandler;

    setElementValue(value: any);

    initDatetimePicker($form: JQuery);
    initTimePicker($form: JQuery);

    elementValue(): any;

    detectValuetype(): any;

    setupValueOnFocus(): JQuery;

    insertAtCaret(text: string): void;

    showOrHide(showIt: boolean): JQuery;

    slim(): JQuery;

    ResetAndValidate(): boolean;

    resetAndValidate(): boolean;

    SetValueByProperty(dataObj: any): JQuery;

    // requestToken(optionalToken?: string): string;
    // requestTokenUpdate(requestToken: string);
    cleanWhitespace(): JQuery;

    setClick(options?: any, callback?: any): JQuery;

    getForm(): JQuery;

    // Requires loading (bgrins-spectrum) spectrum
    spectrum(opts, extra?: any);

    registerForm(formID: string, dynamicForm: object): void;

    registerResolveForm(): void;

    resolveForm(formID: string, context?: any): any;

    buttonToggles(options: object): JQuery;

    // Authorization by role
    setRoles(requiredRoles: any);

    setVisibleByRole(currentRoles: any);

    setReadonlyByRole(currentRoles: any);

    isAuthorized(currentRoles: any);

    enableByRole(currentRoles: any);

    containingForm() : JQuery;

    getElement(propertyName: string) : JQuery;

    setupObeyDisabled(): JQuery;

    setupCmsElement($form: JQuery) : JQuery;

    // Temporary store current elementValue in data(stored_value)
    storeCurrentValue() : JQuery;

    // Restore temporary value in data(stored_value) to elementValue 
    restoreCurrentValue() : JQuery;

    preventScroll();
}
