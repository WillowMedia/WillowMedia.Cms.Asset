﻿(function ($) {
    "use strict";

    $.uploadElement = function ($element, options) {
        this.options = null;
        this.$element = null;
        this.$fileupload = null;
        this.sending = false;

        var me = this;

        this.init = function ($element, options) {
            me.$element = $($element);
            me.options = $.extend({}, $.uploadElement.defaultOptions, options);

            var $me = this.$element;
            var $form = $(me.options.form || this.$element.closest("form"));
            var $fileupload;
            var id = app.uniqueId("upload");

            $me.after($fileupload = $("<input type='file' style='position:absolute;top:-1000px;left:-1000px; visibility: hidden; width: 0px; height: 0px; opacity: 0;' />")
            .attr("id", id)
            .attr("name", "FileUpload")
            .attr("accept", me.options.accept));

            if (me.options.singleFileUploads !== true) {
                $fileupload.prop("multiple", true);
            }

            me.$fileupload = $fileupload.on("click", function (event) {
                if ($(this).isDisabled()) {
                    event.preventDefault();
                    event.stopPropagation();
                    return false;
                }
            });

            me.$fileupload.fileupload({
                url: me.options.url,
                sequentialUploads: true,
                singleFileUploads: me.options.singleFileUploads !== true,
                beforeSend: me.options.beforeSend, // function(xhr, data)
                dataType: 'json',
                autoUpload: false,
                dropZone: me.options.dropZone || $me,
                formData: {},
                maxFileSize: me.options.maxFileSize,
                add: function (e, data) {
                    if (me.sending == false && me.$fileupload.isDisabled()) {
                        return false;
                    }

                    me.sending = true;

                    if (e.delegatedEvent.type === "drop") {
                        data.dropTargetElement = e.delegatedEvent.target;
                    }
                    data.submit();
                },
                start: function (e) {
                    if ((me.options.IEFixCallback || null) !== null) {
                        app.IEUploadFix(me.options.IEFixCallback);
                    }

                    if ((me.options.onStart || null) !== null) {
                        me.options.onStart.call($form);
                    }
                },
                send: function (e, data) {
                    var result = true;

                    if ((me.options.onSend || null) !== null) {
                        result = me.options.onSend.call(e, data) !== false ? true : false;
                    }

                    if (result == true) {
                        $form.addClass("disabled");
                    }
                    return result;
                },
                progress: function (e, data) {
                    if ((me.options.onProgress || null) !== null) {
                        me.options.onProgress(Math.round(parseInt(data.loaded / data.total * 100, 10)));
                    }
                },
                done: function (e, data) {
                    var response = data.result;

                    if ((response.Succes || false) === true) {
                        if ((me.options.onDone || null) !== null) {
                            me.options.onDone.call($me, response);
                        } else if ($me.is("img")) {
                            $me.attr("src", response.Data.Thumbnail);
                            $me.data("rec", response.Data);
                        }

                        // If dropTargetElement is set, we'll trigger a dropped-files event 
                        if (data.dropTargetElement) {
                            $(data.dropTargetElement).trigger("dropped-files", [response.Data]);
                        }
                    } else {
                        app.alertBox("Oops", response.Error || "An unknown error occured", "warning");
                    }
                    ;
                },
                fail: function (e, data) {
                    app.alertBox("Oops", data.Error || "An unknown error occured", "warning");
                }, // fail: null,
                stop: function (e) {
                    $form.removeClass("disabled");
                    me.sending = false;

                    if ((me.options.onFinish || null) !== null) {
                        me.options.onFinish.call($form);
                    }
                },
                drop: function (e, data) {
                    if ((me.options.onDrop || null) !== null) {
                        me.options.onDrop.call($me, data);
                    }
                },
            });

            var $clickElement = $me;
            if (me.options.clickElement) {
                $clickElement = $(me.options.clickElement);
                if (typeof me.options.clickElement === 'function') {
                    $clickElement = me.options.clickElement.call(me);
                } else {
                    $clickElement = $(me.options.clickElement);
                }
            }

            if ($clickElement) $clickElement.eq(0).on("click", function (event) {
                event.preventDefault();

                if ($(this).isDisabled()) {
                    event.preventDefault();
                    event.stopPropagation();
                    return false;
                }

                // Dont use a strong reference. The input[type=file] is replaced after upload
                // and the reference will be invalid
                $("#" + id).trigger("click");
            });
        }

        this.init($element, options);
    }

    $.uploadElement.defaultOptions = {
        url: null,
        singleFileUploads: true,
        beforeSend: null, // function(xhr, data)
        dropZone: null,
        form: null,
        accept: "image/gif, image/jpeg, image/jpg, image/bmp, image/tiff, image/png",
        maxFileSize: null,
        onDrop: null,           // Informative event
        onStart: null,
        onProgress: null,       // function(progress)
        onDone: null,           // function(response) - event on done
        onFinish: null,
        clickElement: null
    };

    $.fn.uploadElement = function (options) {
        return this.each(function () {
            (new $.uploadElement($(this), options));
        });
    };
})(jQuery);