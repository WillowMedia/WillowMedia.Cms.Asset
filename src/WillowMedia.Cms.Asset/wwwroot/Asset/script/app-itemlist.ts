/* Copyright 2014 - 2024 Willow Media

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. /**/

/// <reference path="interfaces.ts" />

"use strict";

class CmsItemListOptions {
    listItemClass: string = "hoverMenu";
    skipSelectectorSerialize: string = ".deleted,.dummy";
    data: any = null;                    // data array
    formData: any = null;
    newButtonElement: JQuery = null;
    template: any = null;                // template array, or function
    renderViewDone: Function = null;     // function ($li)
    renderDone: Function = null;         // function ($element)
    form: Object = null;                 // the dynamic form
    formOptions: Object = null;
    // useOverlay: true,       
    columnElement: string = "span";      // column item
    click: Function = null;              // click override. Normally a form will be shown in an overlay
    sort: Function = null;               // sort function: function(a,b),
    overlay: Object = null;
    distinct: boolean =null;             // boolean for distinct check
    onDistinctCheck: Function = null;    // function(element data [], dataItem)
    onBeforeForm: Function = null;       // function($li, data)
    onBeforeCreateForm: Function = null; // function(form+options); this is the itemlist object
    onBeforeAdd: Function = null;        // function($element) - event before prepend or append 
    onUpdate: Function = null;           // function(event, data), for handling update calls
    onCloseForm: Function = null;        // function($li, data)
}

class CmsSingleItemListOptions extends CmsItemListOptions {
    // Used by singleItemList
    hasSearchForm: boolean = null;
    hasAddForm: boolean = null;
    hasEditForm: boolean = null;
    searchForm: object = null;
    onBeforeFill: Function = null;
    onSearchFormCreate: Function = null;
    onSearchFormLoad: Function = null;
    useSearchFormOnClick: boolean = null;
    onEditFormCreate: Function = null;
    onAdd: Function = null;
    addForm: object = null;
    onAddFormCreate: Function = null;
    editForm: object = null;
    onAddFormLoad: Function = null;
    onEditFormLoad: Function = null;
}

interface JQueryStatic {
    // example: formHandler(element: JQuery, options: any): void;
    itemList($element: JQuery, options?: CmsItemListOptions) : void;
}

/* Interface on elements */
interface JQuery {
    itemList(options?: CmsItemListOptions): JQuery;
    singleItemList(options?: CmsSingleItemListOptions) : JQuery;
}

(function ($) {
    /* 
     * requires:  
     *      app.form.js
     *      autoloadmore.js
     *      app.overlay.js
     */
    $.itemList = function ($element, options) {
        this.options = null;
        this.$element = null;
        this.templateEngine = null;

        var me = this;

        // Initialize the element
        this.init = function ($element, options) {
            me.$element = $element.data("itemList", me);
            me.templateEngine = $.autoLoadMore.GetTemplateEngine();

            me.options = $.extend(new CmsItemListOptions(), options);

            if (typeof (me.options.data || null) === "function") {
                me.fill(me.options.data.call(me.$element) || []);
            } else {
                me.fill((me.options.data || []));
            }

            me.$element
                .addClass($.formHandlerDefaultOptions.elementSerializableClass)
                .addClass("itemList")
                .on("serialize", function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    return $element.data("itemList").serialize();
                })
                .on("set-value", function (event, data) {
                    event.preventDefault();
                    event.stopPropagation();

                    if (me.options.onUpdate) {
                        me.options.onUpdate.call(me, event, data);
                    } else {
                        me.fill(data || []);
                    }
                })
                .on("append", function (event, data) {
                    event.preventDefault();
                    event.stopPropagation();

                    if ((data || null) == null) { return false }

                    me.appendListItem(data);

                    $element.trigger("change");
                })
                .on("updateListItem", function (event, $li, data) {
                    event.preventDefault();
                    event.stopPropagation();

                    me.updateListItem($li, data);

                    $element.trigger("change");
                })
                .on("prependListItem", function (event, data) {
                    event.preventDefault();
                    event.stopPropagation();

                    me.prependListItem(data);

                    $element.trigger("change");
                })
                .on("appendListItem", function (event, data) {
                    event.preventDefault();
                    event.stopPropagation();

                    me.appendListItem(data);

                    $element.trigger("change");
                })
                .on("addNew", function(event) {
                    event.preventDefault();
                    event.stopPropagation();

                    if ((me.options.click || null) !== null) {
                        me.options.click(event, null);
                    } else {
                        me.onClick(event, null);
                    }
                })
                .on("clear", function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                    me.clear();
                    $element.trigger("change");
                });

            if ((me.options.newButtonElement || null) !== null) {
                $(me.options.newButtonElement).setClick(function (event) {
                    me.$element.trigger("addNew");
                });
            }
        };

        // Return array of all data items. If the listitem is marked as "deleted", the 
        // item is not added to the result array
        this.serialize = function () {
            // var data = [];
            var selector = me.options.skipSelectectorSerialize;
            var data = me.$element.find("li")
                .filter(function() { var $li = $(this); return $li.is(selector) == false || !$li.dataSource(); })
                .map(function (item) { return $(this).dataSource(); })
                .toArray();

            return data;
        };

        // Create the list items
        this.fill = function (data) {
            me.$element.empty();

            if (!((data || null) === null || !Array.isArray(data))) {
                // Add data and sort
                $.each(data, function (index, item) {
                    me.$element.append(me.createListItem(item));
                });

                me.sort();
            }

            // Always call render done
            me.renderDone();
        };

        this.renderDone = function () {
            if ((me.options.renderDone || null) !== null) {
                me.options.renderDone.call(me, me.$element);
            }
        };

        this.sort = function () {
            if ((me.options.sort || null) !== null) {
                me.$element.children().sort(me.options.sort).appendTo(me.$element);
            }
        };

        this.prependListItem = function (dataItem) {
            if (me.options.onBeforeAdd) { me.options.onBeforeAdd.call(this, me.$element); }

            me.$element.prepend(me.createListItem(dataItem));
            me.sort();
            me.renderDone();
        };

        this.clear = function() {
            me.$element.empty();
            me.renderDone();
        };

        this.appendListItem = function (dataItem) {
            if (me.onBeforeAdd) { me.onBeforeAdd.call(this, me.$element); }

            // Distinct check
            if ((me.options.onDistinctCheck || null) !== null && me.options.onDistinctCheck(me.serialize(), dataItem)) {
                return false;
            } else if (me.options.distinct === true) {
                var found = false;

                $.each(me.serialize(), function (index, item) {
                    if (item === dataItem) {
                        found = true;
                        return false;
                    }
                });

                // @ts-ignore
                if (found === true) { return false; }
            }

            me.$element.append(me.createListItem(dataItem));
            me.sort();
            me.renderDone();
        };

        // Append a listitem
        this.createListItem = function (dataItem) {
            return me.updateListItem(
                $("<li />")
                    .addClass(me.listItemClass)
                    .attr("tabindex", 0),
                dataItem);
        };

        this.updateListItem = function ($li, dataItem) {
            var $div;
            var hasClick = (me.options.click || me.options.form || null) !== null;

            var template = (typeof (me.options.template || null) === "function" ?
                me.options.template.call(me.$element) :
                me.options.template || null) || [];

            // console.log("updateListItem", template, dataItem, me.options.columnElement);

            $li.empty()
                .append($div = $("<div/>").append(me.templateEngine.render(template, dataItem, me.options.columnElement)))
                .off("click")
                .off("refesh")
                .on("refreshView", function (event) {
                    var data = $(this).dataSource();
                    var $div;

                    $(this)
                        .empty()
                        .append($div = $("<div/>").append(me.templateEngine.render(template, data, me.options.columnElement)));

                    if (typeof data === "string") {
                        $div.prepend($("<span/>").text(dataItem));
                    }

                    if ((me.options.renderViewDone || null) !== null) {
                        me.options.renderViewDone.call(me, $li);
                    }
                });
            $li.dataSource(dataItem);

            if ((dataItem || null) !== null && typeof dataItem !== "object") {
                $div.append($("<span/>").text(dataItem));
            }

            if (hasClick) {
                $li.addClass("clickable")
                    .setClick(function (event) {
                        var $me = $(this);
                        if ((me.options.preClick || null) !== null && (me.options.preClick.call(this, $me) || false) === false) { return false; }

                        var $li = $(this);
                        if ((me.options.click || null) !== null) {
                            me.options.click(event, $li);
                        } else {
                            me.onClick(event, $li);
                        }
                    });
            }

            if ((me.options.renderViewDone || null) !== null) {
                me.options.renderViewDone.call(me, $li);
            }

            return $li;
        };

        this.onCloseItemHandler = function(event, requiresUpdate) {
            var itemList = me;

            if (requiresUpdate) {
                var data = $(this).formHandler().serialize();
                var $originLi = $(this).data("itemListItem");

                // additional stuff

                // Append or update the new schedule to the list
                if (($originLi || null) == null) {
                    itemList.appendListItem(data);
                } else {
                    itemList.updateListItem($originLi, data);
                }
            }

            if (itemList.options && itemList.options.onCloseForm) {
                itemList.options.onCloseForm.call(itemList, $originLi, data);
            }

            $(this).overlay().close();
        };

        this.onClick = function (event, $li) {
            event.preventDefault();
            event.stopPropagation();

            if ($li && $li.isDisabled()) { return false; }
            else if ($element.isDisabled()) { return false; }

            var args = $.extend({
                data: $li ? $li.dataSource() : null,
                formData: me.options.formData,
                isNew: (($li || null) === null),
                events: {
                    close: function (event, requiresUpdate) {
                        event.preventDefault();
                        event.stopPropagation();

                        me.onCloseItemHandler.call(this, event, requiresUpdate);
                    }
                }
            }, me.options.formOptions);

            if ((me.options.onBeforeForm || null) !== null) {
                args.data = me.options.onBeforeForm.call(me, $li, args.data);
            }

            // Use promise
            if ((me.options.form || null) !== null) {
                return new Promise(function(resolve, reject) {
                    // resolveValue might return a promise
                    resolve(window.app.resolveValue.call(me, me.options.form));
                }).then(form => {
                    // Merge form and args
                    form = $.extend(args, form);

                    // Call remote service? then optional mutate args data
                    if (me.options.onBeforeCreateForm) {
                        // Should return the from
                        return me.options.onBeforeCreateForm.call(me, form);
                    }

                    return form;
                }).then(function(form) {
                    // Is in overlay?
                    var existingOverlay = null;
                    var $overlayForm = me.$element.closest("form.inOverlay").eq(0);

                    if ($overlayForm.length === 1 && ($overlayForm.overlay !== null)) {
                        existingOverlay = $overlayForm.overlay();
                    }

                    var $newForm = form.create()
                        .data("itemList", me)
                        .data("itemListItem", $li);

                    // Use overlay.next when in an overlay
                    existingOverlay !== null ?
                        existingOverlay.next($newForm) :
                        $newForm.overlay(
                            $.extend(
                                { autoSize: true, allowClose: true, formResolver: $li },
                                me.options.overlay)
                        ).show();
                });
            }
        };

        this.init($element, options);
    };

    $.fn.itemList = function (options?: CmsItemListOptions) {
        return this.each(function () {
            (new $.itemList($(this), options));
        });
    };

    // Create a single itemlist with search button
    $.fn.singleItemList = function (options: CmsSingleItemListOptions) {
        return this.each(function () {
            /*
             * options
             *  data: ...                           - data object
             *  template: [{data: ""}]              - template for itemlist items
             *  searchForm: {}                      - dynamic form object
             *  addForm: {}                         - dynamic form object
             *  editForm: {}                        - dynamic form object
             *  onSearchFormCreate: function()      - event before form create, returns data object
             *  onSearchFormLoad: function()        - form load
             *  onAddFormCreate: function()         - event before form create, returns (new) data object
             *  onAddFormLoad: function()           - form load
             *  onEditFormCreate: function()        - 
             *  onEditFormLoad: function()          - 
             *  onBeforeFill($form, data)           - called on creating the form
             *  onAdd: function(data)               - event on item changed
             *  renderViewDone: function($li)       - event on item added to list
             *  useSearchFormOnClick: true|false
             */
            var me = this;
            var $me = $(me);
            var overlay = null;
            var createSearchForm = null;
            var createAddForm = null;
            var createEditForm = null;
            var template = (typeof (options.template || null) === "function" ?
                options.template.call($me) :
                options.template || null) || [];

            // is in overlay?
            var $overlayForm = $me.closest("form.inOverlay").eq(0);
            if ($overlayForm.length === 1 && ($overlayForm.overlay !== null)) {
                overlay = $overlayForm.overlay();
            }

            var searchForm = options.searchForm || null;
            if (searchForm !== null) {
                var onBeforeFill = options.onBeforeFill || null;

                createSearchForm = function ($element) {
                    return new Promise(function(resolve, reject) {
                        var value = window.app.resolveValue.call(me, searchForm);
                        value ? resolve(value) : null;
                    }).then(function(form) {
                        return [form, (options.onSearchFormCreate ? options.onSearchFormCreate.call($me) : null)];
                    }).spread(function(form, data) {
                        return form.create({
                            // predefined search values
                            data: data,
                            beforeFill: function($form, data) {
                                if(onBeforeFill) { onBeforeFill($form, data); }
                            },
                            events: {
                                load: function () {
                                    var $form = $(this);

                                    if ((options.onSearchFormLoad || null) !== null) {
                                        options.onSearchFormLoad.call($form);
                                    }
                                },
                                close: function (event, data) {
                                    event.preventDefault();
                                    event.stopPropagation();

                                    // When close is called with an object, the itemlist is
                                    // emptied and the object is added
                                    if ((data || null) !== null) {
                                        $element
                                            .empty()
                                            .trigger("append", [data]);

                                        if ((options.onAdd || null) !== null) {
                                            options.onAdd.call($me, data);
                                        }
                                    }

                                    if (me.options && me.options.onCloseForm) {
                                        me.options.onCloseForm.call(me, null, data);
                                    }

                                    // in overlay, then navigate back
                                    overlay !== null ?
                                        overlay.prev() :
                                        $(this).overlay().close();
                                }
                            }
                        });
                    }).then(function($form) {
                        var overlayOptions = $.extend({ formResolver: $me }, options.overlay);
                        overlay ?
                            overlay.next($form) :
                            ($form as unknown as JQuery).overlay(<CmsOverlayOptions> overlayOptions).show();
                    });
                };

                $me.on("search", function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    if ($(this).isDisabled()) { return false; }
                    createSearchForm($me);
                });
            }

            var addForm = options.addForm || null;
            if (addForm !== null) {
                var onBeforeFill = options.onBeforeFill || null;

                createAddForm = function ($element) {
                    return new Promise(function(resolve, reject) {
                        var value = window.app.resolveValue.call(me, addForm)
                        value ? resolve(value) : null;
                    }).then(function(form) {
                        return [form, (options.onAddFormCreate ? options.onAddFormCreate.call($me) : null)];
                    }).spread(function(form, data) {
                        return form.create({
                            // predefined search values
                            data: data,
                            beforeFill: function($form, data) {
                                if(onBeforeFill) { onBeforeFill($form, data); }
                            },
                            events: {
                                load: function () {
                                    var $form = $(this);

                                    if ((options.onAddFormLoad || null) !== null) {
                                        options.onAddFormLoad.call($form);
                                    }
                                },
                                close: function (event, data) {
                                    event.preventDefault();
                                    event.stopPropagation();

                                    // When close is called with an object, the itemlist is
                                    // emptied and the object is added
                                    if ((data || null) !== null) {
                                        $element
                                            .empty()
                                            .trigger("append", [data]);

                                        if ((options.onAdd || null) !== null) {
                                            options.onAdd.call($me, data);
                                        }
                                    }

                                    if (me.options && me.options.onCloseForm) {
                                        me.options.onCloseForm.call(me, null, data);
                                    }

                                    // in overlay, then navigate back
                                    overlay !== null ?
                                        overlay.prev() :
                                        $(this).overlay().close();
                                }
                            }
                        });
                    }).then(function($form) {
                        var overlayOptions = $.extend({ formResolver: $me }, options.overlay);
                        overlay ?
                            overlay.next($form) :
                            ($form as unknown as JQuery).overlay(<CmsOverlayOptions> overlayOptions).show();
                    });
                };

                $me.on("addNew", function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    if ($(this).isDisabled()) { return false; }
                    createAddForm($me);
                });
            }

            var editForm = options.editForm || null;
            if (editForm !== null) {
                var onBeforeFill = options.onBeforeFill || null;

                createEditForm = function ($element, inOverlay) {
                    var data = $element.elementValue();
                    if (data.length === 0) { return false; }
                    data = data[0];

                    return new Promise(function(resolve, reject) {
                        var value = window.app.resolveValue.call(me, editForm);
                        value ? resolve(value) : null;
                    }).then(function(form) {
                        return [form, (options.onEditFormCreate ? options.onEditFormCreate.call($me, data) : data)];
                    }).spread(function(form, data) {
                        return form.create({
                            // predefined search values
                            data: data,
                            beforeFill: function($form, data) {
                                if(onBeforeFill) { onBeforeFill($form, data); }
                            },
                            events: {
                                load: function () {
                                    var $form = $(this);

                                    if ((options.onEditFormLoad || null) !== null) {
                                        options.onEditFormLoad.call($form);
                                    }
                                },
                                close: function (event, data) {
                                    event.preventDefault();
                                    event.stopPropagation();

                                    // When close is called with an object, the itemlist is
                                    // emptied and the object is added
                                    if ((data || null) !== null) {
                                        $element
                                            .empty()
                                            .trigger("append", [data]);

                                        if ((options.onAdd || null) !== null) {
                                            options.onAdd.call($me, data);
                                        }
                                    }

                                    if (me.options && me.options.onCloseForm) {
                                        me.options.onCloseForm.call(me, null, data);
                                    }

                                    // in overlay, then navigate back
                                    overlay !== null ?
                                        overlay.prev() :
                                        $(this).overlay().close();
                                }
                            }
                        });
                    }).then(function($form) {
                        var overlayOptions = $.extend({ formResolver: $me }, options.overlay);
                        inOverlay ?
                            overlay.next($form) :
                            ($form as unknown as JQuery).overlay(<CmsOverlayOptions> overlayOptions).show();
                    });
                };

                $me.on("edit", function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    if ($(this).isDisabled()) { return false; }

                    createEditForm($me, overlay !== null);
                });
            }

            // Hovermenu for removing the item
            var hoverMenu = null;
            hoverMenu = function () {
                return $("<span class='hoverMenu' />")
                    .append(!createEditForm ? null : $("<a href='#' class='button' title='edit'><i class='icon-edit'></i></a>")
                        .on("click", function (event) {
                            event.preventDefault();
                            event.stopPropagation();
                            if ($(this).isDisabled()) { return false; }

                            // $(this).closest("li").toggleClass("deleted");
                            $me.trigger("edit");
                        }))
                    .append($("<a href='#' class='button'><i class='icon-cancel'></i></a>")
                        .on("click", function (event) {
                            event.preventDefault();
                            event.stopPropagation();

                            if ($(this).isDisabled()) { return false; }

                            $(this).closest("li").toggleClass("deleted");
                        }));
            };

            // Create itemlist
            var itemListOptions = new CmsSingleItemListOptions();
            itemListOptions.hasSearchForm = (searchForm || null) !== null;
            itemListOptions.hasAddForm = (addForm || null) !== null;
            itemListOptions.hasEditForm = (editForm || null) !== null;
            // @ts-ignore
            var property = <string>$me.data("prop");
            itemListOptions.data = options.data && Array.isArray(options.data) ? (options.data[property] || null) : null;
            itemListOptions.template = [hoverMenu].concat(template);
            itemListOptions.renderViewDone = options.renderViewDone || null;
            itemListOptions.form = options.useSearchFormOnClick ? options.searchForm : null;
            itemListOptions.overlay = options.overlay;
            itemListOptions.click = function(event, $li) {
                event.preventDefault();
                event.stopPropagation();

                if (!$li) {
                    debugger;
                    return false;
                }
                if ($li.isDisabled()) { return false; }

                var hasSearchForm = this.hasSearchForm;
                var hasAddForm = this.hasAddForm;
                var hasEditForm = this.hasEditForm;
                var data = $li.dataSource() || null;

                if (data && hasEditForm) {
                    $me.trigger("edit");
                } else if (hasSearchForm === true) {
                    $me.trigger("search");
                } else if (hasAddForm === true) {
                    $me.trigger("addNew");
                }
            };
            // Add dummy item when itemlist is empty
            itemListOptions.renderDone = function ($element) {
                var hasSearchForm = this.options.hasSearchForm;
                var hasAddForm = this.options.hasAddForm;
                var hasEditForm = this.options.hasEditForm;

                // Remove potential dummy element(s)
                $element.find("li.dummy").remove();

                // No element? Then add dummy
                if ($element.find("li").length === 0) {
                    $element.append(
                        $("<li/>")
                            .addClass("clickable")
                            .addClass("dummy")
                            .append("<div/>")
                            .on("click", function (event) {
                                event.preventDefault();
                                event.stopPropagation();

                                if ($(this).isDisabled()) { return false; }

                                // if (hasEditForm === true) { $me.trigger("edit"); }
                                if (hasSearchForm === true) { $me.trigger("search"); }
                                else if (hasAddForm === true) { $me.trigger("addNew"); }
                            }));
                }
            };
            $me.itemList(itemListOptions);

            $me.off("append")
                .on("append", function (event, data) {
                    event.preventDefault();
                    event.stopPropagation();

                    var $me = $(this).trigger("clear");

                    if ((data || null) == null) { return false }

                    $me.trigger("appendListItem", [data]);
                })
        });
    }
})(jQuery);
