/* Copyright 2014 - 2020 Willow Media

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. /**/
var CmsPopperOptions = /** @class */ (function () {
    function CmsPopperOptions() {
        this.placement = 'bottom-start';
        this.beforeClick = null;
        // modifiers: {
        //     flip: {
        //         behavior: ['top', 'bottom']
        //     }
        // }
    }
    return CmsPopperOptions;
}());
"use strict";
(function ($) {
    var popperClickHandler = function (popper, opt, $menu, $element) {
        new Promise(function (resolve) {
            if (opt.beforeClick) {
                resolve(opt.beforeClick.call($menu, $element));
            }
            else {
                resolve(null);
            }
        }).then(function () {
            if ($menu.is(".hide") || !$menu.is(":visible")) {
                $menu.removeClass("hide");
                popper.update();
            }
            else if (popper != null) {
                $menu.addClass("hide");
                // popper.destroy();
            }
            else {
                $menu.addClass("hide");
            }
        });
    };
    $.fn.popper = function ($menu, options) {
        return this.each(function () {
            var opt = $.extend(new CmsPopperOptions(), options);
            var $bttnMenu = $(this);
            var popper = window.Popper.createPopper($bttnMenu[0], $menu[0], opt);
            $menu.addClass("manualBlur")
                .on("manualBlur", function (event, target) {
                event.preventDefault();
                event.stopPropagation();
                if (!(target && $(target).has($menu[0]))) {
                    $menu.addClass("hide");
                }
            });
            $bttnMenu.setClick(function (event) {
                var $element = $(event.target);
                popperClickHandler(popper, opt, $menu, $element);
            });
            $menu.closeOnBlur();
            if (opt.showOnInit)
                popperClickHandler(popper, opt, $menu, null);
        });
    };
    $.fn.closeOnBlur = function () {
        var $menu = $(this).eq(0);
        // Use capture event
        document.addEventListener("click", function (e) {
            //if ($menu.is(":visible")) {
            $menu.addClass("hide");
            //}
        }, true);
    };
})(jQuery);
//# sourceMappingURL=app-popper.js.map