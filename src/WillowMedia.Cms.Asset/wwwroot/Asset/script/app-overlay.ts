/* Copyright 2014 - 2021 Willow Media

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. /**/
/// <reference path="interfaces.ts" />

"use strict";

class CmsOverlayOptions {
    $source: JQuery = null;
    allowClose: boolean = true;
    width: any = null;
    height: any = null;
    autoSize: boolean = false;
    enableLegacyAutoSize: boolean = false;
    enableLegacyFormatting: boolean = false;
    position: any = null;               // position from click { x:0, y:0 }
    speed: number = 150;                // speed to fade in or out
    additionClasses: string = null;     // addition css classes
    disableScroll: boolean = true;
    autoSizeContent: boolean = false;
    noFormatting: boolean = false;      // when true, the form wont be split into parts
    manualPopup: boolean = false;
    onShow: Function = null;            // event that is being called when the overlay has been fade in
    onClose: Function = null;           // event that is being called when the overlay is being closed
    onGetSize: Function = null;         // function() this is the overlay
    formResolver: JQuery = null;        // element to call for resolveForm
    closeHandler: Function = null;      // manually handle close on allowClose or escape

    trackChanges: Boolean = null;       // Does close need to validate if there are changes?
    changedHandler: Function = null;    // this($form, onClose()) Custom handler for validating changes

    constructor(options?: CmsOverlayOptions) {
        if (options) {
            this.allowClose = options.allowClose;
            this.autoSize = options.autoSize;
            this.disableScroll = options.disableScroll;
            this.formResolver = options.formResolver;
            this.noFormatting = options.noFormatting;
        }
    }
}

class CmsOverlayHistory {
    $form: JQuery = null;
    options: CmsOverlayOptions = null;
}

class CmsOverlay {
    options: CmsOverlayOptions = null;
    $form: JQuery = null;
    $outer: JQuery = null;
    $inner: JQuery = null;
    history: Array<CmsOverlayHistory> = [];

    constructor($element: JQuery, options: CmsOverlayOptions) {
        var me = this;
        var $form = $($element).eq(0);

        me.$form = $form;
        me.options = $.extend({}, $.overlayHandler.defaultOptions, options);

        me.setOverlayHandler($form);

        // Create an outer and inner overlay
        me.$outer = $('<div />').addClass('overlay_outer')
            .append(me.$inner = $('<div />').addClass('overlay_inner'))
            .hide()
            .on('close', function (event) {
                event.preventDefault();
                event.stopPropagation();
                me.$form.triggerHandler('manualBlur');
                me.close();
            });
        if (me.options.formResolver) {
            me.$outer.on('resolveForm', function (event, formID) {
                event.preventDefault();
                event.stopPropagation();
                return me.options.formResolver.resolveForm(formID);
            });
        }

        if (options && options.width) {
            me.$inner.css({ maxWidth: options.width });
        }

        // split the form
        me.initForm($form);
        // Append the form
        me.$inner.append($form);
        // Add additional classes if requested
        if ((me.options.additionClasses || null) !== null) {
            me.$inner.addClass(me.options.additionClasses);
        }
        // Add events
        me.setDefaultEvents(me);
        // show
        me.show();
    }

    initForm($form: JQuery) {
        var me = this;
        // Obsolete
        if (me.options.enableLegacyFormatting && me.options.noFormatting !== true) {
            $form = me.splitForm($form);
        }

        $form.addClass('inOverlay');
        $form.on('resize', function (event) {
            me.resizeHandler(event);
        });

        this.register($form);
    };

    // Set reference on $form to this
    setOverlayHandler($form: JQuery) {
        var me = this;
        $($form).eq(0).data('overlayHandler', me);
    };

    // Added resize handler
    resizeHandler(event: any) {
        var me = this;
        if (me.options.enableLegacyAutoSize !== true) {
            return;
        }
        if (me.options.autoSize !== true) {
            return;
        }
        var $currentForm = me.$form;
        var $container = $currentForm.find('div.container').eq(0);
        var $divButtons = $currentForm.find('>div.buttons');
        var height = 0;
        if (me.options.onGetSize) {
            height = me.options.onGetSize.call(me);
            me.$inner.height(height + 5);
            return;
        }
        var padding = parseFloat(me.$inner.css('padding'));
        var border = parseFloat(me.$inner.css('border'));
        if ($divButtons.length > 0) {
            height = $('div.container').outerHeight() + $divButtons.outerHeight() + padding + border;
            me.$inner.height(height);
        }
        else {
            $container.children().each(function () { height += $(this).outerHeight(); });
            me.$inner.height(height + 5);
        }
    };

    show() {
        var me = this, $body = $("body");
        var isAdded = me.$outer.parents().hasElement($body);
        if (isAdded) {
            return;
        }
        if (me.options.disableScroll === true) {
            me.disableScrollWindow();
        }
        // Manual blur everything
        $('.manualBlur').triggerHandler('manualBlur');
        // Append the form
        $body.append(me.$outer);
        if (me.options.manualPopup !== true) {
            me.popup();
        }
        if (me.options.onShow) {
            me.options.onShow.call(me);
        }
    };

    popup() {
        var me = this;
        me.$outer
            .css('opacity', 0)
            .show()
            .animate({ opacity: 1 }, { duration: me.options.speed || 350, queue: false });

        me.$inner.show();

        if (me.options.enableLegacyAutoSize && me.options.autoSize) {
            $(window).on('resize', me.resizeHandler).trigger('resize');
        }

        setTimeout(function () { $(window).trigger('resize'); }, 0);
    };

    disableScrollWindow() {
        var me = this;
        $('html').css({ overflow: 'hidden' });
    };

    reenableScrollWindow() {
        var me = this;
        $('html').css({ overflow: 'auto' });
    };

    setDefaultEvents(me) {
        //var me = this;
        // TODO: should we remove the event handler on close?
        $(document).on('keyup', function (event) {
            me.onKey(event);
        });

        me.$form.on('mousedown', function (event) {
            me._mouseDownTarget = event.target;
        });

        me.$outer.on('mousedown', function (event) {
            me._mouseDownTarget = event.target;
            // if (event.target === event.currentTarget)
            //     console.log('outer mousedown', event.target, event.currentTarget);
        });

        me.$outer.on('mouseup', function (event) {
            // if (event.target === event.currentTarget)
            //     console.log('outer mouseup', event.target, event.currentTarget);
        });

        me.$outer.on('click', function (event) {
            if (me._mouseDownTarget === this && me.options.allowClose) {
                event.preventDefault();
                event.stopPropagation();

                // var handleClose = function () {
                //     if (me.options.closeHandler) {
                //         me.options.closeHandler.call(me);
                //     }
                //     else {
                //         me.close();
                //     }
                // };
                //
                // if (me.$form && me.$form.hasChanges()) {
                //     window.app.alertBox({
                //         title: 'Discard changes?',
                //         text: 'You have unsaved changes. Are you sure you want to discard them?',
                //         type: 'warning',
                //         showCancelButton: true,
                //         confirmButtonText: 'Discard',
                //         confirmButtonColor: '#e53c2b'
                //     }, function () {
                //         handleClose();
                //     });
                // }
                // else {
                //     handleClose();
                // }

                me._validateClose(() => {
                    if (me.options.closeHandler) {
                        me.options.closeHandler.call(me);
                    } else {
                        me.close();
                    }
                });
            }
        });
    };

    splitForm($form: JQuery) {
        var $buttons = $form.find('div.buttons');
        var $children = $form.children();
        $form.empty().append($("<div class='container' />").append($children));
        $form.find('div.buttons').remove();
        $form.append($buttons);
        return $form;
    };

    removeHistory() {
        var me = this;
        me.history = [];
        return me;
    };

    _validateClose(onCloseAllowed) {
        var me = this;

        // Validate allow close?
        if (me.options.trackChanges) {
            // Custom handler for validating changes
            if (me.options.changedHandler) {
                me.options.changedHandler.call(me, me.$form);
            }
            else if (me.$form && me.$form.hasChanges()) {
                window.app.confirmNavigate(onCloseAllowed);
            }
        } else {
            onCloseAllowed && onCloseAllowed();
        }
    };

    close() {
        var me = this;
        me._closeOrPreviousCurrent();
    };

    private _closeOrPreviousCurrent() {
        var me = this;

        // Detect if we have history
        if (me.history && me.history.length > 0) {
            me.prev();
        } else {
            me.closeOverlay();
        }
        return me;
    }

    // Register the message handler
    private register($form) {
        if ($form.is("iframe")) {
            var messageHandler = $.overlayHandler.createIframeMessageHandler();
            messageHandler.frame = $form;
            messageHandler.register();
            $form.data("iframeMessageHandler", messageHandler);
        }
    }

    // Unregister the message handler
    private unregister($form) {
        var messageHandler = $form.data("iframeMessageHandler");
        if (!messageHandler) return false;

        $form.data("iframeMessageHandler", null);
        messageHandler.unregister();
    }

    /* for internal use, don't call direct */
    private closeOverlay() {
        var me = this;
        if (me.options.disableScroll === true) {
            me.reenableScrollWindow();
        }

        me.unregister(me.$form);

        me.$outer.fadeOut(me.options.speed, function () {
            if (me.$form.is('.dispose')) {
                me.$form.trigger('dispose');
            }
            me.$outer.remove();
            if (me.options.enableLegacyAutoSize && me.options.autoSize) {
                $(window).off('resize', me.resizeHandler);
            }
            if (me.options.onClose) {
                me.options.onClose.call(me);
            }
        });

        $(window).trigger('resize');

        return me;
    };

    onKey(event: any) {
        var me = this;

        if (me.options.allowClose) {
            switch (event.keyCode) {
                case 27:
                    me._validateClose(() => {
                        if (me.options.closeHandler) {
                            me.options.closeHandler.call(me);
                        } else {
                            me.close();
                        }
                    });
                    break;
                default:
                    break;
            }
        }
    };

    setContent(html) {
        var me = this;
        me.$inner.html(html);
        if (me.options.enableLegacyAutoSize && me.options.autoSize && me.$form) {
            me.$form.trigger("resize");
        }
        $(window).trigger('resize');
    };

    getContentElement() {
        return this.$inner.children().first();
    };

    // Hide the current form and show the next. On prev the form will be visible again
    next($nextform: JQuery, options?: CmsOverlayOptions) {
        var me = this;

        me.unregister(me.$form);
        me.$form.triggerHandler('manualBlur');

        if (me.options && me.options.onClose) {
            me.options.onClose.call(me);
        }

        // Store current form and hide

        var history = new CmsOverlayHistory();
        history.$form = me.$form.hide();
        history.options = $.extend({}, me.options);

        me.history.push(history);
        me.$form = null;
        me.options = options || (me.options ?  new CmsOverlayOptions(me.options) : null);

        me.setOverlayHandler($nextform);

        // Initialize and register if required
        me.initForm($nextform);

        $nextform.on('load', function (event) {
            $(this).find('>.container').scrollTop(0);
            $(window).trigger('resize');
        });

        me.$inner.append($nextform);
        me.$form = $nextform;

        if (me.options.onShow) {
            me.options.onShow.call(me);
        }

        return me;
    };

    prev() {
        var me = this;
        var prevHistory = me.history.pop();

        if ((prevHistory || null) !== null) {
            if (me.options.onClose) {
                me.options.onClose.call(me);
            }

            if (me.$form.is('.dispose')) {
                me.$form.trigger('dispose');
            }
            me.unregister(me.$form);
            me.$form.remove();
            me.$form = null;

            me.options = prevHistory.options;
            me.$form = prevHistory.$form;
            me.register(me.$form);
            me.$form.show();

            $(window).trigger('resize');
        }
        else {
            me.closeOverlay();
        }
    };
}

class CmsIframeOverlayOptions {
    url: string = null;
    events?: CmsIframeOverlayEvents = null;
}

class CmsIframeOverlayEvents {
    load?: Function = null;
    close?: Function = null;
    "alert-and-close"?: Function = null;
}

(function ($) {
    $.overlayHandler = {
        defaultOptions: new CmsOverlayOptions(),
        createIframeMessageHandler: function () {
            var handler = {
                frame: null,
                onMessage: function (event) {
                    if (window.app.debug) console.log("debug: message", event, event.data);

                    if (event && event.data && event.data.length > 0) {
                        var data = window.app.json.parse(event.data);

                        // TODO: validate message?
                        if (data && data.event) {
                            handler.frame.trigger(data.event, data.parameters);
                        } else {
                            handler.frame.trigger("message", [data]);
                        }
                    }
                },
                register: function () {
                    window.addEventListener("message", handler.onMessage);
                },
                unregister: function () {
                    window.removeEventListener("message", handler.onMessage);
                }
            };
            return handler;
        }
    };

    $.fn.inOverlay = function () {
        var $me = $(this).eq(0);
        var overlayHandler = null;
        if ($me.is('form.inOverlay')) {
            overlayHandler = $me.data('overlayHandler');
        } else {
            $me.parents().each(function () {
                var $me = $(this);
                if ($me.is('form.inOverlay')) {
                    overlayHandler = $me.data('overlayHandler');
                    return false;
                }
            });
        }
        return overlayHandler;
    };

    $.fn.overlay = function (options) {
        if ($(this).length === 1) {
            // Get or create formHandler for the first element selected
            var existing = $(this).data('overlayHandler');
            return existing || new CmsOverlay($(this[0]), options);
        }
        return null;
    };

    $.fn.overlayDetails = function (opties) {
        // Create some events on div.overlay-panel
        return this.each(function () {
            var $me = $(this);
            var clear = function () {
                $me.children().addClass('disabled');
                $me.find('form').find('.manualBlur').triggerHandler('manualBlur');
                $me.find('form.dispose').trigger('dispose');
                $me.empty();
            };
            var clearAndShow = function ($form) {
                clear();
                // Add form
                $me.append($form);
                // Show details
                $me.toggleClass('show', true);
            };
            $me.off('hide').on('hide', function (event) {
                event.preventDefault();
                event.stopPropagation();
                clear();
            });
            $me.off('show').on('show', function (event, $form, onShow) {
                event.preventDefault();
                event.stopPropagation();
                // TODO: check if we are allowed to replace form
                var $currentForm = $me.find('form.editor').eq(0);
                if ($currentForm.hasChanges() === true) {
                    window.app.alertBox({
                        title: 'Discard changes?',
                        text: 'You have unmodified changes. Are you sure you want to discard them?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Discard',
                        confirmButtonColor: '#e53c2b'
                    }, function () {
                        clearAndShow($form);
                        if (onShow)
                            onShow();
                    });
                }
                else {
                    clearAndShow($form);
                    if (onShow)
                        onShow();
                }
            });
            $me.off('close').on('close', function (event) {
                event.preventDefault();
                event.stopPropagation();
                $(this).removeClass('show');
                // Clear
                clear();
            });
        });
    };

    $.fn.iframeOverlay = function (options?: CmsIframeOverlayOptions) {
        return this.eq(0).each(function() {
            var $form = $(this);
            var currentOverlay = $form.inOverlay();

            var $iframe = $("<iframe/>")
                .attr("src", options.url)
                .on("load", function (event) {
                    event.preventDefault();
                })
                .on("alert-and-close", function (event, response) {
                    event.preventDefault();
                    event.stopPropagation();

                    if (response && response.Error) {
                        window.app.alertBox("Oops", (response.Error || window.app.messages.UnknownError), "error");
                    }

                    (currentOverlay || $form.overlay()).close();
                })
                .on("close", function (event, data, reload) {
                    event.preventDefault();
                    event.stopPropagation();

                    if (options && options.events && options.events.close) {
                        options.events.close.call($form, event, data, reload, currentOverlay);
                    } else {
                        (currentOverlay || $form.overlay()).close();
                    }
                });

            // Add (or override) events
            if (options && options.events) {
                $.each(options.events, function (key: string, item: any) {
                    switch (key) {
                        case "close":
                            // Never replace this event
                            break;
                        default:
                            $iframe.off(key).on(key, item);
                            break;
                    }
                });
            }

            if (currentOverlay) {
                currentOverlay.next($iframe);
            } else {
                // Move overlay handler to calling element
                $iframe.overlay().show();
                var handler = $iframe.data('overlayHandler');
                $form.data('overlayHandler', handler);
                $iframe.data('overlayHandler', null)
            }

            return $iframe;
        });
    };
})(jQuery);
//# sourceMappingURL=app-overlay.js.map