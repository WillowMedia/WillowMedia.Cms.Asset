﻿/* Copyright 2014 - 2024 Willow Media

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. /**/

/// <reference path="interfaces.ts" />

/* Interface on elements */
interface JQuery {
    autoLoadMoreForm(options: any): JQuery;

    autoLoadMoreFilter(options: any);
}

interface JQueryStatic {
    autoLoadMoreFilter($element: JQuery, options: any) : void;
}

(function ($) {
    // Requires autoLoadMore
    $.fn.autoLoadMoreForm = function (currentOptions) {
        this.options = $.extend({
            allowNew: false,
            onBeforeSend: null,
            onBeforeDetails: null,          // Update data before calling for details
            form: null,
            formNew: null,
            uniqueField: null,              // defines unique property
            renderViewDone: null,
            clickableClass: "clickable",
            click: null,
            scrollElement: null,
            formOptions: null,
            onBeforeCreate: function (formOptions) { return formOptions; },
            onBeforeNew: null,              // function(data) {}  : function that is being called before the new form is shown 
            baseData: null,
            urlDetails: null,
            urlBeforeNew: null,
            OnViewForm: null,               // function() {}      : Event before form view was called. 'this' is the list
            onCloseForm: null,              // function($form, data, reload, formData)  : Additional event on closing the form. 'this' is the list
            onLoadError: null               // function(response) : called when load returns false
        }, currentOptions, { isNew: false });

        var me = this;

        return this.each(function () {
            let $me = $(this);
            let app = window.app;
            let hasForm = (me.options.form || null) !== null;

            if (hasForm === true) {
                var renderViewDone = me.options.renderViewDone;
                me.options.renderViewDone = function ($item, data, formData) {
                    // Add click
                    $item.addClass(me.options.clickableClass);

                    if ((renderViewDone || null) !== null) {
                        renderViewDone.call(this, $item, data, formData);
                    }
                };

                var click = me.options.click;

                me.options.click = function (event, $li) {
                    event.preventDefault();
                    event.stopImmediatePropagation();

                    if ($li.isDisabled()) { return false; }

                    if ($me.triggerHandler("canClose") === true) {
                        if ((me.options.OnViewForm || null) !== null) {
                            me.options.OnViewForm.call($me);
                        }

                        var _super = function() {
                            $me.trigger("view", [$li]);
                        };

                        if (click) {
                            click.call($me, event, $li, _super);
                        } else {
                            _super();
                        }
                    }
                };
            }

            // Find a desirable container element for scrolling
            if ((me.options.scrollElement || null) === null) {
                var $firstRelative = $me.parents().filter(function () {
                    // reduce to only relative position or "body" elements
                    var $me = $(this);
                    return $me.is('body') || $me.css('position') == 'relative';
                }).eq(0);

                me.options.scrollElement = $firstRelative.length > 0 ? $firstRelative : null;
            }

            $me.autoLoadMore(me.options);

            //if ((hasForm || false) === true) {
            $me.on("canClose", function (event) {
                event.preventDefault();
                event.stopPropagation();

                if (!hasForm) { return true; }

                if ($(this).find("li > form").hasChanges()) {
                    return app.confirmNavigate();
                }
                return true;
            })
                .on("close", function (event, data, reload, formData) {
                    event.preventDefault();
                    event.stopPropagation();

                    // Check canClose?

                    if (reload === true) {
                        // Reload
                        $me.empty();
                        $me.autoLoadMore().reload();
                    } else {
                        $me.trigger("updateSource", [data, formData]);

                        // Hide current views
                        $me.find("li.editor")
                            .hide({
                                complete: function () {
                                    $(this).find("> form").trigger("dispose");
                                    $(this).remove();
                                }
                            });
                        var $li = $me.find("li.hide")//.show()
                            .addClass("notify")
                            .removeClass("hide");

                        setTimeout(function () {
                            $li.removeClass("notify").addClass("notifyDone");
                            setTimeout(function () {
                                $li.removeClass("notifyDone");
                            }, 500)
                        }, 0);
                    }
                })
                .on("updateSource", function (event, data, formData) {
                    event.preventDefault();
                    event.stopPropagation();

                    if (data) {
                        var autoloadmore = $me.autoLoadMore();

                        // Create new listitem and replace current hidden
                        var $li = autoloadmore.renderListItem(data, autoloadmore, formData);
                        $li.addClass("hide");
                        $me.find("li.hide").replaceWith($li);
                    }
                })
                .on("view", function (event, $item) {
                    event.preventDefault();
                    event.stopPropagation();

                    // var options = $me.autoLoadMore().options;
                    var data = $.extend({}, $item.dataSource());

                    $me.trigger("close");
                    $me.addClass("disabled");

                    let showForm = function ($overview?, $item?, data?, response?) {
                        return new Promise(function(resolve, reject) {
                            // resolveValue might return a promise
                            resolve(app.resolveValue.call($me, me.options.form, $item));
                        }).then(function(form) {
                            // Append formData
                            $.extend(form, { formData: (response || {}).FormData });

                            var $form = $.dynamicForm(form).create(
                                me.options.onBeforeCreate.call(
                                    $me,
                                    $.extend({
                                            // isNew: false,
                                            events: {
                                                updateSource: function (event, data) {
                                                    event.preventDefault();
                                                    $overview.trigger("updateSource", [data]);
                                                },
                                                close: function (event, data, reload, formData) {
                                                    event.preventDefault();
                                                    event.stopPropagation();

                                                    if ((me.options.onCloseForm || null) !== null) {
                                                        me.options.onCloseForm.call($me, $(this), data, reload, formData);
                                                    }

                                                    // Validate if we can close the form
                                                    $overview.trigger("close", [data, reload, formData]);
                                                }
                                            }
                                        },
                                        me.options.formOptions,
                                        { data: ((me.options.baseData || null) !== null ? $.extend({}, me.options.baseData, data) : data) })));

                            // Add form
                            $item
                                .addClass("hide")
                                .after($("<li class='editor'/>").append($form));
                        }, function() {
                            console.error(arguments);
                            app.alertBox("Error", arguments[0] || "Unknown error", "error");
                        });
                    };

                    var urlDetails = app.resolveValue(me.options.urlDetails || null);

                    if (urlDetails === null) {
                        // Show form with current data
                        data = me.options.onBeforeDetails ? me.options.onBeforeDetails.call(me, data || {}) :  (data || {});

                        showForm($me, $item, data);
                        $me.removeClass("disabled");
                    } else {
                        if ((me.options.onBeforeSend || null) !== null) {
                            me.options.onBeforeSend.call(this, $me, data);
                        }

                        data = me.options.onBeforeDetails ?
                            me.options.onBeforeDetails.call(me, data || {}) :
                            (data || {});

                        // Retreive current info and show form
                        // @ts-ignore // fix with promise
                        app.fetch({
                            url: urlDetails,
                            data: data,
                            onSucces: function (response) {
                                data = response.Data;
                                showForm($me, $item, data, response);
                            },
                            onError: function (response) {
                                app.alertBox("Oops", response.Error || "Unknown error occured", "error");
                            },
                            onFinish: function () {
                                $me.removeClass("disabled");
                            }
                        });
                    }
                });

            //if ((options.allowNew || false) === true) {
            $me.on("addNew", function (event, data) {
                event.preventDefault();
                event.stopPropagation();

                if ((me.options.allowNew || false) === false) {
                    return false;
                }

                var $overview = $me;
                data = me.options.onBeforeNew ? me.options.onBeforeNew.call(me, data || {}) :  (data || {});

                Promise.resolve(data).then((data) => {
                    if ($overview.triggerHandler("canClose") === true) {
                        // Close current
                        $overview.trigger("close");

                        // Hide all
                        $overview.find("li").addClass("hide");

                        var showForm = function ($overview: JQuery, data?, response?) {
                            return new Promise(function(resolve, reject) {
                                // resolveValue might return a promise
                                resolve(app.resolveValue.call($me, me.options.formNew || me.options.form));
                            }).then(function(form) {
                                var formData = (response || {}).FormData;
                                $.extend(form, { formData: (response || {}).FormData });

                                // Create form
                                var $form = $.dynamicForm(form).create($.extend({
                                        isNew: true,
                                        events: {
                                            close: function (event, data, reload, formDataOverload) {
                                                event.preventDefault();

                                                if ((me.options.onCloseForm || null) !== null) {
                                                    me.options.onCloseForm.call($me, $(this), data, reload, formDataOverload);
                                                }

                                                // Validate if we can close the form
                                                // On new, we should always reload if a data object is being returned
                                                $overview.trigger("close", [data || undefined, reload || (data || null) !== null, formDataOverload || formData]);
                                            }
                                        }
                                    },
                                    me.options.formOptions,
                                    { data: ((me.options.baseData || null) !== null ? $.extend({}, me.options.baseData, (data || null)) : (data || null)) }));

                                // Add form
                                $overview.prepend($("<li class='editor'/>").append($form));
                            }, function() {
                                console.error("Error", arguments[0] || "Unknown error", "error");
                                app.alertBox("Error", arguments[0] || "Unknown error", "error");
                            });
                        };

                        var urlBeforeNew = app.resolveValue(me.options.urlBeforeNew || null);

                        if (urlBeforeNew !== null) {
                            if ((me.options.onBeforeSend || null) !== null) {
                                me.options.onBeforeSend.call(this, $me, data);
                            }

                            // Get some base data from the server before showing the form
                            // @ts-ignore // fix with promise
                            app.fetch({
                                url: urlBeforeNew,
                                data: data,
                                onSucces: function (response) {
                                    data = response.Data;
                                    showForm($overview, data, response);
                                },
                                onError: function (response) {
                                    app.alertBox("Oops", response.Error || "Unknown error occured", "error");
                                },
                                onFinish: function () {
                                    $overview.removeClass("disabled");
                                }
                            });
                        } else {
                            showForm($overview, data);
                        }
                    }
                });

            });

            $me.off("reload")
                .on("reload", function (event, filter, sorting, onDone, onError) {
                    event.preventDefault();
                    event.stopPropagation();

                    if ($me.triggerHandler("canClose") === true) {
                        $me.autoLoadMore().reload(filter, sorting, onDone, onError);
                    }
                });
        });
    };

    $.autoLoadMoreFilter = function($element, options) {
        var $filter = null;
        var localOptions = null;
        var me = this;

        this.init = function($element, options) {
            let $filter = me.$filter = $($element);
            let localOptions = me.localOptions = $.extend({}, options);
            let app = window.app;

            $filter.data("autoLoadMoreFilter", me);

            $filter.on("submit", function (event) {
                event.preventDefault(); return false;
            });
            $filter.formHandler();

            $filter.on("search", function (event) {
                event.preventDefault();
                event.stopPropagation();

                var defaultHandling = function($ul: JQuery, data?) {
                    me.searchCall.call(me, $ul, data);
                };

                if (localOptions.searchHandler) {
                    me.localOptions.searchHandler.call(me, defaultHandling);
                } else {
                    var $ul = $(app.resolveValue(localOptions.element));
                    defaultHandling($ul);
                }
            });

            var $search = (localOptions.searchButton instanceof jQuery ? localOptions.searchButton : $filter.find(localOptions.searchButton))
            $search.setClick(function (event) {
                $filter.trigger("search");
            });

            $filter.find("input, #filter select")
                .on("keypress", function (e) { if (e.which === 13) { $search.trigger("click"); } })
                .eq(0)
                .trigger("focus");


            var $new = (localOptions.newButton instanceof jQuery ? localOptions.newButton : $filter.find(localOptions.newButton));
            if ($new && $new.length > 0) {
                me.localOptions.allowNew = true;
                $new.setClick(function (event) {
                    me.newCall();
                });
            }

            // Use hash
            if (localOptions.useHashOnLoad === true) {
                $(window).bind('hashchange', function () {
                    if ($filter.data("filterHash") === location.hash) {
                        return;
                    }

                    var hval = location.hash.slice(1);

                    if ((hval || null) !== null || hval.length > 0) {
                        $filter.formHandler().fill(JSON.parse($.base64.decode(hval)));
                        $search.trigger("click");
                    }
                });

                // Validate if there is a hash
                if ((document.location.hash || "").length > 0) {
                    $filter.formHandler().fill(JSON.parse($.base64.decode(document.location.hash.slice(1))));
                }

                $search.trigger("click");
            }
        };

        this.newCall = function() {
            if (me.localOptions.allowNew !== true) return false;

            var data = (me.localOptions.onBeforeNew || null) !== null
                ? me.localOptions.onBeforeNew()
                : null;
            $(me.localOptions.element).trigger("addNew", [data]);
        };

        this.searchCall = function($ul, data) {
            // before search
            if ((me.localOptions.onSearch || null) !== null) {
                me.localOptions.onSearch($filter);
            }

            if (me.localOptions.useHash === true) {
                var state = me.$filter.formHandler().serialize();
                var hash = "#" + $.base64.encode(JSON.stringify(state));
                if (document.location.hash !== hash) {
                    document.location = hash;
                    me.$filter.data("filterHash", hash);
                }
            }

            var dataObj = data || me.$filter.formHandler().serialize();

            $ul.trigger("reload", [me.hasProperties(dataObj) ? dataObj : null]);
        };

        this.hasProperties = function(data) {
            if (!data) return false;
            if (typeof data !== "object") return false;

            let count = 0
            for (var properties in data) {
                count = count + 1
            }

            return count > 0;
        }

        this.canClose = function($overview, resolve, reject) {
            if (($overview.hasEvent("canClose") || false) === false || ($overview.triggerHandler("canClose") || false) === true) {
                if (resolve) { resolve(); }
            } else {
                if (reject) { reject(); }
            }
        };

        this.init($element, options);
    };

    $.fn.autoLoadMoreFilter = function (options) {
        // requires app.form.js
        if (($.formHandler || null) === null) {
            console.error("FormHandler is missing");
            return false;
        }

        return this.each(function () {
            return new $.autoLoadMoreFilter(
                $(this),
                $.extend({
                    allowNew: false,
                    element: null,
                    newButton: null,
                    searchButton: null,
                    onSearch: null,                 // function ($filter) { }
                    useHash: false,
                    useHashOnLoad: false,           // make sure the form doesn't initialy load 
                    onBeforeNew: null,              // function() return data object
                    //onBeforeSend: null            // onBeforeSend($form, data)
                    searchHandler: null
                }, options));
        });
    };
})(jQuery);