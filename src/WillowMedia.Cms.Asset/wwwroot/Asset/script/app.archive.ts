/// <reference path="interfaces.ts" />

class cmsArchive extends cmsApp  {
    _ajax(options: CmsAjaxOptions) {
        var app = this;
        var request = new CmsAjaxRequest();
        request.self = this;
        // Merge none existing properties to options object
        request.options = <CmsAjaxOptions> app.extendWithProperties((options || {}), new CmsAjaxOptions());
        request.handled = false;

        // TODO: fix loadingIndicator with single instance with a sort of locking
        app.setLoadIndicator(request);

        // Allow extending
        app.extend?.onAjaxBefore && app.extend.onAjaxBefore.call(this, request.options);

        !!request.options.onStart && request.options.onStart();

        // request.options.data = $.extend({}, request.options.data || {});
        request.options.data = app.extendWithProperties({}, request.options.data || {});

        // Call onBeforeSend to manipulate data object
        !!request.options.onBeforeSend && request.options.onBeforeSend(request.options.data);

        return app.createAjaxRequest(request);
    };

    createAjaxRequest(request: CmsAjaxRequest) {
        var app = this;
        var options = request.options;

        if (request.onStart) { request.onStart(); }

        return new Promise(function (resolve, reject) {
            var token, headers = {
                "Accept": "text/event-stream",
                "x-requested-with": "XMLHttpRequest",
                "content-type": "application/json",
            };
            if (app.sw) headers["ngsw-bypass"] = "1";
            // Add request-token when available
            if (request.options.url[0] == "/" && $.fn.requestToken && (token = $("body").requestToken())) {
                headers["request-token"] = token;
            }
            // Add additional headers to objects
            if (options && options.headers) {
                $.each(app.resolveValue(options.headers), function(key: string, value: string) {
                    headers[key] = value;
                });
            }

            // @ts-ignore
            var source = request.SSE = new SSE(request.options.url, {
                headers: headers,
                payload: (options && options.data) ? JSON.stringify(options.data) : null,
                allowRetry: (request.options && request.options.sseAllowRetry === true) || false,
                handleDone: function(payload) {
                    options.abort = null;

                    if (source.readyState === source.ABORTED) {
                        if (request.onDone) { request.onDone(); }
                        if ((options.onFinish || null) !== null) {
                            options.onFinish();
                        }
                        return;
                    }

                    source.close();

                    if (request.onDone) { request.onDone(); }

                    var response = app.extendWithProperties(payload || {}, new CmsVerboseResponse()) as CmsVerboseResponse;

                    if (app && app.extend && app.extend.onAjaxDone) {
                        // @ts-ignore
                        app.extend.onAjaxDone.call(app, options, response);
                    }

                    if (response && response.Data) {
                        response.Data = app.fixResponse.call(app, response.Data);
                    }

                    request.handled = false;
                    request.response = response;

                    resolve(request);
                }
            });

            let messageState = { open: 0, closed: 0, endReceived: false };

            source.addEventListener('readystatechange', function(e) {
                if (source.readyState === 1) messageState.open++;
                if (source.readyState === 2) messageState.closed++;

                // On close, when noStreamis not set, the connection was open, 
                // and not end message was send
                if (source.readyState === 2 &&
                    source._eventStream === true &&
                    messageState.open > 0 &&
                    messageState.endReceived === false)
                {
                    var response = new CmsVerboseResponse();
                    response.Succes = false;
                    response.Error = "Request closed unexpectedly";
                    source.options.handleDone(response);
                }
            });
            source.addEventListener('message', function(e) {
                if (!e) { return; }

                // Assuming we receive JSON-encoded data payloads:
                let response = null;
                let payload = e && e.data && app.json.parse(e.data) || null;

                // no event stream
                if (e && source.readyState === source.NOSTREAM) {
                    source.options.handleDone(payload);
                    return;
                }

                if (!payload) { return; }
                if (payload.Ping) { return; }

                if (request.options.onHandleMessage) {
                    request.options.onHandleMessage.call(request, payload);
                    return;
                }

                switch (payload.State || "Pending") {
                    case "Done":
                    case "Error":
                        messageState.endReceived = true;
                        source.options.handleDone(payload);
                        break;
                    case "Update":
                    case "Started":
                    case "Pending":
                    default:
                        if (request.options && request.options.onStreamMessage) {
                            request.options.onStreamMessage(payload);
                        }
                        break;
                }
            });
            source.addEventListener('error', function(e) {
                let payload = (e && e.data) ? JSON.parse(e.data) : null;

                if (payload) {
                    source.options.handleDone(payload);
                } else {
                    let response = new CmsVerboseResponse();
                    response.Succes = false;
                    response.Error = "Request failed";
                    source.options.handleDone(response);
                }
            });
            source.addEventListener('abort', function(e) {
                source.options.handleDone();
            });

            options.abort = function() {
                source.abort();
            };

            // Start xhr
            source.stream();
        }).then(function(request: CmsAjaxRequest) {
            if (request.onDone) { request.onDone(); }
            var response = request.response;
            var options = request.options;

            // Remove abort function
            options.abort = null;

            // Handle succes and errors
            if (response.Succes && options && options.onSucces) {
                options.onSucces.call(app, response);
                request.handled = true;
            } else if (options.ignoreExpire !== true && response.sre && response.ExpireHtml) {
                // handle with new promise
                return app.expireRestore(response, request);
            } else if (!response.Succes && response.Redirect) {
                document.location.href = response.Redirect;
                return null;
            } else if (!response.Succes && options && options.onError) {
                if (!response.Error) {
                    // @ts-ignore
                    response.Error = ((app && app.messages && app.messages.UnknownError) ? app.messages.UnknownError : null) ||
                        "Unknown error";
                }

                options.onError.call(app, response);
                request.handled = true;
            } else if (!response.Succes && (!options || !options.globalErrorHandler)) {
                app.showGlobalError.call(app, response.Error, options);
                request.handled = true;
            }

            if ((options.onFinish || null) !== null) {
                options.onFinish();
            }

            return request;
        }, function() {
            // console.log("fail handler", this, arguments);
            request.options.abort = null;
            if (request.onDone) { request.onDone(); }

            if (arguments && arguments.length > 0 && arguments[0] instanceof DOMException) {
                var ex = arguments[0];
            }
        })
            .catch(function(error) {
                if (request.onDone) { request.onDone(); }
                request.options.abort = null;
                var fakeResponse = new CmsVerboseResponse();
                fakeResponse.Error = error;

                if (options && options.onError) {
                    options.onError.call(app, fakeResponse);
                } else {
                    app.showGlobalError.call(app, fakeResponse.Error, options);
                }
            });
    }
}

// Add customEvent for IE11
// https://stackoverflow.com/questions/59032851/how-to-convert-dispatchevent-for-a-customevent-for-ie11#59043009
(function () {
    if (typeof window.CustomEvent === "function") return false;

    function CustomEvent(event, params) {
        params = params || { bubbles: false, cancelable: false, detail: undefined };
        var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }
    CustomEvent.prototype = window.Event.prototype;
    // @ts-ignore
    window.CustomEvent = CustomEvent;

    if(typeof String.prototype.trimRight !== 'function') {
        String.prototype.trimRight = function() {
            return this.replace(/\s+$/g, '');
        }
    }

    if(typeof String.prototype.trimLeft !== 'function') {
        String.prototype.trimLeft = function() {
            return this.replace(/ˆ\s+/g, '');
        }
    }

    if (Number.parseFloat === undefined) {
        Number.parseFloat = parseFloat;
    }
})();


/**
 * Copyright (C) 2016 Maxime Petazzoni <maxime.petazzoni@bulix.org>.
 * All rights reserved.
 */
var SSE = function (url, options) {
    if (!(this instanceof SSE)) {
        // @ts-ignore
        return new SSE(url, options);
    }

    this.INITIALIZING = -1;
    this.CONNECTING = 0;
    this.OPEN = 1;
    this.CLOSED = 2;
    this.NOSTREAM = -2;
    this.ABORTED = -3

    this.url = url;
    this.options = options || {};
    this.allowRetry = (options && options.hasOwnProperty("allowRetry")) ?options.allowRetry === true : true;
    this.headers = (options && options.headers) || {};
    this.payload = options.payload !== undefined ? options.payload : '';
    this.method = options.method || (this.payload && 'POST' || 'GET');
    this.withCredentials = !!options.withCredentials;

    this.FIELD_SEPARATOR = ':';
    this.listeners = {};

    this.xhr = null;
    this.retry = null;
    this.retryTimer = null;
    this.closeRequested = null;
    this.readyState = this.INITIALIZING;
    this.progress = 0;
    this.chunk = '';
    this._eventStream = false;

    this.addEventListener = function(type, listener) {
        if (this.listeners[type] === undefined) {
            this.listeners[type] = [];
        }

        if (this.listeners[type].indexOf(listener) === -1) {
            this.listeners[type].push(listener);
        }
    };

    this.removeEventListener = function(type, listener) {
        if (this.listeners[type] === undefined) {
            return;
        }

        var filtered = [];
        this.listeners[type].forEach(function(element) {
            if (element !== listener) {
                filtered.push(element);
            }
        });
        if (filtered.length === 0) {
            delete this.listeners[type];
        } else {
            this.listeners[type] = filtered;
        }
    };

    this.dispatchEvent = function(e) {
        if (!e) {
            return true;
        }

        e.source = this;

        var onHandler = 'on' + e.type;
        if (this.hasOwnProperty(onHandler)) {
            this[onHandler].call(this, e);
            if (e.defaultPrevented) {
                return false;
            }
        }

        if (this.listeners[e.type]) {
            return this.listeners[e.type].every(function(callback) {
                callback(e);
                return !e.defaultPrevented;
            });
        }

        return true;
    };

    this._setReadyState = function(state) {
        // Updates readystate when changed
        if (this.readyState === state) { return; }

        var event = new CustomEvent('readystatechange');
        // @ts-ignore
        event.readyState = state;
        this.readyState = state;
        this.dispatchEvent(event);
    };

    this._onStreamFailure = function(e) {
        var event = new CustomEvent("error");
        if (this.xhr && this.xhr.response) {
            // @ts-ignore
            event.data = this.xhr.response;
        }
        this.dispatchEvent(event);
        this.close();
    }

    this._onStreamAbort = function(e) {
        this._setReadyState(this.ABORTED);

        if (this.xhr) {
            this.xhr.abort();
            this.xhr = null;
        }

        this.dispatchEvent(new CustomEvent('abort'));
    }

    this._onStreamProgress = function(e) {
        if (!this.xhr) {
            return;
        }

        if (this.xhr.status !== 200) {
            this._onStreamFailure(e);
            return;
        }

        // this.INITIALIZING = -1;
        // this.CONNECTING = 0;
        // this.OPEN = 1;
        // this.CLOSED = 2;
        // this.NOSTREAM = -2;
        // this.ABORTED = -3


        if (this.readyState == this.CONNECTING) {
            this.dispatchEvent(new CustomEvent('open'));
            this._setReadyState(this.OPEN);
        }

        // Handle chuncks of data
        let data = this.xhr.responseText.substring(this.progress);

        this.progress += data.length;
        data.split(/(\r\n|\r|\n){2}/g).forEach(function(part) {
            var event = null;

            if (!this._eventStream) {
                // Setup
                if (this.readyState === this.OPEN) {
                    this.chunk += part;
                } else {
                    this._pushChuck();
                }
            } else if (part.trim().length === 0) {
                event = this._parseEventChunk(this.chunk.trim());
                this.chunk = '';
                this.dispatchEvent(event);
            } else {
                this.chunk += part;
            }

        }.bind(this));
    };

    this._pushChuck = function() {
        this._setReadyState(this.NOSTREAM);
        let event = new CustomEvent("message");
        // @ts-ignore
        event.data = this.chunk;
        this.dispatchEvent(event);
        this.chunk = '';
    };

    this._onStreamLoaded = function(e) {
        this._onStreamProgress(e);

        // Parse the last chunk.
        this.dispatchEvent(this._parseEventChunk(this.chunk));
        this.chunk = '';
    };

    /**
     * Parse a received SSE event chunk into a constructed event object.
     */
    this._parseEventChunk = function(chunk) {
        if (!chunk || chunk.length === 0) {
            return null;
        }

        var e = {'id': null, 'retry': null, 'data': '', 'event': 'message'};
        chunk.split(/\n|\r\n|\r/).forEach(function(line) {
            line = line.trimRight();
            var index = line.indexOf(this.FIELD_SEPARATOR);
            if (index <= 0) {
                // Line was either empty, or started with a separator and is a comment.
                // Either way, ignore.
                return;
            }

            var field = line.substring(0, index);
            if (!(field in e)) {
                return;
            }

            var value = line.substring(index + 1).trimLeft();
            if (field === 'data') {
                e[field] += value;
            } else if (field === 'retry') {
                e[field] = parseInt(value);
            } else {
                e[field] = value;
            }
        }.bind(this));

        var event = new CustomEvent(e.event);
        // @ts-ignore
        event.data = e.data;
        // @ts-ignore
        event.id = e.id;
        // @ts-ignore
        if (e.retry) { this.retry = e.retry; }

        return event;
    };

    this._checkStreamClosed = function() {
        var me = this;

        if (!me.xhr) {
            return;
        }

        if (me.xhr.readyState === XMLHttpRequest.DONE) {
            me._setReadyState(this.CLOSED);

            // Retry
            if (me.allowRetry && me.retry >= 0 && !this.closeRequested) {
                me.retryTimer = setTimeout(function() { me.stream(); }, me.retry);
            }
        }
    };

    this.stream = function() {
        var me = this;
        this.closeRequested = null;

        this._setReadyState(this.CONNECTING);
        this.xhr = new XMLHttpRequest();
        this.xhr.onreadystatechange = function() {
            if (this.readyState === this.HEADERS_RECEIVED) {
                var contentType = this.getResponseHeader("Content-type");
                if (contentType && contentType.toLowerCase().indexOf("text/event-stream") >= 0) {
                    me._eventStream = true;
                }
            }
        }
        this.xhr.addEventListener('progress', this._onStreamProgress.bind(this));
        this.xhr.addEventListener('load', this._onStreamLoaded.bind(this));
        this.xhr.addEventListener('readystatechange', this._checkStreamClosed.bind(this));
        this.xhr.addEventListener('error', this._onStreamFailure.bind(this));
        this.xhr.addEventListener('abort', this._onStreamAbort.bind(this));

        // TEST
        this.xhr.addEventListener('timeout', function() { console.log("SSE Xhr timeout"); });

        this.xhr.open(this.method, this.url);
        for (var header in this.headers) {
            this.xhr.setRequestHeader(header, this.headers[header]);
        }
        this.xhr.withCredentials = this.withCredentials;
        this.xhr.send(this.payload);
    };

    this._isClosedOrAborted = function() {
        return this.xhr == null || this.readyState === this.CLOSED || this.readyState === this.ABORTED;
    };

    this._close = function(newState) {
        this.closeRequested = true;
        clearTimeout(this.retryTimer);
        this.xhr.abort();
        this.xhr = null;
        this._setReadyState(newState);
    };

    // Close is a user request to close the connection
    this.close = function() {
        if (this._isClosedOrAborted()) return;
        this._close(this.CLOSED);
    };

    // Abort is a user request to close the connection
    this.abort = function() {
        if (this._isClosedOrAborted()) return;
        this._close(this.ABORTED);
    };
};