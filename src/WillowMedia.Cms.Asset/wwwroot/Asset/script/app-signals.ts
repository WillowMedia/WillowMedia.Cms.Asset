/// <reference path="interfaces.ts" />

class signals {
    //SSE = null;
    source = null;
    connected = false;
    fails = 0;
    onUpdate = null;
    onError = null;
    onOpen = null;
    onClose = null;
    url = null;
    payload = null;
    allowReconnect = true;

    _handler: CmsSignalHandler = null;

    constructor (options = null) {}

    close = function () {
        let me = this;
        !!me._handler && !!me._handler.close && me._handler.close();
    };

    // handleMessage = function (e) {};

    // andleOpen = function (e, onOpen) {};

    // reconnect = function (url, options) {};

    _connect = function(url) { // (url, options) {
        let me = this;
        let app = window.app;
        let o = new CmsSignalOptions();
        o.url = url || me.url;
        o.payload = me.payload;
        o.allowReconnect = me.allowReconnect;
        o.onConnect = function() {
            !!me.onOpen && me.onOpen();
        };
        o.onMessage = function (data) {
        };
        o.onDisconnect = function() {
            !!me.onClose && me.onClose();
        };
        o.onStreamMessage = function(payload) {
            !!app.debug && console.log("onStreamMessage", payload);
            !!payload && !!me.onUpdate && me.onUpdate(payload);
        };
        me._handler = new CmsSignalHandler(o);
    }

    init = function(url) { // (options) {
        let me = this;
        me._connect(url);
    }
}

class CmsSignalOptions {
    constructor (options = null) {
        if (!!options) {
            if (!!options.url) this.url = options.url;
            if (!!options.headers) this.headers = options.headers;
            if (!!options.payload) this.payload = options.payload;
            if (!!options.onConnect) this.onConnect = options.onConnect;
            if (!!options.onMessage) this.onMessage = options.onMessage;
            if (!!options.onDisconnect) this.onDisconnect = options.onDisconnect;
            if (!!options.onStreamMessage) this.onStreamMessage = options.onStreamMessage;
            if (!!options.reconnect) this.reconnectTimeout = options.reconnect;
        }
    }

    url = null;                          // Connection url
    headers: object = null;
    payload: any = null;                 // Payload on connect (van be a function)
    onConnect: Function = null;
    onMessage: Function = null;
    onDisconnect: Function = null;
    onStreamMessage: Function = null;    // Override the onStreamMessage callback
    reconnectTimeout  = 10000;
    allowReconnect: boolean = true;
}

class CmsSignalHandler {
    options = null;
    _request = null;
    _windowFocus = false;
    _watchdogTimer = null;
    _disposed = null;

    constructor (options = null) {
        this.options = new CmsSignalOptions(options);

        // console.log(options, this.options);

        this.init();
        this.connect();
    }

    _eventHandlers = null;

    init() {
        var me = this;

        me._eventHandlers = {
            windowFocus: function() {
                this._windowFocus = true;
            },
            windowBlur: function() {
                this._windowFocus = false;
            },
            pageShow: function() {
                this._windowFocus = true;
            },
            pageHide: function() {
                this._windowFocus = false;
            },
            visibilitychange: function() {
                this._windowFocus = !!document.visibilityState; // && me.Focussed() || me.Blurred();
            },
            online() {
                console.log('Connection online detected');
            },
            offline() {
                console.warn('Connection offline detected. Closing');
                me.close();
            }
        }

        $(window).on("focus", me._eventHandlers.windowFocus);
        $(window).on("blur", me._eventHandlers.windowBlur);
        window.addEventListener("pageshow", me._eventHandlers.pageShow);
        window.addEventListener("pagehide", me._eventHandlers.pageHide);
        document.addEventListener("visibilitychange", me._eventHandlers.visibilitychange);
        window.addEventListener('online', me._eventHandlers.online);
        window.addEventListener('offline', me._eventHandlers.offline);
    }

    destroy () {
        var me = this;
        me.close();
        me._disposed = true;

        $(window).off("focus", me._eventHandlers.windowFocus);
        $(window).off("blur", me._eventHandlers.windowBlur);
        window.removeEventListener("pageshow", me._eventHandlers.pageShow);
        window.removeEventListener("pagehide", me._eventHandlers.pageHide);
        document.removeEventListener("visibilitychange", me._eventHandlers.visibilitychange);
        window.removeEventListener('online', me._eventHandlers.online);
        window.removeEventListener('offline', me._eventHandlers.offline);

        me._eventHandlers = null;
    }

    watchdog () {
        // TODO: should watchdog validate last recieved message for a certain timeout?
        var me = this;
        if (me._disposed) return false;

        // Clear the timer
        !!me._watchdogTimer && clearTimeout(me._watchdogTimer)

        if (!!me._request) {
            // is active
        } else {
            //console.log("watchdag reconnect");
            me.connect();
        }

        me.setTimeout();
    }

    setTimeout () {
        var me = this;
        me._watchdogTimer = setTimeout(() => me.watchdog(), me.options.reconnectTimeout);
    }
    close() {
        let me = this;
        !!me._watchdogTimer && clearTimeout(me._watchdogTimer)
        !!me._request && !!me._request.abort && me._request.abort();
    };
    connect () {
        let me = this;
        let app = window.app;

        me.close();

        if (me._disposed) return false;

        let request = new CmsFetchOptions();
        request.url = me.options.url;
        request.data = !!me.options.payload && app.resolveValue(me.options.payload);
        request.headers = !!me.options.headers && me.options.headers;
        request.loadIndicator = false;
        request.allow502retry = false;
        request.sseAllowRetry = false;
        request.onStreamMessage =  me.options.onStreamMessage || function(payload) {
            !!app.debug && console.log("onStreamMessage", payload);
            !!payload && !!payload.Succes && !!me.options && !!me.options.onMessage && me.options.onMessage(payload.Data);
        };
        request.onConnect = me.options.onConnect;
        request.onError = null;

        me._request = request;

        app.fetch(me._request).then(request => {
            !!app.debug && console.log("connection closed", request);
            me._request = null;

            // Send last recieved message
            !!request && !!request.response && !!request.response.Data && !!me.options && !!me.options.onMessage && me.options.onMessage(request.response.Data);

            // Call disconnected
            !!me.options.onDisconnect && me.options.onDisconnect();

            // Should we reconnect?
            if (!!me.options.allowReconnect) me.setTimeout();
        });
    }
}
