/* Copyright 2014 - 2024 Willow Media

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. /**/
/// <reference path="interfaces.ts" />
"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var checkboxDropDownListOptions = /** @class */ (function () {
    function checkboxDropDownListOptions(cnt) {
        this.width = null;
        this.multiple = true;
        this.matchValue = null; // = function() {};
        this.getText = null; // = function($option) {};
        this.getValue = null; // = function($option) {};
        this.getDisabled = null; // = function(this) {}
        this._counter = null;
        this.serializeAsArray = false; // Force serialize as array, even if not multiple
        this._counter = cnt;
    }
    return checkboxDropDownListOptions;
}());
(function ($) {
    var debugEvents = false;
    var _counter = 0;
    $.fn.checkboxDropDownList = function (checkboxOptions) {
        var app = window.app;
        return this.map(function () {
            var $me = $(this).eq(0);
            var localOptions = __assign(__assign(__assign({}, new checkboxDropDownListOptions(++_counter)), (checkboxOptions || {})), { multiple: $me.is("select[multiple]") });
            // 'Clone' element
            var $element = $("<span class='chkDropDown' />")
                .addClass($me.attr('class'))
                .attr("id", $me.attr("id"))
                .attr("name", $me.attr("name"))
                .data($me.data())
                .data("options", localOptions)
                .attr("tabindex", 0);
            //@ts-ignore
            $element[0].form = $me.closest("form").get(0)[0];
            var dropdownId = app.uniqueId("chkboxddl");
            var $textSpan = null;
            var $dropdown = $("<div />")
                .addClass('chkDropDownList list noselect')
                .attr("id", dropdownId)
                .on("click", function (event) {
                event.preventDefault();
                event.stopPropagation();
                return false;
            });
            var placeholder = $me.attr("placeholder");
            if (!!placeholder && placeholder.length > 0) {
                $element.append($("<div/>").addClass("placeholder").text(placeholder));
            }
            var errorId;
            var $errorPanel = $("<span />").addClass("hide").attr("id", errorId = app.uniqueId());
            // Function for scroll events on show
            var scrollFollow = function (event) {
                var $doc = $(document);
                var $element = $(this).closest("span.chkDropDown");
                var loc = $element.offset();
                $dropdown.css({
                    top: loc.top - $doc.scrollTop() + $element.outerHeight(true),
                    left: loc.left - $doc.scrollLeft(),
                    minWidth: (localOptions.width !== null ? localOptions.width : $element.outerWidth()) + "px"
                });
            };
            // set element
            $element
                .append($textSpan = $("<span />").addClass("noselect"))
                .addClass("empty")
                .data("errorhighlight", $element)
                .data("errorloc", "#" + errorId)
                .addClass("formHandlerSerializable")
                .addClass("manualBlur")
                .on("serialize", function (event) {
                var $element = $(this).closest("span.chkDropDown");
                !!debugEvents && $.debugEvent.call(this, event);
                event.preventDefault();
                event.stopPropagation();
                var result = null;
                var options = $element.data("options") || {};
                var multiple = !!options.multiple;
                var serializeAsArray = !!options.serializeAsArray;
                if (!!multiple || serializeAsArray) {
                    $dropdown
                        .find(".item.checked")
                        .each(function () {
                        if (result === null) {
                            result = [];
                        }
                        result.push($(this).dataSource());
                    });
                    result = !!result && !!serializeAsArray ? result.slice(0, 1) : result;
                }
                else {
                    result = $dropdown
                        .find(".item.checked")
                        .eq(0)
                        .dataSource();
                }
                return result;
            })
                .on("set-value", function (event, data) {
                var $element = $(this).closest("span.chkDropDown");
                !!debugEvents && $.debugEvent.call(this, event);
                event.preventDefault();
                event.stopPropagation();
                var multiple = ($element.data("options") || null).multiple === true;
                if (!data) {
                    $dropdown.find(".item").removeClass("checked");
                }
                else if (multiple && (data || null) !== null && $.isArray(data) === true) {
                    $.each(data, function (index, item) {
                        $dropdown.find(".item")
                            .filter(function () {
                            var rec = $(this).dataSource();
                            return localOptions.matchValue
                                ? localOptions.matchValue.call(rec, item)
                                : (rec === item || (new String(rec).toString() === new String(item).toString()));
                        })
                            .addClass("checked");
                        if (!(multiple === true)) {
                            return false;
                        }
                    });
                }
                else if (!multiple) {
                    var item = data;
                    $dropdown.find(".item")
                        .filter(function () {
                        var rec = $(this).dataSource();
                        return localOptions.matchValue
                            ? localOptions.matchValue.call(rec, item)
                            : (rec === item || (new String(rec).toString() === new String(item).toString()));
                    })
                        .eq(0)
                        .addClass("checked");
                }
                $element.trigger("updateView");
            })
                .on("clear", function (event) {
                var $element = $(this).closest("span.chkDropDown");
                !!debugEvents && $.debugEvent.call(this, event);
                event.preventDefault();
                event.stopPropagation();
                $dropdown.find(".item")
                    .removeClass("checked")
                    .removeClass("suggest");
                $element.trigger("updateView");
            })
                .on("setup", function (event, $options) {
                var $element = $(this).closest("span.chkDropDown");
                !!debugEvents && $.debugEvent.call(this, event);
                event.preventDefault();
                event.stopPropagation();
                $dropdown.trigger("clear").empty();
                var multiple = ($element.data("options") || null).multiple === true;
                $options.each(function () {
                    var $items;
                    if ($(this).is("optgroup")) {
                        $items = $(this).children();
                        $dropdown.append($("<div/>").text($(this).attr("label")));
                    }
                    else {
                        $items = $(this);
                    }
                    // add items
                    $.each($items, function (index, item) {
                        var $option = $(item);
                        var $div;
                        var text = !!localOptions.getText ? localOptions.getText($option) : $option.text();
                        var value = !!localOptions.getValue ? localOptions.getValue($option) : ($option.data("value") || $option.attr("value"));
                        var disabled = !!localOptions.getDisabled ? localOptions.getDisabled($option) : !!$option.prop("disabled");
                        $dropdown.append($div = $("<div />")
                            .setClick(function (event) {
                            debugEvents && $.debugEvent.call(this, event);
                            if (multiple === true) {
                                $(this).toggleClass("checked");
                            }
                            else {
                                var selected = $(this).is(".checked");
                                $dropdown.find("div").removeClass("checked");
                                $(this).toggleClass("checked", !selected);
                            }
                            $element.trigger("updateView");
                            $element.trigger("change");
                        })
                            .setDataSource(value)
                            .addClass("item")
                            .addClass("noselect")
                            .toggleClass("disabled", !!disabled)
                            .append($("<i class='icon-ok'/>"))
                            .append($("<label />")
                            .addClass("noselect")
                            .text(text)));
                    });
                });
            })
                .on("focus", function (event) {
                var $element = $(this).closest("span.chkDropDown");
                !!debugEvents && $.debugEvent.call(this, event);
                if ($element.isDisabled()) {
                    event.preventDefault();
                    event.stopPropagation();
                    return false;
                }
            })
                .on("updateView", function (event) {
                var $element = $(this).closest("span.chkDropDown");
                !!debugEvents && $.debugEvent.call(this, event);
                event.preventDefault();
                event.stopPropagation();
                var items = [];
                $dropdown.find(".item.checked:not(.hide) > label").each(function () {
                    items.push($(this).text());
                });
                $textSpan.text(items.join(", "));
                $element.toggleClass("notEmpty", items.length !== 0);
            })
                .on("suggestions", function (event, data) {
                var $element = $(this).closest("span.chkDropDown");
                !!debugEvents && $.debugEvent.call(this, event);
                event.preventDefault();
                event.stopPropagation();
                var $items = $element
                    .find("div.list > div.item")
                    .removeClass("suggest");
                if ((data || null) === null) {
                    return false;
                }
                if (!$.isArray(data)) {
                    return false;
                }
                $items.each(function () {
                    $(this).toggleClass("suggest", data.indexOf($(this).data("rec")) >= 0);
                });
            })
                .on("blur", function (event) {
                var $element = $(this).closest("span.chkDropDown");
                !!debugEvents && $.debugEvent.call(this, event);
                event.preventDefault();
                event.stopPropagation();
                var $target = $(event.relatedTarget);
                //@ts-ignore
                var isChildEvent = !!$target.is($element) || $target.parents($element).length > 0;
                if (!!isChildEvent) {
                    return false;
                }
                // If its not one of our elements, then hide
                $element.triggerHandler("hide-overlay");
            })
                .on("manualBlur", function (event, targetElement) {
                var $element = $(this).closest("span.chkDropDown");
                !!debugEvents && $.debugEvent.call(this, event);
                event.preventDefault();
                event.stopPropagation();
                if (!!targetElement) {
                    var $target = $(targetElement);
                    var isChildEvent = !!$target.is($element) || $target.parents().contains($element);
                    if (!!isChildEvent) {
                        return false;
                    }
                }
                $element.triggerHandler("hide-overlay");
            })
                .on("click", function (event) {
                var $element = $(this).closest("span.chkDropDown");
                !!debugEvents && $.debugEvent.call(this, event);
                event.preventDefault();
                event.stopPropagation();
                if ($(this).isDisabled()) {
                    return false;
                }
                $(this).triggerHandler("toggle");
            })
                .on("toggle", function (event) {
                var $element = $(this).closest("span.chkDropDown");
                !!debugEvents && $.debugEvent.call(this, event);
                event.preventDefault();
                event.stopPropagation();
                // Hide if its already open
                if ($element.is(".show")) {
                    $element.triggerHandler("hide-overlay");
                    return;
                }
                else {
                    $element.trigger("show");
                    $(document).triggerHandler("manualBlur", [$element]);
                }
            })
                .on("keydown", function (event) {
                var $element = $(this).closest("span.chkDropDown");
                !!debugEvents && $.debugEvent.call(this, event);
                if (event.which == 32 || event.which >= 24 || event.which <= 27) {
                    event.preventDefault();
                    event.stopPropagation();
                    $element.trigger("show");
                }
            })
                .on("hide-overlay", function (event) {
                var $element = $(this).closest("span.chkDropDown");
                !!debugEvents && $.debugEvent.call(this, event);
                event.preventDefault();
                event.stopPropagation();
                $(window).off("scroll", dropdownId);
                $(window).off("resize", dropdownId);
                $element.removeClass("show");
                $element.removeClass("focus");
            })
                .on("show", function (event) {
                var $element = $(this).closest("span.chkDropDown");
                !!debugEvents && $.debugEvent.call(this, event);
                event.preventDefault();
                event.stopPropagation();
                // Set position
                scrollFollow.call($element);
                $(window).on("scroll", dropdownId, scrollFollow);
                $(window).on("resize", dropdownId, scrollFollow);
                $element.addClass("show");
            });
            $element
                .trigger("setup", [$me.find(">option[value],>optgroup")])
                .trigger("change");
            $me.replaceWith($element);
            $element.after($errorPanel);
            $element.append($dropdown);
            // Attempt to disable scrolling on this element
            $dropdown.preventScroll();
            return $element;
        });
    };
})(jQuery);
//# sourceMappingURL=app-checkboxDropDownList.js.map