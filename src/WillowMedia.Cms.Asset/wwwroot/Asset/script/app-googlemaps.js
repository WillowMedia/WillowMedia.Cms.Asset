﻿
/// <reference path="app.js" />
/* Copyright 2014 - 2015 Willow Media

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. /**/



(function ($) {
    "use strict";

    /* 
     * requires: 
     *      app.form.js
     *      autoloadmore.js
     *      app.overlay.js
     */
    $.appGoogleMap = function ($element, options) {
        this.options = null;
        this.$element = null;
        this._gmap = null;
        this._markers = [];
        this._currentInfoWindow = null;
        this._bounds = null;

        this.defaultMarkerOptions = {
            center: false,
            latitude: null,
            longitude: null,
            title: null,
            url: null,
            $source: null
        };

        this.init = function ($element, options) {
            var me = this;
            this.$element = $($element);

            this.$element
                .empty()
                .data("appGoogleMap", me)
                .addClass("formHandlerSerializable")
                .on("serialize", function (event) {
                    return me.serialize();
                })
                .on("addMarker", function (event, coordinate, center, title, url, source) {
                    var markerOptions = null;
                    switch (arguments.length) {
                        case 2:
                            markerOptions = $.extend(me.defaultMarkerOptions, coordinate);
                        default:
                            markerOptions = $.extend(me.defaultMarkerOptions, {
                                latitude: coordinate.Latitude,
                                longitude: coordinate.Longitude,
                                center: center,
                                title: title,
                                url: url,
                                $source: source
                            });
                            break;
                    } 

                    var marker = me.addMarker(markerOptions); //coordinate.Latitude, coordinate.Longitude);

                    if (me.options.singleMarker === true) {
                        me.fitBounds();
                    }

                    if ((center || false) === true) {
                        me._gmap.setCenter(marker.getPosition());
                    }

                    return marker;
                })
                .on("center", function (event, coordinate) {
                    //map.setCenter(results[0].geometry.location);
                    me._gmap.setCenter(new google.maps.LatLng(coordinate.Latitude, coordinate.Longitude));
                })
                .on("fit", function (event, options) {
                    // console.log(options || {})

                    var zoom = null;
                    if ((options || null) !== null) {
                        zoom = options.maxZoom || null;
                    }

                    // console.log("fit trigger zoom={0}".format(zoom || "null"));

                    me.fitBounds(zoom);
                })
                .on("clear", function (event) {
                    me.clearMarkers();
                });

            me.options = $.extend({
                multiplication: 1000000,
                singleMarker: false,
                draggable: false,
                center: null,               // TODO: default center point,
                data: null,                 // TODO: initial data array of coordinates {lat:...,lng:...}
                initialZoom: null,
                minimumZoomOnFit: null
            }, options);

            me._gmap = new google.maps.Map(this.$element[0], {
                zoom: me.options.initialZoom || 5,
                center: new google.maps.LatLng(52.3568374, 5.6278221),  //),
                disableDefaultUI: false,
                mapTypeControl: true,
                navigationControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            google.maps.event.addListener(me._gmap, 'click', function (event) { me.closeInfo(); });

            if ((me.options.data || null) !== null) {
                if (me.options.singleMarker === true) {
                    // me.options.data should be an object with coordinates
                    me.$element.trigger("addMarker", [me.options.data, true]);
                } else {
                    $.each(me.options.data, function (index, coordinate) {
                        me.$element.trigger("addMarker", [coordinate]);
                    });
                }

                me.fitBounds();
                if ((me.options.initialZoom || null) !== null) {
                    var initialZoom = me.options.initialZoom;

                    var listener = google.maps.event.addListener(this._gmap, "idle", function () {
                        me._gmap.setZoom(initialZoom);
                        google.maps.event.removeListener(listener);
                    });
                }
            }
        };

        this.serialize = function () {
            var me = this;

            if (me._markers === null || me._markers.length == 0) {
                return null;
            } else if (me.options.singleMarker === true) {
                var marker = me._markers[0];
                
                var mult = me.options.multiplication || null;
                if ((mult || null) !== null) {
                    // multiplication is fix for mono bug
                    return { Multiplication: mult, Latitude: Math.round(marker.position.lat() * mult), Longitude: Math.round(marker.position.lng() * mult) };
                } else {
                    return { Latitude: marker.position.lat(), Longitude: marker.position.lng() };
                }

            } else {
               // console.log(me._markers);

                return me._markers;
            }
        };

        this.closeInfo = function () {
            if (this._currentInfoWindow !== null) {
                this._currentInfoWindow.close();
            }
        };

        this.addMarker = function (markerOptions) { 
            if (!markerOptions) { return; }

            var me = this;
            var gmap = this._gmap;
            // TODO: validate lat long?
            var coord = new google.maps.LatLng(markerOptions.latitude || markerOptions.Latitude, markerOptions.longitude || markerOptions.Longitude);
            var marker = new google.maps.Marker({
                map: gmap,
                position: coord,
                title: markerOptions.title || null,
                draggable: me.options.draggable
            });

            /* if ((me.options.draggable || null) === true) {
                google.maps.event.addListener(marker, 'dragend', function () {
                    geocodePosition(marker.getPosition());
                });
            } /**/

            me.closeInfo();
            if (me.options.singleMarker === true) {
                this.clearMarkers();
            }

            if ((markerOptions.title || null) !== null) {
                var $content = $("<div class='infoWindow' />");

                if ((markerOptions.url || null) !== null) {
                    $content.append($("<a />").attr("href", markerOptions.url).attr("title", markerOptions.title).text(markerOptions.title));
                } else {
                    $content.append(markerOptions.title);
                }

                var infowindow = new google.maps.InfoWindow({
                    content: $content[0].outerHTML
                });

                google.maps.event.addListener(marker, 'click', function () {
                    me.closeInfo();

                    infowindow.open(gmap, marker);
                    me._currentInfoWindow = infowindow;
                    // $li.trigger("highlight");
                });
            }

            me._markers.push(marker);

            // Register the bounds
            if (me._bounds == null) { me._bounds = new google.maps.LatLngBounds(); }
            me._bounds.extend(coord);

            return marker;
        };

        this.fitBounds = function (maxZoom) {
            var me = this;
            /* gemap.fitBounds(bounds);
            var listener = google.maps.event.addListener(map, "idle", function () {
                if (map.getZoom() > 16) map.setZoom(16);
                google.maps.event.removeListener(listener);
            }); /**/

            if (this._bounds != null) {
                if (me._markers.length == 0) {
                    me._gmap.setZoom(5);
                    me._gmap.setCenter(new google.maps.LatLng(52.3568374, 5.6278221));
                } else if (me._markers.length == 1 && me.options.minimumZoomOnFit !== null) {
                    me._gmap.setZoom(me.options.minimumZoomOnFit);
                } else {
                    var listener = google.maps.event.addListener(this._gmap, "idle", function () {
                        var zoom = me._gmap.getZoom();

                        if (zoom == 0) {
                            me._gmap.setZoom(5);
                            me._gmap.setCenter(new google.maps.LatLng(52.3568374, 5.6278221));
                        } else if ((maxZoom || null) !== null && zoom > maxZoom) {
                            me._gmap.setZoom(maxZoom || 5);
                        }

                        google.maps.event.removeListener(listener);
                    });

                    this._gmap.fitBounds(this._bounds);
                }

                this._gmap.setCenter(this._bounds.getCenter());
            }
        };

        this.clearMarkers = function () {
            for (var i = 0; i < this._markers.length; i++) {
                this._markers[i].setMap(null);
            }
            this._markers = [];
            this._bounds = null;
        };

        this.init($element, options);
    };

    $.fn.GoogleMap = function (options) {
        return this.each(function () {
            (new $.appGoogleMap($(this), options));
        });
    };
})(jQuery);
