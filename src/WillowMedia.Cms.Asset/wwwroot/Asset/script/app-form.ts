﻿/* Copyright 2014 - 2023 Willow Media

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. /**/

/// <reference path="interfaces.ts" />

// fix against import
declare var dayjs: any;

class DocumentTouch {}

class CmsAppFormOptions {
    waitForLoadDoneClass: string = "wait-for-load-done";
    // (base64) string representation of the form (should be HTML <form>...</form>
    view: string = "";
    View: string = "";
    // Data model to fill the form with
    data: any = null;
    validation: boolean = true;
    // Additional validation rules
    rules: object = {};
    // { close: function(event, ..., ...), onCmsUrlChange: function() }
    events: object = {};
    // Event before fill occurs function($form, data)
    beforeFill: Function = null;
    HtmlEncoded: boolean = false;
    // validate on unload
    trackChanges: boolean = false;
    autocomplete: boolean = false;
    onSerialize: Function = null;
    onFormLoaded: Function = null;
    // ""
    UID: string = undefined;
    // function ($form)
    onLoad: Function = undefined;
    // "", default "rec",
    dataProperty: string = undefined;
    // "",
    Enc: any = null;
    // Additional data to be handled before form init
    formData: Object = null;
    useOriginalDataSource: boolean = false;     // On setDataSource, the original object can be uses

    // Deprecated?
    Id: string = null;
    // template: {
    //      items: [
    //          { label: "...", data: "...", type: "text|checkbox|select|...", valuetype: "date|int|...", itemClass: "..." }
    //      ]
    // }
    template: CmsAppFormTemplate = null;
    Template: CmsAppFormTemplate = null;

    // inherit: bubbles to the child forms, optional use data(prop)
    // direct: data does not bubble to child form
    nestedDataBinding: "inherit"
}

class CmsAppFormTemplate {
    items: Array<CmsAppFormTemplateItem> = null;
}

class CmsAppFormTemplateItem {
    label: string = null;
    data: string = null;
    type: string = null; //"text|checkbox|select|...",
    valuetype: string = null; //"date|int|...", 
    itemClass: string = null; //"..."
}

class CmsFormSerializeOptions {
    data: object = null;
    honorSkip: boolean = true;
    skipClass: string = ".dontserialize";
    form: any = null;
    visible: boolean = false;                 // serialize only visible elements
    exclude: Function = null; //= me.options.exclude;    // function() filter with exclude
    elements: any = null;
    source: any = null;
    debug: boolean = false;
}

(function ($) {
    "use strict";

    $(window).on("resize", function (event) {
        $("form.editor").each(function () {
            var $form = $(this);
            var width = $form.width();

            if (width <= 400) {
                $form.attr("form-width", 400);
            } else if (width <= 600) {
                $form.attr("form-width", 600);
            } else if (width <= 800) {
                $form.attr("form-width", 800);
            } else {
                $form.attr("form-width", null);
            }
        });
    });

    let documentHandler = {
        manualBlur: function(focusElement?: HTMLElement) {
            $(".manualBlur")
                .filter(function () { return !!focusElement && this !== focusElement })
                .each(function() { $(this).triggerHandler("manualBlur", [focusElement]); })
        }
    };

    $(document).on("manualBlur", function(event, focusElement) {
        event.preventDefault();
        event.stopPropagation();
        documentHandler.manualBlur(focusElement);
    });

    $("body").on("click", function(event) {
        documentHandler.manualBlur(event.target);
    })

    Array.prototype.asOptions = function (options) {
        options = { ...{
                value: null,
                text: null,
                disabled: null,
                hide: null
            }, ...options };

        const $options = $("<div/>");

        const functionOrValue = function(property : any) {
            if (typeof property === 'function')
                return property.call(this);
            return property;
        }

        const functionOrProperty = function(property : any) {
            if (typeof property === 'function')
                return property.call(this);
            return this[property];
        }

        $.each(this || [], function (index, item) {
            if (!!!item) { return; }

            let $o = $("<option/>");
            let value = item;
            let text = item;

            if (!!options && !!options.value) {
                value = functionOrProperty.call(item, options.value);
            }
            if (!!options && !!options.text) {
                text = functionOrProperty.call(item, options.text);
            }

            $o.attr("value", value);
            $o.data("value", value);
            $o.text(text);
            $o.dataSource(item);

            let hidden = functionOrProperty.call(item, !!options && options.hide);
            let disabled = functionOrProperty.call(item, !!options && options.disabled);

            $o.toggleClass("hide", !!hidden);
            $o.toggleClass("disabled", !!disabled).prop("disabled", !!disabled);

            $options.append($o);
        });

        return $options.children();
    };

    var isDateInputSupported = function () {
        var elem = document.createElement('input');
        elem.setAttribute('type', 'date');
        elem.value = 'foo';
        return (elem.type == 'date' && elem.value != 'foo');
    };

    var isTouchDevice = function () {
        return true == ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch);
    };

    String.prototype.safeUrl = function () {
        var value = this || '';

        var CharIndicatorIgnore = 1;
        var CharIndicatorNone = 2;
        var CharIndicatorBreak = 3;
        var CharIndicatorCharacter = 4;
        var CharIndicatorHyphen = 5;
        var CharIndicatorAbbreviation = 6;

        // test if unicode.js is loaded
        if (String.prototype.removeDiacritics != undefined) {
            value = value.removeDiacritics();
        }

        var isLettersOrDigit = /[0-9a-z]/i;
        var isControl = /[\x00-\x31]/gi;
        var isWhitespace = /\s/g;
        var words = [];
        var buffer = "";
        var current = CharIndicatorNone;
        var previous = CharIndicatorNone;

        for (var i = 0; i < value.length; i++) {
            var c = value[i];

            if (c == '\u00AD') {
                current = CharIndicatorHyphen;
            } else if (c == '\u2028' || isControl.test(c)) {
                current = CharIndicatorBreak;
            }
            if (isWhitespace.test(c)) {
                current = CharIndicatorBreak;
            } else if (c == '.') {
                if (previous == CharIndicatorBreak)
                    current = CharIndicatorBreak;
                else
                    current = CharIndicatorAbbreviation;
            } else if (c == '\'' || c == '\"') {
                if (previous == CharIndicatorBreak)
                    current = CharIndicatorBreak;
                else
                    current = CharIndicatorHyphen;
            } else if (!isLettersOrDigit.test(c)) {
                current = CharIndicatorBreak;
            } else {
                current = CharIndicatorCharacter;
            }

            switch (current) {
                case CharIndicatorBreak:
                    if (buffer.length > 0) {
                        words.push(buffer);
                    }
                    buffer = "";
                    break;
                case CharIndicatorCharacter:
                    buffer += c.toLowerCase();
                    break;
            }

            previous = current;
        }

        if (buffer.length > 0) {
            words.push(buffer);
        }

        return words.join("-");
    };

    $(window).on('beforeunload', function (obj) {
        var hasChanges = false;

        $("form.trackChanges:data(formHandler):visible").each(function () {
            if ($(this).formHandler().isChanged() == true) {
                hasChanges = true;
                return false;
            }
        });

        if (hasChanges) {
            return "You have unmodified changes. Are you sure you want to discard them?";
        }
    });

    $.fn.preventScroll = function () {
        if (($.event.special.mousewheel || null) === null)
            return;

        // load mousewheel script manually!!
        // http://stackoverflow.com/questions/5802467/prevent-scrolling-of-parent-element
        $(this).bind('mousewheel', function (e, d) {
            // Uses jquery.mousewheel.js
            var t = $(this);
            if (d > 0 && t.scrollTop() === 0) {
                e.preventDefault();
            } else {
                if (d < 0 && (t.scrollTop() === t.get(0).scrollHeight - t.innerHeight())) {
                    e.preventDefault();
                }
            }
        });
    }

    $.fn.storeCurrentValue = function () {
        return this.each(function () {
            $(this).data("stored_value", $(this).elementValue());
            return this;
        });
    };

    $.fn.restoreCurrentValue = function () {
        return this.each(function () {
            $(this).setElementValue($(this).data("stored_value"));
            return this;
        });
    };

    $.fn.getElement = function (propertyName) {
        if (!!!propertyName) return null;
        var $container = $(this).eq(0);
        var isForm = $container.is("form");

        return $container
            .find(":data(prop)")
            .filter(function () { return $(this).data("prop") === propertyName })
            .filter(function () {
                // Only elements from this form (if its a form). Use parent to exclude the element itself
                return !$(this).is($container) &&
                    ($(this).parent().is($container) ||
                        !isForm ||
                        $(this).parent().closest("form").is($container));
            });
    };

    $.fn.containingForm = function () {
        var $target = $(this).eq(0);
        if (!$target.is("form")) {
            $target = $target.parents().filter(() => $(this).is("form")).eq(0);
        }
        return $target;
    };

    $.fn.dataSource = function (value?: any) {
        var key = $.formHandlerDefaultOptions.dataProperty || "rec";
        var $me = $(this).eq(0);

        if (arguments.length > 0) {
            $me.data(key, value = window.app.resolveValue(value));
        }
        return $me.data(key);
    }

    $.fn.setDataSource = function (value: any) {
        var key = $.formHandlerDefaultOptions.dataProperty || "rec";

        $(this).each(function () {
            $(this).data(key, value = window.app.resolveValue(value));
        });

        return this;
    }

    $.fn.setFormData = function (data) {
        var $me = $(this).eq(0);
        $me.data("formData", data);
        if ($me.hasEvent("setFormData")) {
            $me.trigger("setFormData", [data]);
        }
        return this;
    }

    $.fn.formData = function () {
        let formData;
        if (!!!this) return null;
        if (formData = $(this).data("formData")) return formData;

        var $form = $(this).eq(0);
        var formHandler = !!$form && $form.length > 0 && $form.formHandler();
        return !!formHandler && !!formHandler.options && !!formHandler.options.formData;
    }

    $.fn.checkbox = function () {
        return this.each(function () {
            var $me = $(this);
            var $checkbox = $("<span/>")
                .attr("id", $me.attr("id"))
                .attr("name", $me.attr("name"))
                .attr("placeholder", $me.attr("placeholder"))
                .attr("title", $me.attr("title"))
                .data("prop", $me.data("prop"))
                .addClass("checkbox")
                .addClass("formHandlerSerializable")
                .on("serialize", function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    // @ts-ignore
                    return $(this).dataSource() === true;
                })
                .on("set-value", function (event, value) {
                    event.preventDefault();
                    event.stopPropagation();
                    value = (value == true || value == 1 || value == "on") === true;
                    var $me = $(this).data("rec", value);
                    $me.toggleClass("checked", value);
                })
                .setClick(function (event) {
                    var value = !($(this).elementValue() === true);
                    $(this).trigger("set-value", [value]).trigger("change");
                });

            $(this).replaceWith($checkbox);
            return $checkbox;
        });
    }

    $.fn.hasChanges = function () {
        var hasChanges = false;

        this.each(function () {
            if ($(this).is("form.trackChanges:data(formHandler)")) {
                if ($(this).formHandler().isChanged() === true) {
                    hasChanges = true;
                    return false;
                }
            }
        });

        return hasChanges;
    };

    $.fn.weekControl = function (options) {
        return this.each(function () {
            var $week = $(this);
            var isInput = $week.is("input");
            var app = window.app;
            var formatting = window.app.formatting.dateFormatToDateTimePickerFormat;
            // @ts-ignore
            var litePickerLoaded = window && (window.Litepicker || null) !== null;
            var opt = $.extend({
                allowNull: true,
                numberOfColumns: 1,
                renderText: function (current) {
                    if (opt.allowNull && !current) {
                        return "";
                    }

                    var date = dayjs(current);
                    var week = date.isoWeek();
                    var monday = date.startOf('isoWeek');
                    var friday = dayjs(monday).add(4, 'days');

                    if (monday.month() != friday.month()) {
                        return "Week {0} - {1} t/m {2}".format(week, monday.format("DD MMM"), friday.format("DD MMM"));
                    } else {
                        return "Week {0} - {1} t/m {2}".format(week, monday.format("DD"), friday.format("DD MMM"));
                    }

                    return "Week {0} - ".format(week);
                },
                range: null
            }, options);

            $week.addClass("formHandlerSerializable dispose")
                .setClick(function () {
                    var $me = $(this);
                    var value = $(this).dataSource() || new Date();

                    if (litePickerLoaded) {
                    } else {
                        $me.datetimepicker({value: value, defaultDate: value});
                        $me.datetimepicker("show");
                    }
                })
                .on("dispose", function (event) {
                    event.preventDefault();
                    var $me = $(this);

                    if (litePickerLoaded) {
                        var picker = $me.data("picker");
                        if (picker) picker.destroy();
                        $me.data("picker", null);
                    } else {
                        $me.datetimepicker('destroy');
                    }
                })
                .on("serialize", function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    return $(this).dataSource();
                })
                .on("set-value", function (event, value) {
                    event.preventDefault();
                    event.stopPropagation();

                    var current = window.app.convert.toDate(value) || (opt.allowNull ? null : new Date());
                    var $me = $(this).setDataSource(current);
                    var textValue = opt.renderText(current);

                    if (isInput) {
                        $(this).val(textValue);
                    } else {
                        $(this).text(textValue);
                    }
                })
                .on("focus", function (event) {
                    event.preventDefault();
                    $(this).trigger("blur");
                })
                .on("next-week", function(event) {
                    event.preventDefault();

                    var dt = $week.elementValue();
                    if (!dt || !dayjs(dt).isValid()) { dt = Date(); }

                    dt = dayjs(dt).add(7, 'days').toDate();
                    $week.setElementValue(dt);
                    $week.trigger("change");
                })
                .on("prev-week", function(event) {
                    event.preventDefault();

                    var dt = $week.elementValue();
                    if (!dt || !dayjs(dt).isValid()) { dt = Date(); }

                    dt = dayjs(dt).add(-7, 'days').toDate();
                    $week.setElementValue(dt);
                    $week.trigger("change");
                });

            if (litePickerLoaded) {
                var handler = {
                    maxNumberOfColumns: (opt && opt.numberOfColumns) || 1,
                    resizeEvent(event) {
                        var columns = handler.getPreferredColumns();
                        var picker = $week.data("picker");
                        if (!picker) return;

                        // window.app.debug && console.log("resize", columns, picker.numberOfMonths, columns);

                        if (columns !== picker.numberOfMonths) {
                            picker.numberOfColumns = columns;
                            picker.numberOfMonths = columns;
                            picker.setOptions(picker);
                        }
                    },
                    getPreferredColumns() {
                        var columns = 3;
                        var width = $week.closest("form").width();

                        if (width <= 400) {
                            columns = 1;
                        } else if (width <= 600) {
                            columns = 1;
                        } else if (width <= 800) {
                            columns = 2;
                        } else if (width <= 1000) {
                            columns = 2;
                        } else if (width <= 1200) {
                            columns = 2;
                        } else {
                            columns = 3;
                        }

                        // window.app.debug && console.log("getPreferredColumns", $week.closest("form"), width, Math.min(this.maxNumberOfColumns, columns));

                        return Math.min(this.maxNumberOfColumns, columns);
                    }
                };

                var columns = handler.getPreferredColumns();
                var dropdownRange = {"minYear": null, "maxYear": null, "months":true, "years":true };
                if (opt.range) {
                    var maxYear = opt.range.max ? dayjs(opt.range.max).year() : dayjs().add(5, 'years').year();
                    var minYear = opt.range.min ? dayjs(opt.range.min).year() : 1900;

                    dropdownRange = $.extend(dropdownRange, {"maxYear": maxYear, "minYear": minYear });
                } else {
                    var thisYear = dayjs().year();
                    dropdownRange = $.extend(dropdownRange, {"minYear": thisYear - 100, "maxYear": thisYear + 5 });
                }

                // @ts-ignore
                var picker = new Litepicker({
                    element: $week[0],
                    singleMode: true,
                    singleDate: true,
                    // mobileFriendly: true,
                    autoApply: true,
                    autoRefresh: true,
                    dropdowns: dropdownRange,
                    numberOfColumns: columns,
                    numberOfMonths: columns,
                    orientation: "bottom left",
                    lang: app.lang || "nl",
                    inlineMode: false,
                    format: formatting(window.app.dateFormat), //app.dateFormat,
                    autoclose: true,
                    scrollToDate: true,
                    showWeekNumbers: true,
                    hideOnBodyClick: true,
                    disableWeekends: false,
                    setup: (picker) => {
                        picker.on('before:click', (target) => {
                            picker.disableUpdateEvent = null;

                            if ($week.isDisabled()) {
                                picker.preventClick = true;
                            }
                        });
                        picker.on('preselect', (date1, date2) => {
                            // some action
                            $week.setElementValue(date1 && date1.dateInstance);
                            $week.trigger("change");
                        });
                        picker.on('selected', (date1, date2) => {
                            // do not use select, but preselect to set the element value
                            return false;
                        });
                        picker.on('before:show', (el) => {
                            // some action
                            var value = $week.elementValue();
                            if (value) {
                                picker.gotoDate(value);
                                picker.setDate(value);
                            } else {
                                picker.clearSelection(); //setDate(null);
                            }
                        });
                        picker.on('render:day', (day, date) => {
                            $(day).toggleClass("in_past", date.dateInstance < Date.today()); // if day is in past
                        });

                        picker.on('show', () => {
                            // some action after show
                            $(picker.ui).setClick();
                        });
                    },
                });

                picker.updateInput = function() {};
                var pickerShouldShown = picker.shouldShown;
                picker.shouldShown = function(el) {
                    if ($week.isDisabled()) {
                        return false;
                    }
                    return pickerShouldShown.call(this, el);
                };

                $week.data("picker", picker)
                    .addClass("manualBlur")
                    .on("manualBlur", function(event) {
                        event.preventDefault();
                        event.stopPropagation();
                        picker.disableUpdateEvent = true;
                        picker.hide();
                    }).on("keyup", function(event) {
                    picker.disableUpdateEvent = true;
                    picker.hide();
                }).on("dispose", function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                    if (handler) $(window).off("resize", handler.resizeEvent);
                    var picker = $week.data("picker");
                    if (picker) { picker.destroy(); }
                    $week.data("picker", null);
                });

                $(window).on("resize", handler.resizeEvent);
            } else if ($.datetimepicker) { // datetimepicker fallback
                var settings = {
                    datePicker: true,
                    timepicker: false,
                    weeks: true,
                    lang: "nl",
                    formatDate: formatting(window.app.dateFormat),
                    format: formatting(window.app.dateFormat),
                    calformat: window.app.dateFormat,
                    dayOfWeekStart: 1,
                    closeOnDateSelect: true,
                    closeOnWithoutClick: true,
                    validateOnBlur: false,
                    scrollInput: false,
                    scrollMonth: false,
                    scrollTime: false,
                    inline: false,
                    defaultSelect: false,
                    value: $week.dataSource() || new Date(),
                    onShow: function (ct, $this) {
                        if ($week.isDisabled()) {
                            return false;
                        }
                    },
                    onSelectDate: function (ct, $element) {
                        $element.trigger("set-value", [ct]);
                        $element.trigger("change");
                    }
                };

                $week.datetimepicker(settings);
            }

            $week.closest("form").addClass("dispose");
        });
    }

    $.cloneObject = function(obj) {
        if (!obj) { return null; }

        try {
            // @ts-ignore
            if (window.structuredClone) {
                // @ts-ignore
                return window.structuredClone(obj);
            }
        } catch (error) {
            console.warn("Failed to clone object with structuredClone, fallback to JSON clone", obj);
        }

        return JSON.parse(JSON.stringify(obj));
    };

    $.formHandler = function (element: JQuery, options: CmsAppFormOptions) {
        this.options = {};
        this.$form = null;
        var me = this;

        $(element).data('formHandler', this);

        this.cloneObject = function(obj) {
            return $.cloneObject(obj);
        }

        this.init = function (element, options) {
            // Extend options with default options
            this.options = $.extend($.cloneObject($.formHandlerDefaultOptions) || {}, options);

            // Reference the element
            this.$form = $(element);

            this.$form.on("dispose", function (event) {
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();

                if (me.$form.is(".disposing")) {
                    window.app.debug && console.log("Already disposing", me.$form);
                    return false;
                }

                me.$form.addClass("disposing");

                // DatetimePicker
                if ($.datetimepicker) {
                    me.$form.find("input:data(datetimepicker)").datetimepicker('destroy');
                }

                // destroy validation
                if (($.validator || null) !== null) {
                    var $validator = me.$form.validate();
                    if ($validator) {
                        $validator.destroy();
                    }
                }

                // Only dispose objects that belong to this $form
                me.$form.find(".dispose")
                    .filter(function () {
                        return $(this).closest("form").is(me.$form);
                    })
                    .each(function() {
                        $(this).trigger("dispose");
                    });
            });

            if (this.options.trackChanges === true) {
                this.$form.addClass("trackChanges");
            }

            // Manipulate element here ...   
            var me = this;
        };

        this.getElement = function (propertyName) {
            if (!!!propertyName) return null;
            let $container = this.$form;

            return $container
                .find(":data(prop)")
                .filter(function () { return $(this).data("prop") === propertyName })
                .filter(function () {
                    // Only elements from this form (if its a form). Use parent to exclude the element itself
                    return !$(this).is($container) &&
                        ($(this).parent().is($container) ||
                            $(this).parent().closest("form").is($container));
                });
        };

        this.requestToken = function () {
            return $(me.$form).requestToken();
        };

        // Create an ajax request, with using the original data object, in combination with the serialized data from the form
        this.ajax = function (ajaxOptions) {
            var tmp;
            var me = this;
            var $form = me.$form;
            var app = window.app;

            // Get data object from option parameter, or get data from form
            var dataObj =
                ((ajaxOptions && ajaxOptions.data) ? ajaxOptions.data : null) ||
                me.serialize({ source: !!ajaxOptions && ajaxOptions.source });

            if (me.options && me.options.onBeforeSend) {
                me.options.onBeforeSend.call(this, $form, dataObj);
            }

            // Reset validation on send
            if (($.validator || null) !== null) {
                var $validator = $form.validate();
                if ($validator) {
                    $validator.resetForm();
                }
            }

            var uniqueDisabled = "disabled-" + app.uniqueId("form");
            var defaultOptions = new CmsFetchOptions();

            defaultOptions.onStart = function () {
                $form.addClass(uniqueDisabled);
            };
            defaultOptions.onFinish = function () {
                $form.removeClass(uniqueDisabled);
            };
            defaultOptions.onError = function (response) {
                me.handleErrorResponse.call(me, $form, response);
                response.handled = true;
            };
            defaultOptions.data = dataObj;

            // Merge options with default options
            var fetchOptions = app.extendWithProperties((ajaxOptions || {}), defaultOptions) as CmsFetchOptions;

            if (app.extend && app.extend.form && app.extend.form.onBeforeAjax) {
                app.extend.form.onBeforeAjax.call(me, $form, fetchOptions);
            }

            // Fix: call, bind, apply, or without?
            return app.fetch(fetchOptions);
        };

        this.handleErrorResponse = function ($form, response) {
            var app = window.app;
            var match = /^([a-z0-9_-]+)\[[0-9]+\]/i;

            if ($form.validate != undefined) {
                $form.validate().resetForm();
            }

            if ($form.validate != undefined && (response.Errors || null) !== null && response.Errors.length > 0) {
                var errorArray = {};
                var unknownElement = {};

                var cnt = 0;
                $.each(response.Errors, function (index, error) {
                    var elementName = match.test(error.Element) ?
                        match.exec(error.Element)[1] :
                        error.Element;

                    if ($form.find("[name=" + elementName + "]")
                        .filter(function () {
                            var e;
                            return ((e = ($("#" + elementName)[0])) || null) !== null && e.form !== undefined;
                        })
                        .length > 0) {
                        errorArray[elementName] = error.Error;
                    } else {
                        unknownElement[elementName] = error.Error;
                    }
                    cnt++;
                });

                if (cnt > 0) {
                    $form.validate().showErrors(errorArray);

                    // unknownElement
                    $.each(unknownElement, function (index, item) {
                        var $el = $form.find("#" + index).eq(0);
                        if ($el.length === 0) {
                            return;
                        }

                        $form.validate().showLabel($el[0], item);
                    });

                    if ($form.find(".ControlHasError:visible").length === 0) {
                        app.alertBox("Oops", response.Error || "An unknown errors has occured", "error");
                    }
                }
            } else {
                // title, msg, buttons
                app.alertBox("Oops", response.Error || "An unknown errors has occured", "error");
            }
        };

        this.serialize = function (serializeOptions: CmsFormSerializeOptions) {
            // Create options
            var baseOptions = new CmsFormSerializeOptions();
            baseOptions.exclude = me.options.exclude;
            serializeOptions = $.extend(baseOptions, serializeOptions);

            var handler = this;
            var app = window.app;
            var $form = serializeOptions.form || this.$form;
            var data = null;

            if ($form.hasEvent("serialize")) {
                // Let the form handle serialization manually
                data = $form.triggerHandler("serialize");
            } else {
                data = this.cloneObject(
                    serializeOptions.data || ((serializeOptions.visible || false) == false ? $form.data(this.options.dataProperty) : null) || {});

                var $elements = (serializeOptions.elements != null && serializeOptions.elements.length != 0)
                    ? serializeOptions.elements
                    : $form.find("form.editor,.formHandlerSerializable:data(prop),input:data(prop),select:data(prop),textarea:data(prop),password:data(prop)");

                $elements
                    .filter(function () {
                        var $me = $(this);

                        if (serializeOptions.exclude && serializeOptions.exclude.call(this) !== true) {
                            return false;
                        }
                        if ((serializeOptions.visible || false) !== true) {
                            return true;
                        }

                        return $me.is(":visible");
                    })
                    .each(function () {
                        var $me = $(this);
                        var isDate = $me.is("input[type=date],input[type=text].date,input[type=text].time");
                        var isTime = $me.is("input[type=text].time,:data(valuetype=time)");

                        // Only elements from this form
                        if ($form[0] !== $me.parent().closest("form")[0]) {
                            return;
                        }

                        !!serializeOptions && !!serializeOptions.debug && console.debug("serialize", {
                            formId: $form.attr("id"),
                            // closestFormId: $me.parent().closest("form").attr("id"),
                            // isRequestedForm: ($form[0] === $me.closest("form")[0]),
                            // prop: ($me.data("prop") || $me.attr("name") || null), 
                            path: "#" + $form.attr("id") + "/" + $me.parentsUntil($form).map(function () {
                                return $(this).prop("tagName");
                            }).toArray().reverse().join("/") + "/" + $me.prop("tagName") + "#" + ($me.data("prop") || $me.data("id")),
                            // source: !!serializeOptions && serializeOptions.source,
                            // handlerId: !!handler && handler.formId,
                            // handler: handler,
                            // $form: $form,
                            // $me: $me, 
                            // element: $me.attr("id"),
                            // serializerIgnore: $me.is(".serializer-ignore"),
                            // [serializeOptions.skipClass]: (!!serializeOptions.honorSkip && !!$me.is(serializeOptions.skipClass)),
                            // isForm: !!$me.is("form"),
                            // formProp: !!$me.is("form") && $me.data("prop"),
                            // readonly: $me.is("[readonly]"),
                            // data: data,
                        });

                        if ($me.is(".serializer-ignore")) {
                            return;
                        }

                        if ($me.is("[readonly]")) {
                            return;
                        }

                        // Skip items with dontserialize class
                        if (serializeOptions.honorSkip && ($me.is(serializeOptions.skipClass) || $me.parent().is(serializeOptions.skipClass))) {
                            return;
                        }

                        // Handle an embedded form. If the data.prop is set, then add the data to the property, else merge with existing data
                        if ($me.is("form")) {
                            var formProp = $me.data("prop");

                            // Merge or embed
                            if (!!formProp) {
                                // console.log("serialize", "serialize form as property")
                                data[formProp] = $me.formHandler().serialize();
                            } else if ($me.is(".formHandlerSerializable") && $me.hasEvent("set-value")) {
                                data = $.extend(data, $me.triggerHandler("serialize"));
                            } else {
                                // console.log("serialize", "serialize form merged")
                                data = $.extend(data, $me.formHandler().serialize());
                            }

                            return;
                        }

                        // Returns a bool or a string value
                        var value = $me.elementValue(); // handler.elementValue($me) || '';

                        // Add properties recursively
                        var prop = $me.data("prop") || $me.attr("name") || null;
                        if (prop !== null) {
                            prop = prop.split(".");

                            var dataRecursive = data;
                            // Replace empty values with null
                            if (value === null || value === "") {
                                value = null;
                            } else if (isDate === true && isTime == true) {
                                value = app.convert.toTime(value);
                            }

                            for (var i = 0; i < prop.length; i++) {
                                // If property does not exist
                                if (!dataRecursive.hasOwnProperty(prop[i])) {
                                    // Last recursion, so add the value
                                    if (i == prop.length - 1) {
                                        dataRecursive[prop[i]] = value;
                                    } else {
                                        // create new property
                                        dataRecursive[prop[i]] = {};
                                        dataRecursive = dataRecursive[prop[i]];
                                    }
                                } else {
                                    // Property or value exists. If end of recusion, then add the value
                                    if (i == prop.length - 1) {
                                        dataRecursive[prop[i]] = value;
                                    } else {
                                        dataRecursive = dataRecursive[prop[i]];
                                        if (typeof dataRecursive != "object") {
                                            throw "property is not an object";
                                        }
                                    }
                                }
                            }
                        }
                    });
            }

            if ((this.options.onSerialize || null) !== null) {
                this.options.onSerialize($form, data);
            }

            return data;
        };

        this.SetValueByProperty = function (data) {
            var $form = this.$form;

            if (!$.isPlainObject(data)) {
                return false;
            }

            $.each(data, function (prop, value) {
                $form.find("#" + String(prop)).SetValueByProperty(data);
            });
        };

        // Fill a form with the data object, using data('prop'). The hashCode will also be determined
        this.fill = function (data, fillOptions) {
            var $form = this.$form;
            var formOptions = this.options;
            var opt = $.extend({
                ignoreElements: null,
                ignoreNullValues: null
            }, fillOptions);

            //if (!opt.ignoreNullValues) {
            this.setDataSource($form, data);
            //}

            if (this.invalidateForm) {
                this.invalidateForm.call(this, data);
            }

            var form = $form[0];

            $form.find("form.editor,:data(prop)")
                .filter(function () {
                    // Only elements from this form. Use parent to exclude the element itself
                    return !$(this).is($form) &&
                        ($(this).parent().is($form) || $(this).parent().closest("form").is($form));
                })
                .each(function () {
                    var $element = $(this);

                    // Embedded forms might handle a property, or the same object
                    if ($element.is("form")) {
                        // console.log("fill", { form: $form.attr("id"), element: $element.data("prop"), $element: $element[0] });

                        var elementFormOptions = $element.formHandler().options;
                        var inheritDataBinding = false;
                        switch (elementFormOptions.nestedDataBinding || formOptions.nestedDataBinding || "inherit") {
                            case "direct":
                                break;
                            case "inherit":
                            default:
                                inheritDataBinding = true;
                                break;
                        }

                        var formProp = $element.data("prop");

                        /* console.log("fill $element is form", {
                            inheritDataBinding: inheritDataBinding,
                            formProp: formProp,
                            isFormHandlerSerializable: $element.is(".formHandlerSerializable") && $element.hasEvent("set-value"),
                            element: $element,
                            data: data,
                            nestedDataBinding: formOptions.nestedDataBinding,
                            formOptions: formOptions
                        } ); /**/

                        if (!!formProp) {
                            $element.formHandler().fill(!!data ? data[formProp] : null);
                        } else if ($element.is(".formHandlerSerializable") && $element.hasEvent("set-value")) {
                            $element.triggerHandler("set-value", [data]);
                        } else if (!!inheritDataBinding) {
                            $element.formHandler().fill(data);
                        }

                        return;
                    }

                    // console.log("fill", { form: $form.attr("id"), element: $element.data("prop"), $element: $element[0] });

                    if (!!opt.ignoreElements && opt.ignoreElements.filter(function () {
                        return $(this).is($element);
                    }).length > 0) {
                        return;
                    }

                    if (!!opt.ignoreNullValues && (data[$element.data("prop")] || null) === null) {
                        return;
                    }

                    $element.SetValueByProperty(data);
                });

            // Calculate the hashcode of the form content
            // $form.data("hashCode", JSON.stringify(this.serialize()).hashCode());
            $form.data("hashCode", this.getHashcode());

            return $form;
        };

        this.setDataSource = function ($element, data) {
            var key = this.options.dataProperty || $.formHandlerDefaultOptions.dataProperty || "rec";

            // Clone data
            if (this.options && this.options.useOriginalDataSource) {
                $element.data(key, data);
            } else {
                $element.data(key, this.cloneObject(data));
            }

            return $element;
        };

        this.slim = function () {
            this.$form.slim();
        };

        // Match stored hashcode with current hashcode
        this.isChanged = function () {
            if (me.options.onCheckChanged) {
                return me.options.onCheckChanged.call(me);
            } else {
                return this.getHashcode() != (this.$form.data("hashCode") || '');
            }
        };

        this.getHashcode = function () {
            return JSON.stringify(this.serialize()).hashCode();
        };

        this.setHashcode = function () {
            this.$form.data("hashCode", this.getHashcode());
        };

        this.addObeyDisable = function () {
            var $form = this.$form;

            $form.find(":data(prop)").each(function() {
                $(this).setupObeyDisabled();
            });

            // $form.find("input, input[type=text], input[type=password], input[type=radio], textarea, button")
            //     .each(function () {
            //         this.addEventListener("focus", function (event) {
            //             if ($(this).isDisabled()) {
            //                 event.stopPropagation();
            //                 $(this).blur();
            //                 return false;
            //             }
            //         });
            //     });
            //
            // $form.find("select")
            //     .each(function () {
            //         this.addEventListener("mousedown", function (event) {
            //             if ($(this).isDisabled()) {
            //                 event.preventDefault();
            //                 event.stopImmediatePropagation();
            //                 return false;
            //             }
            //         });
            //     });
            //
            // $form.find("input[type=checkbox]")
            //     .each(function () {
            //         $(this).on("click", function (event) {
            //             if ($(this).isDisabled()) {
            //                 event.preventDefault();
            //                 event.stopImmediatePropagation();
            //                 return false;
            //             }
            //         })
            //     });
            //
            // // Disable links/buttons on disabled
            // $form.find("a.button, a.fancyButton")
            //     .each(function () {
            //         this.addEventListener("onclick", function (event) {
            //             if ($(this).isDisabled()) {
            //                 event.preventDefault();
            //                 event.stopImmediatePropagation();
            //                 return false;
            //             }
            //         });
            //     });
        };

        this.setupValidation = function ($form) {
            // window.app.debug && console.debug("setupValidation called");

            var me = this;
            var app = window.app;

            // Add validation?
            if ($.validator) {
                $form = $form || me.$form;

                if ((app.messages || null) !== null) {
                    $.extend($.validator.messages, app.messages);
                }

                // // https://jqueryvalidation.org/required-method/
                // required: function( value, element, param ) {
                //     // Check if dependency is met
                //     if ( !this.depend( param, element ) ) {
                //         return "dependency-mismatch";
                //     }
                //     if ( element.nodeName.toLowerCase() === "select" ) {
                //
                //         // Could be an array for select-multiple or a string, both are fine this way
                //         var val = $( element ).val();
                //         return val && val.length > 0;
                //     }
                //     if ( this.checkable( element ) ) {
                //         return this.getLength( value, element ) > 0;
                //     }
                //     return value !== undefined && value !== null && value.length > 0;
                // },

                $.validator.addMethod(
                    "required",
                    function (value, element) {
                        var $element = $(element), valuetype = null;
                        if ((valuetype = $element.data("valuetype"))) {
                            var realValue = window.app.convert.to(valuetype, value);
                            if (typeof realValue === "number") return true;
                        }

                        if (typeof (value) === "string" && value.length === 0) return false;
                        if (!value) return false;
                        return true;
                    },
                    (app.messages ? app.messages.required : null) || "Field is required"
                );

                $.validator.addMethod(
                    "regexMatch",
                    function (value, element) {
                        var $element = $(element);
                        var rx = $element.data("regexmatch");

                        if (!value || value.length === 0) {
                            return true;
                        }
                        if (!rx || rx.length === 0) {
                            return true;
                        }

                        return new RegExp(rx, "g").test(value);
                    },
                    (app.messages ? app.messages.regexMatch : null) || "Invalid match"
                );

                $.validator.addMethod(
                    "requiresItems",
                    function (value, element) {
                        var $element = $(element);

                        if ($element.data("validation-proxy")) {
                            value = $element.closest("form").find($element.data("validation-proxy")).elementValue();
                        } else {
                            var value = $(element).elementValue();
                        }

                        if (!value || ($.isArray(value) && value.length === 0)) {
                            return false;
                        }

                        return true;
                    },
                    $.validator.messages.required || "Required"
                );

                $.validator.addMethod(
                    "email",
                    function (value, element) {
                        return (value && value.length > 0)
                            ? app.validators.email.test(value)
                            : true;
                    },
                    (app.messages ? app.messages.email : null) || "Invalid email address"
                );

                $.validator.addMethod(
                    "postcodeNL",
                    function (value, element) {
                        return (value && value.length > 0)
                            ? $.cmsValidators.postcodeNL(value)
                            : true;
                    },
                    (app.messages ? app.messages.postcodeNl : null) || "De opgegeven postcode is geen Nederlandse postcode"
                );

                $.validator.addMethod(
                    "phone",
                    function (value, element) {
                        return (value && value.length > 0)
                            ? $.cmsValidators.phone(value)
                            : true;
                    },
                    (app.messages ? app.messages.phone : null) || "Ongeldig telefoonnummer"
                );

                $.validator.addMethod(
                    "integer",
                    function (value, element) {
                        return (value && value.length > 0)
                            ? $.cmsValidators.integer(value.slim())
                            : true;
                    },
                    (app.messages ? app.messages.integer : null) || "Invalid integer"
                );

                $.validator.addMethod(
                    "Decimal",
                    function (value, element) {
                        $(element).data("msg", null);

                        if (!value) return true;                        // No value
                        if (typeof (value) === "number") return true;    // Value is number

                        var normalized = value ? ("" + value).slim() : null;

                        // TODO: update element with normalized value?
                        if (normalized && normalized.length > 0) {
                            var validMsg = null;
                            var valid = $.cmsValidators.decimal(normalized, function (msg) {
                                validMsg = msg;
                            });

                            if (!valid && validMsg) {
                                $(element).data("msg", validMsg);
                            }

                            return valid === true;
                        }
                        return true;
                    },
                    (app.messages ? app.messages.Decimal : null) || "Invalid decimal"
                );

                $.validator.addMethod(
                    "color",
                    function (value, element) {
                        if (value != null && value != '') {
                            return /^\#[0-9a-f]{3,6}$/i.test(value);
                        }
                        return true;
                    },
                    (app.messages ? app.messages.color : null) || "Ongeldige kleur"
                );

                $.validator.addMethod(
                    "time",
                    function (value, element) {
                        var $me = $(element);
                        var settings = $me.data("datetimepicker");
                        var calFormat = $me.data("calformat") || app.timeFormat;

                        // http://stackoverflow.com/questions/1765302/dependency-mismatch-with-jquery-validate-plugin/4901888#4901888
                        if ((settings || null) !== null) {
                            return (this.optional(element) != false) || $.cmsValidators.date(value, settings.calformat);
                        } else if ((calFormat || null) !== null) {
                            return (this.optional(element) != false) || $.cmsValidators.date(value, calFormat);
                        } else {
                            throw("FIX");
                            // return (this.optional(element) != false) || !/Invalid|NaN/.test(new Date(value));
                        }
                    },
                    (app.messages ? app.messages.time : null) || "Invalid time"
                );

                var isValidDate = function (value) {
                    if (value == null) return false;
                    if (!(value instanceof Date)) return false;
                    return !isNaN(value.getTime());
                }

                $.validator.addMethod(
                    "date",
                    function (value, element) {
                        if (!value) {
                            return true;
                        }

                        // Check for our DateTimePicker
                        var $de = $(element);
                        var settings = $de.data("datetimepicker");
                        var calFormat = $de.data("calformat") as string || app.dateFormat;

                        if (typeof (value) === "string" && $de.is(".formHandlerSerializable")) {
                            value = $de.elementValue();
                        }

                        if (value instanceof Date) {
                            return isValidDate(value);
                        } else if ((settings || null) !== null) {
                            // If language is Dutch and date given is dd-mm, then append the year
                            if (settings.lang == 'nl') {
                                var rx = /^(\d{1,2})-(\d{1,2})$/i;
                                var match = rx.exec(value.slim());
                                if (match != null) {
                                    // Create year and optionally increment the year
                                    var date = new Date(new Date().getFullYear(), parseInt(match[2]) - 1, parseInt(match[1]));

                                    if (date < new Date()) {
                                        date.setFullYear(new Date().getFullYear() + 1);
                                    }

                                    value = "{0}-{1}-{2}".format(date.getDate(), date.getMonth() + 1, date.getFullYear());
                                    $de.val(value);
                                }
                            }

                            // var format = settings.format || "d-m-Y";
                            // var rx = new RegExp(format.replace("d", "(\\d{1,2})").replace("m", "(\\d{1,2})").replace("Y", "(\\d{4})"));
                            // return this.optional(element) || rx.test(value);

                            return (this.optional(element) != false) || $.cmsValidators.date(value, settings.calformat);
                        } else if ((calFormat || null) !== null) {
                            return (this.optional(element) != false) || $.cmsValidators.date(value, calFormat);
                        } else {
                            throw("FIX");
                            // return (this.optional(element) != false) || !/Invalid|NaN/.test(new Date(value));
                        }
                    },
                    (app.messages ? app.messages.date : null) || "Invalid date"
                    //$.validator.messages.date
                );

                $.validator.addMethod(
                    "element-validate-value",
                    function (value, element) {
                        var $me = $(element);
                        return $me.triggerHandler("validate-value");
                    },
                    function(invalid, element) {
                        let $element = $(element);
                        return $element.triggerHandler("validate-value-errormsg") || "Invalid value";
                    }
                );

                // Setup rules based on the validation datatype
                var rules = me.options.rules || undefined;
                $form.find(":data(validation)").each(function () {
                    // window.app.debug && console.log("validation", this);

                    rules = rules || [];

                    var validation = $(this).data("validation");
                    var rule = {};
                    rule[validation] = true;
                    rule["required"] = $(this).hasClass("required");

                    rules[$(this).data("prop")] = rule;
                });

                $form.find(":data(prop)")
                    .filter(function () { return $(this).hasEvent("validate-value"); })
                    .each(function (index, element) {
                        let $element = $(element);
                        rules = rules || [];

                        var rule = {};
                        rule["element-validate-value"] = true;
                        rule["required"] = $(this).hasClass("required");
                        rules[$element.data("prop")] = rule;
                    });

                var validationOptions = {
                    errorClass: "ControlHasError",
                    onfocusout: false,
                    onkeyup: false,
                    onchange: true,
                    onsubmit: false,
                    rules: rules,
                    ignore: ":hidden,.no-validation",
                    ignoreTitle: true,
                    highlight: function (element, errorClass) {
                        var $element = $(element);
                        if ($element.is(":data(errorhighlight)")) {
                            var highlight = $element.data("errorhighlight");
                            if (typeof highlight === "function") {
                                highlight.call($element, errorClass);
                            } else if (typeof (highlight) == "string") {
                                $element.closest("form").find(highlight).addClass(errorClass);
                            } else {
                                $(highlight).addClass(errorClass);
                            }
                        } else {
                            $(element).addClass(errorClass);
                        }
                    },
                    unhighlight: function (element, errorClass) {
                        var $element = $(element);
                        if ($element.is(":data(errorhighlight)")) {
                            var highlight = $element.data("errorhighlight");
                            if (typeof (highlight) == "string") {
                                $element.closest("form").find($element.data("errorhighlight")).removeClass(errorClass);
                            } else {
                                $(highlight).removeClass(errorClass);
                            }
                        } else {
                            $(element).removeClass(errorClass);
                        }
                    }
                };

                $form.validate(validationOptions);

                // Alter the elementValue function
                var getElementValue = function (element) {
                    return $(element).eq(0).elementValue();
                };

                var validator = $form.data("validator");
                $.extend(validator, {
                    elementValue: getElementValue,
                    getLength: function (value, element) {
                        if (!value) return 0;
                        if (typeof (value) === "string") return value.length;
                        return ("" + value).length;
                    }
                });

                validator.settings.errorPlacement = function (error, element) {
                    var $element = $(element);

                    if ($element.is(":data(errorloc)")) {
                        var errorloc = $element.data("errorloc");

                        if (typeof errorloc === "function") {
                            errorloc.call($element, error);
                        } else if (typeof (errorloc) == "string") {
                            $element.closest("form").find(errorloc).html(error).show();
                        } else {
                            $(errorloc).html(error).show();
                        }
                    } else {
                        error.insertAfter(element);
                    }
                };

                return true;
            }

            return false;
        };

        this.responseErrorHandler = function (response) {
            this.options.debug && console.log(response);

            if (($.validator || null) === null) {
                alert("Validator not loaded");
            } else if ((response.Errors || null) !== null && response.Errors.length > 0) {
                var $validator = this.$form.validate();
                $validator.resetForm();

                var errors = {};
                $.each(response.Errors, function () {
                    errors[this.Element] = this.Error;
                });
                /* Show errors on the form */
                $validator.showErrors(errors);
            } else {
                alert(response.Error || "Unknown error");
            }
        };

        // Initialize the element
        this.init(element, options);
    };

    $.fn.formHandler = function (options) { //Using only one method off of $.fn
        // Return existing object
        if ($(this).length === 1) {
            var current = $(this).data("formHandler");
            if (current) {
                return current;
            } else {
                // Create formHandler for the first element selected
                return new $.formHandler($(this[0]), options);
            }
        }
        throw("Too many or less elements")
    };

    $.debugEvent = function(event) {
        console.log("event", $(this).attr("id"), event.type, event, this);
    }

    $.formHandlerDefaultOptions = {
        dataProperty: 'rec',
        elementSerializableClass: "formHandlerSerializable",
        debug: false,
        onBeforeSend: null,                 // onBeforeSend($form, dataObj)
        onCheckChanged: null,               // override isChanged check
        exclude: null                       // function() filter to exclude elements on serialize
    };

    $._createFormWithUidDefaults = {
        scriptUID: "{__UID__}",
        _uniId: 0,
        getUniqueId: function (prefix) {
            return (prefix || '') + (this._uniId += 1);
        }
    };

    $.calformatToRegex = function (fmt) {
        // date format to regex
        var rxexpr = '^';
        var hasDay, hasMonth, hasYear, hasMinute, hasHour, hasSecond;
        for (var i = 0; i < fmt.length; i++) {
            var c = fmt[i];
            switch (c) {
                case 'D':
                    if (!hasDay) {
                        hasDay = true;
                        rxexpr += '[0-9]{1,2}'
                    }
                    break;
                case 'M':
                    if (!hasMonth) {
                        hasMonth = true;
                        rxexpr += '[0-9]{1,2}'
                    }
                    break;
                case 'Y':
                    if (!hasYear) {
                        hasYear = true;
                        rxexpr += '[0-9]{1,4}'
                    }
                    break;
                case 'H':
                    if (!hasHour) {
                        hasHour = true;
                        rxexpr += '[0-9]{1,2}'
                    }
                    break;
                case 'm':
                    if (!hasMinute) {
                        hasMinute = true;
                        rxexpr += '[0-9]{1,2}'
                    }
                    break;
                default:
                    rxexpr += c;
                    break;
            }
        }
        rxexpr += '$';

        return new RegExp(rxexpr, 'i');
    };

    // Validators
    $.cmsValidators = {
        hasValueError: function ($element, value, valuetype) {
            var validMsg = null;
            switch ((valuetype || $element.detectValuetype() || "").toLowerCase()) {
                case "float":
                case "double":
                case "valuta":
                case "currency":
                case "percentage":
                case "number":
                case "decimal":
                    if (!this.decimal(value, function (msg) {
                        validMsg = msg;
                    })) {
                        return validMsg || window.app.messages.Decimal || "Invalid decimal";
                    }
                    break;
                case "integer":
                    if (!this.integer(value, function (msg) {
                        validMsg = msg;
                    })) {
                        return validMsg || window.app.messages.integer || "Invalid integer";
                    }
                    break;
            }

            return null;
        },
        date: function (strVal, fmt) {
            fmt = fmt || "DD-MM-YYYY";

            return $.calformatToRegex(fmt).test(strVal) && dayjs(strVal, fmt).isValid();
        },
        time: function (strVal, fmt) {
            fmt = fmt || "HH:mm";

            return $.calformatToRegex(fmt).test(strVal) && dayjs(strVal, fmt).isValid();
        },
        datetime: function (strVal, fmt) {
            fmt = fmt || "DD-MM-YYYY HH:mm";

            return $.calformatToRegex(fmt).test(strVal) && dayjs(strVal, fmt).isValid();
        },
        postcodeNL: function (value) {
            return (/^[0-9]{4}\s{0,1}[a-z]{2}$/i).test(value.slim());
        },
        phone: function (value) {
            return (/^[0-9+]{0,4}[ \-]{0,1}[0-9]{1,4}[ \-]{0,1}[0-9]{6,8}$/i).test(value.replaceAll(" ", ""));
        },
        integer: function (value) {
            return (/^-?\d+$/.test(value + ''));
        },
        decimal: function (value, onInvalid) {
            if (!value || value.length === 0) {
                return false; // NaN
            }

            var app = window.app;

            // Validate
            // Default decimal point comes from settings, but could be set to eg. "," in opts:
            // !$.cmsValidators.decimal("") ? "OK" : "Failed"
            // !$.cmsValidators.decimal(" ") ? "OK" : "Failed"
            // $.cmsValidators.decimal("1") ? "OK" : "Failed"
            // $.cmsValidators.decimal("1000") ? "OK" : "Failed"
            // isNaN($.cmsValidators.decimal("a")) ? "OK" : "Failed"
            // $.cmsValidators.decimal("1.000") ? "OK" : "Failed"
            // $.cmsValidators.decimal("1.000.000") ? "OK" : "Failed"
            // !$.cmsValidators.decimal("10.00") ? "OK" : "Failed"
            // $.cmsValidators.decimal("1.000.000,0") ? "OK" : "Failed"
            // $.cmsValidators.decimal("1.000.000,01") ? "OK" : "Failed"
            // $.cmsValidators.decimal("0,001") ? "OK" : "Failed"
            // !$.cmsValidators.decimal("0,001a") ? "OK" : "Failed"
            // $.cmsValidators.decimal("-0,001") ? "OK" : "Failed"
            // !$.cmsValidators.decimal("1-") ? "OK" : "Failed"
            // !$.cmsValidators.decimal("-") ? "OK" : "Failed"

            var decimal = app.currencyFormat.d || ",";
            var group = app.currencyFormat.g || ".";
            var groupSize = app.currencyFormat.gs || 3;
            var rxInt = /[0-9]/;
            var groupCnt = 0;
            var hasDecimal = false;
            var hasGroup = false;
            var hasNegative = false;
            var nr = 0;

            for (var c = value.length - 1; c >= 0; c--) {
                var char = value[c];

                if (hasNegative) {
                    if (onInvalid) onInvalid.call(this, (app.messages.DecimalBeforeNegative || "Character before negative sign"));
                    return false; //NaN;
                } else if (char === decimal) {
                    if (hasDecimal || hasGroup) {
                        if (onInvalid) onInvalid.call(this, (app.messages.DecimalDecimalPlacement || "Invalid decimal placement"));
                        return false; //NaN;
                    }
                    hasDecimal = true;
                    groupCnt = 0;
                } else if (char === group) {
                    if (groupSize <= 0 || groupCnt !== groupSize) {
                        if (onInvalid) onInvalid.call(this, (app.messages.DecimalGrouping || "Invalid grouping in number"));
                        return false; // NaN;
                    }
                    hasGroup = true;
                    groupCnt = 0;
                } else if (char === '-') {
                    hasNegative = true;
                } else if (!rxInt.test(char)) {
                    if (onInvalid) onInvalid.call(this, (app.messages.DecimalUnexpected || "Unexpected character"));
                    return false; //NaN;
                } else {
                    nr++;
                    groupCnt++;
                }
            }

            return nr !== 0;
        }
    };

    // Add create function to form object
    $.dynamicForm = function (obj: CmsAppFormOptions) {
        return $.extend(obj, {
            create: function (additional) {
                // Always return a new object
                return $.createFormWithUid(
                    $.extend(
                        new CmsAppFormOptions(),
                        this,
                        additional));
            }
        });
    };

    // http://stackoverflow.com/questions/3774622/how-to-base64-encode-inside-of-javascript
    $.base64 = {
        _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
        encode: function (e) {
            var c1, c2;
            var t = "";
            var n, r, i, s, o, u, a;
            var f = 0;
            e = this._utf8_encode(e);
            while (f < e.length) {
                n = e.charCodeAt(f++);
                r = e.charCodeAt(f++);
                i = e.charCodeAt(f++);
                s = n >> 2;
                o = (n & 3) << 4 | r >> 4;
                u = (r & 15) << 2 | i >> 6;
                a = i & 63;
                if (isNaN(r)) {
                    u = a = 64
                } else if (isNaN(i)) {
                    a = 64
                }
                t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
            }
            return t
        }, decode: function (e) {
            var c1, c2;
            var t = "";
            var n, r, i;
            var s, o, u, a;
            var f = 0;
            e = e.replace(/[^A-Za-z0-9\+\/\=]/g, "");
            while (f < e.length) {
                s = this._keyStr.indexOf(e.charAt(f++));
                o = this._keyStr.indexOf(e.charAt(f++));
                u = this._keyStr.indexOf(e.charAt(f++));
                a = this._keyStr.indexOf(e.charAt(f++));
                n = s << 2 | o >> 4;
                r = (o & 15) << 4 | u >> 2;
                i = (u & 3) << 6 | a;
                t = t + String.fromCharCode(n);
                if (u != 64) {
                    t = t + String.fromCharCode(r)
                }
                if (a != 64) {
                    t = t + String.fromCharCode(i)
                }
            }
            t = this._utf8_decode(t);
            return t
        }, _utf8_encode: function (e) {
            var c1, c2;
            e = e.replace(/\r\n/g, "\n");
            var t = "";
            for (var n = 0; n < e.length; n++) {
                var r = e.charCodeAt(n);
                if (r < 128) {
                    t += String.fromCharCode(r)
                } else if (r > 127 && r < 2048) {
                    t += String.fromCharCode(r >> 6 | 192);
                    t += String.fromCharCode(r & 63 | 128)
                } else {
                    t += String.fromCharCode(r >> 12 | 224);
                    t += String.fromCharCode(r >> 6 & 63 | 128);
                    t += String.fromCharCode(r & 63 | 128)
                }
            }
            return t;
        }, _utf8_decode: function (e) {
            var c1, c2, c3;
            var t = "";
            var n = 0;
            var r = c1 = c2 = 0;
            while (n < e.length) {
                r = e.charCodeAt(n);
                if (r < 128) {
                    t += String.fromCharCode(r);
                    n++
                } else if (r > 191 && r < 224) {
                    c2 = e.charCodeAt(n + 1);
                    t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                    n += 2
                } else {
                    c2 = e.charCodeAt(n + 1);
                    c3 = e.charCodeAt(n + 2);
                    t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                    n += 3
                }
            }
            return t;
        }
    };

    // Options:
    // {
    //      view: "", 
    //      View: "",                           // (base64) string representation of the form (should be HTML <form>...</form>
    //      data: {},                           // Data model to fill the form with
    //      validation: true||false,            // Add default 
    //      rules: {},                          // Additional validation rules
    //      events: {},                         // { close: function(event, ..., ...), onCmsUrlChange: function() }
    //      beforeFill: function($form, data)   // Event before fill occurs
    // }
    $.createFormWithUid = function (options: CmsAppFormOptions) {
        var app = window.app;

        // Create a unique id with frm as prefix
        var uid = $._createFormWithUidDefaults.getUniqueId('frm');
        var $form = null;

        // TODO: set local from app.locale or something like that
        // @ts-ignore
        if (window.Lightpick) {
            // init something?
        } else if ($.datetimepicker) {
            $.datetimepicker.setLocale(app.lang || "nl");
        }

        if ((options.View || options.view || null) !== null) {
            var view = (options.View || options.view || null);

            switch ((options.Enc || '').toUpperCase()) {
                case "BASE64":
                    view = $.base64.decode(view);
                    break;
            }

            if ((view || null) === null) {
                throw("Missing view");
            }

            var htmlStr = view.replace(new RegExp($._createFormWithUidDefaults.scriptUID, "g"), uid);

            // Create the HTML element and set the UID to the form
            if (options.HtmlEncoded) {
                $form = $($("<div></div>").html(htmlStr).text());
            } else {
                $form = $(htmlStr);
            }

            $form
                .attr("id", uid)
                .attr("formid", options.Id)
                .cleanWhitespace();

            // If no autocomplete attribute is available
            if (($form.attr("autocomplete") || null) === null) {
                $form.attr("autocomplete", (options.autocomplete || false) === false ? "off" : "on");
            }

        } else if ((options.Template || options.template || null) !== null) {
            // template: {
            //      items: [
            //          { label: "...", data: "...", type: "text|checkbox|select|...", valuetype: "date|int|...", itemClass: "..." }
            //      ]
            // }
            var template = options.Template || options.template;
            var $ul = null;
            $form = $("<form/>").attr("id", uid);

            $.each(template.items, function (index, item) {
                var $li = $("<li/>");
                if ((item.itemClass || null) !== null)
                    $li.addClass(item.itemClass);

                var $input = null;
                var dataField = item.data;

                $li.append($("<label/>").text(item.label || '')).attr("for", dataField);

                switch (this.type || '') {
                    case "checkbox":
                        $input = $("<input type='checkbox' />")
                            .attr("id", dataField)
                            .attr("name", dataField)
                            .data("prop", dataField);
                        break;
                    case "text":
                        $input = $("<input type='text' />")
                            .attr("id", dataField)
                            .attr("name", dataField)
                            .data("prop", dataField);
                        break;
                    default:
                        $input = $("<span/>").data("prop", dataField);
                        break;
                }

                if ($input != null) {
                    if ((item.valuetype || null) !== null)
                        $input.data("valuetype", item.valuetype);

                    $li.append($("<span/>").append($input));
                }

                if ($ul == null) {
                    $ul = $("<ul class='overview' />");
                    $form.append($ul);
                }
                $ul.append($li);
            });
        }

        // Init the form handler
        var handler = $form.formHandler(options);

        if (typeof (options.data || null) === "function") {
            options.data = options.data.call($form);
        }

        // Call optional beforeFill function
        if ((options.beforeFill || null) !== null) {
            options.data = options.data || {};
            options.beforeFill($form, options.data);
        }

        // Fill form
        if ((options.data || null) !== null) {
            //handler.fill(options.data);
            handler.setDataSource($form, options.data);
        }

        // Add focus events
        // $form.on("manualBlur", function (event, $focussedElement) {
        //     $form
        //         .find(".manualBlur")
        //         .filter(function (index, item) {
        //             var $me = $(item);
        //
        //             if ($focussedElement === null) {
        //                 return true;
        //             } else if ($me === $focussedElement || $me.parents().hasElement($focussedElement)) {
        //                 return false;
        //             } else {
        //                 return true;
        //             }
        //         })
        //         .each(function () { $(this).triggerHandler("manualBlur", [$focussedElement]); })
        // });

        // function outline(event) {
        //     if (!!event && !!event.target) {
        //         // Call manualBlur with focussed element
        //         $form.triggerHandler("manualBlur", [$(event.path).filter("[id]").eq(0)]);
        //     }
        // };
        //
        // if (navigator.userAgent.indexOf('Firefox') >= 0) { // Firefox
        //     $form[0].addEventListener('focus', outline, true);
        // } else if ($form[0].addEventListener) {  // Opera, Safari/Chrome
        //     $form[0].addEventListener('DOMFocusIn', outline, false)
        // } else {  // IE
        //     $form[0].onfocusin = outline
        // }

        $form
            .on("submit", function (event) {
                event.preventDefault();
                event.stopPropagation();
                return false;
            })
            .on("load", function () {
                var $form = $(this);

                // Call optional onload
                options && options.onLoad && options.onLoad($form);

                $form.find(":data(prop)").setupObeyDisabled().setupCmsElement($form);

                // Late binding of data
                handler.fill($form.dataSource());

                $form.removeClass(options.waitForLoadDoneClass); // wait-for-load-done
                $form.trigger("change");

                // Add default validation
                if (options.validation === true || (options.rules || null) !== null) {
                    //window.app.debug && console.debug("setupValidation");
                    handler.setupValidation($form);
                }

                handler.setHashcode();

                if (options.onFormLoaded) {
                    options.onFormLoaded.call(handler, $form);
                }

                if (app.extend && app.extend.form && app.extend.form.onFormLoaded) {
                    app.extend.form.onFormLoaded.call(handler, $form, options);
                }

                $(window).trigger("resize");
            });

        // Add events to form
        if ((options.events || null) != null) {
            $.each(options.events, function (event, func) {
                $form.on(event, func);
            });
        }

        // Set formdata
        var formData = app.resolveValue(options.formData || null);
        $form.setFormData(formData);

        // Add script to trigger onload
        var loadScript = "<script type='text/javascript'>"+
            "if (window.jQuery) { $(function() { $('form#{0}').trigger('load'); });} else {var e = document.querySelector('form#{0}'); e && e.dispatchEvent(new Event('load', {bubbles: true }));}" +
            "</script>";
        $form.append(loadScript.format(uid));

        if ($form.hasEvent("invalidate")) {
            $form.triggerHandler("invalidate");
        }

        // $form.css("display", "none");
        // wait-for-load-done
        $form.addClass(options.waitForLoadDoneClass);

        return $form;
    };

    $.fn.setupObeyDisabled = function() {
        return $.each(this, function() {
            var $element = $(this);

            if ($element.is("input, input[type=text], input[type=password], input[type=radio], textarea, button")) {
                $element[0].addEventListener("focus", function (event) {
                    if ($(this).isDisabled()) {
                        event.stopPropagation();
                        $(this).blur();
                        return false;
                    }
                });
            } else if ($element.is("input[type=checkbox]")) {
                $element.on("click", function (event) {
                    if ($(this).isDisabled()) {
                        event.preventDefault();
                        event.stopImmediatePropagation();
                        return false;
                    }
                })
            } else if ($element.is("a.button, a.fancyButton")) {
                $element[0].addEventListener("onclick", function (event) {
                    if ($(this).isDisabled()) {
                        event.preventDefault();
                        event.stopImmediatePropagation();
                        return false;
                    }
                });
            } else if ($element.is("select")) {
                $element[0].addEventListener("onclick", function (event) {
                    if ($(this).isDisabled()) {
                        event.preventDefault();
                        event.stopImmediatePropagation();
                        return false;
                    }
                });
                $element[0].addEventListener("focus", function (event) {
                    if ($(this).isDisabled()) {
                        event.preventDefault();
                        event.stopPropagation();
                        event.stopImmediatePropagation();
                        $(this).blur();
                        return false;
                    }
                });
                $element[0].addEventListener("mousedown", function (event) {
                    if ($(this).isDisabled()) {
                        event.preventDefault();
                        event.stopPropagation();
                        event.stopImmediatePropagation();
                        $(this).blur();
                        return false;
                    }
                });
            }
        });
    };

    $.fn.setupCmsElement = function($form: JQuery) {
        var handler = $form && $form.formHandler();

        var onCmsUrlChange = function () {
            $(this).val(($(this).val() as string).safeUrl());
        };

        return $(this).each(function() {
            var $element = $(this);

            $element.setupObeyDisabled();

            $element.filter(function() { return $(this).is("input:data(valuetype)");  })
                .each(function () {
                    var $me = $(this), valuetype;

                    if ($me.hasClass("formHandlerSerializable") && $me.hasEvent("set-value"))
                        return false;

                    switch (valuetype = (($me.data("valuetype") || '').toLocaleLowerCase())) {
                        case "cmsurl":
                            $me.on("change", onCmsUrlChange);
                            break;
                        case "valuta":
                        case "currency":
                        case "percentage":
                            if (valuetype === "valuta") valuetype = "currency";
                            $me.addClass("formHandlerSerializable")
                                .on("serialize", function (event) {
                                    event.preventDefault();
                                    event.stopPropagation();
                                    event.stopImmediatePropagation();
                                    return $me.dataSource();
                                })
                                .on("set-value", function (event, value) {
                                    event.preventDefault();
                                    event.stopPropagation();
                                    event.stopImmediatePropagation();

                                    $(this).dataSource(value);
                                    $me.val(window.app.formatting.format(value, valuetype));
                                })
                                .setupValueOnFocus();
                            break
                        default:
                            $me.setupValueOnFocus();
                            break;
                    }
                });

            // Init the date controls
            // var requiresDispose = false;
            $element.filter(function() { return $(this).is("input[type=text].date, input[type=text].time"); })
                .each(function() {
                    var $dt = $(this);
                    if ($dt.is(".manualinit")) {
                        return;
                    }

                    $dt.initDatetimePicker($form);
                });

            var $colorpickers = $element
                .filter(function() { return $(this).is("input[type=color]"); })
                .each(function() {
                    $(this).on("click", function (event) {
                        if ($(this).isDisabled()) {
                            event.preventDefault();
                            event.stopPropagation();
                            return false;
                        }
                    });
                })

            if (!window.app.browser.supportsColorpicker()) {
                // TODO: validate if bgrins-spectrum is loaded?
                $colorpickers.each(function () {
                    var me = this;
                    var settings = $.extend({
                        change: function (color) {
                            $(me).val(color.toHexString()); // #ff0000
                        }
                    }, $(this).data("colorpicker"));
                    $(this).spectrum(settings);
                });
            }
        });
    };

    $.fn.initDatetimePicker = function($form) {
        var requiresDispose = false;

        $(this).each(function() {
            var $dt = $(this).eq(0);
            var app = window.app;
            var options = <any> app.fixResponse($dt.data("datetimepicker"));

            if ($dt.is(".manualinit")) {
                return;
            }

            // @ts-ignore
            var litePickerLoaded = window && (window.Litepicker || null) !== null;
            var isTime = (options.datePicker != true &&  options.timepicker == true) || $dt.is(".time");

            // Validate using native input
            if (window.isDateInputSupported === true &&
                window.isTouchDevice === true &&
                $dt.is("input[type=text].date"))
            {
                $dt.attr("type", "date")
                    .attr("calformat", "%m/%d/%Y")
                    .data("dateFormat", "m/d/Y") /* FIX with app settings */
                    .data("datetimepicker", null)
                    .on("focus", function (event) {
                        if ($dt.isDisabled()) {
                            return false;
                        }
                    });
            }
            // @ts-ignore
            else if (litePickerLoaded && isTime) {
                if ($.fn.popper) {
                    var $li, $ul, $menu = $("<div class='hide action-menu highlight time'></div>").append($ul = $("<ul/>"));
                    for (var h=0.0000001; h<24; h+=.5) {
                        $ul.append($li = $("<li/>")
                            .append($("<a href='#' />").text(app.convert.toTime(h))));
                        $li.data("value", h).setClick(function() {
                            $dt.setElementValue(app.convert.toTime($(this).data("value")));
                        });
                    }
                    $form.append($menu);
                    // TODO: scroll to current selection
                    $dt.popper($menu, null);
                }
            }
            // @ts-ignore
            else if (litePickerLoaded) {
                var dropdownRange = {"minYear": null, "maxYear": null, "months":true, "years":true };
                if (options.range) {
                    var maxYear = options.range.max ? dayjs(options.range.max).year() : dayjs().add(5, 'years').year();
                    var minYear = options.range.min ? dayjs(options.range.min).year() : 1900;

                    dropdownRange = $.extend(dropdownRange, {"maxYear": maxYear, "minYear": minYear });
                } else {
                    var thisYear = dayjs().year();
                    dropdownRange = $.extend(dropdownRange, {"minYear": thisYear - 100, "maxYear": thisYear + 5 });
                }

                window.app.debug && console.log("litepicker", $dt, dropdownRange);

                // @ts-ignore
                var picker = new Litepicker({
                    element: $dt[0],
                    singleDate: true,
                    mobileFriendly: true,
                    autoApply: true,
                    autoRefresh: false,
                    dropdowns: dropdownRange,
                    numberOfColumns: options.multipleMonths === true ? 3 : 1,
                    numberOfMonths: options.multipleMonths === true ? 3 : 1,
                    orientation: "bottom left",
                    lang: (options && options.lang) || app.lang || "nl",
                    inlineMode: (options && options.inline) || false,
                    firstDay: (options && options.dayOfWeekStart) || 1,
                    format: app.dateFormat,
                    autoclose: true,
                    scrollToDate: true,
                    showWeekNumbers: true,
                    hideOnBodyClick: true,
                    disableWeekends: false,
                    // separator: ??
                    lockDays: (options && options.disabledDates) || (options && options.range && options.range.exclude) || [],
                    minDate: (options && options.minDate)
                        ? dayjs().add(options.minDate, 'day')
                        : (options && options.range && options.range.min),
                    maxDate: (options && options.range && options.range.max),
                    setup: (picker) => {
                        picker.on('before:click', (target) => {
                            picker.disableUpdateEvent = null;

                            if ($dt.isDisabled()) {
                                picker.preventClick = true;
                            }
                        });
                        picker.on('preselect', (date1, date2) => {
                            // some action
                            $dt.setElementValue(date1 && date1.dateInstance);
                            $dt.trigger("change");
                        });
                        picker.on('selected', (date1, date2) => {
                            // do not use select, but preselect to set the element value
                            return false;
                        });
                        picker.on('before:show', (el) => {
                            // some action
                            var value = $dt.elementValue();
                            if (value) {
                                picker.gotoDate(value);
                                picker.setDate(value);
                            } else {
                                picker.clearSelection(); //setDate(null);
                            }
                        });
                        picker.on('render:footer', (footer) => {
                            $(footer).empty();
                        });
                        picker.on('hide', () => {
                            $dt.trigger("litepicker_hide")
                        });
                        picker.on('show', (el) => {
                            $(picker.ui).setClick();
                            $dt.trigger("litepicker_show")
                        });
                    },
                });

                picker.updateInput = function() {
                };
                var pickerShouldShown = picker.shouldShown;
                picker.shouldShown = function(el) {
                    if ($dt.isDisabled()) {
                        return false;
                    }
                    return pickerShouldShown.call(this, el);
                };

                var litepickerDestroy = function($dt) {
                    var picker = $dt.data("picker");
                    if (picker) picker.destroy();
                    $dt.data("picker", null);
                    window.app.debug && console.log("Litepicker should have been destroyed");
                };

                $dt.data("picker", picker)
                    .addClass("manualBlur")
                    .on("manualBlur", function(event) {
                        event.preventDefault();
                        event.stopPropagation();
                        picker.disableUpdateEvent = true;
                        picker.hide();
                    }).on("keyup", function(event) {
                    picker.disableUpdateEvent = true;
                    picker.hide();
                }).on("show", function(event) {
                    event.preventDefault();
                    event.stopPropagation();

                    var picker = $(this).data("picker");
                    !!picker && picker.show();
                }).on("hide", function(event) {
                    event.preventDefault();
                    event.stopPropagation();

                    var picker = $(this).data("picker");
                    !!picker && picker.hide();
                }).on("litepicker_destroy", function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                    litepickerDestroy($(this));
                }).on("dispose", function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                    litepickerDestroy($(this));
                });
            } else if ($.datetimepicker) {
                $dt.datetimepicker($.extend(
                    $dt.data("datetimepicker"),
                    {
                        onShow: function (ct) {
                            if ($dt.isDisabled()) {
                                return false;
                            }
                        }
                    }
                ));
            }
        });

        if ($form && requiresDispose) {
            $form.addClass("dispose");
        }
    };

    $.fn.initTimePicker = function() {};

    $.fn.setElementValue = function (value) {
        var app = window.app;
        // value = value || null;
        value = !isNaN(value) ? value : (value || null);

        $.each(this, function (index, element) {
            var $element = $(element),
                focusHandler = null,
                valuetype = $element.data("valuetype") || null;

            // Add the value to the input, or as text to the element
            if ($element.is(".formHandlerSerializable") && $element.hasHandler("set-value")) {
                $element.trigger("set-value", [value]);
            } else if ($element.is(".formHandlerSerializable") && $element.hasHandler("update")) {
                console.warn("Element {0} uses the update event handler. This should be changed to set-value".format($element.attr("id")));
                $element.trigger("update", [value]);
            } else if ($element.is("img")) {
                $element.attr("src", value);
                // input:checkbox
            } else if ($element.is("input[type=checkbox]")) {
                $element.prop("checked", (value == true || value == 1 || value == "on") === true);
                // input:radiobutton
            } else if ($element.is("input[type=radio]")) {
                $element.prop('checked', $element.val() == value);
                // select
            } else if ($element.is("select")) {
                $element.val(app.formatting.format(value, (valuetype || null)));
                if ($element.find("option:selected").length == 0) {
                    $element.find("option").eq(0).prop("selected", true);
                }
                // input:text, textarea
            } else if ($element.is("input,textarea")) {
                // input:time
                if (valuetype === "time" || $element.is("input[type=text].time")) {
                    $element.val(app.formatting.format(value, "time"));
                    // input:date
                } else if (valuetype == "date" || $element.is("input[type=date],input[type=text].date")) {
                    value = app.fixDate(value);

                    var settings = $element.data("datetimepicker");
                    var dateFormat = $element.data("dateFormat");

                    if ((settings || null) !== null) {
                        $element.val(app.formatting.dateFormat(value, settings.calformat));
                    } else if (dateFormat) {
                        $element.val(Date.parseDate(value, dateFormat).toISOString());
                    } else {
                        $element.val(app.formatting.format(value, "date"));
                    }
                    // Should we convert the value depending on the valuetype?
                } else if (valuetype) {
                    value = app.convert.to(valuetype, value);
                    $element.val(app.formatting.format(value, (valuetype || null)));
                } else {
                    $element.val(app.formatting.format(value, (valuetype || null)));
                }
                // Dom element
            } else {
                if (valuetype == null && $element.is("div")) {
                    $element.dataSource(value);
                } else {
                    $element.text(app.formatting.format(value, valuetype) || '');
                }
            }

            // If element has handler and focus, then set the edit value
            if ((focusHandler = $element.data("cmsFocusHandler"))) {
                if (focusHandler.hasFocus) {
                    focusHandler.setEditValue(value);
                } else {
                    focusHandler.setViewValue(value);
                }
            }
        });

        return $(this);
    };

    $.fn.insertAtCaret = function (text: string) {
        if (!text || text.length == 0) {
            return;
        }
        var $el = $(this).eq(0);
        var el = $el[0];
        var txtValue = el.value;
        var caretPos = el.selectionStart;
        var endPos = el.selectionEnd;
        // Replace value and reset selection
        el.value = txtValue.substring(0, caretPos) + text + txtValue.substring(endPos);
        el.selectionStart = el.selectionEnd = caretPos + text.length;
    };

    $.fn.setupValueOnFocus = function () {
        return this.each(function () {
            var $me = $(this);
            var $form = $me.closest("form");

            var settings = {
                app: window.app,
                $me: $me,
                $form: $form,
                elementName: $me.attr("name"),
                valuetype: $me.detectValuetype(),
                parseAndSetValue: null,
                parseToNumber: false,
                validator: null,
                convertToNumber: null,
                hasFocus: false,
                hasFocusCancel: false,
                previousValue: null,
                controlHasError: false,
                resetValidation: function () {
                    if (!$.validator) return false;

                    var element = this.$me[0];
                    var name = this.elementName;
                    this.$me.removeClass("ControlHasError");
                    this.$form.find("label.ControlHasError")
                        .filter(function () {
                            return $(this).attr("for") === name;
                        })
                        .remove();
                    // TODO: reset validation state?
                    var validator = this.$form.data("validator");
                    validator.errorList = $(validator.errorList || [])
                        .filter(function () {
                            return this.element !== element
                        })
                        .toArray();
                    if (validator.errorMap && validator.errorMap[name]) {
                        delete validator.errorMap[name];
                    }
                },
                setError: function (errorMsg) {
                    this.controlHasError = true;
                    var validator = this.$form.data("validator");
                    var errors = {};
                    errors[this.elementName] = errorMsg;
                    validator.showErrors(errors);
                },
                isDisabled: function (event) {
                    var $me = this.$me;
                    if ($me.isDisabled()) {
                        event.preventDefault();
                        event.stopPropagation();
                        return true;
                    }
                    return false
                },
                tokenize: function (txt, onParseNumber, onError) {
                    var expr = [];
                    var current = null;
                    var operator = ['/', '*', '+', '-'];
                    var skip = [' ', '\t'];
                    var inNumber = false;

                    for (var i = 0; i < txt.length; i++) {
                        var char = txt[i];

                        // skip white spaces
                        if (skip.indexOf(char) >= 0) {
                            inNumber = false;
                            current = null;
                        } else if (char == '-') {
                            // -x > -1 * x
                            expr.push({v: -1});
                            expr.push({e: "*"});
                            inNumber = false;
                            current = null;
                        } else if (operator.indexOf(char) >= 0) {
                            inNumber = false;
                            current = null;
                            expr.push({e: char});
                        } else if (inNumber && (char == ',' || char == '.')) {
                            current.t += char;
                        } else if (char >= '0' && char <= 9) {
                            // add to current number
                            if (current === null) {
                                current = {t: ""};
                                expr.push(current);
                            }
                            current.t += char;
                            inNumber = true;
                        } else if (inNumber && operator.indexOf(char) >= 0) {
                            inNumber = false;
                            current = null;
                            expr.push({e: char});
                        } else {
                            // error
                            if (onError) onError("Error at character " + i + " (hint='" + char + "')");
                            return null;
                        }
                    }

                    for (var i = 0; i < expr.length; i++) {
                        var e = expr[i];
                        e.e = e.e || e.v || onParseNumber(e.t);
                    }

                    return expr.map(function (e) {
                        return e.e;
                    }).join("");
                },
                onClick: function (event) {
                    if (this.isDisabled(event)) {
                        return false;
                    }
                },
                focusCounter: 0,
                updateCounter: 0,
                changeCounter: 0,
                setViewValue: function (value) {
                    // Value can be of any type
                    // Set value to culture formatted float (no grouping, just decimal character): the edit value
                    if (value !== null && ((this.parseToNumber && typeof value === "number") || this.validator(value))) {
                        value = this.convertToNumber(value);
                        value = this.app.formatting.format(value, settings.valuetype);
                    }

                    $me.val(value);
                },
                onFocus: function (event) {
                    if (this.isDisabled(event)) {
                        return false;
                    }
                    var $me = this.$me;

                    if (!this.hasFocus && !this.controlHasError) {
                        // Remember original value
                        var value = $me.elementValue();
                        this.setEditValue(value);
                    }

                    this.hasFocus = true;
                    this.hasFocusCancel = false;
                    this.changeCounter = 0;
                },
                setEditValue: function (value) {
                    this.previousValue = value;
                    this.updateCounter = this.focusCounter += 1;

                    // Set value to culture formatted float (no grouping, just decimal character): the edit value
                    if (value !== null && ((this.parseToNumber && typeof value === "number") || this.validator(value))) {
                        value = this.convertToNumber(value);
                        value = this.app.numberFormat.float(value) || value;
                    }

                    $me.val(value);
                },
                onKey: function (event) {
                    var $me = this.$me;

                    if (event.key === "Escape" && this.hasFocus) {
                        $me.setElementValue(this.previousValue);
                        this.hasFocusCancel = true;
                        this.controlHasError = false;
                        this.resetValidation();
                    } else if (this.convertToNumber && event.keyCode === 110 && this.app.currencyFormat && this.app.currencyFormat.d) {
                        // Insert the decimal seperator on using . from numeric keypad
                        this.$me.insertAtCaret(this.app.currencyFormat.d);
                        event.preventDefault();
                        return false;
                    }
                    this.hasFocusCancel = false;
                    this.changeCounter++;
                },
                parseAsExpression: function (v) {
                    var me = this, tmp, hasError = false;
                    var onParseValue = function (v) {
                        if (v && (tmp = me.app.convert.numberStringToNumber(v)) !== null) {
                            return tmp;
                        }
                        hasError = true;
                    }
                    var tokenized = this.tokenize(v, onParseValue, function (msg) {
                        hasError = true;
                    });
                    if (!hasError && tokenized) v = eval(tokenized);
                    return !hasError ? v : null;
                },
                onChange: function (event) {
                    this.changeCounter = 0;

                    if (this.hasFocusCancel) {
                        // Value was reset and cancel
                        if (this.controlHasError) this.resetValidation();
                        !!event && event.preventDefault();
                        !!event && event.stopImmediatePropagation();
                        return false;
                    }

                    if (this.hasFocus && this.valuetype && $.validator) {
                        var $me = this.$me, me = this, tmp;
                        var v = this.editValue();

                        // Parse potential expression
                        if (v && (tmp = this.parseAsExpression(v)) !== null) v = tmp;
                        if (v && typeof (v) === "string") {
                            var errorMsg = $.cmsValidators.hasValueError($me, v, this.valuetype);
                            if (errorMsg) {
                                // cancel change and bubbling?
                                this.setError(errorMsg);
                                return !!event && this.onRefocus(event);
                            }
                        }

                        if (v && typeof (v) === "string" && v.length > 0 && this.parseToNumber && !isNaN(tmp = this.app.convert.numberStringToNumber(v))) {
                            v = tmp;
                        }

                        $me.setElementValue(v);
                        this.updateCounter++;
                    }

                    this.resetValidation();
                    this.controlHasError = false;
                },
                editValue: function () {
                    var $me = this.$me,
                        v = $me.val();
                    v = (v && typeof (v) === "string") ? v.slim() : v;
                    return v;
                },
                onRefocus: function (event) {
                    event.preventDefault();
                    event.stopImmediatePropagation();
                    this.$me.trigger("focus");
                    return false;
                },
                onPaste: function(event) {
                    this.changeCounter++;
                },
                onBlur: function (event) {
                    if (this.isDisabled(event)) {
                        //console.log("setupValueOnFocus", "onblur", "disabled");
                        return false;
                    }

                    // Keep track to handle missing change event on safari
                    if (!!this.changeCounter) {
                        this.onChange(null);
                    }

                    // Keep the focus
                    if (!this.hasFocusCancel && this.controlHasError) {
                        return this.onRefocus(event);
                    }

                    // Should we restore/update the display value?
                    if (this.focusCounter === this.updateCounter) {
                        $me.setElementValue(this.previousValue);
                    }

                    this.controlHasError = false;
                    this.hasFocus = false;
                    this.hasFocusCancel = false;

                    // OnBlur the view value should be restore. This might be a bit double with the
                    // setElementValue call before
                    this.setViewValue(this.$me.elementValue());
                    this.resetValidation();
                }
            };

            switch ((settings.valuetype || "").toLocaleLowerCase()) {
                case "integer":
                    settings.parseAndSetValue = true;
                    settings.parseToNumber = true;
                    settings.validator = $.cmsValidators.integer;
                    settings.convertToNumber = Number.parseInt;
                    break;
                case "float":
                case "double":
                case "valuta":
                case "currency":
                case "percentage":
                case "number":
                case "decimal":
                    settings.parseAndSetValue = true;
                    settings.parseToNumber = true;
                    settings.validator = $.cmsValidators.decimal;
                    settings.convertToNumber = Number.parseFloat;
                    break;
            }

            if (settings.parseAndSetValue && settings.validator && settings.convertToNumber) {
                $me.data("cmsFocusHandler", settings);
                $me[0].cmsFocusHandler = settings;

                $me.on("click", function (event) {
                    return (this.cmsFocusHandler || $(this).data("cmsFocusHandler")).onClick(event);
                })
                    .off("change")
                    .on("change", function (event) {
                        return (this.cmsFocusHandler || $(this).data("cmsFocusHandler")).onChange(event);
                    })
                    .off("focus")
                    .on("focus", function (event) {
                        return (this.cmsFocusHandler || $(this).data("cmsFocusHandler")).onFocus(event);
                    })
                    .off("keydown")
                    .on("keydown", function (event) {
                        return (this.cmsFocusHandler || $(this).data("cmsFocusHandler")).onKey(event);
                    })
                    .off("blur")
                    .on("blur", function (event) {
                        return (this.cmsFocusHandler || $(this).data("cmsFocusHandler")).onBlur(event);
                    })
                    .off("paste")
                    .on("paste", function(event) {
                        return (this.cmsFocusHandler || $(this).data("cmsFocusHandler")).onPaste(event);
                    });
            }
        });
    };

    $.fn.detectValuetype = function () {
        var $element = $(this).eq(0);
        var valuetype = $element.data("valuetype") || null;

        if (valuetype === "boolean" || $element.is("input[type=checkbox]")) {
            return "bool";
        }
        if (valuetype === "time" || $element.is("input[type=text].time")) {
            return "time";
        }
        if (valuetype === "date" || $element.is("input[type=date],input[type=text].date")) {
            return "date";
        }

        return valuetype || null;
    };

    $.fn.elementValue = function () {
        if (arguments.length === 1) {
            this.setElementValue(arguments[0]);
            return;
        }

        if (this.length !== 1)
            return null;

        var $element = $(this).first();
        var isDateOrTime = $element.is("input[type=date],input[type=text].date,input[type=text].time,:data(valuetype=date),:data(valuetype=time)");
        var value = null;

        if ($element.is(".formHandlerSerializable")) {
            value = $element.triggerHandler("serialize");
        } else if ($element.is('select[multiple]')) {
            value = $element.val();
        } else if ($element.is('select')) {
            value = $element.find("option:selected").val();
        } else if ($element.is("input[type=checkbox]")) {
            value = $element.is(":checked");
        } else if ($element.is("input[type=radio]")) {
            // A radio button has multiple elements with the same name
            value = $element.closest("form").find("[name=" + $element.attr("name") + "]:checked").val();
        } else if ($element.is("textarea")) {
            value = ($element.val() || '');
        } else {
            var tmpVal;

            switch (($element.data("valuetype") || "").toLocaleLowerCase()) {
                case "integer":
                    // Value from element is always a string, don't use OR to set default value. "0" values would else
                    // be set to null
                    tmpVal = ($element.val() as string).slim(); // (($element.val() || '') as string).slim();

                    if (tmpVal.length > 0 && $.cmsValidators.integer(tmpVal)) {
                        // value = tmpVal.parseNumber();
                        var possibleNumber = window.app.convert.numberStringToNumber(tmpVal) || value;
                        value = !isNaN(possibleNumber) ? possibleNumber || 0 : null;
                    }
                    break;
                case "float":
                case "double":
                case "valuta":
                case "currency":
                case "percentage":
                case "number":
                case "decimal":
                    // Value from element is always a string, don't use OR to set default value. "0" values would else
                    // be set to null
                    tmpVal = ($element.val() as string).slim(); // (($element.val() || '') as string).slim();

                    if (tmpVal.length > 0 && $.cmsValidators.decimal(tmpVal)) {
                        // value = tmpVal.parseNumber();
                        var possibleNumber = window.app.convert.numberStringToNumber(tmpVal) || value;
                        value = !isNaN(possibleNumber) ? possibleNumber || 0 : null;
                    }
                    break;
                default:
                    value = (($element.val() || '') as string).slim();
                    break;
            }
        }

        if (isDateOrTime === true) {
            // Format to iso8601
            var settings = $element.data("datetimepicker");
            var dateFormat = $element.data("dateFormat");

            if (!value) {
                value = null;
            } else if ($element.is("input[type=text].time")) {
                value = dayjs(value, settings.calformat).format("HH:mm"); // app.convert.toTime(value); 
            } else if (!!settings) {
                value = dayjs(value, (settings.parsers || settings.calformat)).toDate(); //.format("YYYY-MM-DDTHH:mm"); //.tz(app.Timezone).utc().toDate();
            } else if (!!dateFormat) {
                value = dayjs(value, dateFormat).toDate(); //.format("YYYY-MM-DDTHH:mm"); //.tz(app.Timezone).utc().toDate();
            }
        } else if (isNaN(value) && ((value || null) === null || value === "")) {
            value = null;
        }

        return value;
    };

    $.fn.showOrHide = function (showIt) {
        return this.each(function () {
            $(this).toggleClass("hide", showIt !== true);
        });
    };

    $.fn.slim = function () {
        return this.each(function () {
            var $me = $(this);
            if ($me.is("form")) {
                // Filter some of the elements
                $me.find("input[type=text]:data(prop),input[type=email]:data(prop)").each(function () {
                    $(this).slim();
                });
            } else {
                if ($me.is("input[type=text]:data(prop),input[type=email]:data(prop),textarea:data(prop)")) {
                    // Not readonly, and element is visible
                    if (!$me.is("[readonly]") && !$me.is(":disabled") && $me.is(":visible")) {
                        $me.val(($me.val() as string).slim());
                    }
                }
            }
        });
    };

    $.fn.ResetAndValidate = function () {
        var app = window.app;

        if ($.validator) {
            if (this.is("form")) {
                this.validate().resetForm();

                // Overwrite the messages if available
                if ((app.messages || null) !== null) {
                    $.extend($.validator.messages, app.messages);
                }

                // Return the validation
                var isValid = this.valid();
                if (!isValid) {
                    var navItem = $(this).find(".ControlHasError").eq(0)[0];
                    if (navItem && navItem.scrollIntoView) {
                        navItem.scrollIntoView({
                            behavior: "smooth",
                            block: 'nearest'
                        });
                    }
                }

                return isValid;
            } else {
                return false;
            }
        } else {
            window.app.debug && console.log("Validator not loaded");
        }

        return false;
    };

    // Compatibility
    $.fn.resetAndValidate = $.fn.ResetAndValidate;

    $.convertTimespanToTime = function (jsonTime) {
        return window.app.convert.timespanToTime(jsonTime);
    };

    $.fn.SetValueByProperty = function (dataObj) {
        var me = this;
        var app = window.app;

        return this.each(function () {
            var $element = $(this);
            var prop = $element.data("prop");

            if (!!prop) {
                // if the property exists in data
                var value = app.hasProperty(prop, dataObj);
                $element.setElementValue(value);
            }
        });
    };

    // http://stackoverflow.com/questions/1539367/remove-whitespace-and-line-breaks-between-html-elements-using-jquery
    $.fn.cleanWhitespace = function () {
        var textNodes = $(this)
            .contents()
            .filter(function () {
                return (this.nodeType == 3 && !/\S/.test(this.nodeValue));
            })
            .remove();
        return this;
    };

    $.fn.getForm = function () {
        var $form = $();

        $(this).each(function () {
            var $me = $(this);
            if ($me.is("form")) {
                $form = $me;
                return false;
            }

            $form = $me.closest("form").eq(0);
            if ($form.length === 1) {
                return false;
            }
        });

        return $form;
    };

    $.loadAsset = function(src: string, callback?: Function, isScript?: boolean, isModule?: boolean) {
        var element = null, selector = null, attributeMatch = null;
        if ((isModule || isScript) || src.endsWith(".js")) {
            element = document.createElement("script")
            element.type = (isModule && !element.readyState) ? "module" : "text/javascript";
            element.src = src;
            selector = "script";
            attributeMatch = "src";
        } else {
            element = document.createElement("link")
            element.type = "text/css";
            element.rel = "stylesheet";
            element.href = src;
            selector = "link";
            attributeMatch = "href";
        }

        // Check if it exists
        var count = $(selector).filter(function () {
            // @ts-ignore
            return $(this).attr(attributeMatch) === src;
        }).length;

        if (count > 0) {
            if (callback) callback();
            return;
        }

        if (callback) {
            if (element.readyState) {  //IE
                element.onreadystatechange = function () {
                    if (element.readyState == "loaded" || element.readyState == "complete") {
                        element.onreadystatechange = null;
                        callback();
                    }
                };
            } else {  //Others
                element.onload = function () {
                    callback();
                };
            }
        }

        document.getElementsByTagName("head")[0].appendChild(element);
    };

    $.resolveAssetFormPromise = function(form) {
        return new Promise(function (resolve, reject) {
            // TODO: handle script loading?
            var scripts = form.Scripts || [];
            var modules = form.Modules || [];
            var stylesheets = form.Css || [];
            var all = scripts.concat(stylesheets).concat(modules);

            var loadNext = function () {
                var next = all.pop();
                if (next) {
                    $.loadAsset(next, loadNext, scripts.includes(next), modules.includes(next));
                } else {
                    resolve(form);
                }
            }

            if (all.length > 0) {
                loadNext();
            } else {
                resolve(form);
            }
        });
    }

    $.fn.registerResolveForm = function () {
        var $me = $(this);

        // Validates if "src" isn't already loaded, if not, attempt to load, and call callback on loaded

        var matchProperty = function(matchCallback) {
            for (var prop in this) {
                if (Object.prototype.hasOwnProperty.call(this, prop)) {
                    if (matchCallback && matchCallback(this[prop], prop)) {
                        return this[prop];
                    }
                }
            }

            return null;
        };

        $me.on("resolveForm", function (event, formID, context) {
            var form, formContainer;

            if (!formID) return null;
            if (typeof (formID) === "string") {
                formContainer = $me.data("form_container");

                form = formContainer && formContainer.forms
                    ? (formContainer.forms[formID] || matchProperty.call(formContainer.forms, function(f) { return (typeof f === "object") && f.Id === formID; }))
                    : null;
            } else if (typeof (formID) === "object") {
                form = formID;
            }

            // Create promise which loads asset and returns the form object
            var resolveFormPromise = function (form) {
                return $.resolveAssetFormPromise(form);
            }

            if (!form && formContainer && formContainer.onMissing) {
                // Should return a promise, that on resolve should call resolveFormPromise
                return formContainer.onMissing.call(formContainer, formID, resolveFormPromise, context);
            }
            if (!form) {
                return null;
            }

            return resolveFormPromise(form);
        });
    };

    $.fn.registerForm = function (formID, dynamicForm) {
        var $me = $(this).eq(0);

        var formContainer = $me.data("form_container");
        if (!formContainer) {
            $me.data("form_container", formContainer = {forms: {}});
        }

        formContainer.forms[formID] = dynamicForm;

        if (!$me.hasEvent("resolveForm")) {
            $me.registerResolveForm();
        }
    };

    $.fn.resolveForm = function (formID, context?: any) {
        if (!this || !(this instanceof jQuery) || this.length === 0) {
            return null;
        }

        var $me = $(this).eq(0),
            resolved = null;

        if ($me.hasEvent("resolveForm") && (resolved = $me.triggerHandler("resolveForm", [formID]))) {
            return resolved;
        }

        $me.parents().each(function () {
            var $me = $(this);
            if ($me.hasEvent("resolveForm") && (resolved = $me.triggerHandler("resolveForm", [formID]))) {
                return false;
            }
        });

        if (!resolved) {
            window.app.debug && console.log("resolveForm failed", formID)
        }

        return resolved;
    };

    $.fn.buttonToggles = function (options) {
        return this.each(function () {
            var opt = $.extend({
                multiple: false,
                onChange: null,
                toggle: true,        // not multiple? true=toggle selected (on/off), false=select only
                toggleDontDeselect: false
            }, options);

            var $me = $(this);
            $me.addClass("formHandlerSerializable")
                .data("options", opt)
                .on("serialize", function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    return $(this).dataSource();
                })
                .on("set-value", function (event, value) {
                    event.preventDefault();
                    event.stopPropagation();
                    var $me = $(this);
                    $me.dataSource(value);
                    $me.trigger("invalidate", [value]);
                })
                .on("invalidate", function (event, value) {
                    event.preventDefault();
                    event.stopPropagation();
                    var $me = $(this);
                    var multiple = $me.data("options").multiple === true;

                    $me.find("> a")
                        .removeClass("selected")
                        .filter(function () {
                            var $me = $(this);
                            var convert = window.app.convert;
                            var toggleValue = $me.data("toggle");
                            var toggleIntValue = convert.toNumber(toggleValue);

                            if (multiple) {
                                if (!isNaN(toggleIntValue)) {       // toggle value is number? then match with &
                                    return (toggleIntValue & convert.toNumber(value)) !== 0;
                                } else if (Array.isArray(value)) {  // Is value an array?
                                    return value.indexOf(toggleValue) >= 0;
                                } else {
                                    return toggleIntValue === value || toggleValue === value;
                                }
                            } else if (value) {
                                if (isNaN(toggleIntValue)) return toggleValue === value;
                                return (toggleIntValue === convert.toNumber(value));
                            }
                        })
                        .addClass("selected");

                    if (opt.onChange) opt.onChange(value);
                });

            $me.find("> a").setClick(function () {
                var $bttn = $(this);
                var multiple = $me.data("options").multiple === true;
                var toggle = multiple ? parseInt($bttn.data("toggle")) : $bttn.data("toggle");

                if (multiple) {
                    $me.setElementValue(parseInt($me.elementValue()) ^ toggle);
                } else {
                    var isSelected = opt.toggle ? $bttn.is(".selected") : false;

                    if (!!isSelected && !!opt.toggleDontDeselect) {
                        $me.setElementValue(toggle);
                    } else {
                        $me.setElementValue(!isSelected ? toggle : null);
                    }
                }

                $me.trigger("change");
            });
        })
    };

    $.roleAuthorization = function() {
        return roleAuthorization;
    };

    // Authorization
    var roleAuthorization = {
        CLASSDISABLED: "disabled-by-role",
        CLASSHIDE: "hide",
        isAuthorized: function (requiredRoles, availableRoles) {
            // requiredRoles should be an array with integers, or a single integer
            if (requiredRoles !== null && ((Array.isArray(requiredRoles) && requiredRoles.length > 0) || !isNaN(requiredRoles))) {
                if (!availableRoles)
                    return false;
                if (Array.isArray(requiredRoles) && Array.isArray(availableRoles))
                    return requiredRoles.filter(function (n) {return availableRoles.indexOf(n) >= 0}).length > 0;
                if (Array.isArray(requiredRoles) && requiredRoles.indexOf(availableRoles) >= 0)
                    return true;
                if (!isNaN(requiredRoles) && Array.isArray(availableRoles) && availableRoles.indexOf(requiredRoles) >= 0)
                    return true;
                return !isNaN(requiredRoles) && !isNaN(availableRoles) && requiredRoles === availableRoles;
            }

            return true;
        },
        getRequiredRoles: function ($element) {
            return $element.data("roles");
        },
        setRequiredRoles: function ($element, roles) {
            $element.data("roles", roles);
        }
    };

    // requiredRoles is an int, or an int array
    $.fn.setRoles = function (requiredRoles) {
        return this.each(function () {
            roleAuthorization.setRequiredRoles($(this), requiredRoles);
        });
    };

    $.fn.setVisibleByRole = function (currentRoles) {
        return this.each(function () {
            var $me = $(this);
            var requiredRoles = roleAuthorization.getRequiredRoles($me);
            var isAuthorized = roleAuthorization.isAuthorized(requiredRoles, currentRoles);
            $me.toggleClass(roleAuthorization.CLASSHIDE, !isAuthorized);
        });
    };

    $.fn.setReadonlyByRole = function (currentRoles) {
        return this.each(function () {
            var $me = $(this);
            var requiredRoles = roleAuthorization.getRequiredRoles($me);
            var isAuthorized = roleAuthorization.isAuthorized(requiredRoles, currentRoles);
            $me.prop("readonly", !isAuthorized);
        });
    };

    $.fn.enableByRole = function (currentRoles) {
        return this.each(function () {
            var $me = $(this);

            if ($me.is("form.editor")) {
                $me.find(":data(roles)")
                    .filter(function() { return $(this).closest("form").is($me); })
                    .each(function() {
                        $(this).enableByRole(currentRoles);
                    });
            }

            var requiredRoles = roleAuthorization.getRequiredRoles($me);
            var isAuthorized = roleAuthorization.isAuthorized(requiredRoles, currentRoles);
            $me.toggleClass(roleAuthorization.CLASSDISABLED, !!!isAuthorized);
        });
    };

    $.fn.isAuthorized = function (currentRoles) {
        var authorized = true;
        var $elements = [];

        this.each(function () {
            var required = roleAuthorization.getRequiredRoles($(this));
            if (!required) return;

            if (!roleAuthorization.isAuthorized(required, currentRoles)) {
                authorized = false;
                return false;
            }
        });

        return authorized;
    };

    // Tests
    // var isTrue = function (value) {
    //     if (value !== true) throw "Invalid value";
    // }
    // var isFalse = function (value) {
    //     if (value !== false) throw "Invalid value";
    // }
    // isFalse(roleAuthorization.isAuthorized(1, null));
    // isTrue(roleAuthorization.isAuthorized());
    // isTrue(roleAuthorization.isAuthorized(null, 1));
    // isTrue(roleAuthorization.isAuthorized([1,2,3], 1));
    // isTrue(roleAuthorization.isAuthorized([1,2,3], [2]));
    // isFalse(roleAuthorization.isAuthorized([1,2,3], null));
    // isFalse(roleAuthorization.isAuthorized([1,2,3], 4));
    // isFalse(roleAuthorization.isAuthorized([1,2,3], [4,5]));
    // isTrue(roleAuthorization.isAuthorized([1,2,3], 2));
    // isTrue(roleAuthorization.isAuthorized([1,2,3], [2]));
    // isTrue(roleAuthorization.isAuthorized([1,2,3], [2, 3]));
    // isTrue(roleAuthorization.isAuthorized([1,2,3], [3, 4]));
    // isTrue(roleAuthorization.isAuthorized(null, [3, 4]));
})(jQuery);
