﻿/* Copyright 2014 - 2019 Willow Media

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. /**/

(function ($) {
    "use strict";

    //
    // Replace element with autocomplete placeholder element
    //
    $.placeHolderAutoComplete = function (element, options) {
        this._arrowNavigation = false;
        this.options = {};
        this.$element = null;
        this.$input = null;
        this.onChange = null;
        this.userEvent = null;

        var myself = this;

        this.init = function (element, options) {
            // Extend options with default options
            this.options = $.extend({}, $.placeHolderAutoComplete.defaultOptions, options);

            //Manipulate element here ...   
            var me = myself;

            // Reference the element
            var $me = $(element);

            // Create element
            var $divError = $("<div class='ControlHasError' />");

            var $div = $("<div class='uapl_container' />")
                .attr("tabindex", 0)
                .addClass("manualBlur")
                .addClass($me.attr("class") || '')
                .attr("id", $me.attr("id"))
                .attr("name", $me.attr("name"))
                .addClass("formHandlerSerializable")
                .toggleClass("required", $me.hasClass("required"))
                .data($.extend({}, $me.data()))
                .data("placeHolderAutoComplete", me)
                .data("errorhighlight", $div)
                .data("errorloc", $divError)
                .on("click", function(event) {
                    if ($(this).isDisabled()) {
                        event.preventDefault();
                        event.stopPropagation();
                        return false;
                    }
                    me.userEvent = true;
                })
                .on("serialize", function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    return me.val();
                })
                .on("set-value", function (event, data) {
                    event.preventDefault();
                    event.stopPropagation();
                    me.setValue(data);
                })
                .on("clear", function (event) {
                    event.preventDefault();
                    event.stopPropagation();

                    myself.clear();
                });

            if (this.options.icon) {
                switch (this.options.icon) {
                    case "search":
                        $div.addClass("search-icon");
                        break;
                }
            }

            var placeholderText = this.options.placeholder || $me.attr("placeholder");
            if ((placeholderText || null) !== null && placeholderText.length > 0) {
                $div.append(
                    $("<div class='placeholder' />")
                        .text(placeholderText));
            }

            if ((this.options.buttons || null) !== null) {
                var $buttons = null;

                $.each(this.options.buttons, function (index, item) {
                    if ($buttons === null) { $buttons = $("<span class='buttons'/>"); }

                    if (typeof item === "function") {
                        $buttons.append(item());
                    } else {
                        $buttons.append($(item));
                    }
                });

                if ($buttons !== null) {
                    $div.append($buttons);
                }
            }

            var $span = $("<span />").hide();
            var $input = $("<input type='text' />")
                .attr("spellcheck", "false")
                .attr("autocomplete", "off")
                .attr("autocorrect", "off")
                .attr("autocapitalize", "off")
                .addClass("ignore")
                .on("change", function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                })
                .on("keydown", function (event) {
                    var $me = me.$input;

                    if ($me.isDisabled()) { event.preventDefault(); return; }

                    var value = $me.val();

                    // Cancel pending autocomplete
                    if (event.which == 27) {                                // Escape
                        event.preventDefault();
                        event.stopPropagation();

                        if ((myself.options.onEscape || null) !== null) {
                            myself.options.onEscape(me);
                        } else {
                            myself.removeList();
                            myself._arrowNavigation = true;
                        }
                        return false;
                    } else if (event.which == 13) {                         // Enter
                        event.preventDefault();
                        event.stopPropagation();

                        if ((me.options.onEnter || null) !== null) {
                            me.options.onEnter(me, $me.val());
                        } else {
                            //  me.options.allowManualItems
                            var item = null;
                            var data = null;

                            // Check if we have selected
                            if (me._$autocompleteList != null && me._arrowNavigation == true && !me.options.allowManualItems) {
                                me._$autocompleteList.find(".hover").first().trigger("click");
                                return false;
                            } else if (me._$autocompleteList != null && me._arrowNavigation == true) {
                                data = me._$autocompleteList.find(".hover").first().data('rec');
                                item = me._$autocompleteList.find(".hover").first().text();
                            } else if (me._$autocompleteList == null && me._arrowNavigation == true && !me.options.allowManualItems && value.length > 0) {
                                // Enter should trigger fetch
                                return true;
                            } else {
                                item = value;
                            }

                            if (me.options.onSelect) {
                                data = data || (me.options.allowManualItems ? (me.options.onAddManual ? me.options.onAddManual(item) : item) : null);
                                me.options.onSelect(data || item, me);
                                $me.val('');
                            } else if (me.options.allowManualItems) {
                                data = (me.options.onAddManual ? me.options.onAddManual(item) : item);
                                $me.trigger("addItem", [data]);
                                $me.val('');
                            }
                        }

                        return false;
                    } else if (event.which == 8 && value == '') {           // Backspace
                        event.preventDefault();
                        event.stopPropagation();

                        if (me.options.disableRemoveWithBackspace === true) {
                            return false;
                        }

                        // remove last item
                        $div.find("li").last().remove();
                        me.updateState();
                        me._arrowNavigation = true;

                        //if ((me.onChange || null) !== null) { me.onChange.call(me); }
                        //$me.trigger("change");

                        if ((me.onChange || null) !== null) { me.onChange.call(me); }
                        me.$element.trigger("change");
                        return false;
                    } else if (me._$autocompleteList != null) {
                        // if we have a dropdown
                        var $current;
                        var maxItems;

                        switch (event.which) {
                            case 38:                                        // up
                                // select next one up
                                event.preventDefault();
                                event.stopPropagation();

                                $current = me._$autocompleteList.find(".hover");
                                if ($current.length > 0) {
                                    if (!$current.is(me._$autocompleteList.children().first())) {
                                        $current.removeClass("hover");
                                        $current.prev().addClass("hover");
                                        me._$autocompleteList.trigger("checkScroll");
                                    }
                                }
                                me._arrowNavigation = true;
                                return false;
                            case 40:                                        // down
                                // select next one down
                                event.preventDefault();
                                event.stopPropagation();

                                $current = me._$autocompleteList.find(".hover");
                                var $next = $current.length > 0 ? $current.next() : me._$autocompleteList.children().first();

                                if (!$current.is(me._$autocompleteList.children().last())) {
                                    $current.removeClass("hover");
                                    $next.addClass("hover");
                                    me._$autocompleteList.trigger("checkScroll");
                                }
                                me._arrowNavigation = true;
                                return false;
                            default:
                                // Count items
                                maxItems = Math.max((me.options.maximumItems || 0), 0);
                                if (maxItems > 0 && me.count() >= maxItems) {
                                    return false;
                                }
                                break;
                        }
                    } else {

                        // Count items
                        maxItems = Math.max((me.options.maximumItems || 0), 0);
                        if (maxItems > 0 && me.count() >= maxItems) {
                            return false;
                        }
                    }

                    me._arrowNavigation = false;
                })
                .on("keyup", function (event) {
                    var $me = me.$input;
                    var value = $me.val();

                    if (!me.options.disableAutoScale) {
                        $me.css("width", $span.text(value).innerWidth() + 25);
                    }

                    me.fetchList(value);
                })
                .on("stopLoading", function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                    me.stopLoading();
                    me.removeList();
                })
                .on("addItem", function (event, value) {
                    event.preventDefault();
                    event.stopPropagation();
                    var $me = me.$input;

                    me.stopLoading();
                    me.removeList();

                    var hasChanges = me.addItem(value);

                    if (hasChanges === true) {
                        // Set focus
                        me.updateState();

                        if ((me.options.onValueAdded || null) !== null) {
                            me.options.onValueAdded.call(me, value);
                        }

                        if (!me.options.readonly) me.$input.focus();

                        if ((me.onChange || null) !== null) { me.onChange.call(myself); }
                        me.$element.trigger("change");
                    } else {
                        if (!me.options.readonly) me.$input.focus();
                    }
                })
                .on("focus", function (event, data) {
                    // console.log("focus");
                    // var isUserClick = $(this).is(".user-event");
                    // $(this).removeClass("user-event");
                    var isUserClick = me.userEvent === true;
                    me.userEvent = false;

                    if ($(this).isDisabled() || me.options.readonly) {
                        event.stopPropagation();
                        $(this).blur();
                        return false;
                    }

                    $div.addClass("focus");
                    me.updateState();

                    if (me.options.loadOnFocus && isUserClick) {
                        me.loadAndFetchList(me.$input.val());
                    }
                })
                .on("blur", function (event) {
                    $div.removeClass("focus");
                    me.userEvent = false;
                    me.updateState();
                })
                .toggleClass("manualScaled", me.options.disableAutoScale === true);

            // Remember the element
            me.$input = $input;

            $div.append($input)
                .append($span)
                .on("click", function (event) {
                    event.preventDefault();
                    $input.focus();
                })
                .on("focus", function (event) {
                    $input.focus();
                })
                .on("manualBlur", function (event) {
                    event.preventDefault();
                    event.stopPropagation();

                    me.stopLoading();
                    me.removeList();
                    $div.removeClass("focus");
                    me.updateState();
                })
                .on("append", function (event, data) {
                    event.preventDefault();
                    // dont trigger change automatic
                    me.addItem(data);
                    me.updateState();
                });

            // reference to container
            me.$element = $div;
            $me.after($div);
            $div.after($divError);

            // Push current items to the list
            var objId = $div.data("prop") || $div.attr("id");
            var data = me.options.data || (((me.options.dataObj || null) !== null) ? me.options.dataObj[objId] : null) || null;
            if (data !== null) {
                $input.trigger("set-value", [data])
            }

            $(document).bind('click', function (event) {
                me.checkClick(event);
            });

            // Add change
            me.onChange = me.options.onChange || null;
            $me.remove();
        };

        this._createListItem = function(value, displayValue) {
            var me = myself;

            return $("<li />")
                .append($("<i class='icon-cancel'/>"))
                .append($("<span/>").text(displayValue))
                .setDataSource(value)
                .setClick(function (event) {
                    if (myself.options.readonly) { return false; }

                    var $me = $(this);
                    $me.remove();
                    myself.updateState();

                    if ((myself.onChange || null) !== null) { myself.onChange.call(myself); }
                    myself.$element.trigger("change");
                    myself.$input.focus();
                });
        };

        this.addItem = function (value) {
            var me = myself;

            // Count items
            var maxItems = Math.max((me.options.maximumItems || 0), 0);
            if (maxItems > 0 && me.count() >= maxItems) {
                return false;
            }

            var displayValue = '';

            // Get display value (value could be an object)
            if ((me.options.onAddItemDisplayValue || null) !== null) {
                displayValue = me.options.onAddItemDisplayValue(value);
            } else if ((me.options.onParseResponse || null) !== null) {
                displayValue = me.options.onParseResponse(value);
            } else {
                displayValue = me.slim(value || '');
            }

            // No displayvalue available, then use the value itself
            if ((displayValue || null) === null) {
                displayValue = value;
            }

            var hasChanges = false;

            if ((value || null) !== null && displayValue.length > 0) {
                myself.$input.val('');

                if (!myself.contains(value)) {
                    var $item = myself._createListItem(value, displayValue);

                    hasChanges = true;

                    // Add item before the input
                    myself.$input.before($item);

                    // 'serialize' the values to the hidden input
                    // me.$element.val((me.val() || []).join(","));
                }
            }

            return hasChanges;
        };

        // Set values is called to programmatically set the value of the element. No change event
        // should occure
        this.setValue = function (dataArray) {
            var me = myself;
            var $me = myself.$input;

            me.removeList();
            me.$element.trigger("clear");

            if ((dataArray || null) !== null && $.isArray(dataArray)) {
                $.each(dataArray, function (index, value) {
                    var displayValue = '';

                    // Get display value (value could be an object)
                    if ((me.options.onAddItemDisplayValue || null) !== null) {
                        displayValue = me.options.onAddItemDisplayValue(value);
                    } else if ((me.options.onParseResponse || null) !== null) {
                        displayValue = me.options.onParseResponse(value);
                    } else {
                        displayValue = me.slim(value || '');
                    }

                    // No displayvalue available, then use the value itself
                    if ((displayValue || null) === null) {
                        displayValue = value;
                    }

                    if (value != null && displayValue.length > 0) { // && ) {
                        $me.val('');

                        if (!me.contains(value)) {
                            var $item = myself._createListItem(value, displayValue);

                            // var $item = $("<li />")
                            //  .append($("<i class='icon-cancel'/>"))
                            //  .append($("<span/>").text(displayValue))
                            //  .setDataSource(value)
                            //  .setClick(function (event) {
                            //     if (me.options.readonly) { return false; }
                            //     $(this).remove();
                            //     myself.$input.focus();
                            //  });

                            // Add item before the input
                            $me.before($item);
                        }
                    }
                });
            }

            //me.$me.val((me.val() || []).join(","));
            me.updateState();
        };

        this.preventScroll = function ($element) {
            if (($.event.special.mousewheel || null) !== null)
                return;

            // load mousewheel script manually!!
            // http://stackoverflow.com/questions/5802467/prevent-scrolling-of-parent-element
            $element.bind('mousewheel', function (e, d) {
                // Uses jquery.mousewheel.js
                var t = $(this);
                if (d > 0 && t.scrollTop() === 0) {
                    e.preventDefault();
                }
                else {
                    if (d < 0 && (t.scrollTop() == t.get(0).scrollHeight - t.innerHeight())) {
                        e.preventDefault();
                    }
                }
            });
        };

        this.count = function () {
            return myself.$element.find("li:data('rec')").length;
        };

        // Check if value exists in the current selected list. Does attempt to parse value with onaddItemDisplayValue method
        this.contains = function (value) {
            var me = myself;
            var result = false;

            if ((me.options.onDistinctCheck || null) !== null && me.options.onDistinctCheck(me.val(), value)) {
                result = true;
            } else {
                me.$element.find("li:data('rec')").each(function () {
                    var displayValue = $(this).data("rec");

                    // Get display value (value could be an object)
                    if ((me.options.onAddItemDisplayValue || null) !== null) {
                        displayValue = me.options.onAddItemDisplayValue(displayValue);
                    } else if ((me.options.onParseResponse || null) !== null) {
                        displayValue = me.options.onParseResponse(displayValue);
                    } else {
                        // no parsing 
                        displayValue = me.slim(displayValue || '');
                    }

                    if (displayValue == value) {
                        result = true;
                        return false; // exit loop
                    }
                });
            }

            return result;
        };

        // Returns array of selected values (data values, not displayed values)
        this.val = function () {
            var me = myself;
            var result = null;

            if ((me.options.onValue || null) !== null) {
                result = me.options.onValue.call(this, this);
            } else {
                me.$element.find("li:data('rec')").each(function () {
                    if (result == null) { result = []; }

                    result.push($(this).dataSource());
                });
            }

            return result;
        };

        // Slim down a string
        this.slim = function (stringValue) {
            // Fixed empty string slim()
            var m = new String(stringValue || '').match(/\S+/gi);
            return (m != null ? m.join(' ') : "");
        };

        this._load = null;
        this._timeout = null;
        this._lastValue = null;

        this.stopLoading = function () {
            // if a timer exists, then reset it
            if (myself._timeout != null) {
                clearTimeout(myself._timeout);
                myself._timeout = null;
            }

            // Cancel loading
            if (myself._load && myself._load.abort) {
                myself._load.abort();
                myself._load = null;
            }

            myself.$element.removeClass("loading");
        };

        this.fetchList = function (value) {
            var me = myself;

            // Check if the value has changes
            if (me._lastValue == value)
                return false;

            me._lastValue = value;

            // Is the requested query value bigger then the minimum amount of characters?
            if (value.length < me.options.minimumCharacters) {
                me.removeList();
                return false;
            }

            me.loadAndFetchList(value);
        }

        this.loadAndFetchList = function(value) {
            var me = myself;

            // Clear timer and loading
            me.stopLoading();

            if (!!!me.options.url) {
                return null;
            }

            // Create a new timer, which will do the ajax callback
            me._timeout = setTimeout(function () {
                var data = ((me.options.onCreateQueryObject || null) !== null ?
                    me.options.onCreateQueryObject.call(me, value) :
                    { search: value });

                // Create and store the callback
                var ajaxOptions = {
                    loadIndicator: false,
                    url: me.options.url,
                    data: data,
                    onStart: function () {
                        // Show progress indicator?

                        if ((me.options.onLoadStart || null) !== null) {
                            me.options.onLoadStart();
                        } else {
                            me.$element.addClass("loading");
                        }
                    },
                    onSucces: function (response) {
                        // replace or show current list
                        if ((response.Data || null) === null || (response.Data || []).length == 0) {
                            me.removeList();
                            if ((me.options.onNoResult || null) !== null) {
                                me.options.onNoResult.call(me);
                            }
                        } else {
                            if ((me.options.onShowResult || null) !== null) {
                                me.options.onShowResult.call(me, response.Data);
                            } else {
                                me.showList(response.Data);
                            }
                        }
                    },
                    onError: function (response) {
                        if ((me.options.onError || null) !== null) {
                            me.options.onError(response);
                        } else {
                            // app.alertBox(response.Error || "Unknown error");
                            app.alertBox("Oops", response.Error || "Unknown error", "error");
                        }
                    },
                    onFinish: function () {
                        // Remove progress indicator
                        me._load = null;

                        if ((me.options.onLoadFinish || null) !== null) {
                            me.options.onLoadFinish();
                        } else {
                            me.$element.removeClass("loading");
                        }
                    }
                };

                if (me.options.onBeforeSend) { data = me.options.onBeforeSend.call(this, data, ajaxOptions); }

                me._load = ajaxOptions;
                app.ajax(me._load);
            }, me.options.keyTimeout);
        };

        var _$autocompleteList = null;

        this.clear = function () {
            // Remove items, update state, trigger onChange change events
            myself.$element.find("li").remove();
            myself.updateState();
            myself.$input.val("");

            if ((myself.onChange || null) !== null) { myself.onChange.call(myself); }
            myself.$element.trigger("change");

            return this;
        };

        this.checkClick = function (event) {
            var me = myself;

            if (me._$autocompleteList != null && !($(event.target).parent().addBack().is(me._$autocompleteList))) {
                me.removeList();
            }
        };

        // Remove an existing list
        this.removeList = function () {
            var me = myself;

            if (me._$autocompleteList != null)
                me._$autocompleteList.remove();

            me._$autocompleteList = null;
            me.updateState();
        };

        this.updateState = function () {
            var me = myself;

            var notEmpty = (me.$element.find("li:data(rec)").length + me.$input.val().length) > 0;
            me.$element.toggleClass("notEmpty", notEmpty);

            // console.log("updateState", notEmpty);

            return notEmpty;
        }

        // Create a new list. Data is the result array from the server
        this.showList = function (data) {
            var me = myself;

            var $acdiv = $("<div />")
                .addClass(me.options.autocompleteListClass)
                .hide();

            $(data).each(function () {
                var $li = $("<li/>").setDataSource(this);

                var displayValue = (me.options.onParseResponse || null) !== null ? me.options.onParseResponse(this) : this;
                if (displayValue instanceof jQuery) {
                    $li.append(displayValue);
                } else {
                    $li.text(displayValue);
                }

                // TODO: allow parsing the content to display
                $li.on("click", function (event) {
                    event.preventDefault();
                    event.stopImmediatePropagation();

                    if ((me.options.onSelect || null) !== null) {
                        me.options.onSelect($(this).data("rec"), me);
                    } else {
                        me.$input.trigger("addItem", [$(this).data("rec")]);
                    }

                    me.updateState();
                });

                $acdiv.append($li);
            });

            if ((me.options.onAfterCreateList || null) !== null) {
                me.options.onAfterCreateList.call(me, $acdiv, data);
            }

            $acdiv.find("li")
                .hover(
                    function () {
                        $(this).parent().find("li").removeClass("hover");
                        $(this).addClass('hover');
                        me._arrowNavigation = false;
                    },
                    function () {
                        $(this).removeClass('hover');
                        me._arrowNavigation = false;
                    }
                );

            $acdiv
                .on("checkScroll", function (event) {
                    var $me = $(this);

                    var $hover = $me.find(".hover");
                    if ($hover.length == 1) {
                        // get dimensions view
                        var meTop = $me.scrollTop();
                        var meHeight = $me.innerHeight();
                        var meBottom = meTop + meHeight;

                        var hoverTop = $hover.position().top + meTop;
                        var hoverBottom = hoverTop + $hover.outerHeight();

                        // If view under hover
                        if (meTop > hoverTop) {
                            // scroll to visible
                            $me.scrollTop(hoverTop);
                            // If view above hover
                        } else if (meBottom < hoverBottom) {
                            $me.scrollTop(hoverBottom - meHeight + 2);
                        }
                    }
                })
                .on("resizeDropdownlist", function (event) {
                    var loc = me.$element.offset();
                    var width = (me.options.width || null) !== null ? me.options.width : me.$element.outerWidth();

                    switch (me.options.alignment || '') {
                        case "right":
                            $acdiv.offset({ top: loc.top + me.$element.outerHeight(), left: ((loc.left + me.$element.outerWidth()) - width) });
                            $acdiv.width(width);
                            break;
                        default:
                            $acdiv.offset({ top: loc.top + me.$element.outerHeight(), left: loc.left });
                            $acdiv.width(width);
                            break;
                    }
                });

            me.preventScroll($acdiv);

            // Remove current
            myself.removeList();

            // Add new
            $("body").append($acdiv);

            myself._$autocompleteList = $acdiv.show().trigger("resizeDropdownlist");

            // add resize event
            $(window).on("resize", function (event) {
                $acdiv.trigger("resizeDropdownlist");
            });
        };

        // Initialize the element
        this.init(element, options);
    };

    $.fn.placeHolderAutoComplete = function (options) { // Using only one method off of $.fn
        // Return existing object
        if ($(this).length === 1) {
            if ($(this).data("placeHolderAutoComplete") != null) {
                return $(this).data("placeHolderAutoComplete");
            } else {
                // Create formHandler for the first element selected
                return new $.placeHolderAutoComplete($(this).eq(0), options);
            }
        } else {
            return this.each(function () {
                var $me = $(this);
                new $.placeHolderAutoComplete($me, options);
            });
        }
    };

    $.placeHolderAutoComplete.defaultOptions = {
        keyTimeout: 350,
        readonly: false,
        allowManualItems: true,
        loadOnFocus: false,
        minimumCharacters: 3,
        maximumItems: 0,
        alignment: null,
        url: null,
        data: null,                     // array of items, or
        dataObj: null,                  // the object that contains the property with the values. The ID is the name of the property
        width: null,
        placeholder: null,
        onError: null,                  // override the error handling
        onAddItemDisplayValue: null,    // callback to get the display value from a data object on add
        onSelect: null,                 // onSelect(data, placeholderAutocomplete)
        onParseResponse: null,          // Is being call after data is fetched, and it needs parsing for display
        onDistinctCheck: null,          // method to check if item exists function (itemList, value) {
        onCreateQueryObject: null,      // onCreateQueryObject(value) for creating custom data objects for callback
        onShowResult: null,             // override the custom showItems
        onNoResult: null,               // event when no data is returned
        onEnter: null,                  // override the enter key (function(placeHolderAutoComplete))
        onEscape: null,                 // override the escape key (function(placeHolderAutoComplete))
        onLoadStart: null,              // override the onStart of ajax load function
        onLoadFinish: null,             // override the onFinish of ajax load function,
        onValue: null,                  // override the get value
        disableAutoScale: false,        // disable scaling of input element
        onAddManual: null,              // function(item) { return { value: item }; } - callback to parse the manual text item to an object
        disableRemoveWithBackspace: false,
        onChange: null,
        autocompleteListClass: "autocompleteList",
        buttons: null,
        onBeforeSend: null,             // event before ajax callback (function(data, ajaxOptions)
        onAfterCreateList: null,        // function($div, data) : event after the list is filled
        icon: null
    };
})(jQuery);