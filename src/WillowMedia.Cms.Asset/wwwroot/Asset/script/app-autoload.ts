/* Copyright 2014 - 2024 Willow Media

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. /**/
/// <reference path="interfaces.ts" />

interface CmsTemplateEngine {
    obeyWidth: boolean;

    // layout should match CmsAutoloadMoreLayout
    renderHeader(template: Array<any>, data: Object, columnElement: string, layout?: any): JQuery;

    render(template: Array<any>, data, columnElement: string, formData: Object): JQuery;
}

interface JQueryStatic {
    // example: formHandler(element: JQuery, options: any): void;
    autoLoadMore: {
        //constructor(element: JQuery, options?: object): Object;
        defaultOptions: CmsAutoLoadMoreOptions,
        GetTemplateEngine(): CmsTemplateEngine;
    }
}

/* Interface on elements */
interface JQuery {
    autoLoadMore(options?: CmsAutoLoadMoreOptions): CmsAutoloadMore;
}

class CmsAutoLoadMorePager {
    page: number = 0;
    pagesize: number = 50;
    end: boolean = false;
    loadingCnt: number = 0;
    loadingError: boolean = false;
    sorting: string = null;
    filter: any;
    hash: any;
}

enum CmsAutoloadMoreLayout {
    default = 1,
    flexrows = 2
}

enum CmsAutoloadMoreLoadAndUpdate {
    Pager = 1,
    Replace = 2,
    Sync = 3
}

class CmsAutoLoadMoreOptions {
    // pager, replace
    loadAndUpdate: string = CmsAutoloadMoreLoadAndUpdate[CmsAutoloadMoreLoadAndUpdate.Pager].toString();
    filter: any = null;
    pagesize = null;
    sorting = null;                                 // Default sorting
    direction = true;                               // Default direction (true = ascending, false=descending)
    url = null;
    columnElement = "span";
    busyIcon = '../image/waiting.16.gif';
    clickableClass = "clickable";
    initialLoad = true;
    addHeader = true;
    preLoad = null;
    template = null;                                 // Template array
    renderHeader = null;                             // function (element, data, autoLoadMore) { },
    renderView = null;                               // function (element, data, autoLoadMore) { },
    beforeAddView = null;                            // function ($autoload, data). This is the autoload object
    renderViewDone = null;                           // function (element, data, formdata)
    renderDone = null;                               // function (count)
    renderManual = null;                             // function (data, cnt)
    click = null;
    defaultParentClass = "autoload";
    defaultClass = "accselect";
    scrollElement = null;                            // element or jquery selector
    //scrollElementId: null,                         // deprecated
    initialLoadEmpty = null;                         // function ($element): called when first load is empty
    uniqueField = null;                              // defines unique property in data
    debug = false;
    // onLoadFail = function (response) { window.app.failHandler(response); };
    onLoadStart = null;
    onLoadDone = null;
    onLoadError = null;                              // function(response)
    onBeforeSend = null;                             // function(autoloadMore, data, CmsAjaxOptions)
    basedata = null;
    initialPager: CmsAutoLoadMorePager = null;
    emptyOnFirstLoad = true;                          // empty the list on first load
    disableScrollLoading = false;
    disableObserver = false;
    hoverMenu = null;
    layout: CmsAutoloadMoreLayout = CmsAutoloadMoreLayout.default;
    onFlexRowsSelector: null;                         // function for returning a selector for the style
    flexrowStyleId: string = null;
    manualReload: Function = null;                    // handle reload manually

    onCreateListItem: Function = null;                  // function(data, formData), this is autoload
}

class CmsAutoloadMore {
    options: CmsAutoLoadMoreOptions = null;
    pager: CmsAutoLoadMorePager = null;
    el = null;
    autoloader = this;
    offset = 30;
    private filter: any = null;
    private loaded: boolean = null;
    private _initialLoad = false;
    private _currentRequest = null;
    private templateEngine = null;
    private layout: CmsAutoloadMoreLayout = CmsAutoloadMoreLayout.default;

    constructor(element: JQuery, options?: CmsAutoLoadMoreOptions) {
        this.templateEngine = $.autoLoadMore.GetTemplateEngine();
        //this.templateEngine.autoloader = this;                                  // reference this to the template engine
        this.options = $.extend(new CmsAutoLoadMoreOptions(), options);

        var me = this;
        var $element = $(element).eq(0);
        me.el = $element;
        me.layout = this.options.layout || me.layout;

        if (me.isFlexrows(me.layout)) {
            me.templateEngine.obeyWidth = false;
            me.el.addClass("flexrows");
            me.setupFlexrows(this);
        }

        $element.data('autoloadmore', me);

        // set default pagesize
        if (me.options.pagesize) {
            me.pager.pagesize = me.options.pagesize;
        }

        if (me.options.filter) {
            me.pager.filter = me.options.filter;
        }

        // Create pagers
        me.pager = $.extend(new CmsAutoLoadMorePager(), me.options.initialPager);

        // Reference the element and set default events
        $element
            .addClass(me.options.defaultParentClass)
            .off("reload")
            .on("reload", function (event, filter, sorting) {
                event.preventDefault();
                event.stopPropagation();
                me.reload(filter, sorting);
            });

        this.setupScrollHandling(me);

        // Initial setup pager
        if (me.options.sorting) {
            var pager = me.pager;
            pager.sorting = (me.options.direction ? "" : "!") + me.options.sorting;
        }

        // Load data on construct
        if (me.options.initialLoad === true) {
            me._initialLoad = true;
            me.loadMore($element);
        } else {
            me._initialLoad = false;
        }
    }

    private isFlexrows = function (layout: any): boolean {
        return layout === CmsAutoloadMoreLayout[CmsAutoloadMoreLayout.flexrows].toString();
    }

    public setupFlexrows = function (me) {
        var selector, template = me.options.template;

        if (me.options.onFlexRowsSelector) {
            selector = me.options.onFlexRowsSelector.call(me)
        } else {
            var id = me.el.attr("id") || window.app.uniqueId("ul");
            me.el.attr("id", id);
            selector = "#" + id;
        }

        // Create style
        var style = me.renderFlexrowsStyle(selector, template);
        var styleId = me.options.flexrowStyleId || window.app.uniqueId("style");
        me.el.before($(`<style>${style}</style>`).attr("id", styleId));

        me.options.flexrowStyleId = styleId;
    };

    public renderFlexrowsStyle = function(selector, template) {
        let baseSelector = [
            `ul${selector}.flexrows > li`,
            `ul${selector}.flexrows li + ul:not(.form) > li`,
            `ul${selector}.flexrows li.container > div`
        ]
        return this.renderFlexrowsStyleColumns(baseSelector, template);
    };

    public renderFlexrowsStyleColumns = function(baseSelector, template) {
        var defaultTemplate = { width: null, wrap: false, minWidth: null, fill: null, align: null };
        var style = "", columnIndex = 0;

        if (!Array.isArray(baseSelector))
            baseSelector = [baseSelector];

        $(template)
            .map(function() { return $.extend({}, defaultTemplate, this); })
            .each(function(index: number, item) {
                if (item && item.hide === true) { return; }
                columnIndex++;

                var justify = "safe flex-start";
                var textAlign = "left";
                switch (item.align || "") {
                    case "right":
                        justify = "safe flex-end";
                        textAlign = "right";
                        break;
                    case "center":
                        justify = "safe center";
                        textAlign = "center";
                        break;
                }

                // `ul${selector}.flexrows li.autoload > span:nth-child(${columnIndex}), ` +
                // `ul${selector}.flexrows > li.header > span:nth-child(${columnIndex}), ` +
                // `ul${selector}.flexrows > li.column-header > span:nth-child(${columnIndex}), ` +
                // `ul${selector}.flexrows li.autoload.container > div > span:nth-child(${columnIndex}) { ` +

                let cssSelector = baseSelector.map(s => `${s} > span:nth-child(${columnIndex})`).join(", ");

                // ${baseSelector} > span:nth-child(${columnIndex}), ` +
                //     `${baseSelector}.container > div > span:nth-child(${columnIndex})


                style += `${cssSelector} { ` +
                    `flex: ${parseInt(item.width) || 1} 0 ${item.fill ? "auto": 0 }; ` +
                    `white-space: ${!item.wrap ? "nowrap" : "normal"}; ` +
                    `flex-wrap: ${item.wrap ? "wrap": "nowrap"}; ` +
                    `min-width: ${item.minWidth || 0}; ` +
                    `overflow: hidden; text-overflow: ellipsis; ` +
                    `justify-content: ${justify}; ` +
                    `text-align: ${textAlign};` +
                    `}\n`;
            });

        // console.log(selector, style);

        return style;
    };

    private setupScrollHandling = function (me) {
        // Default callback for scroll event (also used for the .off call)
        var $scrollElement = window.app.resolveValue(me.options.scrollElement) ? $(me.options.scrollElement).eq(0) : $(window);
        me.mousewheelCallback = function (event) {
            me.handleMouseWheel(event, me, $scrollElement)
        };
        // @ts-ignore
        $scrollElement.on('mousewheel DOMMouseScroll', me.mousewheelCallback);

        var hasObserver = 'IntersectionObserver' in window;
        if (me.options.disableObserver || !hasObserver) {
            // console.log("using default scroll detection");
            // Use scrolling to detect new load
            me.scrollEventCallback = function (event) {
                me.handleScrollEvent(event, me, $scrollElement)
            };
            // @ts-ignore
            $scrollElement.on("scroll", me.scrollEventCallback);
        } else {
            // console.log("using observer scroll detection");
            var buildThresholdList = function () {
                let thresholds = [];
                let numSteps = 20;

                for (let i = 1.0; i <= numSteps; i++) {
                    let ratio = i / numSteps;
                    thresholds.push(ratio);
                }

                thresholds.push(0);
                return thresholds;
            }

            this.observer = new IntersectionObserver(
                (entries, observer) => {
                    if (me.options.disableScrollLoading === true) {
                        return false;
                    }
                    if (me.loaded !== true) {
                        return false;
                    }
                    if (!me.el.is(":visible")) {
                        return false;
                    }

                    entries.forEach(entry => {
                        var $item = $(entry.target),
                            visible = entry.intersectionRatio > 0;

                        if (me.el.children().last().is($item) && visible) { // trigger load
                            // console.log("trigger load from observer")
                            me.loadMore(me.el);
                        }
                    });
                },
                {
                    root: $scrollElement[0],
                    rootMargin: "35% 0px",
                    threshold: buildThresholdList()
                });
        }
    };

    private observer: IntersectionObserver = null;
    private mousewheelCallback: Function = null;
    private scrollEventCallback: Function = null;

    protected handleMouseWheel = function (event, me, $scrollElement) {
        var $element = me.el;

        if ($element.isDisabled()) {
            event.preventDefault();
            event.stopPropagation();
            return false;
        }
    };

    protected handleScrollEvent = function (event, me, $scrollElement) {
        var me = this;
        var offset = me.offset;
        var $element = me.el;

        if (me.options.disableScrollLoading === true) {
            return false;
        }

        // Dont load on scroll if we didnt initially load something
        if (me.loaded !== true) {
            return false;
        }

        // Don't load if we are not visible
        if (!me.el.is(":visible")) {
            return false;
        }

        if ($scrollElement.is($(window))) {
            if ($scrollElement.scrollTop() >= $(document).height() - $scrollElement.height() - offset) {
                me.loadMore($element);
            }
        } else {
            var scrHeight = $scrollElement[0].scrollHeight;
            if (($scrollElement.scrollTop() + $scrollElement.innerHeight()) > scrHeight - offset) {
                me.loadMore($element);
            }
        }
    }

    // Reload will start fetching data from the top of the collection. Additionally with a filter, or different sorting.
    // 
    // filter	:	an object with the filter values, that are request. Empty values should be skipped
    // sorting	:	should be null, if you want to keep the same sorting as before. Else the name of the requested column
    //  			with optionally a leading !. This will trigger the descending order
    reload(filter? : any, sorting?: any, onDone?: Function, onError?: Function) {
        let me = this;
        let pager = me.pager;

        if (me.options.manualReload) {
            return me.options.manualReload.call(me, filter, sorting, onDone, onError);
        }

        // reset page, so loadMore will ++ it to 1
        pager.page = 0;
        pager.end = false;
        pager.hash = null;

        // Get sorting and check for descending
        if (sorting) {
            var direction = sorting.toLowerCase() === (pager.sorting || '').toLowerCase();
            me.options.sorting = sorting;
            me.options.direction = !direction;
            pager.sorting = (!direction ? "" : "!") + sorting;
        } else if (me.options.sorting) {
            pager.sorting = (me.options.direction ? "" : "!") + me.options.sorting;
        }

        // Create filter
        if ((filter || null) !== null) {
            pager.filter = filter;
        }

        // set pager
        me.pager = pager;

        // load
        me.loadMore(me.el, true, onDone, onError);
    };

    // showSorting = function(sorting, direction) {
    //     var me = this;
    //     if (!me.options.addHeader) return;
    //
    //     var columnElement = me.options.columnElement || "div";
    //     var $header = me.el.find("li.header");
    //
    //     var $columns = $header.find(columnElement).removeClass("sorting-asc").removeClass("sorting-desc");
    //     $columns.filter(function() { return $(this).data("sorting") === sorting; }).addClass(direction ? "sorting-asc" : "sorting-desc");
    // };

    remove = function () {
        var me = this;
        me.el.data('autoloadmore', null);
        // Remove the scroll event handling
        var $scrollElement = me.options.scrollElement ? $(me.options.scrollElement).eq(0) : $(window);
        $scrollElement
            .off("scroll", me.scrollEventCallback)
            .off('mousewheel DOMMouseScroll', me.mousewheelCallback);
    };

    fixResponse = function (obj) {
        var app = window.app;
        return app.fixResponse(obj);
    };

    loadMore = function (element: JQuery, allowAbort?: boolean, onDone?: Function, onError?: Function) {
        let me = this;
        let app = window.app;
        let autoload = element.data('autoloadmore');
        let usePager = me.options.loadAndUpdate === CmsAutoloadMoreLoadAndUpdate[CmsAutoloadMoreLoadAndUpdate.Pager].toString();
        let urlAutoload = app.resolveValue(autoload.options.url);

        me.loaded = true;

        if (!!!urlAutoload) {
            return false;
        }

        // Marked as finished?
        if (me.options.loadAndUpdate === CmsAutoloadMoreLoadAndUpdate[CmsAutoloadMoreLoadAndUpdate.Replace].toString()) {
            autoload.pager.page = 0;
            autoload.pager.pagesize = 0;
        } else if (autoload.pager && autoload.pager.end === true) {
            return;
        } else if (autoload.pager.loadingError === true) {
            return;
        } else if (++autoload.pager.loadingCnt > 1) {
            // Attempt to stop current process and load new
            var active = autoload._currentRequest;

            if ((active || null) !== null) {
                // If allowAbort is not specificly specified as true, then break
                if (allowAbort !== true) {
                    return;
                } else if (active.abort) {
                    // if (autoload.options.debug) { app.log("aborting current request"); }
                    active.abort();
                }

                autoload._currentRequest = null;
            }
        }

        // Next page
        if (usePager) autoload.pager.page++;

        // Show busy cursor
        // <div id="loadMore" style="display:none;" data-page=""><img src="~/Content/Images/waiting.16.gif" /></div>
        var $busy = $('<li class="autoload loadMore" />');
        if (usePager) $(element).append($busy);

        // its a load from the start, then clear all items
        if (usePager && autoload.pager.page === 1) {
            if ((autoload.options.preLoad || null) !== null) {
                autoload.options.preLoad();
            }

            // Remove current list
            if (autoload.options.emptyOnFirstLoad === true) {
                $(me.el).find("li").remove();
            }
        } else if ((autoload.options.preLoad || null) !== null) {
            // Call preLoad 
            autoload.options.preLoad();
        }

        // Call function before start loading
        if ((autoload.options.onLoadStart || null) !== null) {
            autoload.options.onLoadStart.call(me);
        }

        var data = $.extend(app.resolveValue(autoload.options.basedata || {}), autoload.pager);

        // Loader
        var ajaxOptions = new CmsFetchOptions();
        ajaxOptions.url = urlAutoload;
        ajaxOptions.data = data;
        ajaxOptions.onError = null;
        ajaxOptions.onFinish = function () {
            autoload._currentRequest = null;
            !!usePager && $busy.remove();

            // Call function when loading is done
            if ((autoload.options.onLoadDone || null) !== null) {
                autoload.options.onLoadDone.call(me);
            }
        };

        if ((me.options.onBeforeSend || null) !== null) {
            me.options.onBeforeSend.call(me, me, data, ajaxOptions);
        }

        autoload._currentRequest = ajaxOptions;
        app.fetch(ajaxOptions).then(request => {
            let response = request.response;

            !!usePager && $busy.remove();

            if (!!!response.Succes) {
                if (!!request.handled || request.state === CmsFetchState.Cancelled) {
                    return;
                }

                if (autoload && autoload.pager)
                    autoload.pager.end = true;


                // do something with the error?
                if (me.options.onLoadError && typeof me.options.onLoadError === "function") {
                    me.options.onLoadError.call(me, response);
                } else if (response.Error) {
                    console.error(response.Error);

                    var $error = $('<li class="autoload error" />').append($("<span/>").text(response.Error));
                    $(element).append($error);
                }

                if (onError) {
                    onError.call(me);
                }
            } else {
                let cntRows = 0;

                autoload.pager.page = response.Page;
                autoload.pager.hash = response.Hash;

                if (me.options.loadAndUpdate === CmsAutoloadMoreLoadAndUpdate[CmsAutoloadMoreLoadAndUpdate.Replace].toString()) {
                    $(element).empty();
                }

                if ((response.Sorting || null) !== null) {
                    autoload.pager.sorting = response.Sorting;
                }

                if (response.End === true) {
                    autoload.pager.end = true;
                }

                if ((response.Data || null) !== null) {
                    if (response.Data.length === 0) {
                        autoload.pager.end = true;
                    } else {
                        // call onLoadSuccess
                        autoload.options && autoload.options.onLoadSucces && autoload.options.onLoadSucces.call(me, response.Data, response.FormData);

                        cntRows += me.renderData(element, autoload, response.Data, response.FormData);
                    }
                }

                if (autoload.options.renderDone) {
                    autoload.options.renderDone.call(me, cntRows, response);
                }

                // Call onEmpty if initial load row count is 0
                // FIX: initialLoadEmpty on page = 0
                //if (autoload._initialLoad === true && cntRows === 0 && autoload.options.initialLoadEmpty) {
                if (autoload.pager.page === 1 && cntRows === 0 && autoload.options.initialLoadEmpty) {
                    autoload.options.initialLoadEmpty(autoload.el);
                }

                autoload._initialLoad = false;
                autoload.pager.loadingCnt = 0;

                if (onDone) {
                    onDone.call(me);
                }
            }
        });
    };

    renderData = function (element: JQuery, autoload: CmsAutoloadMore, dataArray: Array<any>, formData?: any) {
        var me = this;
        var $list = $(element);
        var cntRows = 0;
        var cnt = -1;

        // Sync works with the unique id property
        var usePager= me.options.loadAndUpdate === CmsAutoloadMoreLoadAndUpdate[CmsAutoloadMoreLoadAndUpdate.Pager].toString();
        var useSync= me.options.loadAndUpdate === CmsAutoloadMoreLoadAndUpdate[CmsAutoloadMoreLoadAndUpdate.Sync].toString();
        var sync = useSync ? $list.find("li:data(rec)").map(function() { return $(this)[0]; }).toArray() : null;

        // Get the data record
        $.each(dataArray, function (index, data) {
            if (!dataArray.hasOwnProperty(index)) { return; }

            cntRows++;
            cnt++;

            // Fix dates
            autoload.fixResponse(data);

            var $item = null;

            if ((autoload.options.renderManual || null) !== null) {
                // renderManual returns an element (li)
                $item = autoload.options.renderManual(data, cnt);
            } else {
                // Create header line
                if (!usePager || (autoload.options.addHeader === true && cnt === 0 && autoload.pager.page === 1)) {
                    var $itemHeader = null;

                    var getOrAddHeader = function ($list) {
                        var $itemHeader = $list.find("li.header");
                        if ($itemHeader.length == 0) {
                            $itemHeader = $('<li></li>').addClass("header");
                            $list.prepend($itemHeader);
                        }
                        return $itemHeader;
                    }

                    if (!!autoload.options.renderHeader) {
                        $itemHeader = getOrAddHeader($list).empty();
                        autoload.options.renderHeader($itemHeader, data, autoload, formData);
                    } else if (Array.isArray(autoload.options.template) && autoload.options.template.length > 0) {
                        $itemHeader = getOrAddHeader($list).empty();
                        $itemHeader.append(autoload.templateEngine.renderHeader(
                            autoload.options.template, data, autoload.options.columnElement, autoload.options.layout));
                    }

                    // Attach click event to header items with sorting data
                    if ($itemHeader != null) {
                        me.setupHeaderSorting(me, $itemHeader, function (event) {
                            me.headerSortingClick.call(this, event, me);
                        })
                    }
                }

                $item = autoload.renderListItem(data, autoload, formData);
            }

            if ($item != null) {
                // Call function before item is added
                if ((autoload.options.beforeAddView || null) !== null) {
                    autoload.options.beforeAddView.call(me, $list, data, formData);
                }

                // Check if records exists
                // 1. do we have a 'unique' property?
                var uniqueField = autoload.options.uniqueField,
                    replaced = false,
                    primaryKey = data[uniqueField];

                if (uniqueField && primaryKey) {
                    // 1.1 find record with this ID 
                    var primaryKey = data[uniqueField];

                    $list.find("li:data(rec)").each(function () {
                        if ($(this).dataSource()[uniqueField] === primaryKey) {
                            // 1.2 replace
                            $(this).replaceWith($item);
                            replaced = true;

                            return false;
                        }
                    });
                }

                if (!replaced) {
                    // 2. no, then add
                    $list.append($item);

                    // If we use the observer, then add item to observer
                    if (me.observer) {
                        me.observer.observe($item[0]);
                    }
                }

                if (!!useSync) {
                    var element = $item[0];
                    sync = sync.filter(function(item) { return item !== element });
                }
            }
        });

        if (!!useSync) {
            // remove all left overs
            $(sync).remove();
        }

        return cntRows;
    };

    headerSortingClick = function (event, autoload) {
        autoload.reload(autoload.filter, $(this).data("sorting"));
    };

    setupHeaderSorting = function (autoload, $itemHeader, onClick) {
        var me = this;
        var sorting = me.options.sorting;
        var direction = me.options.direction;

        $itemHeader.find(":data(sorting)").each(function () {
            var $column = $(this).addClass("sortable").css("cursor", "pointer");
            if (onClick) $column.setClick(onClick);

            // If sorted, then show indicators
            if (sorting && $column.data("sorting") === sorting) {
                if (direction) {
                    $column.addClass("sorting-asc").prepend("<i class='icon-down-dir'/>");
                } else {
                    $column.addClass("sorting-desc").prepend("<i class='icon-up-dir'/>");
                }
            }
        });
    };

    addClickListItem = function ($item: JQuery, onClick: Function) {
        var me = this;

        $item.on("click", function (event) {
            var $target = $(event.target);

            if ($target.is("input, select")) {

            } else if (onClick) {
                event.preventDefault();
                event.stopPropagation();

                onClick(event, $(this));  //me.options.click(event, $(this));
            }
        });

        if ((me.options.clickableClass || null) !== null) {
            $item.addClass(me.options.clickableClass);
        }
    };

    createListItem = function(data: any, formData: any) {
        return !!(this.options && this.options.onCreateListItem)
            ? this.options.onCreateListItem.call(this, data, formData)
            : $('<li/>').addClass("autoload");
    }

    renderListItem = function (data: any, autoload: CmsAutoloadMore, formData?: any) {
        var app = window.app;
        // Create a node, and add the data record to it
        var $item = autoload.createListItem.call(autoload, data, formData); // $('<li />').addClass("autoload");
        $item.dataSource(data);

        // Use manual rendering
        if ((autoload.options.renderView || null) !== null) {
            // RenderView will add the elements to the item. If result is true, then add the item to the list
            if (autoload.options.renderView($item, data, autoload, formData)) {
                return null;
            }
            // use template render engine
        } else if ((autoload.options.template || null) !== null) {
            $item.append(autoload.templateEngine.render(autoload.options.template, data, autoload.options.columnElement, formData));
            if (autoload.options.defaultClass) {
                $item.addClass(autoload.options.defaultClass);
            }
            // Fallback if there is no renderview defined
        } else {
            $item.append($("<span/>").text(app.json.toJSON(data)));
            if (autoload.options.defaultClass) {
                $item.addClass(autoload.options.defaultClass);
            }
        }

        // Do the options define a click?
        if (autoload.options.click) {
            autoload.addClickListItem.call(autoload, $item, autoload.options.click);
        }

        // Optional add hover menu
        if ((autoload.options.hoverMenu || null) !== null) {
            var $hoverMenu = $("<" + autoload.options.columnElement + "/>").addClass("hoverMenu");

            $(autoload.options.hoverMenu).each(function () {
                var $a = $("<a />");
                // Fontello icon
                $a.append($("<i/>").addClass(this.icon || "arrow"));
                if ((this.title || null) !== null)
                    $a.attr("title", this.title);

                var clickEvent = this.click;
                $a.setClick(function (event) {
                    if ((clickEvent || null) !== null) {
                        clickEvent($item);
                    }
                });
                $hoverMenu.append($a);
            });
            $item.append($hoverMenu);
        }

        // If the li has content...
        if ($item.html().length > 0 && autoload.options.renderViewDone) {
            autoload.options.renderViewDone.call(autoload, $item, data, formData);
        }

        return $item;
    };
}

(function ($) {
    "use strict";

    // Backwards compatibility
    $.autoLoadMore = {
        defaultOptions: new CmsAutoLoadMoreOptions(),
        GetTemplateEngine: function () {
            return {
                obeyWidth: true,
                renderHeader: function (template, data, columnElement, layout?: any) {
                    var me = this;
                    var output = $("<li />").addClass("header");
                    var isFlexboxLayout = layout &&
                        (layout.valueOf() === CmsAutoloadMoreLayout[CmsAutoloadMoreLayout.flexrows].valueOf() ||
                            layout === CmsAutoloadMoreLayout.flexrows);

                    $.each(template, function (x, f) {
                        if (f && f.hide === true) {
                            return; // Do nothing
                        }

                        var oi = $("<" + (columnElement || "div") + "/>");
                        var align = null;

                        if (f instanceof jQuery) {
                            oi.append(<JQuery>f);
                        } else {
                            // Default settings from valuetype
                            if (f.valuetype && $.isFunction(f.valuetype)) {
                            } else {
                                switch ((f.valuetype || '').toLowerCase()) {
                                    case "currencywithzero":
                                    case 'currency':
                                    case 'valuta':
                                        align = "right";
                                        break;
                                    default:
                                        break;
                                }
                            }

                            if (f.width && me.obeyWidth) {
                                oi.css("width", f.width);
                            }

                            if (f.sortable) {
                                oi.data("sorting", f.sortable);
                            }

                            // Only add align if its not a flexbox layout
                            if (!isFlexboxLayout && (f.align || align)) {
                                oi.css("text-align", f.align || align);
                            }

                            oi.attr("title", f.label).html(f.label);
                        }

                        output.append(oi);
                    });


                    return output.children();
                },
                render: function (template, data, columnElement, formData) {
                    // {
                    //    label: "MatName",
                    //    data: "MatName"
                    //    sortable: "MatName",
                    //    valuetype: "",
                    //    width: "25%",
                    //    render: function (data) { return data.MatName; }
                    //    cssClass: (class)
                    //    title: ""
                    // } 

                    var me = this;
                    var app = window.app;
                    var output = $("<li></li>");

                    $.each(template, function (x, f) {
                        // var f = template[x];
                        if (f instanceof jQuery) {
                            var element = <JQuery>f;
                            output.append(element);
                        } else if (typeof (f) === "function") {
                            output.append(f.call(null, data, formData, f));
                        } else if (f && f.hide === true) {
                            // Do nothing
                        } else {
                            var oi = $("<" + (columnElement || "div") + "/>");
                            var align = null;

                            if (f.cssClass) {
                                oi.addClass(f.cssClass);
                            }

                            // Default settings from valuetype
                            if (f.valuetype && typeof f.valuetype === "function") {
                            } else {
                                switch ((f.valuetype || '').toLowerCase()) {
                                    case "currencywithzero":
                                    case 'currency':
                                    case 'valuta':
                                        align = "right";
                                        break;
                                    default:
                                        break;
                                }
                            }

                            if (f.title) {
                                oi.attr("title", f.title);
                            }

                            if (f.width && me.obeyWidth) {
                                oi.css("width", f.width);
                            }

                            if (f.align || align) {
                                oi.css("text-align", f.align || align).css("justify-content", f.align || align);
                            }

                            if (f.button) {
                                // Render a button
                                //var click = f.click;
                                // create link with reference to the click function
                                var $button = $("<a></a>").attr("href", "#").addClass("fancyButton").text(f.button).data("myClick", {click: f.click}).click(function (event) {
                                    event.preventDefault();
                                    event.stopPropagation();

                                    var ref = $(this).data("myClick");
                                    if (ref && ref.click) {
                                        // Get our li
                                        var $li = $(this).closest("li");

                                        ref.click($li);
                                    }
                                });
                                oi.append($button);
                            } else if (f.renderControl) {
                                oi.append(f.renderControl.call(oi, data, formData));
                            } else if (f.render) {
                                oi.text(f.render.call(oi, data, formData, f));
                            } else if (data) {
                                // No render function, then attempt to use the label as property name
                                var val;
                                if (typeof f.data === "function") {
                                    val = f.data(data);
                                } else {
                                    val = app.hasProperty((f.data || f.label || ""), data);
                                }

                                if (f.valuetype && typeof f.valuetype === "function") { //} $.isFunction(f.valuetype)) {
                                    // Call function, if it returns a value, its added as text. If it renders some html,
                                    // it should add it to oi itself
                                    var result = f.valuetype(val, oi);
                                    if (result) {
                                        oi.text(result);
                                    }
                                } else {
                                    switch ((f.valuetype || '').toLowerCase()) {
                                        case 'check':
                                            var c = new String(val || '0').toUpperCase();
                                            var element = $("<input/>").attr("type", "checkbox").attr("disabled", "disabled").data("val", val);
                                            switch (c) {
                                                case 'Y':
                                                case '1':
                                                case 'J':
                                                case 'TRUE':
                                                case 'on':
                                                    // var element = 
                                                    // oi.append(element);
                                                    element.attr("checked", "checked");
                                                    break;
                                                default:
                                                    //oi.html($("<input></input>").attr("type", "checkbox").attr("disabled", "disabled").data("val", val));
                                                    //oi.append($("<input/>").attr("type", "checkbox").attr("disabled", "disabled").data("val", val));
                                                    break;
                                            }
                                            oi.append(element);
                                            break;
                                        case 'currencywithzero':
                                        case 'currency':
                                        case 'valuta':
                                        case 'percentage':
                                        case 'datetime':
                                        case 'date':
                                            oi.text(app.formatting.format(val, f.valuetype));
                                            break;
                                        case 'time':
                                            if (val && val !== '') {
                                                oi.text(app.formatting.format(val, f.valuetype));
                                            }
                                            break;
                                        case 'email':
                                            if ((val || null) != null && val.length > 0) {
                                                oi.append($("<a/>").attr("href", "mailto:" + val).text(val));
                                            }
                                            break;
                                        default:
                                            oi.text(val);
                                            break;
                                    }
                                }
                            }
                            output.append(oi);

                            if (f.onRenderDone) {
                                f.onRenderDone.call(oi, data, formData);
                            }
                        }
                    });
                    return output.children();
                }
            };
        }
    };

    $.fn.autoLoadMore = function (options?: CmsAutoLoadMoreOptions) { //Using only one method off of $.fn
        // Return existing object
        if ($(this).length === 1 && $(this).data("autoloadmore") != null)
            return $(this).data("autoloadmore");

        return this.each(function () {
            (new CmsAutoloadMore($(this), options));
        });
    };

    //    template: [
    //        {
    //            label: "MatName",       // name
    //            sortable: "MatName",    // fieldname
    //            width: "25%",       
    //            render: function (data) { return data.MatName; }    // function to render the field
    //        }, ...
})(jQuery);