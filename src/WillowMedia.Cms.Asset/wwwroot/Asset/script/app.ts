﻿/// <reference path="./interfaces.ts" />

/* Copyright 2014 - 2024 Willow Media

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. /**/

// fix against import
// import {Obj} from "@popperjs/core";

declare var dayjs: any;

HTMLElement.prototype.hasClass = function (className) {
    if (this.classList) {
        return this.classList.contains(className);
    }
    return (' ' + this.className + ' ').indexOf(' ' + className + ' ') > -1;
};

Array.asArray = function(someArray) {
    if (!someArray) {
        throw("Argument is null");
    }
    if (!Array.isArray(someArray)) {
        throw("Argument is not an array");
    }

    var result = [];
    for (var item in someArray) {
        if (!someArray.hasOwnProperty(item)) {
            continue;
        }
        result.push(someArray[item]);
    }
    return result;
};

String.prototype.format = function (...args: any[]) {
    // var args = arguments;
    return this.replace(/\{\{|\}\}|\{(\d+)\}/g, function (m, n) {
        if (m == "{{") {
            return "{";
        }
        if (m == "}}") {
            return "}";
        }
        return args[n];
    });
};

String.prototype.slim = function () {
    // http://www.daniweb.com/web-development/javascript-dhtml-ajax/code/418008/string.normalize
    var m = this.match(/\S+/gi);
    return (m != null ? m.join(' ') : "");
};

String.prototype.isNumber = function () {
    return /^\d+$/.test(this);
}

String.prototype.ConvertToDate = function () {
    return window.app.convert.toDate(this);
};

if (typeof String.prototype.camelCase !== 'function') {
    String.prototype.camelCase = function () {
        return this.toString().trim()
            .replace(/([A-Z]+)/g, function (m, l) {
                return l.substr(0, 1).toUpperCase() + l.toLowerCase().substr(1, l.length);
            })
            .replace(/[\s]{1,}(.)/g, function (m, l) {
                return ' ' + l.toUpperCase();
            })
            .replace(/^(.)/g, function (m, l) {
                return l.toUpperCase();
            });
    };
}

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/endsWith
if (!String.prototype.endsWith) {
    String.prototype.endsWith = function(search: string, this_len?: number) : boolean {
        if (this_len === undefined || this_len > this.length) {
            this_len = this.length;
        }
        return this.substring(this_len - search.length, this_len) === search;
    };
}

if (typeof String.prototype.replaceAll !== 'function') {
    String.prototype.replaceAll = function (search, replacement) {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    };
}

if (typeof String.prototype.htmlEscape !== 'function') {
    String.prototype.htmlEscape = function () {
        return String(this)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
    }
}

if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, '');
    };
}

// TODO: should be recursive
if (typeof String.prototype.hashCode !== 'function') {
    String.prototype.hashCode = function () {
        var hash = 0, i, l, char;
        if (this.length === 0) return hash;
        for (i = 0, l = this.length; i < l; i++) {
            char = this.charCodeAt(i);
            hash = ((hash << 5) - hash) + char;
            hash |= 0; // Convert to 32bit integer
        }
        return hash;
    };
}

if (typeof Array.prototype.includes !== 'function') {
    Array.prototype.includes = function (value) {
        var found = false;
        $.each(this, function(index, item) {
            if (item === value) {
                found = true;
                return false;
            }
        });
        return found;
    };
}

if (typeof Array.prototype.groupBy !== 'function') {
    Array.prototype.groupBy = function (getProperty) {
        var groups = this.reduce(function (groups, item) {
            var name = (getProperty && typeof getProperty === "function")
                ? getProperty(item)
                : item[getProperty];

            var group = groups[name] || (groups[name] = []);
            group.push(item);

            return groups;
        }, {});

        return groups;
    };
}

if (typeof Array.prototype.joinStringReadable !== 'function') {
    Array.prototype.joinStringReadable = function () {
        return String.joinReadable(this);
    };
}

// Idea from http://openexchangerates.github.io/accounting.js/
if (typeof String.prototype.parseNumber !== 'function') {
    String.prototype.parseNumber = function () {
        return window.app.convert.numberStringToNumber(this) || NaN;
    };
}

if (typeof String.prototype.hasIndexerDescription !== 'function') {
    String.prototype.hasIndexerDescription = function () {
        return /^([a-z0-9]+)\[([0-9]+)\]$/i.exec(this);
    }
}

// String statics

String.join = function (separator, values) {
    var result = '';
    var cnt = 0;

    $.each((values || []), function (index, item) { //for (x in values || []) {
        var val = String(item || '').slim();

        if (val.length == 0) {
            return; // continue;
        }

        if (cnt++ > 0) {
            result += separator;
        }
        result += val;
    });

    return result;
}

String.joinReadable = function (list) {
    if (!list || !Array.isArray(list)) { return ""; }
    var result = "";
    var cnt = 0;

    $.each(list.reverse(), function (index, item) {
        switch (cnt) {
            case 1:
                result = item + " & " + result;
                break;
            case 0:
                result = item;
                break;
            default:
                result = item + ", " + result;
                break;
        }
        cnt++;
    });

    return result;
}

String.encaps = function (value: string, left: string, right: string) {
    var val = String(value || '').slim();
    return (val.length == 0 ? '' : (left || '') + val + (right || ''));
}

String.findFirst = function (haystack, needle) {
    // Find first match
    if (haystack == null || haystack.length == 0) {
        return null;
    }

    var result = null;

    $.each(needle, function (index, item) {
        var search = (item || '').slim().toLowerCase();

        $.each(haystack, function (h, itemHaystack) {
            if (itemHaystack.toLowerCase() == search) {
                result = item;
                return false;
            };
        });

        if (result !== null)
            return false;
    });

    return result;
};

Number.prototype.currencyFormat =
    function (this: number, decPlaces: number, thouSeparator: string, decSeparator: string) {
        let n = this;
        var sign = n < 0 ? "-" : "",
            i = parseInt((n = Math.abs(+n || 0)).toFixed(decPlaces)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces;
        decSeparator = decSeparator == undefined ? "." : decSeparator;
        thouSeparator = thouSeparator == undefined ? "," : thouSeparator;

        return sign + (j ? i.substr(0, j) + thouSeparator : "") +
            i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) +
            (decPlaces ? decSeparator + Math.abs(n - parseInt(i)).toFixed(decPlaces).slice(2) : "");
    };

Number.prototype.pad = function () {
    var n = this;
    return (n < 10) ? ("0" + n) : "" + n;
};

Number.prototype.countDecimals = function () {
    if (Math.floor(this.valueOf()) === this.valueOf()) {
        return 0;
    }
    return this.toString().split(".")[1].length || 0;
};

// Date

if (typeof Date.prototype.isToday !== "function") {
    Date.prototype.isToday = function () {
        var now = new Date();
        return this.getFullYear() === now.getFullYear() && this.getMonth() === now.getMonth() && this.getDate() === now.getDate();
    };
}

if (typeof Date.prototype.addDays !== "function") {
    Date.prototype.addDays = function (days: number) {
        var date = new Date(this.valueOf());
        date.setDate(date.getDate() + days);
        return date;
    };
}

if (typeof Date.prototype.dateOnly !== "function") {
    Date.prototype.dateOnly = function () {
        return new Date(this.getFullYear(), this.getMonth(), this.getDate());
    };
}

if (typeof Date.today !== "function") {
    Date.today = function () {
        var now = new Date();
        return new Date(now.getFullYear(), now.getMonth(), now.getDate());
    };
}

if (typeof Date.prototype.isValid !== "function") {
    Date.prototype.isValid = function () {
        // An invalid date object returns NaN for getTime() and NaN is the only
        // object not strictly equal to itself.
        return !isNaN(this.getTime());
    };
}

if (typeof Date.prototype.dateStamp !== "function") {
    Date.prototype.dateStamp = function () {
        return (this.getFullYear() * 10000) + ((this.getMonth() + 1) * 100) + this.getDate();
    }
}

if (typeof Date.prototype.getWeekWithYear !== "function") {
    Date.prototype.getWeekWithYear = function () {
        var year = this.getFullYear();
        var b = this.getDayOfYear() + (4 - this.getDay());
        var a = new Date(this.getFullYear(), 0, 1);
        var c = (7 - a.getDay() + 4);
        var week = Math.ceil((b - c) / 7) + 1;

        return (year * 100) + week;
    };
}

// .format("YYYY-MM-DDTHH:mm");
// Always override the toJSON function
Date.prototype.toJSON = function () {
    return dayjs(this).format("YYYY-MM-DDTHH:mm");
}

Date.prototype.dateFormat = function (b) {
    if (b == "unixtime") {
        return Math.round(this.getTime() / 1000);
    }
    if (Date.formatFunctions[b] == null) {
        Date.createNewFormat(b);
    }
    var a = Date.formatFunctions[b];
    return this[a]();
};

Date.prototype.getTimezone = function () {
    return this.toString().replace(/^.*? ([A-Z]{3}) [0-9]{4}.*$/, "$1").replace(/^.*?\(([A-Z])[a-z]+ ([A-Z])[a-z]+ ([A-Z])[a-z]+\)$/, "$1$2$3");
};

Date.prototype.getGMTOffset = function () {
    return (this.getTimezoneOffset() > 0 ? "-" : "+") + String.leftPad(Math.floor(Math.abs(this.getTimezoneOffset()) / 60), 2, "0") + String.leftPad(Math.abs(this.getTimezoneOffset()) % 60, 2, "0");
};

Date.prototype.getDayOfYear = function () {
    var a = 0;
    Date.daysInMonth[1] = this.isLeapYear() ? 29 : 28;
    for (var b = 0; b < this.getMonth(); ++b) {
        a += Date.daysInMonth[b];
    }
    return a + this.getDate();
};

Date.prototype.getWeekOfYear = function () {
    var b = this.getDayOfYear() + (4 - this.getDay());
    var a = new Date(this.getFullYear(), 0, 1);
    var c = (7 - a.getDay() + 4);
    return String.leftPad(Math.ceil((b - c) / 7) + 1, 2, "0");
};

Date.prototype.isLeapYear = function () {
    var a = this.getFullYear();
    return ((a & 3) == 0 && (a % 100 || (a % 400 == 0 && a)));
};

Date.prototype.getFirstDayOfMonth = function () {
    var a = (this.getDay() - (this.getDate() - 1)) % 7;
    return (a < 0) ? (a + 7) : a;
};

Date.prototype.getLastDayOfMonth = function () {
    var a = (this.getDay() + (Date.daysInMonth[this.getMonth()] - this.getDate())) % 7;
    return (a < 0) ? (a + 7) : a;
};

Date.prototype.getDaysInMonth = function () {
    Date.daysInMonth[1] = this.isLeapYear() ? 29 : 28;
    return Date.daysInMonth[this.getMonth()];
};

Date.prototype.getSuffix = function () {
    switch (this.getDate()) {
        case 1:
        case 21:
        case 31:
            return "st";
        case 2:
        case 22:
            return "nd";
        case 3:
        case 23:
            return "rd";
        default:
            return "th";
    }
};

String.escape = function (a) {
    return a.replace(/('|\\)/g, "\\$1");
};

String.leftPad = function (d, b, c) {
    var a = String(d);
    if (c == null) {
        c = " ";
    }
    while (a.length < b) {
        a = c + a;
    }
    return a;
};

Date.daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
Date.monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
Date.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
Date.y2kYear = 50;
Date.monthNumbers = {
    Jan: 0,
    Feb: 1,
    Mar: 2,
    Apr: 3,
    May: 4,
    Jun: 5,
    Jul: 6,
    Aug: 7,
    Sep: 8,
    Oct: 9,
    Nov: 10,
    Dec: 11
};
Date.patterns = {
    ISO8601LongPattern: "Y-m-d H:i:s",
    ISO8601ShortPattern: "Y-m-d",
    ShortDatePattern: "n/j/Y",
    LongDatePattern: "l, F d, Y",
    FullDateTimePattern: "l, F d, Y g:i:s A",
    MonthDayPattern: "F d",
    ShortTimePattern: "g:i A",
    LongTimePattern: "g:i:s A",
    SortableDateTimePattern: "Y-m-d\\TH:i:s",
    UniversalSortableDateTimePattern: "Y-m-d H:i:sO",
    YearMonthPattern: "F, Y"
};

// -----------

// Extend window

Window.prototype.addEvent = function (type: string, fn: Function) {
    if (!this) {
        throw("Null reference exception");
    }

    if (this.addEventListener) {
        this.addEventListener(type, fn, false);
    } else if (this.attachEvent) {
        this["e" + type + fn] = fn;
        this[type + fn] = function () {
            this["e" + type + fn](window.event);
        }
        this.attachEvent("on" + type, this[type + fn]);
    } else {
        this["on" + type] = this["e" + type + fn];
    }
}


/// Requests

enum CmsFetchState {
    Pending,
    Connecting,
    Connected,
    Done,
    Cancelled,
    Exception = -1
}

class CmsFetchOptions {
    url?: string = null;                     // url to call
    data?: any = null;                      // data to post
    loadIndicator?: boolean = true;         // true|false - show default loading indicator
    onStart?: Function = null;              // function() - Before post
    onProgress?: Function = null;
    onHandleMessage?: Function = null;      // function(payload) - handle the message manually
    onConnect?: Function = null;            // If sse connection is up, before first read
    onStreamMessage?: Function = null;      // function(payload)
    onSucces?: Function = null;             // function(data) - when response.Succes == true. After this, onDone wont be called
    onCancelled?: Function = null;          // function(request) - when cancelled occurs. Cancel breaks the promise workflow
    onError?: Function = null;              // function(data) - event when there is an errormessage
    //onDone: Function = null;              // function(response) - on done event
    onFail?: Function = null;               // function(xhr) - on fail event
    onFinish?: Function = null;             // function() - after done, or failed, onfished is called,
    onBeforeSend?: Function = null;         // function(data)
    onLoadSuccess?: Function = null;        // function(data, formdata)
    showAlertOnFalse?: Boolean = true;      // true|false
    ignoreExpire?: Boolean = false;
    globalErrorHandler?: Boolean = true;    // enable fallback for error handling
    timeout?: number = null;
    abort?: Function = null;                // Abort() should be set by the ajax function
    headers?: Object = null;
    sseAllowRetry?: boolean = false;        // Should SSE be allowed to retry connecting?
    allowRetry?: boolean = false;
    allow502retry?: boolean = true;         // Allow retry on 502, disable if retry is implemented manually
    allow502retryMax?: number = 5;          // Maximum number of retries
    onSseRetry: Function = null;            // Callback on retry
    fixResponse?: boolean = true;           // Response handling should fix the response data
}

class CmsSseMessage {
    id?: string = null;
    retry?: number = null;
    data?: string = "";
    event?: string = 'message';
}

class CmsAjaxOptions extends CmsFetchOptions {}

class CmsFetchRequest implements ICmsFetchRequest {
    self: any = null;
    state: CmsFetchState = null

    retry: number = 5000;
    options: CmsFetchOptions = null;
    response: CmsVerboseResponse = null;
    handled?: boolean = false;

    onStart?: Function = null;
    onDone?: Function = null;

    // abort?: Function = null;
    _controller: any = null;
    _signal: any = null;
    _retry502Counter?: number;

    headers: HeadersInit = null;
}

class CmsAjaxRequest extends CmsFetchRequest {
    xhr: XMLHttpRequest = null;
    options: CmsAjaxOptions = null;
}

class CmsVerboseResponse implements ICmsVerboseResponse {
    Data: any = null;
    Succes?: boolean = false;
    Error: string = null;
    Redirect: string = null;
    ExpireHtml: string = null;
    sre: any = null;
    RestoreUrl: string = null;
    Cancelled?: boolean = null;
}

class CmsWaitTimer {
    seconds: number = 10;
    private _timer: number = null;

    onAfterWait: Function = null;
    onValidateWaitLonger: Function = null;
    onValidateWaitFailed: Function = null;
    onWait: Function = null;

    next() {
        let me = this;

        me.seconds = me.seconds - 1;

        // onValidateWaitLonger returns false
        if (!!me.onValidateWaitLonger && !!!me.onValidateWaitLonger()) {
            !!me.onValidateWaitFailed && me.onValidateWaitFailed();
        } else if (me.seconds <= 0) {
            !!me.onAfterWait && me.onAfterWait();
        } else {
            me._timer = setTimeout(() => me.next(), 1000);
            !!me.onWait && me.onWait(me.seconds);
        }
    };

    cancel() {
        var me = this;
        !!me._timer && clearTimeout(me._timer);
    }
}

// Local storage handler
class StorageHandler implements IStorageHandler {
    private _useSW: boolean;
    private _available: boolean;
    private _catchEventSetup: boolean;
    private _register : object = {};
    private _localCache: object = {};
    private _currentWindow: Window;

    constructor(window) {
        this._currentWindow = window;
        this.handleBroadcastEvent();
    }

    parseToJson(jsonString: string) {
        try {
            return JSON.parse(jsonString);
        } catch (err) {
            return null;
        }
    };

    mod: 'localstorage_available';

    available() {
        var me = this;

        // Test once if local storage is available
        if ((me._available || null) === null) {
            // Test if service worker is registered, with postMessage
            if (navigator.serviceWorker && navigator.serviceWorker.controller && navigator.serviceWorker.controller.postMessage) {
                me._useSW = true;
                me._available = true;
            } else {
                // Test if localStorage is available
                try {
                    var localStorage = me._currentWindow.localStorage;
                    localStorage.setItem(me.mod, me.mod);
                    localStorage.removeItem(me.mod);
                    me._available = true;
                } catch (e) {
                    me._available = false;
                }
            }
        }

        return me._available;
    };

    handleBroadcastEvent() {
        var me = this;

        if (me.available()) {
            if (me._catchEventSetup !== true) {
                me._catchEventSetup = true;

                // When a serviceworker is detected, updates for localstorage are send with a serviceworker message,
                // and the localstorage events are not registered. So, never set a localstorage value without using
                // the app.storage functions
                if (me._useSW) {
                    !!window.app && !!window.app.debug && console.log("handleBroadcastEvent setup with serviceworker");

                    navigator.serviceWorker.addEventListener('message', function(event) {
                        if (window.app && window.app.debug) console.log("handleBroadcastEvent message throught serviceworker", event.data);

                        if (event && event.data && event.data.type === "localStorage" && event.data.key) {
                            me._updateItem(event.data.key, event.data.value);
                            me.handleRegisteredCallback(event.data.key, event.data.value);
                        }
                    });
                } else {
                    !!window.app && !!window.app.debug && console.log("handleBroadcastEvent setup with localstorage");

                    // Use localStorage events
                    this._currentWindow.addEventListener('storage', function(event) {
                        me.handleStorageEvent.call(me, event);
                    }, false);
                }
            }

            return true;
        }
        return false;
    };

    handleStorageEvent(event) {
        var me = this;

        if (window.app && window.app.debug && (!event.key || event.key === "undefined")) {
            debugger;
        }

        if (event.key === me.mod) { return; }

        // window.app && window.app.debug && console.log("handleStorageEvent", event, event.key, (event.newValue ? me.parseToJson(event.newValue) : null), event);

        me.handleRegisteredCallback(event.key, (event.newValue ? me.parseToJson(event.newValue) : null));
    };

    handleRegisteredCallback(key, value) {
        var me = this;

        var callback = me._register ? me._register[key] : null;
        if (callback && typeof callback === "function") {
            callback(value);
        }
    }

    _updateItem(key, value) {
        if (this._useSW) {
            this._localCache[key] = value;
        }

        if (!value) {
            localStorage.removeItem(key);
        } else if (typeof value == "string") {
            localStorage.setItem(key, value);
        } else {
            localStorage.setItem(key, JSON.stringify(value));
        }
    };

    setItem(key, value) {
        this._updateItem(key, value);

        if (this._useSW) {
            navigator.serviceWorker.controller.postMessage({
                type: 'localStorage',
                key: key,
                value: value
            });
        }
    };

    getItem(key) {
        // When a service worker is being used, a local cache should be used for the values. When the cache is
        // empty, we will fetch the value from the localStorage
        var value = this._useSW
            ? this._localCache[key] ?? localStorage[key]
            : localStorage[key];

        if ((value || null) !== null) {
            return (value[0] === '{' || value[0] === '[')
                ? JSON.parse(value)
                : value;
        }

        return null;
    };

    register(key, callback) {
        if (this.available()) {
            // this.getItem(key);
            this.handleBroadcastEvent();

            if (typeof callback !== "function") { debugger; }
            (this._register || (this._register = {}))[key] = callback || null;
        }
    };

    clearRegister() {
        this._register = {};
    }
}

// CMS
class CmsToaster implements ICmsToaster {
    static readonly toastAttrId = "toastid";
    current = [];
    queue = [];
    timer = null;
    show(toast: any) {
        var me = this;
        var $existing= toast && toast.id && me.exists(toast);

        if ($existing && $existing.length > 0) {
            if (!!!toast.text) {
                // empty text with existing toast, then remove
                me._removeToast($existing.remove());
                return;
            }

            me.updateExisting(toast, $existing);
            return;
        }

        // add to queue
        me.queue.push(toast);
        me.updateView();
    };
    exists(toast) {
        var me = this, id = toast && toast.id;
        var existing = id && me.current.filter(function(item) { return $(item).attr(CmsToaster.toastAttrId) === id }).slice(0,1)[0];

        if (existing) {
            return $(existing);
        }
        return null;
    };
    updateExisting(toast, $existing) {
        var me = this;
        me._fillToast($existing, toast);
        me._updateTimer($existing, toast, !!toast.buttons);
    };
    updateView() {
        var me = this, peek, toastId, toast;

        // Get the first toast in list
        toast = me.queue.slice(0, 1)[0];
        toastId = toast && toast.id;

        // Match with current toast(s)
        var $existing = me.exists(toast);

        // Update current
        if ($existing && $existing.length > 0) {
            // Remove from queue
            toast = me.queue.shift();

            // Toast has text
            if (!!toast.text) {
                me.updateExisting(toast, $existing);
            } else {
                // No text, then remove toast and remove all with this id
                me._removeToast($existing);
                me.queue = me.queue.filter((t) => t.id !== toast.id);
                me.updateView();
            }

            return;
        }

        if (!!toast && me.current.length === 0) {
            toast = me.queue.shift();
            me._showToast(toast);
        }
    };

    _removeToast($current, onFadeDone?: Function) {
        var me = this;
        var index = me.current.indexOf($current[0]);
        if (index > -1) {
            // remove item from array
            me.current = me.current.filter(function(item) { return item != $current[0] });
        }

        $current.fadeOut(500, function () {
            $current.hide().remove();
            me.updateView();

            onFadeDone && onFadeDone();
        });
    };

    _showToast(toast) {
        var me = this;

        if ((toast.text || null) === null) {
            var $existing = toast.id &&
                me.current.filter(function() { return $(this).attr(CmsToaster.toastAttrId) === toast.id }).slice(0,1)[0];
            var index = $existing && me.current.indexOf($existing[0]);
            if (index > -1) {
                me.current.splice(index, 1);
            }
            return;
        }

        var $newElement = $("<div/>")
            .hide()
            .addClass("toast")
            .attr(CmsToaster.toastAttrId, toast.id)
            .data("toast", toast)
            .off("leave").on("leave", function (event) {
                event.preventDefault();
                var $toast = $(this).addClass("leaving");
                me._removeToast($toast, () => { me.updateView(); })
            })
            .off("click").on("click", function (event) {
                $(this).trigger("leave");
            });

        this._fillToast($newElement, toast);

        $("body").append($newElement);
        me.current.push($newElement[0]);
        me._updateTimer($newElement, toast, !!toast.buttons);

        // console.log("showToast", me.current);
    };

    _fillToast($div, toast) {
        var me = this, $span;
        var handle =  {
            toast: toast,
            $current: $div,
            _removeToast: me._removeToast,
            remove: function(onFade?: Function) {
                this._removeToast.call(me, this.$current, onFade);
            }
        };

        $div.empty().append($span = $("<span/>").append($("<span/>").text(toast.text)));

        if (toast.buttons) {
            $.each(toast.buttons, function(index, button) {
                var $button;
                $span.append($button = $("<button></button>").text(button.text));

                $button.data("rec", button).on("click", function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                    var button = $(this).data("rec");

                    handle.remove(() => {
                        button && button.onClick && button.onClick.call(handle);
                    })
                });
            });
        }
    };

    _updateTimer($current: JQuery, toast, persistent) {
        var me = this;
        clearTimeout(me.timer);

        $current.fadeIn(350, function () {
            me.timer = !persistent && setTimeout(
                function () {
                    $current.trigger("leave");
                },
                Math.max(2000, toast.duration));
        });
    }
}

class CmsLoadOnDemand implements ICmsLoadOnDemand {
    private _app : ICmsApp;

    constructor(app: ICmsApp) {
        this._app = app;
    }

    readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    };

    private _allowLoad: boolean = null;

    public get allowLoad() {
        return this._allowLoad;
    };

    // AllowLoad informes if the use set its preferences
    public set allowLoad(value: boolean) {
        this._allowLoad = value;
        this.onLoad(null);
    };

    queue = [];
    onQueued = null;

    validate() {
        var me = this;
        var cookie = me.readCookie("ac");
        if (!!!cookie || cookie.length === 0) {
            me.allowLoad = false;
        } else {
            var settings = me._app.parseJwt(cookie);
            me.allowLoad = !!settings && !!settings.v;
        }
    }

    onLoad(callback) {
        var me = this;
        if (me.allowLoad === true) {
            var settings = me._app.parseJwt(me.readCookie("ac"));

            if (me.queue.length > 0) {
                var item;
                while ((item = me.queue.shift())) {
                    !!item && item(settings);
                }
            }

            // Optional call on done
            !!callback && callback(settings);
        } else if (!!callback) {
            me.queue.push(callback);

            !!me.onQueued && me.onQueued.call(me);
        }
    }
}

class CmsLocation {
    latitude: number = null;
    longitude: number = null;
    denied: boolean = false;
    error: boolean = false;
    requested: boolean = false;
    available: boolean = false;
    errorMsg: string = null;
    done: boolean = false;
}

class CmsLocationHelper implements ICmsLocation {
    private _location = new CmsLocation();

    getLocation() {
        var loc = this._location;

        return loc.available
            ? {latitude: loc.latitude, longitude: loc.longitude}
            : null;
    };

    requestLocation(callback) {
        var me = this;

        var loc = me._location;
        loc.error = false;
        loc.done = false;

        if ((navigator || null) !== null && (navigator.geolocation || null) !== null) {
            loc.requested = true;
            navigator.geolocation.getCurrentPosition(
                function (position) {
                    me.responseLocation(position, callback);
                },
                function (error) {
                    me.responseError(error, callback);
                });
        } else {
            loc.requested = false;
        }
    };

    responseError(error, callback) {
        var me = this;

        var loc = me._location;
        loc.error = true;
        loc.available = false;
        loc.done = true;

        switch (error.code) {
            case error.PERMISSION_DENIED:
                loc.errorMsg = "User denied the request for Geolocation."
                loc.denied = true;
                break;
            case error.POSITION_UNAVAILABLE:
                loc.errorMsg = "Location information is unavailable."
                break;
            case error.TIMEOUT:
                loc.errorMsg = "The request to get user location timed out."
                break;
            case error.UNKNOWN_ERROR:
                loc.errorMsg = "An unknown error occurred."
                break;
        }

        if (callback) {
            callback(loc);
        }
    };

    responseLocation(position, callback) {
        var me = this;

        var loc = me._location;
        loc.available = true;
        loc.done = true;
        loc.latitude = position.coords.latitude;
        loc.longitude = position.coords.longitude;

        if (callback) {
            callback(loc);
        }
    }
};

// CMS related
class CmsConverter {
    app: ICmsApp = null;
    rxDate = /^[0-9]{4}-[0-9]{2}-[0-9]{2}T?/i;

    constructor(app) {
        this.app = app;
    }

    to = function(valuetype, value): any {
        if (!valuetype) return value;

        switch (valuetype.toLocaleLowerCase()) {
            case "integer":
            case "float":
            case "double":
            case "valuta":
            case "currency":
            case "percentage":
            case "number":
            case "decimal":
                return (typeof(value) === "string" && value.length > 0)
                    ? this.numberStringToNumber(value.slim())
                    : value;
            case "date":
                return this.toDate(value);
            case "time":
                return this.toTime(value);
            case "boolean":
                return this.toBoolean(value);
            case "string":
                return String(value);
        }

        return value;
    };

    toNumber = function(value) : number {
        return Number(value);
    };

    toBoolean(value) {
        if (!value) {
            return false;
        }
        if (value === 1 || value === true) {
            return true;
        }

        value = ("" + value).toUpperCase();
        return value === "TRUE" || value === "ON" || value === "1";
    };

    // converts the value to a HH:MM string
    toTime(val) {
        var app = this.app;

        function isNumeric(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }

        if (!val || (val || null) === null || val == '') {
            return null;
        } else if (val instanceof Date) {
            return app.formatting.time(val);
        } else if (app.validators.timespan.test(val)) {
            return app.convert.timespanToTime(val);
        } else if (app.validators.dateSerialized.test(val)) {
            return app.formatting.time(new Date(parseFloat(app.validators.dateSerialized.exec(val)[1])))
        } else if (app.validators.iso8601.test(val)) {
            return new Date(val);
        } else if (app.validators.time.test(val)) {
            return val;
        } else if (isNumeric(val) && val >= 0 && val < 24) {
            return app.formatting.time("{0}:{1}".format(Math.floor(val), 60 * (val % 1)));
        } else {
            return null
        }
    };

    timeToFloat(val) {
        if (!val || !!!(/^[0-9]{1,2}\:[0-9]{2}$/.test(val))) return null;

        var m = dayjs(val, "HH:mm");
        if (!!!m || !m.isValid()) return null;

        var hour = (m ? m.hour() : 0) || 0;
        var minute = (m ? m.minute() : 0) || 0;
        return (hour%24) + ((minute%60) / 60);
    };

    isDate(obj) {
        return !!obj && !!obj.getDate;
    };

    toDate(val, format: string) {
        var m;
        var app = this.app;

        if (!!!val) {
            return null;
        } else if (this.isDate(val)) {
            return val;
        } else if (val.toDate) {
            return val.toDate();
        } else if ((m = dayjs(val, (format || app.dateFormat))).isValid()) {
            return m.toDate();
        } else if ((m = dayjs.utc(val, "YYYY-MM-DDTHH:mm:ssZ", true)).isValid()) {
            return m.toDate();
        } else if ((m = dayjs.utc(val, "YYYY-MM-DDTHH:mm:ss.SSSSSZ", true)).isValid()) {
            return m.toDate();
        } else if ((m = dayjs(val)).isValid()) {
            return m.toDate();
        }

        return null;
    }

    timespanToTime(jsonTime) {
        var app = this.app;

        var timeArray = app.validators.timespan.exec(jsonTime);

        if (!timeArray || timeArray.length === 0) {
            return null;
        }

        return ("00" + parseInt(timeArray[1] || "0")).substr(-2) + ":" + ("00" + parseInt(timeArray[3] || "0")).substr(-2);
    }

    fixDate(val) {
        var app = this.app;
        var m;

        if (!!!val) {
            return null;
        } else if ((m = dayjs(val)).isValid()) {
            return m.toDate();
        } else if (typeof val === "string") {
            if (this.rxDate.test(val) && (m = dayjs(val)).isValid) {
                return m.toDate();
                // @ts-ignore
            } else if ((m = dayjs.utc(val, "YYYY-MM-DDTHH:mm:ssZ", true)).isValid()) {
                return m.toDate();
                // @ts-ignore
            } else if ((m = dayjs.utc(val, "YYYY-MM-DDTHH:mm:ss.SSSSSZ", true)).isValid()) {
                return m.toDate();
            }
        }

        return val;
    };

    private prepend(value, array) {
        var newArray = array.slice();
        newArray.unshift(value);
        return newArray;
    }

    numberStringToNumber(value) {
        var isString = typeof value === "string";
        if (!isString) return NaN;
        // Don't use or to get a "default" value, cause 0 will always be set to undefined
        if (value.length === 0) return NaN;

        var decimal = this.app.currencyFormat.d || ",";
        var group = this.app.currencyFormat.g || ".";
        var groupSize = this.app.currencyFormat.gs || 3;
        var rxInt = /[0-9]/;
        var groupCnt = 0;
        var hasDecimal = false;
        var hasGroup = false;
        var hasNegative = false;
        var nr = 0;
        var error = false;

        var output = [];

        for (var c = value.length - 1; c >= 0; c--) {
            var char = value[c];

            if (hasNegative) {
                error = true;
                break;
            } else if (char === decimal) {
                if (hasDecimal || hasGroup) {
                    error = true;
                    break;
                }
                hasDecimal = true;
                groupCnt = 0;
                char = ".";
            } else if (char === group) {
                if (groupSize <= 0 || groupCnt !== groupSize) {
                    error = true;
                    break;
                }
                hasGroup = true;
                groupCnt = 0;
                continue;
            } else if (char === '-') {
                hasNegative = true;
            } else if (!rxInt.test(char)) {
                error = true;
                break;
            } else {
                nr++;
                groupCnt++;
            }

            output = this.prepend(char, output)
        }

        return !error && output.length > 0 ? Number.parseFloat(output.join("")) : null;
    }

    round(value: number, digits: number) : number {
        return value && +(value.toFixed(digits));
    }
}

class CmsFormatter {
    app: ICmsApp = null;

    constructor(app) {
        this.app = app;
    }

    _isNaN(value) {
        return typeof value !== "number";
    }

    maskformat(value: any, pattern: string): string {
        var i = 0,
            v = value.toString(),
            output = "";

        for (var c=0; c<pattern.length; c++) {
            if (i >= v.length) break;

            switch (pattern[c]) {
                case '#':
                    output += v[i++];
                    break;
                default:
                    if (v[i] == pattern[c]) i++; /* skip matching character in value */
                    output += pattern[c];
                    break;
            }
        }

        return output;
    };

    secondsToMinutes(fVal: number) {
        var s = 0;
        fVal = fVal || 0;
        return Math.floor(fVal / 60) + ':' + ((s = fVal % 60) < 10 ? '0' + s : s);
    };

    percentage(fVal) {
        if (this._isNaN(fVal)) return '';
        var val = parseFloat(fVal || 0);
        return (val % 1 === 0 ? val : val.toFixed(2)) + "%";
    };

    integer(fVal) {
        var app = this.app;
        if (this._isNaN(fVal)) return '';
        return ""+parseInt(fVal);
    }

    float(fVal) {
        var app = this.app;
        if (this._isNaN(fVal)) return '';
        return app.numberFormat.float(fVal);
    }

    decimal(fVal) {
        var app = this.app;
        if (this._isNaN(fVal)) return '';
        return app.numberFormat.decimal(fVal);
    };

    number(fVal) {
        var app = this.app;
        if (this._isNaN(fVal)) return '';
        return app.numberFormat.format(fVal);
    };

    currency(fVal) {
        var app = this.app;
        if (this._isNaN(fVal)) return '';

        var formatting = app.currencyFormat;
        var f = parseFloat(fVal || 0).currencyFormat(formatting.n, formatting.s, formatting.d);
        return (f.length > 0 ? formatting.c + " " + f : f);
    };

    datetime(val) {
        var app = this.app;
        var me = this;
        var d = (typeof val === "string" ? app.date(val, me.dateFormatToParser(app.datetimeFormat)) : val || null);
        return this.dateFormat(d, app.datetimeFormat);
    };

    date(val) {
        var me = this;
        var app = me.app;
        var d = (typeof val === "string" ? app.date(val, me.dateFormatToParser(app.dateFormat)) : val || null);
        return this.dateFormat(d, app.dateFormat);
    };

    time(val) {
        var me = this;
        var app = me.app;
        var d = (typeof val === "string" ? app.date(val, me.dateFormatToParser(app.timeFormat)) : val || null);
        return this.dateFormat(d, app.timeFormat);
    };

    boolean(value) {
        return this.app.convert.toBoolean(value);
    }

    padding(value: number, padding: number) {
        return String.leftPad(value, padding, "0");
    }

    // Format a date to a string
    dateFormat(dateVal, format) {
        var m = dayjs(dateVal);
        return m.isValid() ? m.format(format) : "";
    };

    isDate(obj) {
        return (!!obj && typeof obj === "object" && !!obj.getDate);
    };

    format(value, valuetype) {
        var result = "";
        var isDate = false;

        if (!!!valuetype && this.isDate(value)) {
            valuetype = "datetime";
            isDate = true;
        }

        switch ((valuetype || "").toLocaleLowerCase()) {
            case "bool":
            case "boolean":
                result = this.boolean(value) ? "true" : "false";
                break;
            case "percentage":
                result = this.percentage(value);
                break;
            case "decimal":
                result = this.decimal(value);
                break;
            case "number":
            case "double":
            case "float":
                result = this.number(value);
                break;
            case "currency":
            case "valuta":
                result = this.currency(value);
                break;
            case "integer":
                result = this.integer(value);
                break;
            case "datetime":
                result = this.datetime(value);
                break;
            case "date":
                result = this.date(value);
                break;
            case "time":
                result = this.time(value);
                break;
            case "week":
                var dt = this.isDate(value) ? value: this.app.convert.toDate(value);
                result = !!dt && dayjs(dt).isoWeek();
                break;
            default:
                result = value;
                break;
        }

        return result;
    };

    dateFormatToDateTimePickerFormat = function(format) {
        if (!!!format) {
            console.warn("Format for dateFormatToDateTimePickerFormat not set");
            return "";
        }

        var output = "", used = [], c, o;
        for (var i = 0; i < format.length; i++) {
            c = format[i];
            o = '';
            switch (c) {
                case "Y":
                    if (used.indexOf(c) < 0) { used.push(c); o = "Y"; }
                    break;
                case "M":
                    if (used.indexOf(c) < 0) { used.push(c); o = "m"; }
                    break;
                case "D":
                    if (used.indexOf(c) < 0) { used.push(c); o = "d"; }
                    break;
                case "H":
                    if (used.indexOf(c) < 0) { used.push(c); o = "h"; }
                    break;
                case "i":
                case "m":
                    if (used.indexOf(c) < 0) { used.push(c); o = "i"; }
                    break;
                default:
                    o = c;
                    break;
            }
            output += o;
        }

        return output;
    };

    dateFormatToParser(format: string) {
        if (!!!format) {
            console.warn("Format for dateFormatToParser not set");
            return "";
        }
        var parserformat = "";

        for (var i = 0; i < format.length; i++) {
            if (format[i] == '%' && i < format.length) {
                // read next character
                switch (format[i + 1]) {
                    case 'd':   // day
                        parserformat += "d";
                        i++;
                        break;
                    case 'm':   // month
                        parserformat += "m";
                        i++;
                        break;
                    case 'Y':
                    case 'y':   // year
                        parserformat += "Y";
                        i++;
                        break;
                    case 'H':
                    case 'h':   // hour
                        parserformat += "H";
                        i++;
                        break;
                    case 'M':   // minutes
                        parserformat += "i";
                        i++;
                        break;
                    case 's':
                    case 'S':   // seconds
                        parserformat += "S";
                        i++;
                        break;
                    default:
                        parserformat += format[i];
                        break;
                }
            } else {
                parserformat += format[i];
            }
        }

        return parserformat;
    };
}

class CmsFocusTracker implements ICmsFocusTracker {
    _EventBlur = "documentBlur";
    _EventFocus = "documentFocus";
    // todo: add to queue if not focussed
    // or limit timeout on not focussed
    hasFocus = true;

    constructor() {
        var me = this;

        if (/*@cc_on!@*/false) { // check for Internet Explorer
            document.onfocusin = function () {
                me.onFocus();
            };
            document.onfocusout = function () {
                me.onBlur();
            };
        } else {
            window.onfocus = function () {
                me.onFocus();
            };
            window.onblur = function () {
                me.onBlur();
            };
        }

        if (document.hasFocus() == false) {
            this.onBlur();
        } else {
            this.onFocus();
        }
    };

    onBlur() {
        $("body").addClass("blurred").trigger(this._EventBlur);
        $("." + this._EventBlur).triggerHandler(this._EventBlur);
    };

    onFocus() {
        $("body").removeClass("blurred").trigger(this._EventFocus);
        $("." + this._EventFocus).triggerHandler(this._EventFocus);
    };
}

enum ReadyStates {
    loaded = "loaded",
    complete = "complete",
    interactive = "interactive"
}

class CmsMessages implements ICmsMessages {
    Decimal: string;
    DecimalBeforeNegative: string;
    DecimalDecimalPlacement: string;
    DecimalGrouping: string;
    DecimalUnexpected: string;
    UnknownError: string;
    color: string;
    email: string;
    integer: string;
    phone: string;
    postcodeNl: string;
    regexMatch: string;
    required: string;
    time: string;
    date: string;
    RequestCancelled: "Request was cancelled"
}

class CmsAppExtend  {
    constructor() {
        this.form = {
            onFormLoaded: null,
            onBeforeAjax: null
        };
    }

    onAjaxBefore: Function;
    onAjaxDone: Function;
    onAjaxFail: Function;
    form: {
        onFormLoaded: Function,
        onBeforeAjax: Function
    };
}

class CmsColor implements ICmsColor{
    colourIsLight(r, g, b) {
        // Counting the perceptive luminance
        // human eye favors green color...
        var a = 1 - (0.299 * r + 0.587 * g + 0.114 * b) / 255;
        return (a < 0.5);
    };

    contrastColorLight(color) {
        var rgb = null;
        if (typeof color === "string") {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(color);
            rgb = [
                parseInt(result[1], 16),
                parseInt(result[2], 16),
                parseInt(result[3], 16)
            ];
        }

        return this.colourIsLight.apply(this, rgb);// ? 'black' : 'white';
    };

    stringToHslColor(str, s, l) {
        var hash = 0;
        for (var i = 0; i < str.length; i++) {
            hash = str.charCodeAt(i) + ((hash << 5) - hash);
        }

        var h = hash % 360;
        return this.hsl2hex(h, s, l);
    }

    hsl2rgb(h,s,l)
    {
        let a=s*Math.min(l,1-l);
        let f= (n,k=(n+h/30)%12) => l - a*Math.max(Math.min(k-3,9-k,1),-1);
        return [f(0),f(8),f(4)];
    }

    hsl2hex(h,s,l)
    {
        let a=s*Math.min(l,1-l);
        let f= (n,k=(n+h/30)%12) => l - a*Math.max(Math.min(k-3,9-k,1),-1);
        //return [f(0),f(8),f(4)];
        return "#{0}{1}{2}".format(this.getHex(f(0)), this.getHex(f(1)), this.getHex(f(2)));
    }

    getHex(i: number) {
        return this.pad(parseInt(""+(255*(i||0))%255).toString(16), 2);
    }

    pad(num, size) {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    }
}

class CmsSettings implements ICmsSettings {
    requestTimeout?: number;
}

// function cmsApp() {
class cmsApp implements ICmsApp {
    constructor(window) {
        var me = this;
        me.window = window;
        me.isIE = /(trident)|(msie)/ig.test(navigator.userAgent || navigator.appVersion);
        me.storage = (window.storageHandler || (window.storageHandler = new StorageHandler(window)));
        me.loadOnDemand = new CmsLoadOnDemand(me);
        me.geo = new CmsLocationHelper();
        me.extend = new CmsAppExtend();
        me.color = new CmsColor();
        me.sw = ((navigator && navigator.serviceWorker && navigator.serviceWorker.controller) || null) !== null;

        me.numberFormat = {
            format: function (number) {
                var cf = me.currencyFormat;
                var f = (number || 0).currencyFormat(2, cf.s, cf.d);
                return f;
            },
            decimal: function (number) {
                var cf = me.currencyFormat;
                var cnt = (number || 0).countDecimals();
                var f = (number || 0).currencyFormat(cnt, cf.s, cf.d);
                return f;
            },
            float: function (number) {
                var cf = me.currencyFormat;
                var cnt = (number || 0).countDecimals();
                var f = (number || 0).currencyFormat(cnt, "", cf.d);
                return f;
            }
        }
        me.convert = new CmsConverter(me);
        me.formatting = new CmsFormatter(me);
        me.focus = new CmsFocusTracker();
        me.settings = new CmsSettings();

        me.bodyScrollAllowed = true;
        $("body").on("mousewheel", function (event, delta) {
            if (me.bodyScrollAllowed === false)
                event.preventDefault();
        });

        me.currencyFormat = {
            d: ".",
            s: ",",
            n: 2,
            c: "€"
        };

        document.documentElement.setAttribute("data-browser", navigator.userAgent || "");
    };

    onReady = function() {
        var window = this.window;
        var currentPending = !!window.appReady && window.appReady;

        // Replace the appReady with a new array
        window.appReady = [];
        window.appReady.handleNext = function() {
            if (this.length < 1) return false;

            var next = this.pop();
            try {
                !!next && next.call(window);
            } catch {}

            this.handleNext();
        };
        var originalPush = window.appReady.push;
        window.appReady.push = function(value) {
            originalPush.call(window.appReady, value);
            this.handleNext();
        };

        !!currentPending && currentPending.forEach((v) => window.appReady.push(v));
    };

    _instance = null;

    // is bodyScrollAllowed still relevant?
    window = null;
    settings = null;
    color: ICmsColor = null;
    sw: boolean = false;
    bodyScrollAllowed: boolean;
    messages = null;
    isIE: boolean;
    loadOnDemand = null;
    storage = null;
    debug = false;
    cookieAvailableCheck = false;
    // unloading = false;
    dateFormat = null;
    datetimeFormat = null;
    timeFormat = null;
    scriptUID = null;
    onAlert = null;
    geo = null;
    _uniId = 0;
    numberFormat = null;
    // @deprecated
    basePath = null;
    convert = null;
    formatting = null;
    focus = null;
    // Language selected (used from dateTimePicker)
    lang = null;

    uuidv4 = function() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    };

    browser = {
        _supportsColorpicker: null,
        supportsColorpicker() {
            var colorInput;
            return this._supportsColorpicker ||
                (this._supportsColorpicker = (colorInput = $('<input type="color" value="!" />')[0]) != null && colorInput.type === 'color' && colorInput.value !== '!');
        },
        svg() {
            return document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Shape", "1.0");
        }
    };

    validators = {
        pc6: /^[0-9]{4}[\s\xA0]{0,1}[a-z]{2}$/i,
        iso8601: /(\d{4})-(\d{2})-(\d{2})T(\d{2})\:(\d{2})\:(\d{2})[+-](\d{2})\:(\d{2})/,
        timespan: /^-?P(?:\d+D)?(?:T(?:(\d+)H)?((\d+)M)?(([0-9\.]+)S)?)?$/,
        time: /^([0-9]{2})\:([0-9]{2})/,
        dateSerialized: /Date\(([^)]+)\)/,
        // dateISO: /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*))(?:Z|(\+|-)([\d|:]*))?$/,
        dateISO: /^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/,
        reMsAjax: /^\/Date\((d|-|.*)\)[\/|\\]$/,
        email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    };

    requires(validate, src, callback) {
        if (!!!src) return;

        // TODO: return promise?
        let app = this;
        let loadScript = app.loadScript;

        if (typeof (validate) === 'function' && validate()) {
            callback();
        } else if (validate === true) {
            callback();
        } else {
            if (Array.isArray(src)) { //} $.isArray(src)) {
                var loop = {
                    index: -1,
                    src: src,
                    callback: callback,
                    run: function () {
                        this.next();
                    },
                    next: function () {
                        let me = this;
                        if (me.index >= me.src.length - 1) {
                            me.callback();
                        } else {
                            me.index++;
                            loadScript.call(app, me.src[me.index], function () {
                                me.next()
                            });
                        }
                    }
                };
                loop.run();
            } else {
                loadScript.call(app, src, callback);
            }
        }
    };

    loadScript(src, callback) {
        let app = this;

        if (/(\.css)$/i.test(src)) {
            // <link type="text/css" rel="stylesheet" href="/theme/wm/css/default.css?v=20240905212109">
            var css = document.createElement("link");
            css.type = "text/css";
            css.setAttribute("rel", "stylesheet");
            css.href = src;
            app.loadAsset.call(app, css, callback);
        } else {
            var script = document.createElement("script");
            script.type = "text/javascript";
            script.src = src;
            app.loadAsset.call(app, script, callback);
        }
    };

    loadAsset(element, callback) {
        if (callback) {
            if (element.readyState) {  //IE
                element.onreadystatechange = function () {
                    if (element.readyState == ReadyStates.loaded|| element.readyState == ReadyStates.complete) {
                        element.onreadystatechange = null;
                        callback();
                    }
                };
            } else {  //Others
                element.onload = function () {
                    callback();
                };
            }
        }

        document.getElementsByTagName("head")[0].appendChild(element);
    };

    parseURL(url: string) {
        /* https://www.abeautifulsite.net/parsing-urls-in-javascript */
        var parser = document.createElement('a'),
            searchObject = {},
            queries, split, i;

        // Let the browser do the work
        parser.href = url || window.location.href;

        // Convert query string to object
        queries = parser.search.replace(/^\?/, '').split('&');
        for (i = 0; i < queries.length; i++) {
            if (queries[i].length === 0) {
                continue;
            }
            split = queries[i].split('=');
            searchObject[split[0]] = split[1] || "";
        }

        return {
            protocol: parser.protocol,
            host: parser.host,
            hostname: parser.hostname,
            port: parser.port,
            pathname: parser.pathname,
            search: parser.search,
            searchObject: searchObject,
            hash: parser.hash,
            _query: function () {
                var query = "";
                $.each(this.searchObject, function (key: string, value: string) {
                    if (query.length > 0) {
                        query += "&";
                    }
                    query += encodeURIComponent(key) + "=" + encodeURIComponent(value);
                });
                return query;
            },
            addQuery: function (key, value) {
                searchObject[key] = value;
                this.search = this._query();
            },
            toUrl: function () {
                var query = this._query();

                return this.protocol + "//" + this.host +
                    ((this.pathname || "")[0] === '/' ? '' : '/') + (this.pathname || "") +
                    ((query && query.length > 0) ? "?" + query : "") +
                    ((this.hash && this.hash.length > 0) ? this.hash : "");
            },
            toRelativeUrl: function () {
                var query = this._query();

                //return this.protocol + "//" + this.host +
                return ((this.pathname || "")[0] === '/' ? '' : '/') + (this.pathname || "") + //(this.pathname || "/") +
                    ((query && query.length > 0) ? "?" + query : "") +
                    ((this.hash && this.hash.length > 0) ? this.hash : "");
            }
        };
    };

    resolveValue(obj) {
        if (obj && typeof obj === "function") {
            var args = arguments.length > 1 ? Array.prototype.slice.call(arguments, 1) : null; /* Fix for IE11 */
            return obj.apply(this, args);
            // return obj.apply(this, (arguments.length > 0 ? Array.from(arguments).slice(1) : undefined));
        }
        return obj;
    };

    trigger($elements, eventType) {
        // A custom event that will be created
        var event;

        if (document.createEvent) {
            event = document.createEvent("HTMLEvents");
            event.initEvent(eventType, true, true);
        } else {
            // IE
            event = document.createEventObject();
            event.eventType = eventType;
        }

        event.eventName = eventType;

        $.each($elements, function () {
            if (document.createEvent) {
                this.dispatchEvent(event);
            } else {
                this.fireEvent("on" + event.eventType, event);
            }
        });
    };

    confirmNavigate(onTrue) {
        // TODO: add translation
        if ((window.sweetAlert || null) !== null && onTrue) {
            // TODO: add translation?
            window.sweetAlert({
                    title: "Wait!",
                    text: "There are unsaved changed. Are you sure you want to discard them?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Discard",
                    closeOnConfirm: true
                },
                function () {
                    onTrue();
                });
        } else {
            if (!confirm("There are unsaved changed. Are you sure you want to discard them?")) {
                return false;
            } else if (onTrue) {
                onTrue();
            }

            return true;
        }
    };

    IEUploadFix(url) { /* deprecated */
        if (this.isIE === true) {
            $.ajax({
                //type: 'POST',
                url: url,
                async: false,
                cache: false
            });
        }
    };

    toaster = new CmsToaster();

    toast(data) {
        var options = {
            text: null,
            duration: 2000,
            buttons: null
        };

        if (typeof data === "string") {
            options.text = data;
        } else if (typeof data === "object") {
            options = $.extend(options, data);
        }

        this.toaster.show(options);
    };

    public get locationHash() {
        var hval;
        var hash = location.hash || "";
        if ((hash.length > 0 && hash != "#_=_") && (hash = hash.slice(1)).length > 0) {
            try {
                //return JSON.parse($.base64.decode(hash));
                return JSON.parse(window.atob(hash));
            } catch (err) {
            }
        }
        return null;
    };

    public set locationHash(obj) {
        // var hash = !obj ? "" : "#" + $.base64.encode(JSON.stringify(obj))
        var hash = !obj ? "" : "#" + window.btoa(JSON.stringify(obj))

        // Only change hash if changed
        if (document.location.hash != hash) {
            document.location.hash = hash;
        }
    };

    public parseJwt (token: string) : any {
        if (!!!token) return null;

        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));

        return JSON.parse(jsonPayload);
    };

    json = {
        toJSON(obj) {
            return JSON.stringify(obj);
        },
        parse(jsonString) {
            try {
                return JSON.parse(jsonString);
            } catch (err) {
                console.error("error on parsing json", jsonString);
                return null;
            }
        }
    };

    extend: CmsAppExtend = null;

    findObject(myArray, callback) {
        // myCallback should be a function to determine if we have a hit:
        // ([obj, obj, obj], function (obj) { return obj.Id == variation })
        var result = undefined;

        for (var item in myArray) {
            if (!myArray.hasOwnProperty(item)) {
                continue;
            }

            if (callback(item)) {
                result = item;
                break;
            }
        }

        return result;
    };

    alertBox(...args: any[]) {
        if (this.onAlert) { // Override error handling
            // @ts-ignore
            this.onAlert.apply(this, arguments);
        } else if ((window.sweetAlert || null) !== null) {
            window.sweetAlert.apply(this, arguments);
        } else if (arguments && !$.isPlainObject(arguments[0])) {
            this.alert(
                arguments.length > 0 ? arguments[0] : null,
                arguments.length > 1 ? arguments[1] : null,
                arguments.length > 2 ? arguments[2] : null);
        } else {
            console.warn('app.alertBox called without any arguments');
        }
    };

    alert(title, msg, icon) {
        alert(msg || title.text || title);
    };

    readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    };

    scroll = {
        bodyScrollAllowed: true,
        bodyTopLast: 0,
        allow(bValue) {
            var $body = $("body");

            if (bValue === true && this.bodyScrollAllowed === false) {
                this.bodyScrollAllowed = true;

                $body.removeClass("noScroll");
                if (document.body.scroll) {
                    // @ts-ignore
                    document.body.scroll = "yes"; // ie onl
                }
                $body.scrollTop(this.bodyTopLast);
            } else if (bValue === false && this.bodyScrollAllowed === true) {
                this.bodyScrollAllowed = false;
                this.bodyTopLast = $body.scrollTop();

                $body.addClass("noScroll").css('top', -1 * this.bodyTopLast + 'px');
                if (document.body.scroll) {
                    // @ts-ignore
                    document.body.scroll = "no"; // ie only
                }
            }
        },
        // TODO: move this to an extension
        preventScroll: function ($element) {
            if (($.event.special.mousewheel || null) === null)
                return;

            // load mousewheel script manually!!
            // http://stackoverflow.com/questions/5802467/prevent-scrolling-of-parent-element
            $element.bind('mousewheel', function (e, d) {
                // Uses jquery.mousewheel.js
                var t = $(this);

                if ($(e.toElement).is(t)) {
                    if (d > 0 && t.scrollTop() === 0) {
                        e.preventDefault();
                    } else {
                        if (d < 0 && (t.scrollTop() == t.get(0).scrollHeight - t.innerHeight())) {
                            e.preventDefault();
                        }
                    }
                }
            });
        },
    };

    uniqueId(prefix) {
        return (prefix || '') + 'app' + (this._uniId += 1);
    };

    currencyFormat = null;

    resolveUrl(relPath) {
        if (relPath[0] === "~") {
            return this.basePath + (relPath.length > 1 ? relPath.substring(1) : "");
        }
        return relPath;
    };

    // FIX: is this double implemented?
    cookie(key) {
        var cookies = document.cookie.split('; ');
        for (var i = 0, l = cookies.length; i < l; i++) {
            var parts = cookies[i].split('=');
            if (parts.shift() === key) {
                var cookie = parts.join('=');
                return cookie;
            }
        }
        return null;
    };

    // @deprecated
    fixDates(obj) {
        // Obsolete
        return this.fixResponse(obj);
    };

    fixDate(val) {
        return this.convert.fixDate(val);
    };

    fixResponse(obj) {
        var app = this;
        var m;

        if (obj && Array.isArray(obj)) {
            $.each(obj, function (index, item) {
                obj[index] = app.fixResponse(item);
            });
        } else if (typeof obj === "object") {
            // Doesnt fix it recursively
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    obj[prop] = app.fixResponse(obj[prop]);
                }
            }
        } else {
            var isString = typeof obj === "string";

            // Fix primatives
            if (obj === false) {
                return false;
            } else if (!!!obj) {
                return null;
            } else if (obj === true) {
                return true;
            } else if (typeof obj === "number") {
                return obj;
            } else if (!!obj && !!obj.getDate) { /* is date */
                return obj;
            } else if (isString && (m = dayjs.utc(obj, "YYYY-MM-DDTHH:mm:ssZ", true)).isValid()) {
                return m.toDate();
            } else if (isString && (m = dayjs.utc(obj, "YYYY-MM-DDTHH:mm:ss.SSSSSZ", true)).isValid()) {
                return m.toDate();
            } else if (isString && app.convert.rxDate.test(obj) && (m = dayjs(obj)).isValid()) {
                return m.toDate();
            } else if (isString && (m = dayjs(obj, app.datetimeFormat, true)).isValid()) {
                return m.toDate();
            } else if (isString && (m = dayjs(obj, app.dateFormat, true)).isValid()) {
                return m.toDate();
            }

            return obj;
        }

        return obj;
    };

    // Find recursive property in object
    hasProperty(propertyStr, data) {
        var app = this;

        if (!!!data || !!!propertyStr || propertyStr == '') {
            return undefined;
        }

        if (typeof propertyStr === "string") {
            var items = propertyStr.split(".");
            if (items.length > 1) {
                propertyStr = items;
            }
        }

        if (typeof propertyStr === "string") {
            // Return direct if hasOwnProperty returns ok
            if (data.hasOwnProperty(propertyStr)) {
                return data[propertyStr];
            }

            // First call with property string name
            return app.hasProperty(propertyStr.split('.'), data);
        } else if (Array.isArray(propertyStr)) {
            var dataRecursive;
            var indexer = null;

            if ((indexer = propertyStr[0].hasIndexerDescription()) !== null && data.hasOwnProperty(indexer[1])) {
                dataRecursive = data[indexer[1]][indexer[2]];
            } else if (!data.hasOwnProperty(propertyStr[0])) {
                return undefined;
            } else {
                dataRecursive = data[propertyStr[0]];
            }

            if (dataRecursive === undefined) {
                return undefined;
                // If this is not the final property requested
            } else if (propertyStr.length > 1) {
                // Get recursive
                if (typeof dataRecursive == "object") {
                    return app.hasProperty(propertyStr.slice(1), dataRecursive);
                } else {
                    return undefined;
                }
            } else {
                // Return the value
                return dataRecursive;
            }
        } else {
            return undefined;
        }
    };

    log(obj) {
        if (typeof console === "object") {
            console.log(obj);
        }
    };

    showGlobalError(obj, options?) {
        var me = this;
        options = options || {};

        if (typeof obj == "object" && (obj.Error || null) !== null) {
            if ((obj.InlineHtml || null) !== null) {
                // Inline html contains the html that handles the error
                var overlay = $(obj.InlineHtml).hide();
                $("body").append(overlay);
                overlay.fadeIn(500);

                // return true, cause the html should handle the messaging
                return true;
            } else {
                if (obj.Error && options.showAlertOnFalse) {
                    me.alertBox("Oops", obj.Error, "error");
                }
                return false;
            }
        } else {
            if (options.showAlertOnFalse) {
                me.alertBox("Oops", obj, "error");
            }
            return false;
        }
    };

    // Get date from string
    date(s, format) {
        return this.convert.toDate(s, format);
    };

    extendWithProperties(obj1: Object, obj2: Object) {
        if (!obj1) debugger;
        if (!obj2) debugger;

        // Merge none existing properties to options object
        for (var key in obj2) {
            if (!(key in obj1)) {
                obj1[key] = obj2[key];
            }
        }

        return obj1;
    };

    protected setLoadIndicator(request: CmsFetchRequest) {
        if (request.options.loadIndicator === true) {
            var $ind = null;

            request.onStart = function() {
                $ind = $('#loadingIndicator').clone();
                $("body").append($ind);
                $ind.show(150);
            }

            request.onDone = function() {
                if ($ind != null) {
                    $ind.remove();
                    $ind = null;
                }
            }
        }
    }

    fetch(options: CmsFetchOptions) {
// console.debug('fetch', options);
        let me = this;
        let request = new CmsFetchRequest();

        request.self = this;
        request.options = <CmsFetchOptions> me.extendWithProperties((options || {}), new CmsFetchOptions());
        request.handled = false;
        request.state = CmsFetchState.Pending;

        this.setLoadIndicator(request);

        // Allow extending
        me.extend?.onAjaxBefore && me.extend.onAjaxBefore.call(this, request.options);

        // Call onStart
        !!request.options.onStart && request.options.onStart();

        // Merge data to new object 
        request.options.data = me.extendWithProperties({}, request.options.data || {});

        // Call onBeforeSend to manipulate data object
        !!request.options.onBeforeSend && request.options.onBeforeSend(request.options.data);

        // TODO: isnt this double?
        !!request.onStart && request.onStart();

        let token;
        let headers = {
            "Accept": "text/event-stream",
            "Content-Type": "application/json",
            'Connection': 'keep-alive',
            "x-requested-with": "XMLHttpRequest"
        }

        if (!!me.sw) {
            headers["ngsw-bypass"] = "1";
        }

        // Add (or overwrite) headers from options
        if (!!options && !!options.headers) {
            $.each(me.resolveValue(options.headers), function(key: string, value: string) {
                headers[key] = value;
            });
        }

        request.headers = headers;

        return this.fetchRequestPromise(request);
    }

    // Parse the SSE message (chunk) to SseMessage object
    protected parseSseMessage(line: string) : CmsSseMessage {
        const FIELD_SEPARATOR = ":";
        let e = new CmsSseMessage();

        //line = line.trimRight();
        const index = line.indexOf(FIELD_SEPARATOR);

        if (index <= 0) {
            // Line was either empty, or started with a separator and is a comment.
            // Either way, ignore.
            return;
        }

        let field = line.substring(0, index).toLocaleLowerCase();
        if (!(field in e)) {
            return;
        }

        let value = line.substring(index + 1).trimLeft();
        if (field === 'data') {
            e[field] += value;
        } else if (field === 'retry') {
            e[field] = parseInt(value);
        } else {
            e[field] = value;
        }

        return e;
    };

    // Recursive calling function to handle Sse messages. Returns new promise while reading
    private fetchResponsePump(reader: ReadableStreamDefaultReader<Uint8Array>, request: CmsFetchRequest, onDone: Function, buffer?: string) {
        const me = this;
        const decoder = new TextDecoder('utf-8');
        const emptyLine = /\n{2}/;

        request.state = CmsFetchState.Connected;
        if (!!!buffer) buffer = "";

        return reader.read().then(({done, value}) => {
            let messageDone = false;
            let index;
            const text = decoder.decode(value);
            buffer += text;

            while ((index = buffer.search(emptyLine)) >= 0)  {
                const line = !!done && index < 0
                    ? buffer
                    : buffer.slice(0, index);
                buffer = buffer.slice(line.length+2);

                // console.log("handling buffer", {done: done, index: index, line:line, buffer: buffer});

                if (line.length <= 0)
                    continue;

                // We have a buffer, and following line is empty
                const e = me.parseSseMessage(line);

                // console.log("parseSseMessage", e);

                if (!!e) {
                    if (!!e.retry) {
                        // set retry timeout
                        request.retry = e.retry;
                    }

                    if (!!e.data && e.data.length > 0) {
                        const payload = me.json.parse(e.data) || null;

                        if (!!!payload)
                            debugger;

                        if (!!payload) {
                            if (!!payload.Ping) {
                                !!me.debug && console.log("Received ping", payload);
                            } else {
                                switch (payload.State || "Pending") {
                                    case "Done":
                                    case "Error":
                                        // Always push done or error message as result
                                        messageDone = true;
                                        request.response = payload;

                                        !!me.debug && console.debug(payload.State, request.response);
                                        break;
                                    case "Update":
                                    case "Started":
                                    case "Pending":
                                    default:
                                        !!request.options && !!request.options.onStreamMessage && request.options.onStreamMessage(payload)
                                        break;
                                }
                            }
                        }
                    }
                }

                // if (!!done && buffer.length == 0)
                //     break;
                if (!!messageDone)
                    break;
            }

            if (!!done || !!messageDone) {
                const retry = !!request.options.sseAllowRetry || !!request.options.allowRetry;


// console.debug("fetchResponsePump !!done || !!messageDone", request, { retry, done, messageDone });
                // console.log("fetchResponsePump", { done: done, messageDone: done, retry: retry });

                // console.log({done:done, messageDone:messageDone, retry:retry})
                // If message done is send from server, then close the connection
                if (!!done || !!messageDone) {
                    !!onDone && onDone(request);
                    return;
                }

                // !!!messageDone && 
                if (!!retry) {
// console.debug("fetchResponsePump !!retry, calling onReconnectRetry", request, { retry, done, messageDone });

                    !!request.options.onSseRetry && request.options.onSseRetry(!!request && request.retry);
                    request.state = CmsFetchState.Pending;

                    return me.onReconnectRetry(request);
                }
            }

            return me.fetchResponsePump(reader, request, onDone, buffer);
        });
    }

    private onReconnectRetry(request: CmsFetchRequest) : Promise<any> {
        const app = this;

        return new Promise(resolve => setTimeout(resolve, Math.max(0, request.retry)))
            .then(() => {
                return app.fetchRequestPromise(request);
            });
    }

    // Handle the result of a fetch (verboseResponseResult)
    protected onFetchRequestDone(request: CmsFetchRequest) : any {
        const app = this;

        !!request.onDone && request.onDone();

        let response = request.response;
        let options = request.options;

        // Remove abort function
        options.abort = null;

        // Fix response?
        if (!!request.options && !!request.options.fixResponse && !!response) {
            app.fixResponse(response);
        }

        // console.debug("onFetchRequestDone", {
        //     state: request.state,
        //     succes: !!response && response.Succes
        // });

        // Handle succes and errors
        if (request.state == CmsFetchState.Cancelled) {
            !!app.debug && console.warn("Request was cancelled, mark as handled and return request", request);
            response = new CmsVerboseResponse();
            response.Succes = false;
            response.Cancelled = true;
            request.response = response;
            request.handled = true;

            !!options && !!options.onCancelled && options.onCancelled(request);
        } else if (response.Succes) {
            !!options && !!options.onSucces && options.onSucces.call(app, response);
            request.handled = true;
        } else if (!!!options.ignoreExpire && !!response.sre && !!response.ExpireHtml) {
            // handle with new promise
            return app.expireRestore(response, request);
        } else if (!response.Succes && !!response.Redirect) {
            document.location.href = response.Redirect;
            return null;
        } else if (!response.Succes && !!options && !!options.onError) {
            if (!response.Error) {
                // @ts-ignore
                response.Error = ((!!app && !!app.messages && !!app.messages.UnknownError)
                    ? app.messages.UnknownError
                    : null) || "Unknown error";
            }

            options.onError.call(app, response);
            request.handled = true;
        } else if (!response.Succes && (!options || !options.globalErrorHandler)) {
            app.showGlobalError.call(app, response.Error, options);
            request.handled = true;
        }

        !!options.onFinish && options.onFinish();

        return request;
    }

    protected requestIsEventStream(response: Response) : boolean {
        let contentType = response.headers.get('Content-Type').toLocaleLowerCase();
        return !!contentType && contentType.startsWith('text/event-stream');
    }

    protected fetchRetry(request: CmsFetchRequest, reject: Function = null) {
        let me = this;

        let waitTimer = new CmsWaitTimer();
        waitTimer.seconds = 10;
        waitTimer.onWait = function (seconds) {
            console.warn("Wait for {0} seconds".format(seconds), request);
        }
        waitTimer.onValidateWaitLonger = function () {
            return request.state != CmsFetchState.Cancelled;
        };
        waitTimer.onValidateWaitFailed = function () {
            console.warn("Call reject");
            !!reject && reject();
        }
        waitTimer.onAfterWait = function () {
            // !!app.debug && console.log("retry");
            return me.fetchRequestPromise(request);
        }
        waitTimer.next();
    }

    protected fetchRequestPromise(request: CmsFetchRequest) : Promise<CmsFetchRequest> {
        const app = this;
        const options = request.options;
        const token = $("body").requestToken();

        if (!!options && !!options.url && options.url.length > 0 && options.url[0] == "/" && !!$.fn.requestToken && !!token) {
            request.headers["request-token"] = token;
        }

        return new Promise(function (resolve, reject) {
            // //@ts-ignore
            // if (!!!request._debugPromiseCounter) { request._debugPromiseCounter = 0;}
            // //@ts-ignore
            // request._debugPromiseCounter = request._debugPromiseCounter + 1;
            // //@ts-ignore
            // console.debug("fetchRequestPromise: new Promise", request._debugPromiseCounter);

            request.state = CmsFetchState.Connecting;
            request._controller = new AbortController();
            request._signal = request._controller.signal;

            request.options.abort = function () {
                request.state = CmsFetchState.Cancelled;
                !!request._controller && request._controller.abort();
            };

            let json:any = !!options.data && JSON.stringify(options.data);
            if (!!json && json.length > 64000) {
                request.headers['Accept'] = 'application/json';
            }

            fetch(options.url, {
                method: "POST",
                headers: request.headers,
                body: json,
                cache: "no-cache",
                redirect: "follow",
                signal: request._signal
            }).then(response => {
                if (request.state == CmsFetchState.Cancelled) {
                    //!!app.debug && console.debug("fetchRequestPromise: Request was cancelled");
                    resolve(app.onFetchRequestDone(request));
                } else if (response.status === 502 && !!request.options && !!request.options.allow502retry) {
                    // !!app.debug && console.warn("fetch allow502retry", response)
                    // console.debug("fetchRequestPromise: response.status == 502", response);

                    // wait for X seconds
                    app.fetchRetry(request, reject);
                } else if (response.status != 200) {
                    // !!app.debug && console.warn("fetch none 200", response);
                    // !!app.debug && console.warn("Request returned " + response.status);
                    // console.debug("fetchRequestPromise: response.status != 200", response);

                    response.text().then(errorText => {
                        var data = errorText.startsWith("{") ? app.json.parse(errorText) : {
                            Succes: false,
                            Data: errorText,
                            Error: ("Request returned " + response.status)
                        };
                        var errorResponse = <CmsVerboseResponse>app.extendWithProperties(data || {}, new CmsVerboseResponse()) as CmsVerboseResponse;
                        request.response = errorResponse;
                        request.handled = false;

                        resolve(app.onFetchRequestDone(request));
                    });
                } else if (app.requestIsEventStream(response)) {
                    // !!app.debug && console.warn("fetch requestIsEventStream", response);
                    // console.debug("fetchRequestPromise: response.requestIsEventStream", response);

                    !!options.onConnect && options.onConnect();

                    let reader = response.body.getReader();
                    return app.fetchResponsePump(reader, request, (request) => {
                        request.state = CmsFetchState.Done;

                        if (!!!request.response) {
                            request.response = new CmsVerboseResponse();
                            request.response.Succes = true;
                        }

                        // console.debug("fetchRequestPromise: response.requestIsEventStream, calling resolve with onFetchRequestDone", response);
                        resolve(app.onFetchRequestDone(request));
                    });
                } else {
                    // !!app.debug && console.warn("fetch get response", response);
                    // console.debug("fetchRequestPromise: connected without sse", response);
                    request.state = CmsFetchState.Connected;

                    // Handle without SSE
                    response.json().then(data => {
                        request.response = <CmsVerboseResponse>app.extendWithProperties(data || {}, new CmsVerboseResponse());
                        request.state = CmsFetchState.Done;
                        // !!app.debug && console.log("state set to done");

                        resolve(app.onFetchRequestDone(request));
                    })
                }
            }).catch(error => {
                // console.warn("Catch fetch error", error, request);

                const sseRetry = !!request.options.sseAllowRetry || !!request.options.allowRetry;
                let resolveIt = false;

                if (!!sseRetry && request.state !== CmsFetchState.Cancelled) {
                    // !!me.debug && console.log("reconnect after timeout");
                    !!request.options.onSseRetry && request.options.onSseRetry(!!request && request.retry);

                    request.state = CmsFetchState.Pending;
                    // console.debug("fetchRequestPromise error handling, calling onReconnectRetry", {error, request});
                    resolve(app.onReconnectRetry(request));
                }

                request.response = new CmsVerboseResponse();
                request.response.Succes = false;

                let allow502Retry =
                    !!request.options &&
                    !!request.options.allow502retry &&
                    (!!!request.options.allow502retryMax ||
                        ((request._retry502Counter || 0) < request.options.allow502retryMax));

                if (request.state == CmsFetchState.Cancelled) {
                    //!!app.debug && console.warn("[Catch] Request was cancelled");

                    resolveIt = true;
                } else if (error instanceof TypeError && error.message.indexOf("Failed") >= 0 && !!allow502Retry) {
                    request._retry502Counter = (request._retry502Counter || 0) + 1;
                    // console.warn("retry", { _retry502Counter: request._retry502Counter, allow502retryMax: request.options.allow502retryMax })
                    // !!app.debug && console.warn("fetch error allow502retry, going to retry", error, request);
                    // Retry
                    app.fetchRetry(request, reject);
                } else if (error instanceof Error) {
                    // !!app.debug && console.warn("fetch error instanceof Error", error, request);

                    request.state = CmsFetchState.Exception;
                    request.response.Error = error.message;
                    request.handled = false;
                    resolveIt = true;
                } else {
                    // !!app.debug && console.warn("fetch error unhandled", error, request);

                    request.state = CmsFetchState.Exception;
                    request.response.Error = error;
                    request.handled = false;
                    resolveIt = true;
                }

                if (!!resolveIt)
                    resolve(app.onFetchRequestDone(request));
            });
        });
    }

    // Always call this function with this set to the app object, with bind or call
    ajax(options: CmsAjaxOptions) {
        var app = this;
        var fetchOptions = <CmsFetchOptions> app.extendWithProperties((options || {}), new CmsFetchOptions());
        return app.fetch(fetchOptions);
    }

    expireRestore(response: CmsVerboseResponse, request: CmsFetchRequest) {
        var app = this;

        var showExpire = function (request: CmsFetchRequest, resolve?: Function, reject?: Function) {
            app.storage.setItem("refreshToken", null);

            if ((response.ExpireHtml || null) !== null) {
                var $html = $(response.ExpireHtml);

                $html.eq(0).on("close", function (event, value) {
                    event.preventDefault();
                    event.stopPropagation();

                    if (!!value) {
                        resolve(request);
                    }
                });

                $("body").append($html);
            } else {
                reject();
            }
        };

        return new Promise(function(resolve, reject) {
            if (!!response && !!response.RestoreUrl) {
                // Create options for expire
                var options = new CmsFetchOptions();
                options.url = response.RestoreUrl;
                options.data = {
                    sre: response.sre,
                    RefreshToken: app.storage.getItem("refreshToken"),
                };
                options.onError = null;

                app.fetch(options).then(expireRequest => {
                    var response = !!expireRequest && expireRequest.response;

                    if (!!!response) {
                        !!app.debug && console.error("Response is missing");
                    } else if (!!!response.Succes) {
                        showExpire(request, resolve);
                    } else {
                        $(document.body).requestTokenUpdate(response.Data.rv);
                        $(document.body).refreshTokenUpdate(response.Data.rt);
                        resolve(request); /* return */
                    }
                });
            } else {
                showExpire(request, resolve);
            }
        }).then(function(request: CmsAjaxRequest) {
            request.options.ignoreExpire = true;
            return app.fetchRequestPromise(request);
        }).catch(error => {
            console.error("Error not handled!", error, request);

            request.state = CmsFetchState.Exception;
            request.response.Error = error;
            request.handled = false;

            app.onFetchRequestDone(request);
        });
    };

    // url, filename, mimetype, inline
    downloadInlineOrAttached(url: string, filename?: string, mimetype?: string, inline?: boolean) {
        var me = this, element = document.createElement('a');

        var clickElement = function() {
            document.body.appendChild(element);
            element.click();
            document.body.removeChild(element);
        }
        element.href = url;
        element.style.display = 'none';

        // TODO: add plugin handling?

        // Set download attribute to force download
        if (element && element.download !== undefined  && inline !== true) {
            element.download = filename || url.split('/').pop();
            clickElement();
            return;
        }

        var win = window.open(url, "_blank");
        if(!win || win.closed || typeof win.closed=='undefined')
        {
            // Use anchor?
            if (true) {
                element.download = filename || url.split('/').pop();
                clickElement();
                return;
            } else {
                //POPUP BLOCKED
                me.downloadFrame(url);
            }
        } else {
            win.focus();
        }
    }

    download(url) {
        document.location = url;
    };

    downloadFrame(url) {
        var me = this;
        var uniqueId = me.uniqueId("dwnldfrm");
        var $frame = $("<iframe style='position:absolute;top:-1000px;left:-1000px;width: 1px;height:1px;'></iframe>")
            .attr("name", uniqueId)
            .attr("src", url);

        if ($frame.ready) {
            // Remove frame
            $frame.ready(function () {
                setTimeout(function () {
                    $frame.remove();
                }, 5000);
            });
        }
        $frame.appendTo("body");
    };

    templateEngine(html, options) {
        var re = /<%([^%>]+)?%>/g,
            reExp = /(^( )?(if|for|else|switch|case|break|{|}))(.*)?/g,
            code = 'var r=[];\n',
            cursor = 0,
            match;
        var add = function (line, js?) {
            js ? (code += line.match(reExp) ? line + '\n' : 'r.push(' + line + ');\n') :
                (code += line != '' ? 'r.push("' + line.replace(/"/g, '\\"') + '");\n' : '');
            return add;
        };

        while (match = re.exec(html)) {
            add(html.slice(cursor, match.index))(match[1], true);
            cursor = match.index + match[0].length;
        }
        add(html.substr(cursor, html.length - cursor));
        code += 'return r.join("");';
        return new Function(code.replace(/[\r\t\n]/g, '')).apply(options);
    };
}

window.storageHandler = new StorageHandler(window);
window.app = new cmsApp(window);

// TODO: use npm, bundler and without versioning
window.app.requires((!!window.Promise), "/Asset/script/promise.min.js", function () {
    // https://stackoverflow.com/questions/22773920/can-promises-have-multiple-arguments-to-onfulfilled
    // @ts-ignore
    if (typeof Promise.prototype.spread !== 'function') {
        // @ts-ignore
        Promise.prototype.spread = function (fn) {
            return this.then(function (args) {
                return Promise.all(args); // wait for all 
            }).then(function (args) {
                //this is always undefined in A+ complaint, but just in case
                return fn.apply(this, args);
            });
        };
    }
});

window.app.onReady();

// http://james.padolsey.com/javascript/extending-jquerys-selector-capabilities/
// Wrap in self-invoking anonymous function:
//interface JQueryX extends JQuery {

(function ($) {
    // Get or set the requestToken
    $.fn.requestToken = function (optionalToken) {
        if (optionalToken) {
            if ($(this).is("form")) {
                $(this).data("requesttoken", null);
            }
            $("body").data("requesttoken", optionalToken);
            // Push update to other tabs, and remove
        } else if (arguments.length === 0) {
            var token = null;
            // Backwards compatible
            if ($(this).is("form")) {
                token = $(this).data("requesttoken");
            }
            return (token || $("body").data("requesttoken") || null);
        }
    };

    // Push token update
    $.fn.requestTokenUpdate = function (requestToken) {
        // Update local requesttoken
        $("body").requestToken(requestToken);
        // Broadcast requesttoken
        window.app.storage.setItem("requesttoken-update", requestToken);
    };

    // Push token update
    $.fn.refreshTokenUpdate = function (refreshToken) {
        window.app.storage.setItem("refreshToken", refreshToken);
    };

    !!window.app && !!window.app.debug && console.log("register requesttoken-update");

    window.app.storage.register("requesttoken-update", function(token) {
        !!window.app && !!window.app.debug && console.log("requesttoken-update", token);

        if (token) {
            $("body").requestToken(token);
        }
    });

    $.fn.setClick = function (options: any, callback?: any) {
        if (arguments.length === 1 && typeof arguments[0] === "function") {
            callback = options;
            options = {};
        }

        return $(this).each(function () {
            var opt = $.extend({
                preventDefault: true,
                validateForm: false,
                isDisabled: true,
                slim: false
            }, options);

            // Add event with default options
            $(this)
                .off("click")
                .on("click", function (event) {
                    var me = this, $me = $(me), $form = null;

                    event.preventDefault();
                    event.stopPropagation();

                    if (opt.isDisabled && $me.isDisabled()) {
                        return false;
                    }
                    if (opt.slim === true) {
                        ($form = $me.getForm()).slim();
                    }
                    if (opt.validateForm === true) {
                        $form = $form || $me.getForm();
                        if (!$form.closest("form").ResetAndValidate()) {
                            return false;
                        }
                    }

                    if (callback) {
                        callback.call(me, event);
                    }
                });
        });
    };

    $.fn.hasElement = function ($element) {
        var $result = null;

        $(this).filter(function () {
            if ($(this).is($element)) {
                $result = $(this);
                return false;
            }
        });

        return $result != null;
    };

    $.fn.isDisabled = function () {
        var $me;
        if (this == null || ($me = $(this)).length == 0) {
            return false;
        }

        if ($me.is(".disabled,:disabled,[class*=disabled-],.readonly,[readonly]")) {
            return true;
        } else if ($me.is(".norecursion")) {
            return false;
        } else {
            return $me.parent().isDisabled();
        }
    };

    $.fn.contains = function ($element) {
        var found = false;

        $element = $element.eq(0);

        $(this).parents().each(function () {
            if ($(this).is($element)) {
                found = true;
                return false;
            }
        });

        return found;
    };

    $.fn.hasEvent = function (eventName) {
        if ((eventName || null) === null || eventName == '' || this.length == 0) {
            return false;
        }

        var cntEvent = 0;

        $.each(this, function (index, element) {
            // @ts-ignore
            var obj = $._data(element, 'events');
            if ((obj || null) !== null && (obj[eventName] || null) !== null) {
                cntEvent++;
            }
        });

        return this.length == cntEvent;
    };

    $.fn.sortChildren = function (sortingFunction) {
        var children = $(this).children().get();
        // @ts-ignore
        children.sort(sortingFunction);
        $(this).append(children);
        return this;
    };

    $.fn.hasHandler = function (handlerName) {
        var hasHandler = false;
        var $me = $(this);
        // @ts-ignore
        var value = handlerName in (jQuery._data($me[0], "events") || {});
        return (value || null) !== null;
    };

    $.fn.triggerHandlerRecursive = function (handler) {
        var $me = $(this).eq(0);
        if ($me.hasHandler(handler)) {
            return $me.triggerHandler(handler);
        }

        var value = false;
        $me.parents().each(function (i, parent) {
            var $parent = $(parent);
            if ($parent.hasHandler(handler)) {
                value = $parent.triggerHandler(handler);
                return false;
            }
        });

        return value;
    };

    // Extend jQuery's native ':'
    $.extend($.expr[':'], {
        // New method, "data"
        data: function (a, i, m) {
            var e = $(a).get(0), keyVal: Array<string>;

            // m[3] refers to value inside parenthesis (if existing) e.g. :data(___)
            if (!m[3]) {
                // Loop through properties of element object, find any jquery references:
                for (var x in e) {
                    if ((/jQuery\d+/).test(x)) {
                        return true;
                    }
                }
            } else {
                // Split into array (name,value):
                keyVal = m[3].split('=');

                // If a value is specified:
                if (keyVal[1]) {
                    // Test for regex syntax and test against it:function (e) { e.preventDefault() });
                    if ((/^\/.+\/([mig]+)?$/).test(keyVal[1])) {
                        // @ts-ignore
                        return (new RegExp(keyVal[1].substr(1, keyVal[1].lastIndexOf('/') - 1), keyVal[1].substr(keyVal[1].lastIndexOf('/') + 1))).test($(a).data(keyVal[0]));
                    } else {
                        // Test key against value:
                        // @ts-ignore
                        return $(a).data(keyVal[0]) == keyVal[1];
                    }
                } else {
                    // Test if element has data property:
                    if ($(a).data(keyVal[0])) {
                        return true;
                    } else {
                        // If it doesn't remove data (this is to account for what seems
                        // to be a bug in jQuery):
                        $(a).removeData(keyVal[0]);
                        return false;
                    }
                }
            }

            // Strict compliance:
            return false;
        }
    });
})(jQuery);