﻿/* Copyright 2015 Willow Media

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. /**/

(function ($) {
    "use strict";

    $.tree = function ($container, options, data) {
        var me = this;

        this.$container = null;
        this.options = {};

        this.init = function ($container, options, data) {
            this.options = $.extend({
                // properties
                nodeId: "Id",
                nodeChildren: "Chapters",
                form: null,
                allowNew: false,
                data: null,
                sortable: true,
                chapterNumber: true,
                onClick: null,                  // function($node) - 'this' is the tree
                onRender: null                  // function($container, data) - add your elements to the $container
            }, options);

            this.$container = $container
                .empty()
                .data("tree", this)
                .addClass("tree formHandlerSerializable unselectable")
                .on("serialize", function (event) {
                    return me.serialize();
                })
                .on("addNew", function (event, $parent) {
                    event.preventDefault();
                    event.stopPropagation();

                    if (me.options.allowNew === true) {
                        me.addNew($parent);
                    }
                });

            data = data || options.data || $container.data("rec");

            // Data should be an array
            this.createList(this.$container, data);

            // TODO: Set up sorting
            if (me.options.sortable) {
                this.setupSortable(this.$container.find(">ol"));
            }

            if (me.options.chapterNumber) {
                this.renderChapterNumbers();
            }
        };

        this.serialize = function () {
            return me.resursiveSerialize(me.$container);
        };

        this.resursiveSerialize = function ($parent) {
            var data = [];

            $parent.find("> ol > li > div.node:not(.deleted)")
                .each(function () {
                    var item = $(this).data("rec");
                    data.push(item);

                    item[me.options.nodeChildren] = me.resursiveSerialize($(this).parent("li"));
                });

            return data.length === 0 ? null : data;
        };

        this.setupSortable = function ($list) {
            if (!me.options.sortable) { return false; }

            $list.addClass("unselectable").sortable({
                nested: true,
                handle: 'i.icon-move',
                exclude: '.readonly',
                onDrop: function ($item, container, _super) {
                    _super($item, container, _super);

                    // Render chapter numbers
                    if (me.options.chapterNumber) {
                        me.renderChapterNumbers();
                    }
                }
            });
        }

        this.click = function ($node, isNew) {
            if (me.$container.isDisabled()) { return false; }
            if ($node.is(".deleted")) { return false; }

            if (me.options.onClick) {
                me.options.onClick.call(me, $node);
            } else {
                // Check form change
                if (me.validateChanges() !== true) { return false; }

                // Remove open forms
                me.removeForm();

                // Create (new) form
                me.openForm($node, isNew);
            }
        }

        this.validateChanges = function () {
            if (me.$container.find("form.trackChanges:data(formHandler):visible").hasChanges()) {
                if (!confirm("You have unmodified changes. Are you sure you want to discard them?")) {
                    return false;
                }
            }

            return true;
        }

        this.openForm = function ($node, isNew) {
            var $form = me.options.form.create({
                data: $node.data("rec"),
                validation: true,
                trackChanges: true,
                isNew: isNew === true,
                events: {
                    close: function (event, data) {
                        event.preventDefault();
                        event.stopPropagation();

                        var isNew = $(this).formHandler().options.isNew;
                        data = data || null;

                        if (isNew && data === null) {
                            // new, without data, then remove
                            $node.closest("li").remove();
                        } else if (data !== null) {
                            // update the div
                            me.setupNodeDiv($node, data);
                            $node.show();
                            $(this).remove();
                        } else {
                            $node.show();
                            $(this).remove();
                        }

                        if (me.options.chapterNumber) {
                            me.renderChapterNumbers();
                        }
                    }
                }
            });

            // forms.$list.addClass("disabled").sortable("disable");
            $node.after($form.addClass("trackChanges"));
            $node.hide();
        }

        this.removeForm = function () {
            var $form = me.$container.find("form.trackChanges:data(formHandler):visible"); //.remove();
            $form.trigger("close");
            $form.parent().find("div.node").show();
        }

        //this.onDrop = 

        this.renderChapterNumbers = function () {
            //chapterNumber
            this.$container
                .find("div.node")
                .each(function () {
                    var $chapterNumber = $(this).find("> span.chapterNumber");

                    // Render
                    var $li = $(this).closest("li");
                    $chapterNumber.text($li.prevAll().length + 1 + ". ");
                });
        }

        this.setupNodeDiv = function ($nodeDiv, node) {
            node = node || {};
            $nodeDiv.addClass("node")
                .data("rec", node)
                .empty();

            // Add menu
            var $mnu = $("<div/>")
                .addClass("menu");

            if (me.options.allowNew === true) {
                $mnu.append($("<a href='#' />")
                    .append("<i class='icon-plus' />")
                    .on("click", function (event) {
                        event.preventDefault();
                        event.stopImmediatePropagation();
                        if ($(this).isDisabled()) { return false; }
                        // trigger new with this li as argument
                        var $node = $(this).closest("li");//.find(">div");
                        me.$container.trigger("addNew", [$node]);
                    })
                    .append($("<a href='#'/>")
                        .append("<i class='icon-cancel' />")
                        .on("click", function (event) {
                            event.preventDefault();
                            event.stopImmediatePropagation();
                            if ($(this).isDisabled()) { return false; }
                            // trigger new with this li as argument
                            $(this).closest("li").find(">div.node").toggleClass("deleted");
                        })));
            }

            $nodeDiv.append($mnu);

            if (me.options.sortable) {
                $nodeDiv.append($("<i class='icon-move' />")
                    .on("click", function (event) {
                        event.preventDefault();
                        event.stopPropagation();
                    }));
            }

            $nodeDiv.append($("<span class='chapterNumber' />"));

            if (me.options.onRender) {
                me.options.onRender.call(me, $nodeDiv, node);
            } else {
                $nodeDiv.append($("<span/>").text(node.Title || "..."));
            }
        }

        this.createList = function ($container, items) {
            var $ol = $("<ol/>").addClass("tree");
            var cnt = 1;
            $container.append($ol);

            if ((items || null) !== null && items.length > 0) {
                $.each(items, function (index, node) {
                    var $li, $nodeDiv;

                    $ol.append($li = me.createItem(node))

                    // Recursive create children. Always call recursive, even if children is null
                    // to create the ol on which a node can be dropped
                    me.createList($li, node[me.options.nodeChildren] || null);

                    cnt++;
                });
            }
        }

        this.createItem = function (node) {
            var $li = $("<li />")
                .attr("edit", 1);

            if ((node || null) !== null) {
                $li.attr("id", "node" + node[me.options.nodeId])
            }

            // Add icons and description
            var $nodeDiv = $("<div class='node' />");
            me.setupNodeDiv($nodeDiv, node);

            // Add click
            if (me.options.form !== null || me.options.onClick) {
                $nodeDiv
                    .addClass("clickable")
                    .on("click", function (event) {
                        event.preventDefault();
                        event.stopPropagation();

                        me.click($(this));
                    });
            }

            // Add the node
            $li.append($nodeDiv);

            return $li;
        };

        this.addNew = function ($parentNode) {
            $parentNode = $parentNode || null;

            if (me.options.allowNew !== true || me.$container.isDisabled()) {
                return false;
            }

            // Check form change
            if (me.$container.find("form.trackChanges:data(formHandler):visible").hasChanges()) {
                if (!confirm("You have unmodified changes. Are you sure you want to discard them?")) {
                    return false;
                }
            }

            // Remove open forms
            var $form = me.$container.find("form.trackChanges:data(formHandler):visible"); //.remove();
            $form.trigger("close");
            $form.parent().find("div.node").show();
            //$form.remove();

            // Create li with div
            var $li = this.createItem();
            if ($parentNode !== null) {
                $parentNode.find("> ol").append($li);
            } else {
                this.$container.find(">ol").append($li);
            }
            $li.append($("<ol/>").addClass("tree"));
            me.click($li.find(">div"), true);

            if (me.options.chapterNumber) {
                me.renderChapterNumbers();
            }
        }

        this.init($container, options);
    };

    // Requires autoLoadMore
    $.fn.tree = function (options) {
        return this.each(function () {
            var $me = $(this);

            $.tree($me, options);
        });
    };
})(jQuery);
