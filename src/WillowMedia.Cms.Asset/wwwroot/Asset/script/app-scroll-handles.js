/* Copyright 2019 Willow Media

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. /**/

(function ($) {
    "use strict";
    $.scrollHandles = function ($container, options) {
        var me = this;
        this.options = {};
        this.$container = null;
        this.$scrollContainer = null;
        this.$arrowUp = null;
        this.$arrowDown = null;
        this.$arrowLeft = null;
        this.$arrowRight = null;

        this.init = function ($container, options) {
            me.$container = $container;
            me.options = $.extend({
                scrollContainer: null,  // function to return the scrollContainer
                direction: null,        // default vertical. Other is horizontal
                offset: 50,
                handleOffsetSize: 10,
                iconUp: "icon-up-open",
                iconDown: "icon-down-open",
                iconLeft: "icon-left-open",
                iconRight: "icon-right-open",
            }, options);

            me.$scrollContainer =
                (this.options.scrollContainer ? this.options.scrollContainer.call(me) : null) ||
                $container;

            // Make sure the container is position relative
            me.$container.addClass("scroll-handles");

            // Create buttons
            switch (me.options.direction || "") {
                case "horizontal":
                    me.$arrowLeft = $("<div/>").addClass("scroll-handle").addClass("arrow-left")
                        .append($("<i/>").addClass(me.options.iconLeft))
                        .on("click",
                            function(event) {
                                me.scrollLeft();
                            });
                    me.$arrowRight = $("<div/>").addClass("scroll-handle").addClass("arrow-right")
                        .append($("<i/>").addClass(me.options.iconRight))
                        .on("click",
                            function(event) {
                                me.scrollRight();
                            });
                    me.$container
                        .append(me.$arrowLeft)
                        .append(me.$arrowRight);
                    break;
                case "vertical":
                default:
                    me.$arrowUp = $("<div/>").addClass("scroll-handle").addClass("arrow-up")
                        .append($("<i/>").addClass(me.options.iconUp))
                        .on("click",
                            function(event) {
                                me.scrollUp();
                            });
                    me.$arrowDown = $("<div/>").addClass("scroll-handle").addClass("arrow-down")
                        .append($("<i/>").addClass(me.options.iconDown))
                        .on("click",
                            function(event) {
                                me.scrollDown();
                            });
                    me.$container
                        .append(me.$arrowUp)
                        .append(me.$arrowDown);
                    break;
            }

            me.$scrollContainer
            .on("scroll", function(event) { me.updateScrollHandles(); });
            $(window)
            .resize(function(event) { me.updateScrollHandles(); });

            // Are we part of a form?
            me.$container.closest("form")
            .eq(0)
            .on("change", function() { me.updateScrollHandles(); });

            me.updateScrollHandles();
        };

        this.scrollLeft = function() {
            me.$scrollContainer.scrollLeft(me.$scrollContainer.scrollLeft() - me.options.offset);
        };

        this.scrollRight = function() {
            me.$scrollContainer.scrollLeft(me.$scrollContainer.scrollLeft() + me.options.offset);
        };

        this.scrollUp = function() {
            me.$scrollContainer.scrollTop(me.$scrollContainer.scrollTop() - me.options.offset);
        };

        this.scrollDown = function() {
            me.$scrollContainer.scrollTop(me.$scrollContainer.scrollTop() + me.options.offset);
        };

        this.updateScrollHandles = function() {
            var $container = me.$container;
            var $scrollContainer = me.$scrollContainer;
            // var offset = me.options.offset || 50;
            var handleOffset = me.options.handleOffsetSize;

            switch (me.options.direction || "") {
                case "horizontal":
                    var $handleLeft = me.$arrowLeft;
                    var $handleRight = me.$arrowRight;
                    var containerWidth = $container.outerWidth();
                    // Get height of content
                    var innerWidth = $scrollContainer.children().eq(0).outerWidth();

                    // at the beginning
                    if (innerWidth <= containerWidth) {
                        // hide buttons
                        $handleLeft.removeClass("show");
                        $handleRight.removeClass("show");
                        $scrollContainer.scrollLeft(0);
                    } else {
                        var scrollLeft = $scrollContainer.scrollLeft();
                        $handleLeft.toggleClass("show", scrollLeft > handleOffset);
                        $handleRight.toggleClass("show", (innerWidth - containerWidth - scrollLeft) > handleOffset);
                    }
                    //console.log("horizontal", containerWidth, innerWidth, innerWidth <= containerWidth, (innerWidth - containerWidth - scrollLeft) > handleOffset);
                    break;
                case "vertical":
                default:
                    var $handleUp = me.$arrowUp;
                    var $handleDown = me.$arrowDown;
                    var containerHeight = $container.outerHeight();
                    // Get height of content
                    var innerHeight = $scrollContainer.children().eq(0).outerHeight();

                    // at the beginning
                    if (innerHeight <= containerHeight) {
                        // hide buttons
                        $handleUp.removeClass("show");
                        $handleDown.removeClass("show");
                        $scrollContainer.scrollTop(0);
                    } else {
                        var scrollTop = $scrollContainer.scrollTop();
                        $handleUp.toggleClass("show", scrollTop > handleOffset);
                        $handleDown.toggleClass("show", (innerHeight - containerHeight - scrollTop) > handleOffset);
                    }
                    break;
            }
        };

        this.init($container, options);
    };

    $.fn.scrollHandles = function (options) {
        return this.each(function () {
            new $.scrollHandles($(this), options);
        });
    };
})(jQuery);