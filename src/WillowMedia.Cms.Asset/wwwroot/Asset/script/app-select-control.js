(function($) {
    "use strict";

    Array.prototype.firstOrNull = function() {
        if (this && this.length > 0) {
            return this[0];
        }
        return null;
    };
    
    
    $.selectControl = function(element, options) {
        this.options = {};
        this.element = null;

        element.data('selectControl', this);

        this.init = function($element, options) {
            var me = this;

            me.element = $element;
            me.options = $.extend({
                multiple: false,         // single or multiple select
                dataSource: null,        // array of objects
                dataSourceValue: null,   // property of value
                dataSourceText: null,    // property of text
                onGetValue: null         // function to override the return value
            }, options);

            $element
                .addClass("select-control control")
                .addClass("formHandlerSerializable")
                .on("serialize", function (event) {
                    event.preventDefault();
                    return me.getValue.call(me);
                })
                .on("set-value", function(event) {
                    event.preventDefault();
                    return me.setValue.call(me);
                })  
                .on("dataSource", function(event, dataSource) {
                    event.preventDefault();
                    me.updateDataSource.call(me, dataSource);
                });
            
            this.updateDataSource.call(me, me.options.dataSource);
        };

        this.getValue = function() {
            var me = this;
            var $element = $(me.element);

            var $qualified = $element.find("li.selected");

            if ($qualified.length === 0) {
                return null;
            } 
            
            var selectedValues = $qualified.map(function() { return $(this).data("rec") }).toArray();
                
            if (me.options.onGetValue) {
                return me.options.onGetValue.call(me, selectedValues);
            } else if (me.options.multiple) {
                return selectedValues;
            } else {
                return selectedValues.firstOrNull();
            }
        };

        this.setValue = function(value) {
            var me = this;
            var $element = $(me.element);

            $element.find("li").removeClass("selected");

            if (value) {
                var $qualified = $element.find("li").filter(function () {
                    return $(this).attr("value") == value || $(this).data("rec") === value;
                });

                if (me.options.multiple) {
                    $qualified.addClass("selected");
                } else {
                    $qualified.eq(0).addClass("selected");
                }
            }
        };
        
        this.updateDataSource = function(dataSource) {
            var me = this;
            var $element = $(me.element).empty();

            $.each(dataSource || [], function(index, item) {
                $element.append(
                    $("<li/>")
                        .data("rec", item)
                        .attr("value", item[me.options.dataSourceValue])
                        .text(item[me.options.dataSourceText])
                        .on("click", function(event) {
                            event.preventDefault();
                            if ($(this).isDisabled()) { return false }
                            me.selectItem.call(me, $(this));
                        }));
            });
        };

        this.selectItem = function($li) {
            var me = this;
            var $element = $(me.element);

            $element.find("li").removeClass("selected");

            if ($li && $li.length > 0) {
                $li.addClass("selected");
            }
        };
        
        // initialize the element
        this.init(element, options);
    };

    $.fn.selectControl = function(options) {
        return this.each(function () {
            (new $.selectControl($(this), options));
        });
    };
})(jQuery);