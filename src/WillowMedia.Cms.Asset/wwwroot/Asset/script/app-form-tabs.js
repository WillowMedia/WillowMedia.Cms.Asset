﻿/* Copyright 2017-2018 Willow Media

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. /**/
(function ($) {
  "use strict";

  $.formtabs = function ($element, options) {
    this.options = {};
    this.$form = null;
    this.$tabs = null;
    this.$container = null;
    this.defaultTabItem = {
      id: null,
      label: null,
      validate: false,
      trackChanges: false,        // set trackChanges on form
      form: null,
      formData: null,
      onPrepareData: null,        // event called before the form is created, to alter the data object
      events: null,               // event object passed to the form };
      urlDetails: null,           // url to call before opening form. The current data object will be posted
      onCheckChanged: null,       // event override for onCheckChanged
      onBeforeRemove: null,       // Call before tab changes is handled
      onTabChanged: null,         // Call on tab has changed
      onFormAdd: null,            // event after form is appended to tab
      data: null,                 // The data object for the form, which can also be a function
      formOptions: null,          // additional form options object. Can override the defaults for the tabbed form
      getState: null,             // get a state
      selectable: true,           // disable selection
      visible: true,
      placeholder: null           // function which return placeholder element
    };
    this.current = {
      currentForm: null,
      index: null,
      forms: []
    };
    this.navigator = null;

    var me = this;

    this.init = function ($element, options) {
      me.options = $.extend({}, $.formtabs.defaultOptions, options);
      me.$form = $($element);
      me.$form.data("formtabs", me);

      // Navigator?
      if (me.options.usePopStateNavigator) {
        me.setupPopStateNavigator(me.options.usePopStateNavigator);
      }

      me.current.forms = [];
      $.each((me.options.tabs || []), function (index, item) {
        // Merge tab with default properties
        Object.keys(me.defaultTabItem).forEach(function(prop) {
          if (!(prop in item)) {
            item[prop] = me.defaultTabItem[prop];
          }
        });

        me._encapsulatePropertyForUpdate(item, "visible");
        me._encapsulatePropertyForUpdate(item, "selectable");
        me._encapsulatePropertyForUpdate(item, "label");

        me.current.forms.push(item);
      });

      if ((me.options.tabContainer || null) === null) {
        me.$form.append(me.$tabs = $("<div class=\"tabs\"><ul></ul></div>"));
      } else {
        me.$tabs = $(me.options.tabContainer);
      }

      if ((me.options.formContainer || null) === null) {
        me.$form.append(me.$container = $("<div class=\"form\"></div>"));
      } else {
        me.$container = $(me.options.formContainer);
      }

      me.$form
          .off("get-tab-state")
          .on("get-tab-state", function(event) {
            event.preventDefault();
            event.stopPropagation();
            return me.getTabState();
          });

      me.$form
          .off("get-tab-handler")
          .on("get-tab-handler", function(event) {
            event.preventDefault();
            event.stopPropagation();
            return me;
          });

      me.$form.addClass("dispose").on("dispose", function(event) {
        event.preventDefault();
        event.stopPropagation();
        me.dispose();
      });

      me.$form.off("fill-tab").on("fill-tab", function (event, tabIndex, optionalData) {
        event.preventDefault();
        event.stopPropagation();
        event.stopImmediatePropagation();
        //me.fillTab(tabIndex, optionalData);
        me.clearAndFill(tabIndex, optionalData);
      });

      me.$form.off("change-tab")
          .on("change-tab", function (event, tabIndex, force, optionalData, onDone) {
            event.preventDefault();
            event.stopPropagation();
            event.stopImmediatePropagation();
            if ($(this).isDisabled()) { return false; }

            me.changeTab(tabIndex, force, optionalData, onDone);
          });

      var $actions = me.$form.find("div.tabs > div.actions");
      switch (me.options.showActionMenu || "hover") {
        case "popper":
          // Hover menu
          var $menu = $actions.find("> div.action-menu");
          var $bttnMenu = $actions.find("> i");
          $bttnMenu.popper($menu, {
            beforeClick: function() {
              if (me.options.onActionsShow) {
                me.options.onActionsShow.call($actions, me);
              }
            }
          });
          break;
        case "click":
          $actions.addClass("useClick");
          var $actionsMenuButton = $actions.find("> i");
          var $actionsMenu = $actions.find("> div.action-menu");

          var contextMenu = {
            shows: false,
            hide: function() {
              // console.log("hide called");

              if (contextMenu.shows) {
                contextMenu.shows = false;
                $("body").off("click", contextMenu.hide);
                $actions.removeClass("show");
              }
            },
            onShow: function() {
              contextMenu.shows = true;
              $("body").on("click", contextMenu.hide);
              $actions.addClass("show");
            }
          };

          $actionsMenuButton
              .setClick(function(event) {
                var shows = $actions.is(".show");
                if (shows) {
                  contextMenu.hide();
                } else {
                  if (me.options.onActionsShow) {
                    me.options.onActionsShow.call($actions, me);
                  }
                  contextMenu.onShow();
                }
              });

          $actionsMenu
              .addClass("dispose manualBlur")
              .off("hide")
              .on("hide", function(event) {
                event.preventDefault();
                $actions.removeClass("show");
              })
              .off("manualBlur")
              .on("manualBlur", function(event, $element) {
                event.preventDefault();
                event.stopPropagation();

                if (!contextMenu.show) { return; }

                // If the clicked element is a child of the action menu
                if ($element && $element.parents().filter(function() { return $(this).is($actions); }).length > 0) {
                  // setTimeout(contextMenu.hide, 0);
                } else {
                  contextMenu.hide();
                }
              })
              .off("dispose")
              .on("dispose", function(event) {
                event.preventDefault();
                event.stopPropagation();
                // console.log("dispose");
                contextMenu.hide();
              });
          break;
        default:
          $actions.removeClass("useClick");
          break;
      }

      me.addTabs();

      // me.fillTab(me.options.selected || 0);
      if (!me.options.intialChangeTab) {
        me.clear();
      } else {
        me.changeTab(me.options.selected || 0);
      }
    };

    this._encapsulatePropertyForUpdate = function(tab, property) {
      if (!!!tab) return false;
      var me = this;
      var privateProperty = "_" + property;
      tab[privateProperty] = tab[property];

      // Replace with getter setter
      Object.defineProperty(tab, property, {
        get: function () {
          return this[privateProperty];
        },
        // Create a new setter for the property
        set: function (val) {
          this[privateProperty] = val;
          me.updateTab(this);
        }
      });
    };

    this.setupPopStateNavigator = function(template) {
      var me = this;

      me.navigator = {
        stateUrlTemplate: template || document.location.href,
        initialState: true,
        fromState: false,
        navigateFromState: function (state) {
          if (!state || !state.selectedTab) return;
          me.navigator.fromState = true;
          me.$form.trigger("change-tab", state.selectedTab);
        },
        onFormChanged: function ($form) {
          if (me.navigator.fromState) {
            me.navigator.fromState = false;
            return;
          }
          var tab = this.current.forms[this.current.index];
          var selectedTab = tab.id || tab.label;
          me.navigator.pushState(selectedTab, me.navigator.stateUrlTemplate.format(selectedTab));
        },
        pushState: function (selectedTab, url, initialState) {
          if (!selectedTab) return;
          var state = { selectedTab: selectedTab };

          me.navigator.initialState === true
              ? history.replaceState(state, null, url)
              : history.pushState(state, null, url);
          me.navigator.initialState = false;
          me.navigator.fromState = false;
        },
        popState: function (event) {
          var state = event && event.originalEvent ? event.originalEvent.state : null;
          if (state && state.selectedTab) {
            me.navigator.navigateFromState(state);
          }
        }
      };
      $(window).bind('popstate', me.navigator.popState);

      return me.navigator;
    }

    this.clear = function () {
      if (!!me.current.currentForm) {
        me.current.currentForm.trigger("manualBlur");

        // console.log("clear is called, should call dispose", me.current.currentForm, $(me.current.currentForm).is(".dispose"));

        !!$(me.current.currentForm).is(".dispose") && me.current.currentForm.trigger("dispose");

        me.current.currentForm = null;
        me.$container.empty();
      }
    };

    this.dispose = function() {
      this.clear();
    }

    this.getTabState = function() {
      var current = me.current;
      var tab = current.forms[current.index];

      return {
        tab: current.index,
        tabState: tab && tab.getState ? tab.getState.call(current, current.currentForm) : null
      };
    };

    this.getTabIndex = function(index, hideMessage) {
      var current = me.current;
      var forms = current.forms;

      if (typeof index === "string") {
        var itemindex = -1;
        $.each(forms, function(tabindex, item) {
          if (item && item.id && item.id.toLocaleUpperCase() === index.toLocaleUpperCase()) { itemindex=tabindex; return false }
          if (item.label && item.label.toLocaleUpperCase() === index.toLocaleUpperCase()) { itemindex=tabindex; return false }
        });

        if (itemindex < 0) {
          // debugger;
          !hideMessage && app.alertBox("Oops"," Can't find tab with name", "error");
          return false;
        }

        index = itemindex;
      }

      return index;
    };

    this.changeTab = function (tabIndex, force, optionalData, onDone) {
      tabIndex = me.getTabIndex(tabIndex);
      if (!force && tabIndex === me.current.index) {
        if (onDone) onDone(me.current, false);
        return false;
      }

      var navigateToTab = function(tabIndex, optionalData) {
        return new Promise(function(resolve, reject) {
          // console.log("navigateToTab in promise");
          if (me.options.onBeforeRemove) { me.options.onBeforeRemove.call(me); }

          resolve({ tabIndex: tabIndex, optionalData: optionalData });
        }).then(function(data) {
          // console.log("navigateToTab filltab");
          return me.clearAndFill(data.tabIndex, data.optionalData);
        }).then(function() {
          // console.log("navigateToTab before ontabchange");
          if (me.options.onTabChanged) {
            me.options.onTabChanged.call(me, tabIndex);
          }

          return me.current;
        });
      };

      return new Promise(function (resolve, reject) {
        var formHandler;

        // Validate if form is allowed to close
        if (me.options.confirmNavigate) {
          if (me.options.confirmNavigate.call(me, navigateToTab)) {
            resolve({ tabIndex: tabIndex, optionalData: optionalData });
          } else {
            reject();
          }
        } else if (me.current.currentForm &&
            me.options.validateNavigation === true &&
            (formHandler = me.current.currentForm.formHandler()).options.trackChanges == true &&
            formHandler.isChanged())
        {
          app.confirmNavigate(
              function() { resolve({ tabIndex: tabIndex, optionalData: optionalData }); },
              reject);
        } else {
          resolve({ tabIndex: tabIndex, optionalData: optionalData });
        }
      }).then(function (data) {
        return navigateToTab(data.tabIndex, data.optionalData);
      }).then(function(current) {
        if (onDone) {
          onDone(current, true);
        }
        return current;
      });
    };

    // Clear the entire container
    this.clearAndFill = function(tabIndex, optionalData) {
      // Remember height for during load (DEPRECATED)
      if (me.$container && me.options.compatibility.setContainerHeightAuto) {
        me.$container.height(me.$container.innerHeight());
      }

      var $placeholder =
          (!!me.options.placeholder && me.options.placeholder.call(me)) ||
          $("<div/>").hide();

      // Dispose should be triggered before the form is removed
      // if (!!me.current.currentForm && me.current.currentForm.is(".dispose")) {
      //   $(me.current.currentForm).trigger("dispose");
      // }
      me.clear();

      if (!!me.$container.is(".dispose")) {
        $(me.$container).trigger("dispose");
      }

      me.$container.empty();
      me.$container.append($placeholder)

      return me.fillTab(tabIndex, optionalData, $placeholder)
    };

    this.fillTab = function (tabIndex, optionalData, $location) {
      return new Promise(function (resolve, reject) {
        // Set selected tab
        me.$tabs.find("li")
            .removeClass("selected")
            .eq(tabIndex)
            .addClass("selected");

        // Create form
        var definedFormByIndex = (tabIndex in me.current.forms) ? me.current.forms[tabIndex] : null;
        if (!definedFormByIndex) {
          reject("Form at index is missing");
        } else {
          var current = $.extend({}, definedFormByIndex || {}); //, { index: tabIndex });
          var data = app.resolveValue(
              optionalData ||
              current.data ||
              (me.options.useOriginalDataSource === true
                  ? me.$form.data("rec")
                  : $.extend({}, me.$form.data("rec"))));

          var ajaxOptions = current.urlDetails ? {
            url: current.urlDetails,
            onSucces: function (response) {
              resolve({ response: response, current: current, index: tabIndex });
            },
            onError: function (response) {
              console.error(response);
              reject(Error(response.Error));
            }
          } : null;

          if (current.onPrepareData) {
            data = current.onPrepareData.call(current, data, ajaxOptions);
          }

          // if Action is set, the fetch data will be used
          if (current.urlDetails) {
            // Fetch data
            ajaxOptions.data = data;
            app.ajax(ajaxOptions);
          } else {
            resolve({
              response: {
                Data: data,
                //FormData: current.formData
              },
              current: current,
              index: tabIndex
            });
          }
        }
        // Resolve response data and formData before then()
      })
          .then(function (data) {
            var response = data && data.response;
            var current = data && data.current;
            var tabIndex = data && data.index;

            if (current.onBeforeCreate) { current.onBeforeCreate.call(me, response); }
            if (me.options.onFormChange) { me.options.onFormChange.call(me, tabIndex); }

            var responseFormData = response && !!response.FormData && response.FormData;
            var options = {
              data: app.resolveValue(response && response.Data ? response.Data : undefined),
              formData: app.resolveValue((current && current.formData) || responseFormData, responseFormData),
              isNew: current.isNew === true,
              validate: current.validate === true,
              validation: !!current.validate,
              onCheckChanged: current.onCheckChanged,
              trackChanges: current.trackChanges === true,
              events: $.extend({
                    close: function (event) {
                      event.preventDefault();
                      event.stopPropagation();

                      var args = Array.prototype.slice.call(arguments).slice(1);

                      // Close view
                      me.$form.trigger("close", args);
                    }
                  },
                  current.events)
            };

            if (current.form) {
              // Current.form might be a value, a function or a promise
              return new Promise(function(resolve, reject) {
                resolve(app.resolveValue.call(current, current.form));
              }).then(function(form) {
                // debugger;
                me.current.currentForm = form.create($.extend(options, current.formOptions));
                return current;
              });
            }

            return current;
          }, function(error) {
            app.alertBox("Oops", (error || "Unhandled error"), "error");
          })
          .then(function(current) {
            me.current.index = tabIndex;

            if (current && current.onFormCreate) {
              current.onFormCreate.call(me.current.currentForm);
            }

            // Add form to container
            if (me.options.compatibility.setContainerHeightAuto){
              me.$container.css("height", "auto");
            }

            // Set new form as view
            if ($location) {
              $location.replaceWith(me.current.currentForm);
            } else {
              me.$container.empty().append(me.current.currentForm);
            }

            if (current && current.onFormAdd) {
              current.onFormAdd.call(me.current.currentForm);
            }

            // trigger navigator (if available) on form change 
            if (me.navigator) { me.navigator.onFormChanged.call(me, me.current.currentForm); }

            if (me.options.onFormChanged) {
              me.options.onFormChanged.call(me, me.current.currentForm);
            }

            if (me.options.onSetState) {
              me.options.onSetState.call(me, me.current.currentForm);
            }

            return current;
          });
    };

    this.updateTab = function(tabWithUpdate) {
      console.error(this, "updateTab", tabWithUpdate);
      var me = this;

      // Find tab which mathes tabWithUpdate
      me.$tabs
          .children()
          .filter(function() { return $(this).data("tab-item") === tabWithUpdate })
          .each(function() {
            var $li = $(this);

            // console.log(this, "updateTab", $li,
            //     !!!app.resolveValue(tabWithUpdate.visible),
            //     !!app.resolveValue(tabWithUpdate.selectable),
            //     tabWithUpdate.label);

            $li.toggleClass("hide", !!!app.resolveValue(tabWithUpdate.visible));
            $li.toggleClass("selectable", !!app.resolveValue(tabWithUpdate.selectable))
            $li.text(app.resolveValue(tabWithUpdate.label));
          });
    };

    this.addTabs = function () {
      me.$tabs.empty();

      $.each(me.current.forms, function (index, item) {
        var $li;
        var hide = app.resolveValue(item.visible) !== true;

        me.$tabs.append(
            $li = $("<li/>")
                .data("tab", index)
                .data("tab-item", item)
                .toggleClass("selectable", app.resolveValue(item.selectable))
                .toggleClass("hide", hide)
                .setClick(function (event) {
                  var $me = $(this);
                  if (app.resolveValue($me.data("tab-item").selectable) == false) {
                    event.stopImmediatePropagation();
                    return false;
                  }

                  $me.addClass("disabled");

                  me.$form.trigger("change-tab", [$(this).data("tab")]);

                  $me.removeClass("disabled");
                }));

        if (me.options.tabElement) {
          $li.append($(me.options.tabElement).text(item.label));
        } else {
          $li.text(item.label);
        }
      });
    };

    // Initialize the element
    this.init($element, options);
  };

  $.fn.formtabs = function (options) {
    return this.each(function () {
      new $.formtabs($(this), options);
    });
  };

  $.formtabs.defaultOptions = {
    compatibility: {
      setContainerHeightAuto: false
    },
    tabContainer: null,         // ul for containing the tabs
    formContainer: null,        // the element that will contain the current form (mostly a $div) 
    tabs: null,                 // array with the tabs
    selected: null,
    tabElement: null,           // embed the element in an element
    onTabChanged: null,         // event after the tab was changed: function(tabIndex). This is the formTabs container
    onBeforeRemove: null,        // event before the current tab is being removed
    onBeforeCreate: null,       // event just before form options are compiled and tab is filled
    onFormChanged: null,        // event after the form was changed: function($form). This is the formTabs container
    onFormChange: null,         // event before the form is being changed: function(tabIndex). This is the formTabs container
    onSetState: null,           // me.function($form) - call to set state
    validateNavigation: true,
    confirmNavigate: null,      // this.function(_super()) {}
    useOriginalDataSource: false,

    // popStateNavigator
    usePopStateNavigator: null, // 

    // action menu
    showActionMenu: null,       // default is hover, click
    onActionsShow: null,        // event before show actions menu (only when showActionMenu is click)
    onActionsHide: null,        // event when action menu hides

    intialChangeTab: true       // should the tab be changed on init?
  };

})(jQuery);
