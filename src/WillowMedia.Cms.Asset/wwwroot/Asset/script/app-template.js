﻿/* Copyright 2014 - 2015 Willow Media

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. /**/

(function ($) {
    $.listTemplate = function (element, options) {
        this.options = {};
        this.$element = null;

        this.init = function (element, options) {
            $(element).data('listTemplate', this);

            // Extend options with default options
            this.options = $.extend({}, $.listTemplate.defaultOptions, options);

            // reference the element
            this.$element = $(element);

            //Manipulate element here ...   
            var me = this;

            this.generate(options.data);
        };

        this.generate = function (collection) {
            var me = this;

            // $element should be ol or ul
            var $list = this.$element;
            $list.empty();

            // Add header
            if (me.options.header) {
                var $li = me.generateHeader();
                me.$element.append($li);
            }

            $(collection || null).each(function () {
                me.generateAndAddItem(this);
            });
        };

        this.generateAndAddItem = function (record) {
            var me = this;

            var $li = me.generateItem(record);
            me.$element.append($li);
        }

        this.generateHeader = function () {
            var me = this;
            var $li = $("<li/>");

            if ((me.options.headerClass || null) !== null)
                $li.addClass(me.options.headerClass);

            $(me.options.template).each(function () {
                var templateField = this;
                var $span = $("<span/>")
                    .attr("title", (templateField.label || templateField.prop || ''))
                    .text(templateField.label || templateField.prop || '');

                if ((templateField.width || null) !== null)
                    $span.css("width", templateField.width + '%');

                // Add span to listitem
                $li.append($span);
            });

            return $li;
        };

        this.generateItem = function (record) {
            var me = this;
            var $li = $("<li/>");
            $li.data(me.options.dataProperty, record);

            $(me.options.template).each(function () {
                var templateField = this;
                var $span = $("<span/>");

                if ((templateField.prop || null) !== null)
                    $span.text(record[templateField.prop] || '');
                if ((templateField.render || null) !== null)
                    templateField.render($span, record);
                if ((templateField.class || null) !== null)
                    $span.addClass(templateField.class);
                if ((templateField.width || null) !== null)
                    $span.css("width", templateField.width + '%');

                // Add span to listitem
                $li.append($span);
            });

            if ((me.options.onRenderDone || null) !== null)
                me.options.onRenderDone($li);

            if ((me.options.click || null) !== null) {
                $li.on("click", function(event) {
                    me.options.click(event, [$(this)]);
                });
            }

            return $li;
        };

        // options: {
        //      deselect: "..."       // jquery selector
        //      dataTemplate: {}      // 
        // }
        this.toArray = function (options) { 
            var me = this;
            var options =
                $.extend({
                    deselect: null,
                    dataTemplate: null
                }, options);

            var data = [];

            me.$element.children().each(function () {
                var dataItem = $(this).data(me.options.dataProperty);

                if ((dataItem || null) !== null && !$(this).is(options.deselect)) {
                    var item = $.extend(options.dataTemplate, dataItem);
                    data.push(item);
                }
            });

            return data;
        };        

        this.init(element, options);
    };

    $.listTemplate.defaultOptions = {
        template: null,
        dataProperty: 'rec',
        data: null,
        click: null,
        onRenderDone: null,
        header: true,
        headerClass: 'header',
        IEFixCallback: null             // URL to used to send a 'ping' before the upload. Fix for IE
    };

    // options: {
    //      template: [
    //          { prop: "", width: ?, style: '', class: '' },
    //          { ... }
    //      ],
    //      data: [],
    //      click: function(event),
    //      render: function($span, record)
    // }
    $.fn.listTemplate = function (options) {
        if ($(this).length == 1) {
            if ($(this).data("listTemplate") != null) {
                return $(this).data("listTemplate");
            } else {
                // Create formHandler for the first element selected
                return new $.listTemplate($(this[0]), options);
            }
        }
    };
})(jQuery);