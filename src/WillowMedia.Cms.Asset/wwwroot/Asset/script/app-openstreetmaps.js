﻿
/// <reference path="app.js" />
/* Copyright 2014 - 2015 Willow Media

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. /**/



(function ($) {
    "use strict";

    /* 
     * requires: 
     *      app.form.js
     *      autoloadmore.js
     *      app.overlay.js
     */
    $.openStreetMap = function ($element, options) {
        this.options = null;
        this.$element = null;
        this._markers = [];
        this._currentInfoWindow = null;
        this._bounds = null;

        this.map = null;
        this.mapLat = 52.3568374;
        this.mapLng = 5.6278221;
        this.mapDefaultZoom = 6;
        this.vectorSource = null;
        this.vectorLayer = null;
        this.mapView = null;
        this.rasterLayer = null;
        this.popupLayer = null;
        this.$popup = null;
        this.clusterLayer = null;
        this.clusterSource = null;

        this.defaultMarkerOptions = {
            center: false,
            latitude: null,
            longitude: null,
            title: null,
            url: null,
            $source: null
        };

        this.defaultOptions = {
            multiplication: null,
            singleMarker: false,
            draggable: false,
            center: null,               // TODO: default center point,
            data: null,                 // TODO: initial data array of coordinates {lat:...,lng:...}
            initialZoom: null,
            minimumZoomOnFit: null,
            cluster: null,
            popup: null,                // popup element
            OSMurlTemplate: "//tile.openstreetmap.nl/osm/{z}/{x}/{y}.png"
            // "https://b.tile.openstreetmap.org/{z}/{x}/{y}.png"  
            // "https://a.tile.openstreetmap.org/{z}/{x}/{y}.png"
        };

        this.init = function ($element, options) {
            var me = this;
            this.$element = $($element);

            this.$element
            .off()
            .empty()
            .data("openStreetMap", me)
            .addClass("formHandlerSerializable")
            .on("serialize", function (event) {
                event.preventDefault();
                event.stopPropagation();

                return me.serialize();
            })
            .on("set-value", function(event, value) {
                event.preventDefault();
                event.stopPropagation();

                me.clearMarkers();
                var $me = $(this);

                if (value) {
                    if (me.options.singleMarker === true) {
                        //me.addMarker(value);
                        $me.trigger("addMarker", [value]);
                    } else {
                        $.each(value, function(index, item) {
                            // me.addMarker(item);
                            $me.trigger("addMarker", [item]);
                        });
                    }
                }
            })
            .on("addMarker", function (event, coordinate, center, title, url, source) {
                event.preventDefault();
                event.stopPropagation();

                var markerOptions = null;
                switch (arguments.length) {
                    case 2:
                        markerOptions = $.extend(me.defaultMarkerOptions, coordinate);
                    default:
                        markerOptions = $.extend(me.defaultMarkerOptions, {
                            latitude: coordinate.Latitude,
                            longitude: coordinate.Longitude,
                            center: center,
                            title: title,
                            url: url,
                            $source: source
                        });
                        break;
                }

                var marker = me.addMarker(markerOptions);

                if (me.options.singleMarker === true) {
                    me.fitBounds();
                }

                if ((center || false) === true) {
                    me.center(coordinate.Latitude, coordinate.Longitude);
                }

                return marker;
            })
            .on("center", function (event, coordinate) {
                event.preventDefault();
                event.stopPropagation();
                me.center(coordinate.Latitude, coordinate.Longitude);
            })
            .on("fit", function (event, options) {
                event.preventDefault();
                event.stopPropagation();

                var zoom = null;
                if ((options || null) !== null) {
                    zoom = options.maxZoom || null;
                }

                me.fitBounds(zoom);
            })
            .on("clear", function (event) {
                event.preventDefault();
                event.stopPropagation();

                me.clearMarkers();
            })
            .on("updateSize", function(event) {
                event.preventDefault();
                event.stopPropagation();

                me.updateSize();
            });

            me.options = $.extend({}, me.defaultOptions, options);

            // me.map = new google.maps.Map(this.$element[0], {
            //     zoom: me.options.initialZoom || me.mapDefaultZoom,
            //     center: new google.maps.LatLng(me.mapLat, me.mapLng),  //),
            //     disableDefaultUI: false,
            //     mapTypeControl: true,
            //     navigationControl: true,
            //     mapTypeId: google.maps.MapTypeId.ROADMAP
            // });

            var id = $element[0].getAttribute("id");
            if (!id || id === '') {
                $element[0].setAttribute("id", id = app.uniqueId());
            }

            me.rasterLayer = new ol.layer.Tile({
                source: new ol.source.OSM({
                    url: me.options.OSMurlTemplate //"https://a.tile.openstreetmap.org/{z}/{x}/{y}.png"
                })
            });

            var source = new ol.source.Vector({
                features: []
            });

            me.clusterSource = new ol.source.Cluster({
                distance: 20, //parseInt(10),
                source: source
            });

            var styleCache = {};
            me.clusterLayer = new ol.layer.Vector({ //.VectorLayer({
                source: me.clusterSource,
                style: function(feature) {
                    // debugger;
                    var size = feature.get('features').length;
                    var style = styleCache[size];
                    if (!style) {
                        style = new ol.style.Style({
                            image: new ol.style.Circle({
                                radius: 10,
                                stroke: new ol.style.Stroke({
                                    color: '#fff'
                                }),
                                fill: new ol.style.Fill({
                                    color: '#ff0000'
                                })
                            }),
                            text: new ol.style.Text({
                                text: size.toString(),
                                fill: new ol.style.Fill({
                                    color: '#fff'
                                })
                            })
                        });
                        styleCache[size] = style;
                    }
                    return style;
                }
            });

            me.mapView = new ol.View({
                center: ol.proj.fromLonLat([me.mapLng, me.mapLat]),
                zoom: me.mapDefaultZoom
            });

            me.map = new ol.Map({
                target: id,
                layers: [me.rasterLayer, me.clusterLayer],
                view: me.mapView,
                controls: ol.control.defaults({
                    attributionOptions: {
                        collapsible: false
                    }
                })
            });

            me.map.on('click', function(evt) { me.click.call(me, evt); });

            if (me.options.popup) {
                me.$popup = $(me.options.popup);
                me.popupLayer = new ol.Overlay({
                    element: me.$popup[0],
                    autoPan: false,
                    positioning: 'bottom-center',
                    stopEvent: false,
                    offset: [0, -10]
                });
                me.map.addOverlay(me.popupLayer);
            }

            if ((me.options.data || null) !== null) {
                if (me.options.singleMarker === true) {
                    // me.options.data should be an object with coordinates
                    me.$element.trigger("addMarker", [me.options.data, true]);
                } else {
                    $.each(me.options.data, function (index, coordinate) {
                        me.$element.trigger("addMarker", [coordinate]);
                    });
                }

                me.fitBounds();

                if ((me.options.initialZoom || null) !== null) {
                    var initialZoom = me.options.initialZoom;
                }
            }

        };

        this.click = function(evt) {
            var me = this;
            if (me.options.popup) {
                var show = false;
                me.map.forEachFeatureAtPixel(evt.pixel,
                    function (feature, layer) {
                        var coord = me.map.getCoordinateFromPixel(evt.pixel);

                        var cfeatures = feature.get('features');

                        if (cfeatures) {
                            var $p = me.$popup.empty();

                            $.each(cfeatures, function(index, feature) {
                                if (index>0) { $p.append("<br/>"); }
                                $p.append($("<a/>").attr('href', feature.get("url")).text(feature.get('title')));
                            });

                            me.popupLayer.setPosition(coord);
                            me.$popup.show();
                            show = true;
                        } else {
                            me.$popup.hide();
                        }
                    });

                if (show === false) { me.$popup.hide(); }
            }

            return false;
        };

        this.updateSize = function() {
            var me =this;
            me.map.updateSize();
        };

        this.serialize = function () {
            var me = this;

            if (me._markers === null || me._markers.length == 0) {
                return null;
            } else if (me.options.singleMarker === true) {
                var marker = me._markers[0];

                var mult = me.options.multiplication || null;
                if ((mult || null) !== null) {
                    // multiplication is fix for mono bug
                    return { Multiplication: mult, Latitude: Math.round(marker.position.lat() * mult), Longitude: Math.round(marker.position.lng() * mult) };
                } else {
                    return { Latitude: marker.latitude, Longitude: marker.longitude };
                }
            } else {
                return me._markers;
            }
        };

        this.closeInfo = function () {
            if (this._currentInfoWindow !== null) {
                this._currentInfoWindow.close();
            }
        };

        this.center = function(latitude, longitude) {
            var me = this;
            me.map.getView().setCenter(
                new ol.geom.Point(ol.proj.transform([parseFloat(longitude), parseFloat(latitude)], 'EPSG:4326', 'EPSG:3857')));
        };

        this._getSource = function() {
            var me = this;
            return me.clusterSource.getSource();
        };

        this.addMarker = function (markerOptions) {
            if (!markerOptions) { return; }
            var me = this;

            var feature =  new ol.Feature({
                geometry: new ol.geom.Point(ol.proj.transform([parseFloat(markerOptions.longitude), parseFloat(markerOptions.latitude)], 'EPSG:4326', 'EPSG:3857')),
                title: markerOptions.title,
                url: markerOptions.url
            });

            me._getSource().addFeature(feature);
            me._markers.push(markerOptions);
        };

        this.fitBounds = function (maxZoom) {
            var me = this;
            var map = me.map;

            var extent = me._getSource().getExtent();
            map.getView().fit(extent, { size:map.getSize(), maxZoom: me.mapDefaultZoom }); //map.getSize());
        };

        this.clearMarkers = function () {
            var me = this;
            //me.clusterLayer.getSource().clear();
            me._getSource().clear();
            me._markers = [];
        };

        this.init($element, options);
    };

    $.fn.openStreetMap = function (options) {
        return this.each(function () {
            (new $.openStreetMap($(this), options));
        });
    };
})(jQuery);
