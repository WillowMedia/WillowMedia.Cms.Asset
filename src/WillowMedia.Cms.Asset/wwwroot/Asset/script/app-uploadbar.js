﻿"use strict";

(function ($) {
    $.fn.uploadBar = function (options) {
        options = $.extend({
            value: null,
            data: null,                     // The single data object, containing the file info
            dataObj: null,                  // Entire data object as provided to the form
            dropZone: undefined,            // dropzone, if not provided the div will be used
            emptyImageUrl: null,            // "@Url.Content("~/Asset/image/empty.gif")"
            uploadPath: null,               // "~/Data/UploadTempFile" 
            waitingImageUrl: null,          // "@Url.Content("~/Asset/image/waiting.16.gif")"
            mimetypes: "image/gif, image/jpeg, image/jpg, image/bmp, image/tiff, application/pdf, image/png",
            start: null,                    // Event on start upload
            finish: null,                    // Event on done upload,
            IEFixCallback: null,             // URL to used to send a 'ping' before the upload. Fix for IE
            noThumbnail: null,
            maxFileSize: null,
            onDone: null                    // function(response)
        }, options);

        return this.each(function () {
            // replace element
            var $me = $(this);
            var meId = $me.attr("id");
            //var uploadId = meId + "FileUpload";

            var $divFileinfo, $fileupload, $change, $thumb;
            $fileupload = $("<input type='file' name='FileUpload' style='position:absolute;top:-1000px;left:-1000px; visibility: hidden; width: 0px; height: 0px; opacity: 0;' />")
            .attr("accept", options.mimetypes);
            $divFileinfo = $("<div class='fileinfo barcontainer' />")
            .attr("id", meId)
            .data($me.data())
            .append($change = $("<input type='hidden' id='" + meId + "Change' name='" + meId + "change' />"))
            .append(options.noThumbnail === true ? "" : $("<div class='bc left image' />").append($thumb = $("<img />").attr("src", options.emptyImageUrl)))
            .append($("<div class='bc remaining' />").append("<div class='bc right action'><a href='#'><i class='icon-cancel'></i></a></div>").append("<div class='bc remaining title'></div>"));

            var fileinfo = options.value || options.data || ((options.dataObj || null) !== null
                ? options.dataObj[$divFileinfo.data("prop")]
                : null) || null;

            $divFileinfo
            .addClass("formHandlerSerializable")
            .setDataSource(fileinfo)
            .on("serialize", function () {
                return $(this).is(".deleted") ? null : $(this).dataSource();
            })
            .on("set-value", function (event, file) {
                var $divFileinfo = $(this);

                if ((file || null) === null || ((file.Filename || null) === null)) {
                    $divFileinfo.setDataSource(null).removeClass("deleted");
                    $divFileinfo.find(".title").text('');
                    $divFileinfo.find(".image > img, .action > a").hide();
                } else {
                    $divFileinfo.setDataSource(file).removeClass("deleted");
                    $divFileinfo.find(".title").text(file.Filename);
                    $divFileinfo.find(".action > a").show();

                    if (options.noThumbnail !== true) {
                        $thumb.show().removeClass("waiting").attr("src", file.Thumbnail || options.emptyImageUrl);
                    }
                }

                $divFileinfo.trigger("change");
            })
            .find("div.action > a")
            .on("click", function (event) {
                event.preventDefault();
                event.stopPropagation();
                if ($(this).isDisabled()) {
                    return false;
                }
                $divFileinfo.toggleClass("deleted");
                $divFileinfo.trigger("change");
            });

            // Add file uploader
            $divFileinfo
            .trigger("set-value", [fileinfo])
            .data("upload", $fileupload)
            .on("click", function (event) {
                event.preventDefault();
                $(this).data("upload").trigger("click");
            });

            var upload = $fileupload.on("click", function (event) {
                if ($(this).isDisabled() || $divFileinfo.isDisabled()) {
                    event.preventDefault();
                    event.stopPropagation();
                    return false;
                }
            }).fileupload({
                url: options.uploadPath,
                sequentialUploads: true,
                singleFileUploads: true,
                dataType: 'json',
                autoUpload: false,
                dropZone: options.dropZone || $divFileinfo,
                formData: {},
                maxFileSize: options.maxFileSize,
                start: function (e) {
                    if ((options.IEFixCallback || null) !== null) {
                        app.IEUploadFix(options.IEFixCallback);
                    }

                    if ((options.start || null) !== null) {
                        options.start();
                    }
                },
                add: function (e, data) {
                    if ($divFileinfo.isDisabled()) {
                        return false;
                    }

                    $divFileinfo.data("upload", $(this));

                    $change.val(new Date());

                    $.each(data.files, function (index, file) {
                        data.context = $divFileinfo;
                        $divFileinfo.find(".title").text(file.name);
                        $divFileinfo.find(".image > img").show().addClass("waiting").attr("src", options.waitingImageUrl);
                        $divFileinfo.find(".action > a").hide();
                        $divFileinfo.show();
                    });

                    data.submit();
                },
                progress: function (e, data) {
                    $.each(data.files, function (index, file) {
                        var $divFileinfo = $(data.context);
                        $divFileinfo.find(".title").text("({1}%) {0}".format(file.name, Math.round(parseInt(data.loaded / data.total * 100, 10))));
                    });
                },
                done: function (e, data) {
                    var response = data.result;
                    var $divFileinfo = $(data.context);

                    if ((options.onDone || null) !== null) {
                        options.onDone.call($divFileinfo, response);
                    } else {
                        if ((response.Succes || false) === true) {
                            $.each(response.Data, function (index, item) {
                                $divFileinfo.trigger("set-value", [item]);
                            });
                        } else if ((response.Error || null) !== null) {
                            $divFileinfo.trigger("set-value", [null]);
                            alert(response.Error);
                        }
                    }

                    if ((options.finish || null) !== null) {
                        options.finish();
                    }
                },
                fail: function (e) {
                    $divFileinfo.trigger("set-value", [null]);

                    if ((options.finish || null) !== null) {
                        options.finish();
                    }
                }
            });

            $me.replaceWith([$divFileinfo, $fileupload]);
        });
    }
})(jQuery);
