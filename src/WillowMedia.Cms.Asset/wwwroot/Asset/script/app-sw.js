
var settings = settings || {
    currentCacheName: "fallback_cachename_should_never_be_used",
    notificationTitle: "WillowMedia.Cms",
    allowCaching: false,
    usePush: false,
    showDebug: false,
    pathsToCache: [],
    filesToCache: null
};

// Always reload the serviceworker:
// 
// - On default html page, do nothing and wait for the user to reload
// - On CMS pages/forms, we could show a notification that it would be preferrable to reload the page. In most
//   cases the resources were already loaded and it wont make a difference.

var settingsHelper = {
    notificationTitle: settings.notificationTitle || "WillowMedia.Cms",
    pathsToCache: settings.pathsToCache || [],
    protocolLess: /^\/\//,
    protocolMatch: /^(http:|https:)\/\//,
    pathMatchesCachePaths: function (response) {
        let me = this;
        let url = new URL(response.url);
        let urlPath = url.pathname;
        let index;
        let count = this.pathsToCache.filter(function(path) {
            // match path (case sensitive) or match from beginning (//{hostname})  
            // TODO: case insensitive?
            // path: /Asset
            // path: //static.gfonts.com
            // path: http://static.gfonts.com
            // path: https://static.gfonts.com

            return urlPath.indexOf(path) === 0 ||
                (me.protocolLess.test(path) && (index = response.url.indexOf(path) >= 0) && index <= 6) ||
                (me.protocolMatch.test(path) && response.url.indexOf(path) === 0);
        }).length;
        return count > 0;
    }
}

// This code executes in its own worker or thread
self.addEventListener("install", event => {
    settings.showDebug && console.log("Service worker installed, call skipWaiting");

    self.skipWaiting();
});

self.addEventListener("activate", event => {
    settings.showDebug && console.log("Service worker activated");

    var cacheVersion = settings.currentCacheName;

    settings.showDebug && console.log("cache name set to: ", cacheVersion);

    event.waitUntil(
        caches.keys()
            .then(function (cacheNames) {
                cacheNames.map(function (cacheName) {
                    if (!!cacheName && cacheName.indexOf(cacheVersion) < 0) {
                        return caches.delete(cacheName);
                    }
                });
            }).then(function() {
            var filesToCache = settings.filesToCache;

            if (!!filesToCache && !!cacheVersion) {
                settings.showDebug && console.log("Cache certains files for offline usage");

                return event.waitUntil(caches
                    .open(cacheVersion)
                    .then(cache => cache.addAll(filesToCache))
                    .catch(err => {
                        settings.showDebug && console.error('Fout tijdens cachen filesToCache:', err);
                    }));
            }

            return null;
        }).catch(err => {
            settings.showDebug && console.error('Fout tijdens cache verwijdering in activate:', err);
        }));
});

self.addEventListener('message', (event) => {
    // if (event.data === 'skipWaiting') {
    //     !!settings.showDebug && console.log("Got skipwaiting message");
    //     return skipWaiting();
    //     // return false;
    // }

    if (!!event && !!event.data && !!self) {
        !!settings.showDebug && console.log(self, event);

        // Select who we want to respond to
        !!self.clients && self.clients.matchAll({
            includeUncontrolled: true,
            type: 'window',
        }).then(clients => {
            if (!!clients && clients.length > 0) {
                // Send a response - the clients array is ordered by last focused
                clients.map(client => client.postMessage(event.data))
            }
        }).catch(err => {
            !!settings.showDebug && console.error('Fout tijdens message:', err);
        });
    }
});

self.addEventListener('push', function(event) {
    if (!!!settings.usePush) return;
    if (!(self.Notification && self.Notification.permission === 'granted')) {
        return;
    }

    if (event && event.data) {
        let data = event.data.json() || event.data.text();
        let options = {
            vibrate: [100, 50, 100],
            data: {
                dateOfArrival: Date.now(),
                primaryKey: null
            }
        };

        settings.showDebug && console.log("push", event.data, data);

        // const options = {
        //     "//": "Visual Options",
        //     "body": "<String>",
        //     "icon": "<URL String>",
        //     "image": "<URL String>",
        //     "badge": "<URL String>",
        //     "vibrate": "<Array of Integers>",
        //     "sound": "<URL String>",
        //     "dir": "<String of 'auto' | 'ltr' | 'rtl'>",
        //     "//": "Behavioural Options",
        //     "tag": "<String>",
        //     "data": "<Anything>",
        //     "requireInteraction": "<boolean>",
        //     "renotify": "<Boolean>",
        //     "silent": "<Boolean>",
        //     "//": "Both Visual & Behavioural Options",
        //     "actions": "<Array of Strings>",
        //     "//": "Information Option. No visual affect.",
        //     "timestamp": "<Long>"
        // }                
        let title = settingsHelper.notificationTitle;

        if (!!data && typeof data === "object") {
            if (!!data.Title) title = data.Title;
            if (!!data.Message) options.body = data.Message;
            if (!!data.Tag) options.tag = data.Tag;
            if (!!data.Icon) options.icon = data.Icon;
            if (!!data.Image) options.image = data.Image;
            if (!!data.Badge) options.badge = data.Badge;
            if (!!data.PrimaryKey) options.data.primaryKey = data.PrimaryKey;
            if (!!data.Timestamp) options.data.timestamp = data.Timestamp;
            // timestamp
        } else if (typeof data === "string") {
            options.body = data;
        } else {
            options.body = event.data.text();
        }

        settings.showDebug && console.log(title, options);

        event.waitUntil(self.registration.showNotification(title, options));
    } else {
        settings.showDebug && console.log('This push event has no data.');
    }
});


self.addEventListener('fetch', event => {
    // settings.showDebug && 
    // let request = !!event && !!event.request && event.request;
    // console.debug("skip cache", {
    //     allowCaching: settings.allowCaching,
    //     method: !!request && request.method,
    //     headers: !!request && request.headers,
    //     url: !!request && request.url,
    //     request: !!request && request,
    //     keepalive: !!request && request.keepalive
    // });

    // if (event.request.method === 'POST' && event.request.headers.get('Accept') === 'application/json') {
    //     return;
    // }

    event.respondWith((async () => {
        // New serviceworker awaiting? then skipwaiting and reload the current request
        if (event.request.mode === "navigate" &&
            event.request.method === "GET" &&
            self.registration.waiting)
        {
            return fetch(event.request).then(response => {
                settings.showDebug && console.log("reload due to new serviceworker");
                self.registration.waiting.postMessage('skipWaiting');
                return response;
            });
        }

        // Caching disabled, no cache name, or not a GET request?
        if (!!!settings.allowCaching ||
            !!!settings.currentCacheName ||
            event.request.method !== 'GET')
        {
            const useKeepAlive = event.request.headers.get('Accept') === 'text/event-stream';

            return fetch(event.request, useKeepAlive
                ? { keepalive: true }
                : null);
        }

        settings.showDebug && console.log("caches match");

        return caches
            .match(event.request, { ignoreSearch: true })
            .then(response => {
                // Return response from cache, or fetch the request
                return response || fetch(event.request);
            })
            .then(response => {
                if (response.status !== 200)
                {
                    settings.showDebug && console.log("response didnt return 200");
                    return response;
                }

                // Path of response is not in list? then return without caching
                if (!settingsHelper.pathMatchesCachePaths(response)) {
                    settings.showDebug && console.log("Return without caching");
                    return response;
                }

                settings.showDebug && console.log("Adding response to cache");

                return caches
                    .open(settings.currentCacheName)
                    .then(cache => {
                        cache.put(event.request.url, response.clone());
                        return response;
                    }).catch(err => {
                        settings.showDebug && console.error('Fout tijdens toevoegen aan cache:', err);
                    });
            })
            .catch(error => {
                console.error("Catch error serviceworker", error);
                console.error(error);

                // return caches.match('/offline.html');
                // new Response('<p>Hello from your friendly neighbourhood service worker!</p>', {
                //        headers: { 'Content-Type': 'text/html' }
                // }); 
            });
    })());
});