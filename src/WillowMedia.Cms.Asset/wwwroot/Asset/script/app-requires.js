"use strict";

// <link type="text/css" rel="stylesheet" href="/Asset/script/sweetalert/dist/sweetalert.min.css?v=20191223115856">
function loadCss(src, callback) {
    var linkElement = document.createElement("link")
    linkElement.type = "text/css";
    linkElement.rel = "stylesheet";
    linkElement.href = src;

    // Check if it exists
    var count = $("link").filter(function() { return $(this).attr("href") == src; }).length;
    if (count > 0) {
        if (callback) callback();
        return;
    }

    if (callback) {
        if (linkElement.readyState) {  //IE
            linkElement.onreadystatechange = function () {
                if (linkElement.readyState == "loaded" || linkElement.readyState == "complete") {
                    linkElement.onreadystatechange = null;
                    callback();
                }
            };
        } else {  //Others
            linkElement.onload = function () {
                callback();
            };
        }
    }

    document.getElementsByTagName("head")[0].appendChild(linkElement);
}

/* scripts /**/
function loadScript(src, callback) {
    var script = document.createElement("script")
    script.type = "text/javascript";
    script.src = src;

    // Check if it exists
    var count = $("script").filter(function() { return $(this).attr("src") == src; }).length;
    if (count > 0) {
        if (callback) callback();
        return;
    }

    if (callback) {
        if (script.readyState) {  //IE
            script.onreadystatechange = function () {
                if (script.readyState == "loaded" || script.readyState == "complete") {
                    script.onreadystatechange = null;
                    callback();
                }
            };
        } else {  //Others
            script.onload = function () {
                callback();
            };
        }
    }

    document.getElementsByTagName("head")[0].appendChild(script);
}

function requires(validate, src, callback) {
    if (typeof (validate) === 'function' && validate()) {
        callback();
    } else if (validate === true) {
        callback();
    } else {
        if ($.isArray(src)) {
            var loop = {
                index: -1,
                src: src,
                callback: callback,
                run: function () {
                    this.next();
                },
                next: function () {
                    var me = this;
                    if (me.index >= me.src.length - 1) {
                        //console.log("callback");
                        me.callback();
                    } else {
                        me.index++;
                        //console.log(me.index, me.src[me.index]);

                        loadScript(me.src[me.index], function () { me.next() });
                    }
                }
            }.run();
        } else {
            loadScript(src, callback);
        }
    }
}