﻿(function ($) {
    "use strict";

    $.fn.instantDropdown = function(options) {
        if ($(this).length !== 1) { return this; }

        options = $.extend(
            {
                position: "absolute",
                ddlClass: "dropdownlist",
                data: null,                                                     // Array of data items
                onClick: null,                                                  // function(dataObj). This is the element
                onAfterFill: null,                                              // function($acdiv, options.data)
                template: function(dataObj) { return new String(dataObj); },    // function(dataObj), returns $element as child of the $li
                alignment: null
            },
            options);

        return this.each(function() {
            var $me = $(this);

            // Remove dropdown
            var $current = $me.data("instantDropdown") || null;
            if ($current !== null) { $current.trigger("close"); }

            // Create new dropdownlist
            var $acdiv = $("<div />");
            $me.data("instantDropdown", $acdiv);

            switch (options.position) {
                case "absolute":
                    $acdiv.css({position: "absolute", zIndex: 99999});
                    break;
                case "relative":
                default:
                    break;
            }

            $acdiv.addClass(options.ddlClass)
                .hide()
                .on("close", function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    // console.log("close");
                    // remove event on body
                    $("body").off("mouseup", onBodyClick);
                    $(this).hide().remove();
                })
                .on("blur", function(event) {
                    // console.log("blur");
                    $(this).trigger("close");
                })
                .on("selected", function (event, data) {
                    // console.log("selected");
                    event.preventDefault();
    
                    if ((options.onClick || null) !== null) {
                        options.onClick.call($me, data);
                    }
    
                    $(this).trigger("close");
                })
                .on("checkScroll", function (event) {
                    // console.log("checkScroll");
                    var $me = $(this);
    
                    var $hover = $me.find(".hover");
                    if ($hover.length === 1) {
                        // get dimensions view
                        var meTop = $me.scrollTop();
                        var meHeight = $me.innerHeight();
                        var meBottom = meTop + meHeight;
                        var hoverTop = $hover.position().top + meTop;
                        var hoverBottom = hoverTop + $hover.outerHeight();
    
                        // If view under hover
                        if (meTop > hoverTop) {
                            // scroll to visible
                            $me.scrollTop(hoverTop);
                            // If view above hover
                        } else if (meBottom < hoverBottom) {
                            $me.scrollTop(hoverBottom - meHeight + 2);
                        }
                    }
                })
                .on("resizeDropdownlist", function (event) {
                    var loc = $me.offset();
                    var width = (options.width || null) !== null ? options.width : $me.outerWidth();
    
                    switch (options.alignment || '') {
                        case "right":
                            $acdiv.offset({ top: loc.top + $me.outerHeight(), left: ((loc.left + me.$element.outerWidth()) - width) });
                            $acdiv.width(width);
                            break;
                        default:
                            $acdiv.offset({ top: loc.top + $me.outerHeight(), left: loc.left });
                            $acdiv.width(width);
                            break;
                    }
                });

            $(options.data).each(function () {
                var $li = $("<li/>")
                    .on("mousedown", function(event) {
                        event.preventDefault();
                        event.stopPropagation();
                        $acdiv.trigger("selected", [$(this).dataSource()]);
                    })
                    .on("click", function (event) {
                        event.preventDefault();
                        event.stopPropagation();
                    })
                    .append(options.template(this));

                $li.dataSource(this);

                $acdiv.append($li);
            });

            $me.on("manualBlur", function(event) {
                event.preventDefault();
                event.stopPropagation();
                $acdiv.trigger("close");
            })
            .on("blur", function(event) {
                $acdiv.trigger("blur");
            });

            if (options.onAfterFill) {
                options.onAfterFill.call($acdiv, options.data);
            }

            //$("body").append($acdiv);
            // $me.after($acdiv);
            $("body").append($acdiv);
            $me.closest("form").trigger("manualBlur");

            $acdiv.show().trigger("resizeDropdownlist");

            // add resize event
            $(window).on("resize", function (event) {
                $acdiv.trigger("resizeDropdownlist");
            });

            var onBodyClick = function (event) {
                var $target = $(event.target);
                if ($target.is($acdiv)) { return false; }

                if ($acdiv.is(":visible")) {
                    var contains = false;
                    $target.parents().filter(function () { if ($(this).is($acdiv)) { contains = true; return false; }; }); //.contains($acdiv)) {
                    if(contains !== true) {
                        event.preventDefault();
                        event.stopPropagation();
                        $acdiv.trigger("close");
                    }
                }
            };

            $("body").on("mouseup", onBodyClick);
        });
    }
})(jQuery);