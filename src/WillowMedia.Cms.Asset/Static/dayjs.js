
// Add the plugins
dayjs.extend(dayjs_plugin_weekOfYear);
dayjs.extend(dayjs_plugin_isoWeek);
dayjs.extend(dayjs_plugin_isoWeeksInYear);
dayjs.extend(dayjs_plugin_utc);
dayjs.extend(dayjs_plugin_advancedFormat);
dayjs.extend(dayjs_plugin_customParseFormat);
dayjs.extend(dayjs_plugin_duration);
dayjs.extend(dayjs_plugin_relativeTime);

if (!!!window.moment) {
    window.moment = dayjs;
}

