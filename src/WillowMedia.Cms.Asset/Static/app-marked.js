// Underline
const markedUnderline = {
    name: 'underline',
    level: 'inline',
    start(src) {
        return src.indexOf('__');
    },
    tokenizer(src) {
        const match = src.match(/^__(\w+)__/);

        if (!match) {
            return;
        }
        
        return {
            type: 'underline', raw: match[0], text: match[1]
        };
    },
    renderer(token) {
        return `<u>${token.text}</u>`;
    }
};

// superscript.js
const markedSuperscript = {
    name: 'superscript', 
    level: 'inline', 
    start(src) {
        return src.indexOf('^');
    }, 
    tokenizer(src) {
        const match = src.match(/^\^(\w+)\^/);

        if (!match) {
            return;
        }

        return {
            type: 'superscript', raw: match[0], text: match[1]
        };
    }, 
    renderer(token) {
        return `<sup>${token.text}</sup>`;
    }
};

// subscript.js
const markedSubscript = {
    name: 'subscript', 
    level: 'inline',
    start(src) {
        return src.indexOf('~');
    }, 
    tokenizer(src) {
        const match = src.match(/^~(\w+)~/);

        if (!match) {
            return;
        }

        return {
            type: 'subscript', raw: match[0], text: match[1]
        };
    }, 
    renderer(token) {
        return `<sub>${token.text}</sub>`;
    }
};

// highlight
const markedHighlight = {
    name: 'highlight', 
    level: 'inline', 
    start(src) {
        return src.indexOf('==');
    }, 
    tokenizer(src) {
        var match = src.match(/^==([^=]+)==/);
        
        if (!match) {
            return;
        }

        return {
            type: 'highlight', 
            raw: match[0], 
            text: this.lexer.inlineTokens(match[1].trim())
        };
    }, 
    renderer(token) {
        return `<mark>${this.parser.parseInline(token.text)}</mark>`;
    }
};

// image({ href, title, text }: Tokens.Image): string {
//     const cleanHref = cleanUrl(href);
//     if (cleanHref === null) {
//         return text;
//     }
//     href = cleanHref;
//
//     let out = `<img src="${href}" alt="${text}"`;
//     if (title) {
//         out += ` title="${title}"`;
//     }
//     out += '>';
//     return out;
// }

const caret = /(^|[^\[])\^/g;
const edit = function(regex, opt) {
    let source = typeof regex === 'string' ? regex : regex.source;
    opt = opt || '';
    const obj = {
        replace: function(name, val) {
            let valSource = typeof val === 'string' ? val : val.source;
            valSource = valSource.replace(caret, '$1');
            source = source.replace(name, valSource);
            return obj;
        },
        getRegex: function(){
            return new RegExp(source, opt);
        },
    };
    return obj;
}
const _inlineLabel = /(?:\[(?:\\.|[^\[\]\\])*\]|\\.|`[^`]*`|[^\[\]\\`])*?/;
const attributes =  /(?:\{(.*?)\})?/;
const linkWithAttribute = edit(/^!\[(label)\]\(\s*(href)(?:\s+(title))?\s*\)attributes/)
    .replace('label', _inlineLabel)
    .replace('attributes', attributes)
    .replace('href', /<(?:\\.|[^\n<>\\])+>|[^\s\x00-\x1f]*/)
    .replace('title', /"(?:\\"?|[^"\\])*"|'(?:\\'?|[^'\\])*'|\((?:\\\)?|[^)\\])*\)/)
    .getRegex();

// console.log("linkWithAttribute", "![bla](/bla)", linkWithAttribute.test("![bla](/bla)"));
// console.log("linkWithAttribute", "[bla](/bla)", linkWithAttribute.test("[bla](/bla)"));


// const parseKeyValuePairs = function(input) {
//     // Reguliere expressie om key-value paren te matchen
//     const regex = /(\s*[\w]+)\s*=\s*([\'\"]?[\w\s]*[\'\"]?)(?:\s*[,;]|\s*$)/g;
//     const result = [];
//     let match;
//
//     while ((match = regex.exec(input)) !== null) {
//         const key = match[1].trim(); // Verwijder eventuele voorloopspaties van de key
//         let value = match[2];
//
//         // Verwijder quotes rondom de value als die er zijn
//         if (value.startsWith('"') && value.endsWith('"') || value.startsWith("'") && value.endsWith("'")) {
//             value = value.slice(1, -1);
//         }
//
//         result.push({ key, value });
//     }
//
//     return result;
// }

function parseKeyValuePairs(input) {
    const result = [];

    // Regex om key-value paren te matchen
    const regex = /(\s*[\w]+)\s*=\s*(['"]?)(.*?)\2(?=\s*[,;]|\s*$)/g;

    let match;
    while ((match = regex.exec(input)) !== null) {
        const key = match[1].trim(); // Verwijder eventuele voorloopspaties van de sleutel
        const value = match[3]; // Waarde kan zijn zonder quotes of tussen quotes

        result.push({ key, value });
    }

    return result;
}

const markedImageAttributedInline = {
    name: 'image',
    level: 'inline',
    start(src) {
        return src.indexOf('![');
    },
    tokenizer(src) {
        var match = linkWithAttribute.exec(src);
        if (!match) {
            return;
        }

        return {
            type: 'image',
            raw: match[0],
            text: match[1], //this.lexer.inlineTokens(match[1].trim())
            href: match[2],
            title: match[3],
            attributes: match[4]
        };
    },
    renderer(token) {
        var $img = $("<img/>")
            .attr("src", token.href)
            .attr("title", token.title || token.text);
        
        if (!!token.attributes) {
            try {
                var keyPairs = parseKeyValuePairs(token.attributes);
                $.each(keyPairs, function (index, pair) {
                    switch (pair.key.toLocaleLowerCase()) {
                        case 'class':
                            $img.addClass(pair.value);
                            break;
                        case "width":
                            $img.css(pair.key, pair.value);
                            break;
                        default:
                            $img.attr(pair.key, pair.value);
                            break;
                    }
                })
            } catch (e) {
                console.error(e);
            }
        }
        
        return $img[0].outerHTML;
    }
};

// https://github.com/digitallinguistics/ling-md/blob/main/extensions/attributes/
// const attrRegExp = /^\s*\{(?<attributes>.+?)\}/v; // Anchored to start of string
//
// const inlineAttribute = {
//   level: `inline`,
//   name:  `attributes`,
//   renderer() {
//     return ``
//   },
//   start(src) {
//     return src.indexOf(`{`) === 0
//   },
//   tokenizer(src, tokens) {
//     const match = attrRegExp.exec(src);
//     const token = tokens.at(-1);
//
//     if (match && tokens.length) {
//       token.attributes = parseAttributes(match.groups.attributes);
//
//       return {
//         raw:  match[0],
//         type: `attributes`,
//       };
//     }
//   },
// }
//
// export default function withAttributes(render) {
//     return function renderAttributes(token) {
//         if (!token.attributes) return render.call(this, token)
//         const attributesString = createAttributesString(token.attributes)
//         return render.call(this, token).replace(tagNameRegExp, `<$<tag> ${ attributesString }$<suffix>`)
//     }
// }



// Copy van de table extension

const getTableCell = (text, cell, type, align, width) => {
    if (!cell.rowspan) {
        return '';
    }
    const tag = `<${type}`
        + `${cell.colspan > 1 ? ` colspan=${cell.colspan}` : ''}`
        + `${cell.rowspan > 1 ? ` rowspan=${cell.rowspan}` : ''}`
        + `${width > 0 ? ` width=${width}%` : ''}`
        + `${align ? ` align=${align}` : ''}>`;
    return `${tag + text}</${type}>\n`;
};

const splitCells = (tableRow, count, prevRow = []) => {
    const cells = [...tableRow.matchAll(/(?:[^|\\]|\\.?)+(?:\|+|$)/g)].map((x) => x[0]);

    // Remove first/last cell in a row if whitespace only and no leading/trailing pipe
    if (!cells[0]?.trim()) { cells.shift(); }
    if (!cells[cells.length - 1]?.trim()) { cells.pop(); }

    let numCols = 0;
    let i, j, trimmedCell, prevCell, prevCols;

    for (i = 0; i < cells.length; i++) {
        trimmedCell = cells[i].split(/\|+$/)[0];
        cells[i] = {
            rowspan: 1,
            colspan: Math.max(cells[i].length - trimmedCell.length, 1),
            text: trimmedCell.trim().replace(/\\\|/g, '|')
            // display escaped pipes as normal character
        };

        // Handle Rowspan
        if (trimmedCell.slice(-1) === '^' && prevRow.length) {
            // Find matching cell in previous row
            prevCols = 0;
            for (j = 0; j < prevRow.length; j++) {
                prevCell = prevRow[j];
                if ((prevCols === numCols) && (prevCell.colspan === cells[i].colspan)) {
                    // merge into matching cell in previous row (the "target")
                    cells[i].rowSpanTarget = prevCell.rowSpanTarget ?? prevCell;
                    cells[i].rowSpanTarget.text += ` ${cells[i].text.slice(0, -1)}`;
                    cells[i].rowSpanTarget.rowspan += 1;
                    cells[i].rowspan = 0;
                    break;
                }
                prevCols += prevCell.colspan;
                if (prevCols > numCols) { break; }
            }
        }

        numCols += cells[i].colspan;
    }

    // Force main cell rows to match header column count
    if (numCols > count) {
        cells.splice(count);
    } else {
        while (numCols < count) {
            cells.push({
                colspan: 1,
                text: ''
            });
            numCols += 1;
        }
    }
    return cells;
};

// https://github.com/calculuschild/marked-extended-tables
// copy the code from inside the export here
const endRegex = [];
const extendedTables = {
    name: 'spanTable',
    level: 'block', // Is this a block-level or inline-level tokenizer?
    start(src) { return src.match(/^\n *([^\n ].*\|.*)\n/)?.index; }, // Hint to Marked.js to stop and check for a match
    tokenizer(src, tokens) {
        // const regex = this.tokenizer.rules.block.table;
        let regexString = '^ *([^\\n ].*\\|.*\\n(?: *[^\\s].*\\n)*?)' // Header
            + ' {0,3}(?:\\| *)?(:?-+:? *(?:\\| *:?-+:? *)*)(?:\\| *)?' // Align
            + '(?:\\n((?:(?! *\\n| {0,3}((?:- *){3,}|(?:_ *){3,}|(?:\\* *){3,})' // Cells
            + '(?:\\n+|$)| {0,3}#{1,6} | {0,3}>| {4}[^\\n]| {0,3}(?:`{3,}'
            + '(?=[^`\\n]*\\n)|~{3,})[^\\n]*\\n| {0,3}(?:[*+-]|1[.)]) |'
            + '<\\/?(?:address|article|aside|base|basefont|blockquote|body'
            + '|caption|center|col|colgroup|dd|details|dialog|dir|div|dl|dt'
            + '|fieldset|figcaption|figure|footer|form|frame|frameset|h[1-6]'
            + '|head|header|hr|html|iframe|legend|li|link|main|menu|menuitem'
            + '|meta|nav|noframes|ol|optgroup|option|p|param|section|source'
            + '|summary|table|tbody|td|tfoot|th|thead|title|tr|track|ul)'
            + '(?: +|\\n|\\/?>)|<(?:script|pre|style|textarea|!--)endRegex).*(?:\\n|$))*)\\n*|$)'; // Cells

        regexString = regexString.replace('endRegex', endRegex.map(str => `|(?:${str})`).join(''));
        const regex = new RegExp(regexString);
        const cap = regex.exec(src);

        if (cap) {
            const item = {
                type: 'spanTable',
                header: cap[1].replace(/\n$/, '').split('\n'),
                align: cap[2].replace(/^ *|\| *$/g, '').split(/ *\| */),
                rows: cap[3] ? cap[3].replace(/\n$/, '').split('\n') : [],
                // PH: add place to store width
                width: []
            };

            // Get first header row to determine how many columns
            item.header[0] = splitCells(item.header[0]);

            const colCount = item.header[0].reduce((length, header) => {
                return length + header.colspan;
            }, 0);

            if (colCount === item.align.length) {
                item.raw = cap[0];

                let i, j, k, row;
                
                // Get alignment row (:---:)
                let l = item.align.length;

                // PH: Determine if there are different widths in the table
                let hasMultipleSizes = [...new Set(item.align.map(v => v.length))].length > 1; 

                for (i = 0; i < l; i++) {
                    // PH: store the size of the column
                    if (hasMultipleSizes)
                        item.width[i] = (item.align[i] || "").length;
                    
                    if (/^ *-+: *$/.test(item.align[i])) {
                        item.align[i] = 'right';
                    } else if (/^ *:-+: *$/.test(item.align[i])) {
                        item.align[i] = 'center';
                    } else if (/^ *:-+ *$/.test(item.align[i])) {
                        item.align[i] = 'left';
                    } else {
                        item.align[i] = null;
                    }
                }

                // PH: determine column width percentage
                if (hasMultipleSizes) {
                    let total = 0;
                    for (i = 0; i < l; i++) {
                        total += item.width[i];
                    }
                    for (i = 0; i < l; i++) {
                        item.width[i] = Math.round((item.width[i] / total)*100, 1);
                    }
                }

                // Get any remaining header rows
                l = item.header.length;
                for (i = 1; i < l; i++) {
                    item.header[i] = splitCells(item.header[i], colCount, item.header[i - 1]);
                }

                // Get main table cells
                l = item.rows.length;
                for (i = 0; i < l; i++) {
                    item.rows[i] = splitCells(item.rows[i], colCount, item.rows[i - 1]);
                }

                // header child tokens
                l = item.header.length;
                for (j = 0; j < l; j++) {
                    row = item.header[j];
                    for (k = 0; k < row.length; k++) {
                        row[k].tokens = [];
                        this.lexer.inline(row[k].text, row[k].tokens);
                    }
                }

                // cell child tokens
                l = item.rows.length;
                for (j = 0; j < l; j++) {
                    row = item.rows[j];
                    for (k = 0; k < row.length; k++) {
                        row[k].tokens = [];
                        this.lexer.inline(row[k].text, row[k].tokens);
                    }
                }
                return item;
            }
        }
    },
    renderer(token) {
        let i, j, row, cell, col, text;
        let output = '<table>';
        output += '<thead>';
        for (i = 0; i < token.header.length; i++) {
            row = token.header[i];
            let col = 0;
            output += '<tr>';
            for (j = 0; j < row.length; j++) {
                cell = row[j];
                text = this.parser.parseInline(cell.tokens);
                output += getTableCell(text, cell, 'th', token.align[col], !!token.width && token.width.length > 0 ? token.width[col] : null);
                col += cell.colspan;
            }
            output += '</tr>';
        }
        output += '</thead>';
        if (token.rows.length) {
            output += '<tbody>';
            for (i = 0; i < token.rows.length; i++) {
                row = token.rows[i];
                col = 0;
                output += '<tr>';
                for (j = 0; j < row.length; j++) {
                    cell = row[j];
                    text = this.parser.parseInline(cell.tokens);
                    output += getTableCell(text, cell, 'td', token.align[col]);
                    col += cell.colspan;
                }
                output += '</tr>';
            }
            output += '</tbody>';
        }
        output += '</table>';
        return output;
    }
};

window.markedExtensions = [markedUnderline, markedSuperscript, markedSubscript, markedHighlight, extendedTables, markedImageAttributedInline];

marked.use({ extensions: window.markedExtensions });

